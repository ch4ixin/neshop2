package com.tmsps.neshop2.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4Weixin.api.UserAPI;
import com.tmsps.ne4Weixin.beans.UserInfo;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.ne4spring.utils.MD5Util;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_member_address;
import com.tmsps.neshop2.model.t_retailStore;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.web.SessionTools;


@Service
public class MemberService extends BaseService {

	// 无参查询所有设置列表
	public List<Map<String, Object>> selectMemderList(JSONObject srh, Map<String, String> sort_param, Page page,String source) {
		String sql = "select t.*,tl.name level_name from t_member t "
				+ "left join t_member_level tl on tl.kid=t.level_id and tl.status=0 "
				+ "where t.status=0 and t.user_nick like ? and t.level_id like ? and t.note like ? and t.source = ? ";
		NeParamList params = NeParamList.makeParams();
		params.addLike(srh.getString("user_nick"));
		params.addLike(srh.getString("level_id"));
		params.addLike(srh.getString("note"));
		params.add(source);
		List<Map<String, Object>> list = bs.findList(sql, params, sort_param, page);
		return list;
	}
	
	// 无参查询所有设置列表
	public List<Map<String, Object>> fxMemderList(JSONObject srh, Map<String, String> sort_param, Page page,String source) {
		String sql = "select t.*,tl.name level_name from t_member t "
				+ "left join t_member_level tl on tl.kid=t.level_id and tl.status=0 "
				+ "where t.status=0 and t.user_nick like ? and t.level_id like ? and t.note like ? and t.source = ? and t.status_sys = ?  ";
		NeParamList params = NeParamList.makeParams();
		params.addLike(srh.getString("user_nick"));
		params.addLike(srh.getString("level_id"));
		params.addLike(srh.getString("note"));
		params.add(source);
		params.add("正常状态");
		List<Map<String, Object>> list = bs.findList(sql, params, sort_param, page);
		return list;
	}
	
	public List<Map<String, Object>> selectMemderFxList() {
		// 查询所有分销数据
		String sql = "select t.*,tm.user_nick mname from t_retailStore t "
				+ "left join t_member tm on tm.kid=t.member_id "
				+ "where t.status=0 order by t.code asc";
		List<Map<String, Object>> list = jt.queryForList(sql);
		return list;
	}

	public t_member findUser(String kid) {
		String sql = "select * from t_member t where t.status=0 and t.kid=?";
		t_member member = bs.findObj(sql, new Object[] { kid }, t_member.class);
		return member;

	}

	public t_member findMember(String uname, String pwd) {
		// 账号密码查询
		String sql = "select * from t_member t where t.uname=? and t.pwd=?  and t.status=0";
		t_member mem = bs.findObj(sql, new Object[] { uname, MD5Util.MD5(pwd) }, t_member.class);
		return mem;
	}
	
	public t_member findMemberByWx(String openid) {
		String sql = "select * from t_member t where t.openid=? and t.source = '电商' ";
		t_member mem = bs.findObj(sql, new Object[] { openid }, t_member.class);
		return mem;
	}
	
	public t_member findMemberByFxWx(String openid) {
		String sql = "select * from t_member t where t.openid=? and t.source = '分销' ";
		t_member mem = bs.findObj(sql, new Object[] { openid }, t_member.class);
		return mem;
	}

	public List<Map<String, Object>> selectMemderMobile() {
		String sql = "select * mobile from t_member t where t.status=0 ";

		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}

	// 依据cate_code查找数据
	public List<Map<String, Object>> selectCartList(String memberId) {
		String sql = "select * from t_member_address t  where t.status=0 and (t.member_id = ?) ";

		List<Map<String, Object>> list = jt.queryForList(sql, memberId);
		return list;
	}

	// 根据kid查询数据
	@Transactional
	public t_member_address findToCartAddress(String address_id) {
		String sql = "select t.* from t_member_address t where status=0 and t.Kid=? ";

		t_member_address shopDB = bs.findObj(sql, new Object[] { address_id }, t_member_address.class);
		return shopDB;

	}// # delToCartAddress

	// 添加address信息
	@Transactional
	public void addToCartAddress(String address) {
		t_member_address add = new t_member_address();
		String memberId = SessionTools.getCurrentLoginMember().getKid();
		JSONObject json = JsonTools.jsonStrToJsonObject(address);
		add.setMember_id(memberId);
		add.setIs_default("1");
		add.setProvince(json.getString("province"));
		add.setCity(json.getString("city"));
		add.setRegion(json.getString("district"));
		add.setStreet(json.getString("address"));
		add.setMember_name(json.getString("consignee"));
		add.setStatus(0);
		add.setMobile1(json.getString("mobile"));
		add.setMobile2(json.getString("mobile2"));
		bs.saveObj(add);

	}// # addToCartAddress

	// 添加address信息
	@Transactional
	public void addToCartAddress1(String address) {
		t_member_address add = new t_member_address();
		String memberId = SessionTools.getCurrentLoginMember().getKid();
		JSONObject json = JsonTools.jsonStrToJsonObject(address);
		add.setMember_id(memberId);
		add.setIs_default("0");
		add.setProvince(json.getString("province"));
		add.setCity(json.getString("city"));
		add.setRegion(json.getString("district"));
		add.setStreet(json.getString("address"));
		add.setMember_name(json.getString("consignee"));
		add.setStatus(0);
		/* add.setPostcode(json.getString("email")); */
		add.setMobile1(json.getString("mobile"));
		add.setMobile2(json.getString("mobile2"));
		bs.saveObj(add);

	}// # addToCartAddress

	// 根据memberId查询支付金额明细数据
	@Transactional
	public List<Map<String, Object>> findToMoney(String memberId) {
		String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time from t_member_money t where t.status=0 and (t.member_id=?) ";

		List<Map<String, Object>> list = jt.queryForList(sql, memberId);
		return list;

	}// # findToPayMoney

	// 根据memberId查询积分明细数据
	@Transactional
	public List<Map<String, Object>> findToIntegral(String memberId) {
		String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time from t_member_integral t where t.status=0 and (t.member_id=?) ";

		List<Map<String, Object>> list = jt.queryForList(sql, memberId);
		return list;

	}// # findToPayMoney

	// TODO 同步微信用户到 t_member
	public t_member syncMember(t_member member, UserInfo userInfo) {
		logger.info(JsonTools.toJson(userInfo));
		member.setOpenid(userInfo.getOpenid());
		member.setUser_nick(userInfo.getNickname());
		member.setUser_sex(userInfo.getSex());
		member.setUser_language(userInfo.getLanguage());
		member.setUser_city(userInfo.getCity());
		member.setUser_province(userInfo.getProvince());
		member.setUser_country(userInfo.getCountry());
		member.setUser_headimgurl(userInfo.getHeadimgurl());
		member.setSubscribe_time(userInfo.getSubscribe_time());
		member.setUnionid(userInfo.getUnionid());
		member.setRemark(userInfo.getRemark());
		member.setGroupid(userInfo.getGroupid());

		return member;
	}

		// TODO 用户微信取消关注
		public void unsubscribe(UserInfo userInfo, String wx_original_id,String shop_id) {
			logger.info(JsonTools.toJson(userInfo));
			String sql = "select * from t_member t where t.openid=? and t.shop_id=? ";
			t_member member = bs.findObj(sql, new Object[] { userInfo.getOpenid(), shop_id }, t_member.class);
			if (member == null) {
				member = new t_member();
				member.setWx_original_id(wx_original_id);
				member.setStatus_sys("正常状态");
				member = syncMember(member, userInfo);
				member.setReg_time(new Timestamp(System.currentTimeMillis()));
				bs.saveObj(member);
			}
			member.setStatus_sys("取消关注");
			bs.updateObj(member);

		}
		
		// 同步微信端的所有用户到 DB
		@Transactional
		public void syncAllUser(String source, UserAPI userAPI, List<String> userList) {
			for (String wxopenid : userList) {
				t_member member = findMemberByWx(wxopenid, source);
				if (member == null) {
					UserInfo userInfo = userAPI.getUserInfo(wxopenid);
					subscribe(userInfo, null, source);
				}
			}

		}
		
		public t_member findMemberByWx(String openid, String source) {
			// 账号密码查询
			String sql = "select * from t_member t where t.openid=? and t.source=? ";
			t_member member = bs.findObj(sql, new Object[] { openid, source }, t_member.class);
			if (member == null) {
				member = new t_member();
				member.setWx_original_id(null);
				member.setOpenid(openid);
				member.setSource(source);
				member.setStatus_sys("正常状态");
				member.setReg_time(new Timestamp(System.currentTimeMillis()));
				System.out.println(JsonTools.toJson(member));
				bs.saveObj(member);
			}
			if ("取消关注".equals(member.getStatus_sys())) {
				member.setStatus_sys("正常状态");
				bs.updateObj(member);
			}
			return member;
		}
		
		// TODO 用户微信关注
		public t_member subscribe(UserInfo userInfo, String wx_original_id, String source) {
			logger.info(JsonTools.toJson(userInfo));
			String sql = "select * from t_member t where t.openid=? and t.source=? ";
			t_member member = bs.findObj(sql, new Object[] { userInfo.getOpenid(),source}, t_member.class);
			if (member == null) {
				member = new t_member();
				member.setWx_original_id(wx_original_id);
				member.setSource(source);
				member.setStatus_sys("正常状态");
				member.setLevel_id("Bouz9rg5Ftj2A19ZWkhHDX");
				syncMember(member, userInfo);
				member.setReg_time(new Timestamp(System.currentTimeMillis()));
				bs.saveObj(member);
			} else {
				member.setStatus_sys("正常状态");
				member.setUser_nick(userInfo.getNickname());
				member.setUser_headimgurl(userInfo.getHeadimgurl());
				bs.updateObj(member);
			}
			return member;
		}
		
		// 查询所有我的用户列表
		public List<Map<String, Object>> my_cousumer_list(String retail_id) {
			String sql = "select t.* from t_member t where t.status=0 and t.shop_id = '电商' and t.retail_id = ?";
			NeParamList param = NeParamList.makeParams();
			param.add(retail_id);
			List<Map<String, Object>> list = bs.findList(sql, param);
			return list;
		}
		
		// 无参查询所有我的用户列表
		public List<Map<String, Object>> my_distributor_list(String code) {
			String sql = "select t.* from t_retailStore t where t.status=0 and t.code like '@code%' ORDER BY t.code";
			sql = sql.replace("@code", code);
			List<Map<String, Object>> list = bs.findList(sql);
			return list;
		}
		
		// 根据member_id查询数据
		public t_retailStore get_retailStore(String member_id) {
			String sql = "select t.* from t_retailStore t where status=0 and t.member_id=? ";
			t_retailStore retailStore = bs.findObj(sql, new Object[] { member_id }, t_retailStore.class);
			return retailStore;
		}// # get_retailStore
		
		public t_retailStore findRetailStoreByCode(String code) {
			// TODO 根据code查询是否存在
			String sql = "select * from t_retailStore t where t.status=0 and t.code=?";
			t_retailStore retailStore = bs.findObj(sql, new String[] { code },t_retailStore.class);
			return retailStore;
		}

		public t_retailStore findRetailStoreByCode(String kid, String code) {
			String sql = "select * from t_retailStore t where t.status=0 and t.code=? and t.kid!=?";
			t_retailStore retailStore = bs.findObj(sql, new String[] { code, kid },t_retailStore.class);
			return retailStore;
		}
}
