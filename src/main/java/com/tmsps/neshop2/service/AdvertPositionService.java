package com.tmsps.neshop2.service;

/**
 * @author 张志亮
 */

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;

@Service
public class AdvertPositionService extends BaseService {


	    // 查询广告管理列表
		public List<Map<String, Object>> selectAdvertInfoList(JSONObject srh, Map<String, String> sort_param, Page page) {
			String sql = "select t.*,ts.note note from t_net_advert t "
					+ "inner join t_net_advert_position ts "
					+ "on ts.code = t.position_code "
					+ "where t.status=0 and (t.title like ?) and (ts.note like ?) and (t.position_code like ?) ";
			NeParamList param = NeParamList.makeParams();
			param.addLike(srh.getString("title"));
			param.addLike(srh.getString("note"));
			param.addLike(srh.getString("position_code"));
			List<Map<String, Object>> list = bs.findList(sql, param, sort_param, page);
			return list;
		}
		
		// 无参查询所有广告位管理列表
		public List<Map<String, Object>> selectAdvertList() {
			String sql = "select * from t_net_advert_position t where t.status=0 ";
			List<Map<String, Object>> list = bs.findList(sql);
			return list;
		}
}
