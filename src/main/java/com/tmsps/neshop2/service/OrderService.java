package com.tmsps.neshop2.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tmsps.neshop2.model.*;
import com.tmsps.neshop2.web.WebTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.ne4spring.orm.ORMUtil;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.ne4spring.utils.ChkUtil;
import com.tmsps.ne4spring.utils.PKUtil;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.util.ChkTools;
import com.tmsps.neshop2.util.OrderTools;
import com.tmsps.neshop2.util.WxTagTools;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.web.SessionTools;

/**
 * 订单列表
 * 
 * @author 柴鑫
 *
 */
@Service
public class OrderService extends BaseService {

	@Autowired
	WxService wxService;
	@Autowired
	OrderService orderService;



	// 查询所有实物交易订单
	public List<Map<String, Object>> selectOrderList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select t1.*,t2.user_nick from t_order t1 " 
				+ " left outer join t_member t2 on t2.kid = t1.member_id "
				+ " where t1.status=0 and t1.order_no like ?";
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("order_no"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;
	}

	
	/*
	 * 查询所有退货订单
	 * 
	 */
	public List<Map<String, Object>> return_selectOrderList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select t1.*,t2.user_nick,t3.price,t3.cnt,t3.price_total,t3.status_order "
				+ "from t_order_return t1 left outer "
				+ "join t_member t2 on t2.kid = t1.member_id "
				+ "left outer join t_order_detail t3 on t3.kid=t1.order_detail_id where return_type='退货' and t1.status=0  and t1.order_no like ?" ;
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("order_no"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;	
	}
	//查询退款订单
	public List<Map<String, Object>> return_selectOrderMoneyList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select t1.*,t2.user_nick,t3.price,t3.cnt,t3.price_total,t3.status_order "
				+ "from t_order_return t1 left outer "
				+ "join t_member t2 on t2.kid = t1.member_id "
				+ "left outer join t_order_detail t3 on t3.kid=t1.order_detail_id where return_type='退款' and t1.status=0  and t1.order_no like ?" ;
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("order_no"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;	
	}
	public List<Map<String, Object>> return_selectOrderList_success(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select t1.*,t2.user_nick,t3.price,t3.cnt,t3.price_total,t3.status_order "
				+ "from t_order_return t1 left outer "
				+ "join t_member t2 on t2.kid = t1.member_id "
				+ "left outer join t_order_detail t3 on t3.kid=t1.order_detail_id where t1.status=-100  and t1.order_no like ?" ;
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("order_no"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;	
	}
	// 根据订单查询
	public t_order orderList(String order_no) {
		String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time from t_order t where t.status=0 and t.order_no=?";
		t_order order = bs.findObj(sql, new Object[] { order_no }, t_order.class);
		return order;
	}

	// 根据订单查询
	public List<Map<String, Object>> orderTimeList(String order_no) {
		String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time from t_order t where t.status=0 and t.order_no=?";
		List<Map<String, Object>> list = bs.findList(sql, new Object[] { order_no });
		return list;
	}

	// 根据订单查订单项
	public List<Map<String, Object>> ListDetail(String order_id) {
		String sql = "select t1.*,t2.name pname,t2.main_file_id from t_order_detail t1 inner join t_product t2 "
				+ "on t2.kid = t1.product_id where t1.status=0 and t1.order_id= ?";
		List<Map<String, Object>> list = jt.queryForList(sql, order_id);
		return list;
	}

	// 根据订单号查询订单物流
	public t_order_logistics orderIdList(String order_id) {
		String sql = "select * from t_order_logistics t where t.status=0 and t.order_id=?";
		t_order_logistics logistics = bs.findObj(sql, new Object[] { order_id }, t_order_logistics.class);
		return logistics;
	}

	// 根据order_detail_id查询 t_order_return
	public t_order_return get_orderReturn(String order_detail_id) {
		String sql = "select * from t_order_return t where t.status=0 and t.order_detail_id=?";
		t_order_return order_return = bs.findObj(sql, new Object[] { order_detail_id }, t_order_return.class);
		return order_return;
	}

	// 查询我的订单号
	public List<Map<String, Object>> selectOrderList() {
		t_member member = SessionTools.getCurrentLoginMember();
		// logger.info(member.getKid());
		if (ChkUtil.isNotNull(member)) {

			String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') as time from t_order t "
					+ "where t.status=0 and t.member_id=? ";
			List<Map<String, Object>> list = jt.queryForList(sql, member.getKid());
			return list;
		}
		return null;
	}

	public t_pay_no findPayNo(String out_trade_no) {
		String sql = "select * from t_pay_no t where t.trade_no=?";
		t_pay_no payNo = bs.findObj(sql, new String[] { out_trade_no }, t_pay_no.class);

		return payNo;
	}
	public t_order findtOrder(String order_no) {
		String sql = "select * from t_order t where t.order_no=?";
		t_order order = bs.findObj(sql, new String[] { order_no }, t_order.class);

		return order;
	}
	// TODO 处理支付并退单
	@Transactional
	public boolean payToBack(t_order order, t_pay_no payNo, String out_trade_no, String payWxNo, String pay_method,
			String openid, BigDecimal totalFee, String pay_message) {
		if ("是".equals(order.getIs_pay())) {
			return true;
		}

		order.setStatus_order("已退单");
		order.setIs_pay("是");
		order.setPay_type("在线支付");
		order.setPay_way("微信支付");
		order.setOut_trade_no(out_trade_no);
		order.setPay_wx_no(payWxNo);
		order.setPay_user_openid(openid);
		order.setPay_method(pay_method);
		order.setPay_message(pay_message);
		order.setEnd_price(totalFee);
		bs.updateObj(order);
		logger.info("---------------++++++++++++++++++++++++++++++===================" + openid);
		payNo.setStatus_pay("已生效");
		bs.updateObj(payNo);

		// t_member cox = bs.findById(order.getRecv_member_id(),
		// t_member.class);
		// 更改订单项
		// List<t_order_user> odList = this.getOdList(order.getKid());
		// for (t_order_user od : odList) {
		// bs.updateObj(od);
		// }
		/*t_pay_type payType = saveOrderPayType(order.getKid());
		payType.setPay_type("微信支付_退单");
		bs.updateObj(payType);*/
		return true;
	}// #pay

	// TODO 保存订单支付方式
	/*@Transactional
	public t_pay_type saveOrderPayType(String oid) {
		t_order order = bs.findById(oid, t_order.class);
		t_pay_type payType = new t_pay_type();
		payType.setOrder_id(oid);
		payType.setMember_id(order.getMember_id());
		payType.setMoney(order.getTotal_price());
		payType.setPay_type("余额支付");
		payType.setTrade_no(order.getOut_trade_no());
		bs.saveObj(payType);
		return payType;
	}*/

	// 查询订单号下的产品
	public List<Map<String, Object>> selectByOrderId() {
		t_member member = SessionTools.getCurrentLoginMember();

		String sql = "select tp.*,td.* from t_order_detail td join t_product tp on (tp.kid=td.product_id)"
				+ " where tp.status=0 and td.status=0 and td.member_id=? ";
		List<Map<String, Object>> list = jt.queryForList(sql, member.getKid());
		return list;

	}

	// 我的订单
	public List<Map<String, Object>> myOrderList(String status_order) {
		t_member member = SessionTools.getCurrentLoginMember();
		if (ChkUtil.isNotNull(member)) {
			String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time from t_order t where t.status=0 and t.member_id=? and t.status_order = ? order by created DESC";
			List<Map<String, Object>> orderList = jt.queryForList(sql, member.getKid(), status_order);
			return orderList;
		}
		return null;	
	}

	// 我的订单
	public List<Map<String, Object>> myOrderList(String status_order,String is_discuzz) {
		t_member member = SessionTools.getCurrentLoginMember();
		if (ChkUtil.isNotNull(member)) {
			String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time from t_order t where t.status=0 and t.member_id=? and t.status_order = ? and t.is_discuzz = ? order by created DESC";
			List<Map<String, Object>> orderList = jt.queryForList(sql, member.getKid(), status_order,is_discuzz);
			return orderList;
		}
		return null;
	}

	// 我的订单
	public List<Map<String, Object>> OrderList() {
		t_member member = SessionTools.getCurrentLoginMember();
		if (ChkUtil.isNotNull(member)) {
			String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time from t_order t where t.status=0 and t.member_id=? and t.status_order in ('已收货','已发货') order by created DESC";
			List<Map<String, Object>> orderList = jt.queryForList(sql, member.getKid());
			return orderList;
		}
		return null;	
	}
	
	// 我的订单
	public List<Map<String, Object>> myOrderList() {
		t_member member = SessionTools.getCurrentLoginMember();
		if (ChkUtil.isNotNull(member)) {
			String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time from t_order t where t.status=0 and t.member_id=? order by created DESC";
			List<Map<String, Object>> orderList = jt.queryForList(sql, member.getKid());
			return orderList;
		}
		return null;	
	}

	// 店铺订单
	public List<Map<String, Object>> shopOrderList() {
		t_member memberDb = bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
		String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time,tp.name product_name,tp.main_file_id,tp.unit,tr.order_no from t_order_detail t" +
				" left outer join t_order tr on tr.kid = t.order_id"+
				" left outer join t_product tp on tp.kid = t.product_id"+
				" where t.status=0 and t.shop_id=? order by created DESC";
		NeParamList params = NeParamList.makeParams();;
		params.add(memberDb.getShop_id());
		List<Map<String, Object>> list = bs.findList(sql, params);
		return list;
	}

	// 店铺订单
	public List<Map<String, Object>> shopOrderList(String status_order) {
		t_member memberDb = bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
		String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time,tp.name product_name,tp.main_file_id,tp.unit,tr.order_no from t_order_detail t" +
				" left outer join t_order tr on tr.kid = t.order_id"+
				" left outer join t_product tp on tp.kid = t.product_id"+
				" where t.status=0 and t.shop_id=? and t.status_order = ? order by created DESC";
		NeParamList params = NeParamList.makeParams();;
		params.add(memberDb.getShop_id());
		params.add(status_order);
		List<Map<String, Object>> list = bs.findList(sql, params);
		return list;
	}
	// 店铺订单
	public List<Map<String, Object>> shopFinishOrderList() {
		t_member memberDb = bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
		String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') time,tp.name product_name,tp.main_file_id,tp.unit,tr.order_no from t_order_detail t" +
				" left outer join t_order tr on tr.kid = t.order_id"+
				" left outer join t_product tp on tp.kid = t.product_id"+
				" where t.status=0 and t.shop_id=? and t.status_order in ('已发货','已完成') order by created DESC";
		NeParamList params = NeParamList.makeParams();;
		params.add(memberDb.getShop_id());
		List<Map<String, Object>> list = bs.findList(sql, params);
		return list;
	}

	// 查询会员的足迹
	public List<Map<String, Object>> footPrintsList() {
		t_member member = SessionTools.getCurrentLoginMember();

		String sql = "select * from t_foot_prints tf " + "join t_member tm on (tm.kid=tf.member_id) "
				+ "join t_product tp on (tf.show_id=tp.show_id) " + "where tf.status=0 and tm.kid=?";
		List<Map<String, Object>> list = jt.queryForList(sql, member.getKid());

		return list;

	}

	public List<Map<String, Object>> footList() {
		t_member member = SessionTools.getCurrentLoginMember();
		String sql = "select * from t_foot_prints t where t.status=0 and t.member_id= ? ";
		List<Map<String, Object>> list = jt.queryForList(sql, member.getKid());
		return list;
	}

	// 待付款查询--订单号
	public List<Map<String, Object>> noConfirmList() {
		t_member member = SessionTools.getCurrentLoginMember();

		if (ChkUtil.isNotNull(member)) {

			String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') as time from t_order t "
					+ "where t.status=0 and t.member_id=? and t.is_pay='否'";

			List<Map<String, Object>> list = jt.queryForList(sql, member.getKid());
			return list;
		}
		return null;
	}

	// 待付款查询--订单号下的产品
	public List<Map<String, Object>> selectByOrder() {
		t_member member = SessionTools.getCurrentLoginMember();

		String sql = "select tp.*,td.* from t_order_detail td join t_product tp on (tp.kid=td.product_id)"
				+ " where tp.status=0 and td.status=0 and td.member_id=? and td.status_order='下单中'";
		List<Map<String, Object>> list = jt.queryForList(sql, member.getKid());
		return list;

	}

	// 代发货

	public t_order_return getOrderReturnList() {
		String sql = "select * from t_order_return t  where t.status=0";
		t_order_return oreder_return = bs.findObj(sql, t_order_return.class);
		return oreder_return;
	}

	public t_order_return getOrderReturnList(String kid) {
		String sql = "select * from t_order_return t  where t.status=0 and t.kid= ? ";
		t_order_return oreder_return = bs.findObj(sql, new String[] { kid }, t_order_return.class);
		return oreder_return;
	}

	public t_order getOrderList(String member_id) {
		String sql = "select t.*,from_unixtime((t.created/1000),'%Y-%m-%d %H:%i:%S') as oldTime from t_order t  where t.status=0 and t.member_id= ? ";
		t_order order = bs.findObj(sql, new String[] { member_id }, t_order.class);
		return order;
	}

	public t_order_detail getOrderDetailList(String product_id) {
		String sql = "select * from t_order_detail t  where t.status=0 and t.product_id= ? ";
		t_order_detail oreder_detail = bs.findObj(sql, new String[] { product_id }, t_order_detail.class);
		return oreder_detail;
	}

	public t_logistics getOrderLogistics(String code) {
		String sql = "select * from t_logistics t  where t.status=0 and t.code= ? ";
		t_logistics logistics = bs.findObj(sql, new String[] { code }, t_logistics.class);
		return logistics;
	}

	public List<Map<String, Object>> getMemberAddress(String member_id) {
		String sql = "select * from t_member_address t  where t.status=0 and t.member_id= ? order by is_default desc";
		List<Map<String, Object>> list = bs.findList(sql, new String[] { member_id });
		return list;
	}
	
	public t_member_address getDefaultAddress(String member_id) {
		String sql = "select * from t_member_address t  where t.status=0 and t.is_default='是' and t.member_id= ?";
		t_member_address logistics = bs.findObj(sql, new String[] { member_id }, t_member_address.class);
		if(ChkUtil.isNotNull(logistics)){
			return logistics;
		}else{
			return null;
		}
	}

	// 查看所有的退货订单
	public List<Map<String, Object>> SelectList(JSONObject srh, Map<String, String> sort_params, Page page) {

		String sql = "select tr.*,tp.kid tp_kid,tm.mobile,tm.name uname,tp.name,tp.price, "
				+ "from_unixtime((tr.created/1000),'%Y-%m-%d %H:%i:%S') time " + "from t_order_return tr  "
				+ "join t_product tp on (tp.kid=tr.product_id) " + "join t_member tm on tr.member_id=tm.kid "
				+ "where tr.status=0 and (tr.order_no like ?)";

		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("order_no"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;
	}

	// 删除订单
	public t_order_detail selectlist(String productId) {

		String sql = "select * from t_order_detail t where t.status=0 and t.product_id=?";
		t_order_detail od = bs.findObj(sql, new String[] { productId }, t_order_detail.class);
		return od;

	}
	// 订单退货退款表 提交

	public List<Map<String, Object>> selectReturnList(String order_no, String product_id, String shop_id) {
		t_member member = SessionTools.getCurrentLoginMember();
		if (ChkUtil.isNotNull(member)) {

			String sql = "select td.*,t.*,tp.* from t_order_detail td  join t_order t on (td.member_id=t.member_id)   join t_product tp on (tp.kid=td.product_id)"
					+ " where  t.status=0 and (t.order_no=?) and (td.product_id=?) and (td.shop_id=?)";

			List<Map<String, Object>> list = jt.queryForList(sql, order_no, product_id, shop_id);
			return list;
		}
		return null;
	}

	// 订单/退货/维修订单 列表

	public List<Map<String, Object>> SelectBackList() {
		String sql = "select tr.*,tp.*,from_unixtime((tr.created/1000),'%Y-%m-%d %H:%i:%S') time from t_order_return tr join t_product tp on (tp.kid=tr.product_id)  where tr.status=0 ";

		List<Map<String, Object>> list = jt.queryForList(sql);
		return list;
	}

	public List<Map<String, Object>> SelectBackById(String product_id) {
		String sql = "select tr.*,tp.* from t_order_return tr join t_product tp on (tp.kid=tr.product_id)  where tr.status=0 and tr.product_id=?";

		List<Map<String, Object>> list = jt.queryForList(sql, product_id);
		return list;
	}

	// 订单/查看详情的数据
	public List<Map<String, Object>> CheckList(String product_id, String order_no) {

		String sql = "select t.*,t.name uname,td.*,tp.* from t_order_detail td "
				+ " join t_order t on (td.order_id=t.kid) " + " join t_product tp on (td.product_id=tp.kid) "
				+ "where td.status=0 and t.status=0 and td.product_id=? and t.order_no=? ";

		List<Map<String, Object>> list = jt.queryForList(sql, product_id, order_no);

		return list;
	}

	// TODO 创建支付单号
	public t_pay_no mkPayNo(String order_id) {
		t_pay_no payNO = new t_pay_no();
		payNO.setOrder_id(order_id);
		payNO.setPay_type("微支付");
		payNO.setTrade_no(OrderTools.getOrderNO());
		bs.saveObj(payNO);
		return payNO;
	}

	public t_member_pay makeMemberPay(String object, String object2, t_pay_no payNo, t_member member, String pay_type) {
		// TODO 扫码支付
		BigDecimal money = ChkTools.getBigDecimal(object);
		BigDecimal member_money = ChkTools.getBigDecimal(object2);

		t_member_pay pay = new t_member_pay();
		pay.setShop_id(member.getShop_id());
		pay.setMember_id(member.getKid());
		pay.setMoney(money.subtract(member_money));
		pay.setMember_money(member_money);
		pay.setTotal_money(money);
		// pay.setNote("扫码支付");
		pay.setPay_type(pay_type);
		pay.setTrade_no(payNo.getTrade_no());
		bs.saveObj(pay);

		payNo.setOrder_id(pay.getKid());
		bs.updateObj(payNo);

		return pay;
	}

	@Transactional
	public boolean payForSell(String member_id, t_pay_no payNo, String out_trade_no, String payWxNo, String pay_method,
			String openid, BigDecimal totalFee, String xmlMsg) {
		// TODO 扫码支付处理
		if ("已生效".equals(payNo.getStatus_pay())) {
			return false;
		}
		payNo.setStatus_pay("已生效");
		bs.updateObj(payNo);

		t_member member = bs.findById(member_id, t_member.class);

		// 添加现金流水
		t_member_money mm = new t_member_money();
		mm.setShop_id(member.getShop_id());
		mm.setMember_id(member_id);
		mm.setMoney(totalFee);
		mm.setNote("扫码支付");
		bs.saveObj(mm);

		t_member_pay pay = bs.findById(payNo.getOrder_id(), t_member_pay.class);
		pay.setNote("扫码支付");
		bs.updateObj(pay);

		member.setMoney(member.getMoney().subtract(pay.getMember_money()));
		bs.updateObj(member);
		// t_member_pay pay = new t_member_pay();
		// pay.setShop_id(member.getShop_id());
		// pay.setMember_id(member_id);
		// pay.setMoney(totalFee);
		// pay.setNote("扫码支付");
		// pay.setPay_type("微信支付");
		// pay.setTrade_no(payNo.getTrade_no());
		// bs.saveObj(pay);

		// 加入积分
		if (totalFee.intValue() > 0) {
			t_member_integral it = new t_member_integral();
			it.setShop_id(member.getShop_id());
			// it.setOrder_id(null);
			it.setMember_id(member.getKid());
			it.setNote("扫码消费赠送");
			it.setIntegral(totalFee.intValue());
			bs.saveObj(it);
		}

		// 更新会员级别
		findDiscountOfMember(member_id);
		return true;
	}

	@Transactional
	public BigDecimal findDiscountOfMember(String member_id) {
		// TODO 查询可以享受的折扣
		t_member member = bs.findById(member_id, t_member.class);
		// 非会员不享受任何折扣
		if (ChkTools.isNull(member.getMobile())) {
			return new BigDecimal(1);
		}

		String sql = "select sum(t.integral) integral from t_member_integral t where t.member_id=? and t.status=0";
		Map<String, Object> map = bs.findObj(sql, new Object[] { member_id });
		Object integral = map.get("integral");
		if (ChkTools.isNull(integral)) {
			return new BigDecimal(1);
		}
		String sql1 = "select t.* from t_member_level t where t.status=0 and t.integral<=? order by t.integral desc ";
		List<Map<String, Object>> list = bs.findList(sql1, new Object[] { integral });
		if (!list.isEmpty()) {
			// 更新会员级别
			// if (!list.get(0).get("kid").equals(member.getLevel_id())) {
			// 更改会员标签
			WxConfig wxConfig = wxService.getShopWxByType(member.getShop_id());
			WxTagTools.getMemberTag(wxConfig, list.get(0).get("name") + "", member.getOpenid());
			// member.setLevel_id(list.get(0).get("kid") + "");
			// }
			member.setIntegral(Long.parseLong(integral + ""));
			bs.updateObj(member);

			return new BigDecimal(list.get(0).get("discount") + "");
		}
		return new BigDecimal(1);
	}

	public t_order refreshOrder(t_order order) {
		List<Map<String, Object>> odingList = this.getOdingList(order.getKid());
		return refreshOrder(order, odingList);
	}

	// TODO 查询order 中的 od list
	public List<Map<String, Object>> getOdingList(String order_id) {
		String sql = "select t.*,tp.name,tp.unit,tp.brief,tp.main_file_id,tp.cate_id_shop from t_order_detail t "
				+ " left outer join t_product tp on t.product_id=tp.kid "
				+ " left outer join t_shop_product_cate tc on tp.cate_id_shop=tc.kid "
				+ " where t.status=0 and t.status_order='下单中' and t.order_id=? order by tc.code asc ";

		List<Map<String, Object>> odList = jt.queryForList(sql, order_id);
		return odList;
	}

	@Transactional
	public t_order refreshOrder(t_order order, List<Map<String, Object>> odingList) {
		// 查询可以享受的折扣
		BigDecimal discount = findDiscountOfMember(order.getMember_id());

		BigDecimal total = ChkTools.getBigDecimal("0");
		for (Map<String, Object> od : odingList) {
			BigDecimal dtotal = ChkTools.getBigDecimal(od.get("price")).multiply(ChkTools.getBigDecimal(od.get("cnt")));
			total = total.add(dtotal);
			t_order_detail detail = bs.findById(od.get("kid"), t_order_detail.class);
			detail.setPrice_total(dtotal);
			bs.updateObj(detail);
		}
		total = OrderTools.cutDecimal(total);

		// order.setDiscount(discount);
		// order.setTotal_price_all(total);
		order.setTotal_price(total.multiply(discount).setScale(2, BigDecimal.ROUND_HALF_UP));

		bs.updateObj(order);

		return order;
	}

	// @Transactional
	// public t_order judgePayStyle(t_order order) {
	// // TODO 判断支付金额方式
	// t_member member = bs.findById(order.getMember_id(), t_member.class);
	// BigDecimal price = order.getTotal_price();// 需支付的金额
	// String coupon_id = SessionTools.get(SessionTools.WX_COUPON) + "";
	// if (ChkTools.isNotNull(coupon_id)) {
	// // 若有优惠卷，则 不在享受会员折扣
	// price = order.getTotal_price_all();
	//
	// order.setCoupon_id(coupon_id);
	// t_member_poupon poupon = bs.findById(coupon_id, t_member_poupon.class);
	// t_coupon coupon = bs.findById(poupon.getPoupon_id(), t_coupon.class);
	// BigDecimal cmoney =
	// coupon.getMoney_or_discount().multiply(ChkTools.getBigDecimal(0.1));
	// if ("全场折扣券".equals(coupon.getType())) {
	// price = price.multiply(cmoney).setScale(2, BigDecimal.ROUND_HALF_UP);
	// } else if ("兑换券".equals(coupon.getType())) {
	// String sql = "select sum(t.cnt*t.price) total_price from t_order_detail t
	// "
	// + " inner join t_product tp on tp.status=0 and t.product_id=tp.kid "
	// + " where t.status=0 and t.status_order='下单中' and t.order_id=? and
	// tp.cate_id_shop=?";
	// Map<String, Object> map = jt.queryForMap(sql, new Object[] {
	// order.getKid(), coupon.getCate_id() });
	// BigDecimal money = ChkTools.getBigDecimal(map.get("total_price"));
	// BigDecimal cm = money.multiply(cmoney).setScale(2,
	// BigDecimal.ROUND_HALF_UP);
	// price = price.subtract(money).add(cm);
	// } else {
	// String sql = "select sum(t.cnt*t.price) total_price from t_order_detail t
	// "
	// + " where t.status=0 and t.status_order='下单中' and t.order_id=? and
	// t.product_id=? ";
	// Map<String, Object> map = jt.queryForMap(sql, new Object[] {
	// order.getKid(), coupon.getProduct_id() });
	// BigDecimal money = ChkTools.getBigDecimal(map.get("total_price"));
	// if (money.compareTo(cmoney) > 0) {
	// price = price.subtract(cmoney);
	// } else {
	// price = price.subtract(money);
	// }
	// }
	// order.setTotal_price(price);
	// } else {
	// order.setCoupon_id(null);
	// }
	//
	// BigDecimal money = member.getMoney();// 拥有的余额
	// BigDecimal zero = new BigDecimal(0);
	// if (money.compareTo(zero) <= 0) {
	// order.setMember_money_pay(zero);
	// order.setMoney_pay(price);
	// bs.updateObj(order);
	// return order;
	// }
	// // 与余额比较
	// if (money.compareTo(price) >= 0) {
	// order.setMember_money_pay(price);
	// order.setMoney_pay(zero);
	// bs.updateObj(order);
	// } else {
	// order.setMember_money_pay(money);
	// order.setMoney_pay(price.subtract(money));
	// bs.updateObj(order);
	//
	// }
	// return order;
	// }

	// public BigDecimal getPayStyle(String coupon_id, String order_id) {
	// // TODO 获取优惠卷方式下的支付情况
	// t_order order = bs.findById(order_id, t_order.class);
	// BigDecimal price = order.getTotal_price_all();
	//
	// t_member_poupon poupon = bs.findById(coupon_id, t_member_poupon.class);
	// t_coupon coupon = bs.findById(poupon.getPoupon_id(), t_coupon.class);
	// BigDecimal cmoney =
	// coupon.getMoney_or_discount().multiply(ChkTools.getBigDecimal(0.1));
	// if ("全场折扣券".equals(coupon.getType())) {
	// price = price.multiply(cmoney).setScale(2, BigDecimal.ROUND_HALF_UP);
	// } else if ("兑换券".equals(coupon.getType())) {
	// String sql = "select sum(t.cnt*t.price) total_price from t_order_detail t
	// "
	// + " inner join t_product tp on tp.status=0 and t.product_id=tp.kid "
	// + " where t.status=0 and t.status_order='下单中' and t.order_id=? and
	// tp.cate_id_shop=?";
	// Map<String, Object> map = jt.queryForMap(sql, new Object[] {
	// order.getKid(), coupon.getCate_id() });
	// BigDecimal money = ChkTools.getBigDecimal(map.get("total_price"));
	// BigDecimal cm = money.multiply(cmoney).setScale(2,
	// BigDecimal.ROUND_HALF_UP);
	// price = price.subtract(money).add(cm);
	// } else {
	// String sql = "select sum(t.cnt*t.price) total_price from t_order_detail t
	// "
	// + " where t.status=0 and t.status_order='下单中' and t.order_id=? and
	// t.product_id=? ";
	// Map<String, Object> map = jt.queryForMap(sql, new Object[] {
	// order.getKid(), coupon.getProduct_id() });
	// BigDecimal money = ChkTools.getBigDecimal(map.get("total_price"));
	// if (money.compareTo(cmoney) > 0) {
	// price = price.subtract(cmoney);
	// } else {
	// price = price.subtract(money);
	// }
	// }
	// return price;
	// }

	// TODO 处理支付
	@Transactional
	public boolean pay(t_order order, t_pay_no payNo, String out_trade_no, String payWxNo, String pay_method,
			String openid, BigDecimal totalFee, String pay_message) {
		System.err.println(JsonTools.toJson(order));
		if ("是".equals(order.getIs_pay())) {
			return false;
		}

		order.setStatus_order("已下单");
		order.setIs_pay("是");
		order.setPay_type("在线支付");
		order.setPay_way("微信支付");
		order.setOut_trade_no(out_trade_no);
		order.setPay_wx_no(payWxNo);
		order.setPay_user_openid(openid);
		order.setPay_method(pay_method);
		order.setPay_message(pay_message);
		order.setEnd_price(totalFee);
		order.setPay_time(System.currentTimeMillis());
		bs.updateObj(order);

		payNo.setStatus_pay("已生效");
		bs.updateObj(payNo);

		// if (ChkTools.isNotNull(order.getCoupon_id())) {
		// t_member_poupon poupon = bs.findById(order.getCoupon_id(),
		// t_member_poupon.class);
		// poupon.setUse_type("已使用");
		// bs.updateObj(poupon);
		// }

		t_member member = bs.findById(order.getMember_id(), t_member.class);
		member.setMoney(member.getMoney().subtract(order.getMember_money_pay()));
		bs.updateObj(member);

		// 更改订单项
		List<t_order_detail> odList = this.getOdList(order.getKid());
		for (t_order_detail od : odList) {
			od.setStatus_order("已付款");
			bs.updateObj(od);
		}

		// 加入积分
		if (order.getTotal_price().intValue() > 0) {
			t_member_integral it = new t_member_integral();
//			it.setShop_id(order.getShop_id());
			// it.setOrder_id(order.getKid());
			it.setMember_id(order.getMember_id());
			it.setNote("消费赠送");
			it.setIntegral(order.getTotal_price().intValue());
			bs.saveObj(it);
		}

		// 添加现金流水
		t_member_money mm = new t_member_money();
//		mm.setShop_id(order.getShop_id());
		mm.setMember_id(order.getMember_id());
		mm.setMoney(order.getTotal_price());
		mm.setNote("消费");
		bs.saveObj(mm);

		return true;
	}// #pay

	public List<t_order_detail> getOdList(String order_id) {
		String sql = "select * from t_order_detail t where t.status=0 and t.order_id=?  ";

		List<Map<String, Object>> list = jt.queryForList(sql, order_id);
		List<t_order_detail> odList = new ArrayList<>();
		for (Map<String, Object> map : list) {
			t_order_detail od = ORMUtil.fillMapToBean(t_order_detail.class, map);
			odList.add(od);
		}
		return odList;
	}

	
	@Transactional
	public t_order turnCartItemToOrder() {
		t_member member = SessionTools.getCurrentLoginMember();
		t_member memberDb = bs.findById(member.getKid(), t_member.class);
		t_member_address address = orderService.getDefaultAddress(memberDb.getKid());

		String sql = "select * from t_order_detail t where t.status=0 and t.status_order='购物车' and t.member_id=?";
		List<Map<String, Object>> cartItemList = bs.findList(sql, new String[] { member.getKid() });
		if(cartItemList.size() == 0){
			return null;
		}
		//System.out.println(cartItemList);

		t_order order = new t_order();
		order.setMember_id(member.getKid());
		order.setOrder_no(PKUtil.getOrderNO());
		order.setStatus_order("下单中");
		if(ChkUtil.isNotNull(address)){
			order.setAddress(address.getProvince()+address.getCity()+address.getRegion()+address.getStreet());
			order.setName(address.getMember_name());
			order.setPhone(address.getMobile1());
		}
		bs.saveObj(order);

		t_product product = null;
		BigDecimal product_price = new BigDecimal("0");//商品费用
		BigDecimal carriage = new BigDecimal("0");//首件运费
		BigDecimal more_carriage = new BigDecimal("0");//续件运费
		for (Map<String, Object> cartItemMap : cartItemList) {
				t_order_detail order_detail = ORMUtil.fillMapToBean(t_order_detail.class, cartItemMap);
				product = bs.findById(order_detail.getProduct_id(), t_product.class);
				order_detail.setPrice_total(order_detail.getCnt().multiply(order_detail.getPrice()));
				order_detail.setStatus_order("下单中");
				order_detail.setOrder_id(order.getKid());
				bs.updateObj(order_detail);

				 BigDecimal cnt = (BigDecimal) cartItemMap.get("cnt");//多少件
				 if(cnt != new BigDecimal(1)){
					 cnt = cnt.subtract(new BigDecimal(1));
					 more_carriage = product.getMore_carriage().multiply(cnt);//续件运费相加
				 }
				carriage = carriage.add(product.getCarriage());//首件运费费用相加
				product_price = product_price.add(order_detail.getPrice_total());// 所有商品相加 = 商品总价
		} // #for`

		BigDecimal total_price = product_price.add(carriage.add(more_carriage));
		order.setMain_file_id(product.getMain_file_id());
		order.setProduct_sum(cartItemList.size());
		order.setLogistics_price(carriage.add(more_carriage));
		order.setProduct_price(product_price);
		order.setTotal_price(total_price);
		order.setEnd_price(total_price);
		bs.updateObj(order);

		return order;
	}

	@Transactional
	public t_order buyNowToOrder(String product_id,int number) {
		t_member member = SessionTools.getCurrentLoginMember();
		t_product product = bs.findById(product_id, t_product.class);
		t_member_address address = orderService.getDefaultAddress(member.getKid());
		String cookieSN = WebTools.getClientCookieSN();

		BigDecimal product_price = product.getPrice().multiply(BigDecimal.valueOf(number));//商品单价 x 商品数量
		BigDecimal carriage = product.getCarriage();
		BigDecimal more_carriage = new BigDecimal("0");//续件运费
		if(number != 1){
			more_carriage = product.getMore_carriage().multiply(BigDecimal.valueOf(number-1));//第一件运费除外的商品运费 x 多件商品 = 多件的运费
		}

		t_order order = new t_order();
		t_order_detail order_detail = new t_order_detail();
		order_detail.setCnt(new BigDecimal(number));
		order_detail.setCookie_sn(cookieSN);
		if (member != null) {
			order_detail.setMember_id(member.getKid());
		}
		order_detail.setProduct_id(product.getKid());
		order_detail.setType(product.getType());
		order_detail.setPrice(product.getPrice());
		order_detail.setPrice_total(product_price);
		order_detail.setIntegral(product.getIntegral());
		order_detail.setIntegral_total(product.getIntegral() * new BigDecimal(number).intValue());
		order_detail.setIs_print("否");
		order_detail.setStatus_order("下单中");
		bs.saveObj(order_detail);

		order.setMember_id(member.getKid());
		order.setOrder_no(PKUtil.getOrderNO());
		order.setStatus_order("下单中");
		if(ChkUtil.isNotNull(address)){
			order.setAddress(address.getProvince()+address.getCity()+address.getRegion()+address.getStreet());
			order.setName(address.getMember_name());
			order.setPhone(address.getMobile1());
		}
		order.setMain_file_id(product.getMain_file_id());
		order.setProduct_sum(number);//商品数量
		order.setLogistics_price(carriage.add(more_carriage));
		order.setProduct_price(product_price);
		order.setTotal_price(product_price.add(carriage.add(more_carriage)));
		order.setEnd_price(product_price.add(carriage.add(more_carriage)));
		bs.saveObj(order);
		order_detail.setOrder_id(order.getKid());
		bs.updateObj(order_detail);

		return order;
	}
	
	// 查询分销商业绩
	public List<Map<String, Object>> selectMemberOrderList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "SELECT "
					+"	t1.created,t1.one_deduct_money,t1.two_deduct_money,t1.three_deduct_money,t4.name,t2.end_price,t3.user_nick,t1.kid"
					+" FROM"
					+"	t_retail_order t1"
					+" LEFT OUTER JOIN t_order t2 ON t2.kid = t1.order_id"
					+" LEFT OUTER JOIN t_member t3 ON t3.kid = t1.member_id"
					+" LEFT OUTER JOIN t_retailStore t4 ON t4.kid = t1.retail_id"
					+" WHERE t1. STATUS = 0 and t4.name like ?";
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("name"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;
	}
	
	// 查询我的客户交易订单 = 我的业绩
	public List<Map<String, Object>> selectMyMemberOrderList(String retail_id) {
		String sql = "SELECT "
					+"	t1.*,t2.end_price,t3.user_headimgurl,t3.user_nick ,from_unixtime((t1.created/1000),'%Y-%m-%d %H:%i:%S') time "
					+" FROM"
					+"	t_retail_order t1"
					+" LEFT OUTER JOIN t_order t2 ON t2.kid = t1.order_id"
					+" LEFT OUTER JOIN t_member t3 ON t3.kid = t1.member_id"
					+" WHERE t1. STATUS = 0 AND t1.retail_id = ? order by created DESC limit 3";
		List<Map<String, Object>> list = bs.findList(sql, new Object[] { retail_id });
		return list;
	}
	
	// 查询我的客户交易订单 = 我的业绩
	public List<Map<String, Object>> selectMyMemberOrderListt(String retail_id) {
		String sql = "SELECT "
					+"	t1.*,t2.end_price,t2.order_no,t3.user_headimgurl,t3.user_nick ,from_unixtime((t1.created/1000),'%Y-%m-%d %H:%i:%S') time "
					+" FROM"
					+"	t_retail_order t1"
					+" LEFT OUTER JOIN t_order t2 ON t2.kid = t1.order_id"
					+" LEFT OUTER JOIN t_member t3 ON t3.kid = t1.member_id"
					+" WHERE t1. STATUS = 0 AND t1.retail_id = ? order by created DESC";
		List<Map<String, Object>> list = bs.findList(sql,new Object[]{retail_id});
		//logger.info(sql);
		return list;
	}
	
	// 客户交易订单 
	public List<Map<String, Object>> MemberOrderList(String member_id,String retail_id) {
		String sql = "SELECT t1.*,t2.end_price,t3.user_headimgurl,t3.user_nick ,from_unixtime((t1.created/1000),'%Y-%m-%d %H:%i:%S') time  FROM"
					+"	t_retail_order t1"
					+" LEFT OUTER JOIN t_order t2 ON t2.kid = t1.order_id"
					+" LEFT OUTER JOIN t_member t3 ON t3.kid = t1.member_id"
					+" WHERE t1. STATUS = 0 AND t1.member_id = ? AND t1.retail_id = ? order by created DESC";
		List<Map<String, Object>> list = bs.findList(sql, new Object[] { member_id,retail_id });
		return list;
	}
	
	// 根据code 查询
	public t_retailStore get_super_retailStore(String code) {
		String sql = "select t.* from t_retailStore t where t.status=0 and t.code=?";
		t_retailStore retailStore = bs.findObj(sql, new Object[] { code }, t_retailStore.class);
		return retailStore;
	}
	//获取订单评价
	public t_order_discuzz getDiscuzz(String oid) {
		String sql = "select * from t_order_discuzz t where t.status=0 and t.order_id=?";
		t_order_discuzz discuzz = bs.findObj(sql, new String[] { oid }, t_order_discuzz.class);
		return discuzz;
	}

	@Transactional
	public void saveDiscuzz(t_order order, t_member cox, t_order_discuzz discuzz) {
		order.setIs_discuzz("是");
		bs.updateObj(order);
		bs.saveObj(discuzz);
//		cox.setScore_cnt(cox.getScore_cnt() + 1);
//		cox.setScore(cox.getScore() + discuzz.getScore());
		bs.updateObj(cox);
	}
	public static void main(String[] args) {
		String i = "003001003";
		System.out.println(i.substring(0,6));
	}
}
