package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_product;

/**
 * shop 仓库中的商品
 * @author 柴鑫
 *
 */
@Service
public class ProductOnstockService extends BaseService {

	// 查询已下架商品
		public List<Map<String,Object>> selectShopOnstockList(JSONObject srh, Map<String, String> sort_params, Page page){
			String sql = "select t.*,tc.name cname from t_product t "
					+ " left outer join t_product_cate tc on tc.kid=t.cate_code "
					+ " where t.status=0 and t.status_sys='已下架' and (t.name like ?)";
			NeParamList params = NeParamList.makeParams();
			params.addLike(srh.getString("name"));
			List<Map<String, Object>> list = bs.findList(sql, params, sort_params, page);
			return list;
		}
		
		// 查询已下架商品 
		public List<Map<String,Object>> selectOnstockList(JSONObject srh, Map<String, String> sort_params, Page page){
			String sql = "select t.*,tc.name cname from t_product t "
					+ " left outer join t_product_cate tc on tc.kid=t.cate_code "
					+ " where t.status=0 and t.status_sys='已下架' and (t.name like ?)";
			NeParamList params = NeParamList.makeParams();
			params.addLike(srh.getString("name"));
			List<Map<String, Object>> list = bs.findList(sql, params, sort_params, page);
			return list;
		}
	
		//根据kid查询文件
		public t_product findStockByKid(String kid){
			String sql = " select * from t_product t where t.status=0 and (t.status_sys='已下架') and t.kid=? ";
			t_product file = bs.findObj(sql, new Object[]{kid}, t_product.class);
			return file;
		}
		
		
}
