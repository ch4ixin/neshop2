package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;
/**
 * 首页管理
 * 
 * @author 申伟
 *
 */
@Service
public class FloorService extends BaseService{
	
	public List<Map<String, Object>> selectFloorList(JSONObject srh, Map<String, String> sort_param, Page page) {
		String sql = "select t.* from t_net_floor t where t.status=0 and (t.name like ?) ";
		
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("name"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_param, page);
		return list;
	}	
	
	//查询名称缩写
	public List<Map<String, Object>> selectNamesList() {
		String sql = "select t.* from t_net_floor t where t.status=0 order by t.floor asc";
		
		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}	
	
	
	
	
}
