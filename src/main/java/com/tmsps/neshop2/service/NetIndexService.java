package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.neshop2.base.service.BaseService;
@Service
public class NetIndexService extends BaseService {

	public List<Map<String, Object>> selectIndexList() {
		String sql = "select * from t_net_index_searchwords t "
				+ "where t.status=0 order by t.code";

		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}

}
