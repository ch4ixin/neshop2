package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.neshop2.base.service.BaseService;

@Service
public class ShopMenuService extends BaseService {

	// 无参查询所有页面导航列表
	public List<Map<String, Object>> selectMenuList(String shop_id) {
		String sql = "select * from t_shop_menu t where t.status=0 and (t.shop_id=?)";
		List<Map<String, Object>> list = bs.findList(sql, new Object[] { shop_id });
		return list;
	}

}
