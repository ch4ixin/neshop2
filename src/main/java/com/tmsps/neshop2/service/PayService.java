package com.tmsps.neshop2.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_order;
import com.tmsps.neshop2.model.t_pay_no;

/**
 * 支付服务
 * 
 * @author 冯晓东
 *
 */
@Service
public class PayService extends BaseService {

	/**
	 * 创建支付接口
	 * 
	 * @param order
	 * @param pay_type
	 * @return
	 */
	public t_pay_no makePayNo(t_order order, String pay_type) {
		t_pay_no payNo = new t_pay_no();
		payNo.setOrder_id(order.getKid());
		payNo.setStatus_pay("未生效");
		payNo.setPay_type(pay_type);
		bs.saveObj(payNo);
		return payNo;
	}

	@Transactional
	public String payOrderByAlipay(String out_trade_no, String taobao_trade_no) {
		// TODO 淘宝支付订单
		t_pay_no payNo = bs.findById(out_trade_no, t_pay_no.class);
		if ("已生效".equals(payNo.getStatus_pay())) {
			// 已经处理过了
			return payNo.getOrder_id();
		}

		t_order order = bs.findById(payNo.getOrder_id(), t_order.class);

		order.setIs_pay("是");
		order.setPay_type("在线支付");
		order.setPay_way("支付宝");
		bs.updateObj(order);

		payNo.setStatus_pay("已生效");
		payNo.setTrade_no(taobao_trade_no);
		bs.updateObj(payNo);
		
		return order.getKid();
	}// #payOrderByAlipay

	@Transactional
	public String finishOrderByAlipay(String out_trade_no, String taobao_trade_no) {
		// TODO 淘宝完成订单
		t_pay_no payNo = bs.findById(out_trade_no, t_pay_no.class);
		if ("未生效".equals(payNo.getStatus_pay())) {
			payOrderByAlipay(out_trade_no, taobao_trade_no);
		}
		
		return payNo.getOrder_id();

	}// #finishOrderByAlipay
}
