package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;

@Service
public class ArticleService extends BaseService {

	// 查询文章管理列表
	public List<Map<String, Object>> selectArticleInfoList(JSONObject srh, Map<String, String> sort_param, Page page) {
		String sql = "select t.*,ts.cate cate from t_net_article t " + "inner join t_net_article_cate ts "
				+ "on ts.code = t.cate_code "
				+ "where t.status=0 and (t.title like ?) and (ts.cate like ?) and (t.cate_code like ?) ";
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("title"));
		param.addLike(srh.getString("cate"));
		param.addLike(srh.getString("cate_code"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_param, page);
		return list;
	}

	// 无参查询文章列表
	public List<Map<String, Object>> selectArticleInfoListCate() {
		String sql = "select t.* from t_net_article_cate t " + "where t.status=0 order by t.code asc";
		List<Map<String, Object>> list = bs.findList(sql);
		for (Map<String, Object> map : list) {
			String sql2 = "select * from t_net_article t "
					+ "where t.status=0 and t.cate_code = ?  order by t.code asc";
			List<Map<String, Object>> li = bs.findList(sql2, new Object[] { (String) map.get("code") });
			map.put("li", li);
		}
		return list;
	}

	// 无参查询所有文章分类列表
	public List<Map<String, Object>> selectArticleList() {
		String sql = "select * from t_net_article_cate t where t.status=0 ";
		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}

}
