package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;

@Service
public class ShopRoleService extends BaseService {

	// 无参查询所有设置列表
	public List<Map<String, Object>> selectRoleListNoParam(String shopId, JSONObject srh,
			Map<String, String> sort_param, Page page) {
		String sql = "select * from t_shop_role t where t.status=0 and (t.shop_id=?) and (t.name like ?)";
		NeParamList params = NeParamList.makeParams();
		params.add(shopId);
		params.addLike(srh.getString("name"));
		List<Map<String, Object>> list = bs.findList(sql, params, sort_param, page);
		return list;
	}
	
	/**
	 * 2016/8/12 DeBug 管理员列表
	 * @author 柴鑫
	 * @return
	 */
	public List<Map<String, Object>> selectRoleList() {
		String sql = "select * from t_shop_role t  where t.status=0 order by t.name asc ";
		List<Map<String, Object>> list = jt.queryForList(sql);
		return list;
	}

}
