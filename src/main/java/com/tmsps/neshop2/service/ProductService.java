package com.tmsps.neshop2.service;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.web.SessionTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_fk_file;
import com.tmsps.neshop2.model.t_product_cate;
import com.tmsps.neshop2.util.DateTools;
import com.tmsps.neshop2.util.qrcode.QRTools;

/**
 *  商品
 * @author 柴鑫
 *
 */
@Service
public class ProductService extends BaseService {
	
	@Autowired
	public SettingService settingService;

	// 查询所有商品
	public List<Map<String,Object>> selectShopProductList(JSONObject srh, Map<String, String> sort_params, Page page){
		t_member memberDb = bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
		String sql = "select t.*,tc.name cname from t_product t "
				+ " left outer join t_product_cate tc on tc.kid=t.cate_code "
				+ " where t.status=0 and (t.shop_id = ?) and (t.status_sys = ?) and (t.name like ?) ";
		NeParamList params = NeParamList.makeParams();
		params.add(memberDb.getShop_id());
		params.add(srh.getString("status_sys"));
		params.addLike(srh.getString("name"));
		List<Map<String, Object>> list = bs.findList(sql, params, sort_params, page);
//		System.out.println(SessionTools.getCurrentLoginMember().getShop_id());
//		System.out.println(srh.getString("status_sys"));
//		System.out.println(srh.getString("name"));
//		System.out.println(sql);
		return list;
	}

	// 无参查询所有店铺分类列表
	public List<Map<String, Object>> selectCateList(JSONObject srh, Map<String, String> sort_param, Page page) {
		String sql = "select * from t_product_cate t where t.status=0 ORDER BY t.code";
		NeParamList param = NeParamList.makeParams();
		List<Map<String, Object>> list = bs.findList(sql, param, sort_param, page);
		return list;
	}

	public List<Map<String, Object>> selectCateList() {
		String sql = "select * from t_product_cate t where t.status=0 ORDER BY t.code";
		NeParamList param = NeParamList.makeParams();
		List<Map<String, Object>> list = bs.findList(sql, param);
		return list;
	}
	
	
	// 按照名称查询分类
	public t_product_cate getNameCate(String name) {
		String sql = "select t.* from t_product_cate t where t.status=0 and t.name = ?";
		t_product_cate product_cate = bs.findObj(sql,new Object[]{name},t_product_cate.class);
		return product_cate;
	}
	
	// 生成分类二维码
	public String getQRcodeFile(String content) throws IOException{
		int year = DateTools.getYear();
		int month = DateTools.getMonth();
		String folder = year + File.separator + month;
		String folder_url = year + "/" + month;
		
		// 获取保存路径
		String DATA_PATH = settingService.getVal("DATA_PATH");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		Date d=new Date();
		String str=sdf.format(d);
		String imgPath = DATA_PATH + File.separator + folder + File.separator + str+".png"; 
		//"http://192.168.1.104:8080/neshop2/wx/product.htm?cname="+cname+"&rank_code=DESC&test=ds";
		File file =QRTools.getQRCodeFile(content, imgPath);

		t_fk_file tf = new t_fk_file();
		// 保存文件
		tf.setFile_name(file.getName());
		tf.setNew_file_name(file.getName());
		tf.setFolder(folder);
		tf.setFolder_url(folder_url);
		tf.setContent_type("image/jpeg");
		tf.setSize(file.length());
		bs.saveObj(tf);
		return tf.getKid();
	}
}
