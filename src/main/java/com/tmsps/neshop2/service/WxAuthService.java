package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_shop_wx_auth;

@Service
public class WxAuthService extends BaseService {

	public List<Map<String, Object>> selectAuthList() {
		// TODO 查询所有权限数据
		String sql = "select * from t_shop_wx_auth t  where t.status=0 and t.shop_id=? order by t.code asc";
		List<Map<String, Object>> list = jt.queryForList(sql);

		return list;
	}
	
	public List<Map<String, Object>> selectAuthList(String shopId) {
		// TODO 查询所有权限数据
		String sql = "select * from t_shop_wx_auth t  where t.status=0 and t.shop_id=? order by t.code asc";
		List<Map<String, Object>> list = jt.queryForList(sql, shopId);

		return list;
	}

	public t_shop_wx_auth findAuthByCode(String code) {
		// TODO 根据code查询是否存在
		String sql = "select * from t_shop_wx_auth t where t.status=0 and t.code=?";
		t_shop_wx_auth auth = (t_shop_wx_auth) bs.findObj(sql, new String[] { code }, t_shop_wx_auth.class);
		return auth;
	}

	public t_shop_wx_auth findAuthByCode(String kid, String code) {
		String sql = "select * from t_shop_wx_auth t where t.status=0 and t.code=? and t.kid!=?";
		t_shop_wx_auth auth = (t_shop_wx_auth) bs.findObj(sql, new String[] { code, kid }, t_shop_wx_auth.class);
		return auth;
	}
	
	// TODO 查询是否有子节点
	public boolean isHaveSubAuth(String code) {
		String sql = "select count(*) cnt from t_shop_wx_auth t  where t.status=0 and t.shop_id=? and t.code like ?";
		Map<String, Object> cnt = jt.queryForMap(sql, code + "%");
		return ((Number) cnt.get("cnt")).longValue() > 1 ? true : false;
	}

}
