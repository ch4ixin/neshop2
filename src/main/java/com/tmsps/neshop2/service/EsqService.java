package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.tmsps.neshop2.base.service.BaseService;

/**
 * 平台客服
 * @author 申伟
 *
 */
@Service
public class EsqService extends BaseService{
	
	public List<Map<String, Object>> selectEsqList() {
		String sql = "select * from t_net_esq t where t.status=0 order by t.code asc";

		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}
	

}
