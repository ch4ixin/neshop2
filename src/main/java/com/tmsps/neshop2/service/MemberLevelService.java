package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;

@Service
public class MemberLevelService extends BaseService{
	// 无参查询所有设置列表
			public List<Map<String, Object>> selectMemderListNoParam(JSONObject srh, Map<String, String> sort_param, Page page) {
				String sql = "select * from t_member_level t where t.status=0 ";
				NeParamList params = NeParamList.makeParams();
				List<Map<String, Object>> list = bs.findList(sql, params, sort_param, page);
				return list;
			}
	
	
}
