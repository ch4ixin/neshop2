package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.neshop2.base.service.BaseService;

@Service
public class ShopProductCateService extends BaseService {

	// 查询文章管理列表
	public List<Map<String, Object>> selectShopCateInfoList() {
		String sql = "select t.*,ts.cate cate from t_shop_product_cate t " + "inner join t_shop_product_cate_first ts "
				+ "on ts.code = t.cate_code " + "where t.status=0 ";
		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}

	// 无参查询所有店铺分类列表
	public List<Map<String, Object>> selectCateFirstList() {
		String sql = "select t.* from t_shop_product_cate_first t where t.status=0 ";
		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}

}
