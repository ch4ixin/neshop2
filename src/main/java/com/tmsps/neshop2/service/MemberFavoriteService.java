package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.ne4spring.utils.ChkUtil;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_member_favorite_product;
import com.tmsps.neshop2.model.t_product;
import com.tmsps.neshop2.web.SessionTools;

@Service
public class MemberFavoriteService extends BaseService {

	//收藏商品的删除
	public t_member_favorite_product selectlist(String productId) {

		String sql = "select * from t_member_favorite_product t where t.status=0 and t.product_id=?";
		t_member_favorite_product od = bs.findObj(sql, new String[] { productId }, t_member_favorite_product.class);
		return od;

	}

	//商品收藏列表
	public List<Map<String, Object>> selectCollectionList() {
		// TODO 查询购物车列表项
		t_member member = SessionTools.getCurrentLoginMember();

		if (ChkUtil.isNotNull(member)) {
			String sql = "select t.*,tp.name,tp.show_id,tp.main_file_id,tp.price from t_member_favorite_product t "
					+ " left outer join t_product tp on tp.kid=t.product_id where t.status=0 and t.member_id=? ";
			List<Map<String, Object>> list = jt.queryForList(sql, member.getKid());
			return list;
		}
		return null;
	}
   //收藏商品的添加
	public String addFavorite(t_product product, t_member member) {
		String sql = "select * from t_member_favorite_product t where t.status=0 and t.product_id=? and t.member_id=? ";

		t_member_favorite_product fp = bs.findObj(sql, new String[] { product.getKid(), member.getKid() },
				t_member_favorite_product.class);

		if (fp == null) {
			t_member_favorite_product mf = new t_member_favorite_product();
			mf.setMember_id(member.getKid());
			mf.setProduct_id(product.getKid());
			bs.saveObj(mf);
			return "success";
		}
		return"failure";
	}
	
	
	//商店收藏列表
	public List<Map<String, Object>> selectShopList() {
		t_member member = SessionTools.getCurrentLoginMember();

		if (ChkUtil.isNotNull(member)) {
			String sql = "select t.*,ts.name,ts.level from t_member_favorite_shop t "
					+ "join t_shop ts on ts.kid=t.shop_id " 
					+ " where t.status=0 and t.member_id=? ";
			List<Map<String, Object>> list = jt.queryForList(sql, member.getKid());
			return list;
		}
		return null;
	}
	
	//收藏的商店的列表
	public List<Map<String, Object>> shopList() {
		t_member member = SessionTools.getCurrentLoginMember();
		String sql ="select * from t_member_favorite_shop t where t.status=0 and t.member_id=? ";
		List<Map<String, Object>> list=jt.queryForList(sql,member.getKid());
		return list;
	}
	
	

}
