package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;

/**
 * 会员积分查询
 * 
 * @author
 *
 */
@Service
public class MemberIntegralService extends BaseService {

	public List<Map<String, Object>> selectMemderIntegralList(String member_id, JSONObject srh,
			Map<String, String> sort_param, Page page) {
		String sql = "select * from t_member_integral t where t.status=0 and t.member_id=? ";
		NeParamList params = NeParamList.makeParams();
		params.add(member_id);
		List<Map<String, Object>> list = bs.findList(sql, params, sort_param, page);
		return list;
	}

	public List<Map<String, Object>> selectMemderIntegralList(JSONObject srh, Map<String, String> sort_param,
			Page page) {
		String sql = "select * from t_member_integral t where t.status=0 and t.member_id like ? ";
		NeParamList params = NeParamList.makeParams();
		params.addLike(srh.getString("member_id"));
		List<Map<String, Object>> list = bs.findList(sql, params, sort_param, page);
		return list;
	}

}
