package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;
/**
 * 品牌管理
 * @author 申伟
 *
 */
@Service
public class BrandService extends BaseService {
	
	public List<Map<String, Object>> selectBrandList(JSONObject srh, Map<String, String> sort_param, Page page) {
		String sql = "select t.* from t_product_brand t where t.status=0 and (t.name like ?) ";
		
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("name"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_param, page);
		return list;
	}	
}
