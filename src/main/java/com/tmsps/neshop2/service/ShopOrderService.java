package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_logistics;
import com.tmsps.neshop2.model.t_order;
import com.tmsps.neshop2.model.t_order_logistics;

/**
 * 发货
 * 
 * @author 柴鑫
 *
 */
@Service
public class ShopOrderService extends BaseService {
	// 获取已下单、已支付 的订单
	public List<Map<String, Object>> OrderList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select t.*,tp.unit,tm.user_nick mname,tp.name pname,tr.order_no,tr.address,tr.name,tr.phone,tr.pay_time from t_order_detail t "
				+ "inner join t_product tp on tp.kid = t.product_id "
				+ "inner join t_order tr on tr.kid = t.order_id "
				+ "inner join t_member tm on tm.kid = t.member_id "
				+ "where t.status=0 and (t.status_order ='已付款') and (tr.order_no like ?)";
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("order_no"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;
	}

	// 获取物流公司
	public List<Map<String, Object>> selectShopLogisticsParam() {
		String sql1 = "select t.code,t.name from t_logistics t where t.status=0 ORDER BY code";
		List<Map<String, Object>> list1 = bs.findList(sql1);
		return list1;
	}

	// 获取发货中信息
	public List<Map<String, Object>> selectDeliverList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select tp.name pname,tp.unit,tm.user_nick mname,t.cnt,tr.address,tr.name rname,tr.phone rphone,tl.*,tlo.name,tlo.kd_code,t.kid kidd "
				+ "from t_order_detail t "
				+ "inner join t_product tp on tp.kid = t.product_id "
				+ "inner join t_order tr on tr.kid = t.order_id "
				+ "inner join t_order_logistics tl on tl.kid = t.logistics_id "
				+ "inner join t_logistics tlo on tlo.code = tl.logistics_com_code "
				+ "inner join t_member tm on tm.kid = t.member_id "
				+ "where t.status=0 and tl.status_logistics in ('发货中') and (tr.order_no like ?)";
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("order_no"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;
	}

	// 获取已收货信息
	public List<Map<String, Object>> selectTakeList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select tp.name pname,tp.unit,tm.user_nick mname,t.cnt,tr.address,tr.name rname,tr.phone rphone,tl.*,tlo.name,tlo.kd_code,t.kid kidd "
				+ "from t_order_detail t "
				+ "inner join t_product tp on tp.kid = t.product_id "
				+ "inner join t_order tr on tr.kid = t.order_id "
				+ "inner join t_order_logistics tl on tl.kid = t.logistics_id "
				+ "inner join t_logistics tlo on tlo.code = tl.logistics_com_code "
				+ "inner join t_member tm on tm.kid = t.member_id "
				+ "where t.status=0 and tl.status_logistics in ('已收货') and (tr.order_no like ?)";
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("order_no"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;
	}
	
	// 查询该订单下的订单项的状态
	public List<Map<String, Object>> fintOrderStateList(String order_id) {
		String sql = "select t.* from t_order_detail t "
				+ "inner join t_order tr on tr.kid = t.order_id "
				+ "where t.status=0 and t.order_id = ?";
		List<Map<String, Object>> list = bs.findList(sql,new String[] { order_id });
		return list;
	}

	// 根据订单号查询订单
	public t_order orderIdList(String order_id) {
		String sql = "select * from t_order t where t.status=0 and t.order_no=?";
		t_order order = bs.findObj(sql, new Object[] { order_id }, t_order.class);
		return order;
	}

	// 根据物流公司编号查询 物流公司信息
	public t_logistics kdCodeList(String code) {
		String sql = "select * from t_logistics t where t.status=0 and t.code=?";
		t_logistics logistics = bs.findObj(sql, new Object[] { code }, t_logistics.class);
		return logistics;
	}

	// 根据运单号获取发货信息
	public t_order_logistics getOrderLogistics(String logistics_code) {
		String sql = "select * from t_order_logistics t where t.status=0 and t.logistics_code=?";
		t_order_logistics orderLogistics = bs.findObj(sql, new String[] { logistics_code }, t_order_logistics.class);
		return orderLogistics;
	}
}
