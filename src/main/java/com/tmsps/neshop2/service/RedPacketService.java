package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_member;

@Service
public class RedPacketService extends BaseService{
	// 获取红包记录
	public List<Map<String, Object>> selectRedPacketList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select t.*,tm.user_nick member_name from t_red_packet t "
				+ " left outer join t_member tm on tm.kid = t.member_id "
				+ "where t.status=0 ";
		NeParamList param = NeParamList.makeParams();
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;
	}
	
	// 获取红包模板
	public List<Map<String, Object>> selectRedPacketModelList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select t.* from t_redpacket_model t where t.status=0 ";
		NeParamList params = NeParamList.makeParams();
		List<Map<String, Object>> list = bs.findList(sql, params, sort_params, page);
		return list;
	}
	
	//根据openid查询实体
	public t_member findMemberByKid(String openid){
		String sql = " select * from t_member t where t.status=0 and t.openid=? ";
		t_member member = bs.findObj(sql, new Object[]{openid}, t_member.class);
		return member;
	}
	
}
