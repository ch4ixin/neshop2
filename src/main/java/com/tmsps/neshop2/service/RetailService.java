package com.tmsps.neshop2.service;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;

@Service
public class RetailService extends BaseService {

	// 查询所有分销用户提现记录
	public List<Map<String, Object>> selectUserGetMoneyList(JSONObject srh, Map<String, String> sort_params, Page page) {
		String sql = "select t1.*,t2.name from t_retail_get_money_list t1 " 
				+ " left outer join t_retailStore t2 on t2.kid = t1.retail_id "
				+ " where t1.status=0 and t2.name like ?";
		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("name"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_params, page);
		return list;
	}
}
