package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;

/**
 *  首页广告
 * @author 柴鑫
 */
@Service
public class HomeBillboardService extends BaseService {

	// 无参查询首页广告
	public List<Map<String, Object>> selectBillboards(JSONObject srh, Map<String, String> sort_param, Page page) {
		String sql = "select t.*,tp.name pname from t_home_billboards t "
				+ "join t_product tp on tp.kid = t.product_id " 
				+ " where t.status=0 ";
		NeParamList param = NeParamList.makeParams();
		List<Map<String, Object>> list = bs.findList(sql, param, sort_param, page);
		return list;
	}
	
	// 按照位置查询广告
	public List<Map<String, Object>> getSiteBillboards() {
		String sql = "select t.* from t_home_billboards t where t.status=0 ORDER BY t.site asc";
		List<Map<String, Object>> home_billboards = bs.findList(sql);
		return home_billboards;
	}
}
