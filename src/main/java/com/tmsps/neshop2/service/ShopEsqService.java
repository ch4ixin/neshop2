package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.neshop2.base.service.BaseService;

/**
 * 店铺客服表
 * 
 * @author 申伟
 *
 */
@Service
public class ShopEsqService extends BaseService {

	public List<Map<String, Object>> selectShopEsqList(String shopId) {
		String sql = "select * from t_shop_esq t where t.status=0 and t.shop_id=? order by t.code asc";
		List<Map<String, Object>> list = jt.queryForList(sql, shopId);
		return list;
	}

}
