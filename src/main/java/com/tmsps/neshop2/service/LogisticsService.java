package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.neshop2.base.service.BaseService;

/**
 * 物流公司管理
 * @author 张杰
 *
 */
@Service
public class LogisticsService extends BaseService{
	// 无参查询所有设置列表
	public List<Map<String, Object>> selectLogisticsListNoParam() {
		String sql = "select * from t_logistics t where t.status=0";
		
		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}
}
