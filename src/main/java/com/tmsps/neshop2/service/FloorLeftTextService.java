package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;

/**
 * 左侧文字图片管理
 * 
 * @author Administrator申伟
 *
 */
@Service
public class FloorLeftTextService extends BaseService {

	public List<Map<String, Object>> selectLeftTextList() {
		String sql = "select t.* from t_net_floor t where t.status=0  order by t.floor  asc";

		String sql1 = "select t.* from t_net_floor_left_txt t where t.status=0 and t.floor_id=?";

		String sql2 = "select t.* from t_net_floor_img t where t.status=0 and t.floor_id=? and t.position='左下'";

		String sql3 = "select t.* from t_net_floor_img t where t.status=0 and t.floor_id=? and t.position<>'左下'";

		String sql4 = "select t.* from t_net_floor_products t where t.status=0 and t.floor_id=? ";

		List<Map<String, Object>> list = bs.findList(sql);
		for (Map<String, Object> map : list) {
			List<Map<String, Object>> list1 = bs.findList(sql1, new Object[] { (String) map.get("floor") });
			Map<String, Object> left = bs.findObj(sql2, new Object[] { (String) map.get("floor") });
			List<Map<String, Object>> list3 = bs.findList(sql3, new Object[] { (String) map.get("floor") });
			List<Map<String, Object>> list4 = bs.findList(sql4, new Object[] { (String) map.get("floor") });

			map.put("goods", list1);

			map.put("left_img", left);

			map.put("right_imgs", list3);

			map.put("products", list4);

		}
		return list;

	}

	public List<Map<String, Object>> selectTextList(JSONObject srh, Map<String, String> sort_param, Page page) {

		String sql = "select t.* from t_net_floor_left_txt t where t.status=0 and (t.name like ?) order by t.floor_id asc";

		NeParamList param = NeParamList.makeParams();
		param.addLike(srh.getString("name"));
		List<Map<String, Object>> list = bs.findList(sql, param, sort_param, page);
		return list;

	}
}
