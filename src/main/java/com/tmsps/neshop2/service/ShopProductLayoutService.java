package com.tmsps.neshop2.service;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.neshop2.base.service.BaseService;



@Service
public class ShopProductLayoutService extends BaseService{
	
	// 查询所有关联版式列表
		public List<Map<String, Object>> selectLayoutList(String shop_id) {
			String sql = "select * from t_shop_product_layout t where t.status=0 and t.shop_id=? ";
			
			List<Map<String, Object>> list = bs.findList(sql,new Object[]{shop_id});
			return list;
		}
	
}
