package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_product;
import com.tmsps.neshop2.util.ChkTools;

@Service
public class InfoService extends BaseService {

	public t_product SelectInfoData() {
		String sql = "select * from t_product where status=0 ";

		t_product product = bs.findObj(sql, t_product.class);
		if (ChkTools.isNull(product)) {
			product = new t_product();
			bs.saveObj(product);
		}
		return product;

	}

	public List<Map<String, Object>> selectProductAuthList() {
		// TODO 查询所有权限数据
		String sql = "select t.* from t_product_cate t  where t.status=0 and length(t.code)=3 order by t.code asc";
		List<Map<String, Object>> list = jt.queryForList(sql);

		return list;
	}
	
	public List<Map<String, Object>> selectBrandList() {
		String sql = "select t.* from t_product_brand t where t.status=0 ";
		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}	
}
