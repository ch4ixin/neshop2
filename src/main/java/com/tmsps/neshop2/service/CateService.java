package com.tmsps.neshop2.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_product_cate;
import com.tmsps.neshop2.util.tree.CateTreeTools;

@Service
public class CateService extends BaseService {
	public List<Map<String, Object>> selectAuthList() {
		// TODO 查询所有权限数据
		String sql = "select * from t_product_cate t  where t.status=0 order by t.code asc";
		List<Map<String, Object>> list = jt.queryForList(sql);

		return list;
	}

	// 2
	public List<Map<String, Object>> selectAuthList(String code) {
		// TODO 查询所有权限数据
		String sql = "select * from t_product_cate t  where t.status=0 and t.code like '" + code.substring(0, 3)
				+ "%' ";
		List<Map<String, Object>> list = jt.queryForList(sql);

		return list;
	}

	public t_product_cate findAuthByCode(String code) {
		// TODO 根据code查询是否存在
		String sql = "select * from t_product_cate t where t.status=0 and t.code=?";
		t_product_cate auth = (t_product_cate) bs.findObj(sql, new String[] { code });
		return auth;
	}

	public t_product_cate findAuthByCode(String kid, String code) {
		String sql = "select * from t_product_cate t where t.status=0 and t.code=? and t.kid!=?";
		t_product_cate auth = (t_product_cate) bs.findObj(sql, new String[] { code, kid });
		return auth;
	}

	// 3位查询树 2
	public List<Map<String, Object>> selectAllCateTree(String code) {
		List<Map<String, Object>> cateList = this.selectAuthList(code);
		List<Map<String, Object>> cateTreeList = CateTreeTools.turnListToTree(cateList);
		return cateTreeList;
	}

	// 把list转换成树
	public List<Map<String, Object>> selectAllCateToTree() {
		List<Map<String, Object>> cateList = this.selectAuthList();
		List<Map<String, Object>> cateTreeList = CateTreeTools.turnListToTree(cateList);
		return cateTreeList;
	}

	// 依据cate_code查找数据
	public List<Map<String, Object>> selectAuthSpecList(String code) {
		String sql = "select * from t_product_cate_spec t  where t.status=0 and (t.cate_code = ?) order by code";
		List<Map<String, Object>> list = jt.queryForList(sql, code);
		return list;
	}

	// 查找第一级商品分类(3位)
	public List<Map<String, Object>> selectProductOneList() {
		String sql = "select * from t_product_cate t  where t.status=0 and (CHAR_LENGTH(t.code)='3') order by t.code asc";
		List<Map<String, Object>> list = jt.queryForList(sql);

		return list;
	}

	// 查找第二级和三级的商品分类
	public List<Map<String, Object>> selectProductCateList(String oneName) {
		// 根据名称查询商品分类
		String sql = "select t.* from t_product_cate t where t.status=0 and (t.name=?) and (CHAR_LENGTH(t.code)='3')";
		t_product_cate code = bs.findObj(sql, new Object[] { oneName }, t_product_cate.class);
		// 查询二级商品分类
		String sql1 = "select t.* from t_product_cate t where t.status=0 and (CHAR_LENGTH(t.code)='6') and (SUBSTRING(t.code,1,3)=?)";
		List<Map<String, Object>> twoCodeList = bs.findList(sql1, new Object[] { code.getCode() });
		for (Map<String, Object> map : twoCodeList) {
			// 查询三级商品分类
			String sql2 = "select t.* from t_product_cate t where t.status=0 and (CHAR_LENGTH(t.code)='9') and (SUBSTRING(t.code,1,6)=?)";
			List<Map<String, Object>> threeCodeList = bs.findList(sql2, new Object[] { map.get("code") });
			map.put("threeCodeList", threeCodeList);
		}
		return twoCodeList;
	}

	// 根据二级分类的code查询三级商品
	public List<Map<String, Object>> selectProductByCode(String two_code, String sort) {
		String sql = null;
		if ("desc".equals(sort)) {
			sql = "select t.* from t_product t where t.status=0 and (SUBSTRING(t.cate_code,1,6)=?) order by t.price desc";
		} else if ("asc".equals(sort)) {
			sql = "select t.* from t_product t where t.status=0 and (SUBSTRING(t.cate_code,1,6)=?) order by t.price asc";
		} else {
			sql = "select t.* from t_product t where t.status=0 and (SUBSTRING(t.cate_code,1,6)=?)";
		}
		List<Map<String, Object>> list = bs.findList(sql, new Object[] { two_code });

		return list;
	}

}
