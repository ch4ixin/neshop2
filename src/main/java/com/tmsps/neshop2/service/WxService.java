package com.tmsps.neshop2.service;

import org.springframework.stereotype.Service;

import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_shop_wx_config;
import com.tmsps.neshop2.model.t_shop_wx_reply_join;

@Service
public class WxService extends BaseService {

	public t_shop_wx_config getShopWxConfig(String type) {
		String sql = "select * from t_shop_wx_config t where t.status=0 and t.type=? ";
		t_shop_wx_config wx = bs.findObj(sql, new String[] { type }, t_shop_wx_config.class);
		if (wx == null) {
			wx = new t_shop_wx_config();
			wx.setType(type);
			bs.saveObj(wx);
		}
		return wx;
	}

	public WxConfig getShopWxByType(String type) {
		t_shop_wx_config wxConfig = getShopWxConfig(type);
		return turnTToWx(wxConfig);
	}

	public WxConfig getShopWxByDomain(String domain) {
		return turnTToWx(getShopWxConfigByDomain(domain));
	}

	// TODO 微信接口获取店铺微信设置
	public WxConfig getShopWx(String wxid) {
		t_shop_wx_config wxConfig = bs.findById(wxid, t_shop_wx_config.class);
		return turnTToWx(wxConfig);
	}

	public WxConfig turnTToWx(t_shop_wx_config wx) {
		return new WxConfig(wx.getAppid(), wx.getAppsecret(), wx.getAes_key(), false, wx.getToken());
	}

	// 获取自动回复信息
	public t_shop_wx_reply_join getReply() {
		String sql = "select * from t_shop_wx_reply_join t where t.status=0 ";
		t_shop_wx_reply_join reply = bs.findObj(sql, t_shop_wx_reply_join.class);
		if (reply == null) {
			reply = new t_shop_wx_reply_join();
			reply.setType("文字");
			bs.saveObj(reply);
		}

		return reply;
	}
	
	public t_shop_wx_config getShopWxConfigByDomain(String domain) {
		String sql = "select * from t_shop_wx_config t where t.status=0 and t.domain=? ";
		t_shop_wx_config wxConfig = bs.findObj(sql, new String[] { domain }, t_shop_wx_config.class);
		return wxConfig;
	}

}
