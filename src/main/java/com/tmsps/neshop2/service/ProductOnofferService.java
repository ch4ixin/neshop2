package com.tmsps.neshop2.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4spring.orm.param.NeParamList;
import com.tmsps.ne4spring.page.Page;
import com.tmsps.ne4spring.utils.ChkUtil;
import com.tmsps.neshop2.base.service.BaseService;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_order_detail;
import com.tmsps.neshop2.model.t_product;
import com.tmsps.neshop2.web.SessionTools;
import com.tmsps.neshop2.web.WebTools;

/**
 * shop 出售中的商品
 * @author 柴鑫
 *
 */
@Service
public class ProductOnofferService extends BaseService {
		// 查询所有商品
		public List<Map<String,Object>> selectOnofferList(JSONObject srh, Map<String, String> sort_params, Page page){
			String sql = "select t.*,tc.name cname from t_product t "
					+ " left outer join t_product_cate tc on tc.kid=t.cate_code "
					+ " where t.status=0 and t.status_sys='上架中' and (t.name like ?)";
			NeParamList params = NeParamList.makeParams();
			params.addLike(srh.getString("name"));
			List<Map<String, Object>> list = bs.findList(sql, params, sort_params, page);
			return list;
		}
	
		// 查询所有商品
		public List<Map<String,Object>> getAllProductList(){
			String sql = " select * from t_product t where t.status=0 and t.status_sys='上架中' ORDER BY t.rank_code asc";
			List<Map<String, Object>> list = bs.findList(sql);
			return list;
		}

		// 查询所有商品
		public List<Map<String,Object>> upGetAllProductList(String cname,String rank_code,String shop_id){
			String sql;
			List<Map<String, Object>> list;
			NeParamList params = NeParamList.makeParams();
			if(ChkUtil.isNull(cname)){
				if("DESC".equals(rank_code)){
					sql = " select t.*,tc.name cname from t_product t "
							+ " left outer join t_product_cate tc on tc.kid=t.cate_code "
							+ " where t.status=0 and t.status_sys='上架中' and t.shop_id=? ORDER BY t.rank_code desc";
				}else{
					sql = " select t.*,tc.name cname from t_product t "
							+ " left outer join t_product_cate tc on tc.kid=t.cate_code "
							+ " where t.status=0 and t.status_sys='上架中' and t.shop_id=? ORDER BY t.rank_code asc";
				}
				params.add(shop_id);
				list = bs.findList(sql,params);
			}else{
				if("DESC".equals(rank_code)){
					sql = " select t.*,tc.name cname from t_product t "
							+ " left outer join t_product_cate tc on tc.kid=t.cate_code "
							+ " where t.status=0 and t.status_sys='上架中' and t.shop_id=? and tc.name=? ORDER BY t.rank_code desc";
				}else{
					sql = " select t.*,tc.name cname from t_product t "
							+ " left outer join t_product_cate tc on tc.kid=t.cate_code "
							+ " where t.status=0 and t.status_sys='上架中' and t.shop_id=? and tc.name=? ORDER BY t.rank_code asc";
				}
				params.add(shop_id);
				params.add(cname);
				list = bs.findList(sql,params);
			}
			return list;
		}
		
		// 搜索商品
		public List<Map<String,Object>> searchProductList(String pname){
			String sql = " select * from t_product t where t.status=0 and t.status_sys='上架中' and t.name like ? ORDER BY t.rank_code asc";
			String paramValue = "%" + pname + "%";
			List<Map<String, Object>> list = bs.findList(sql,new String[]{ paramValue });
			return list;
		}
		
		// 查询所有商品
		public List<Map<String,Object>> getAscProductList(){
			String sql = " select * from t_product t where t.status=0 and t.status_sys='上架中' ORDER BY t.rank_code asc limit 3";
			List<Map<String, Object>> list = bs.findList(sql);
			return list;
		}

		// 查询所有商品
		public List<Map<String,Object>> getDescProductList(){
			String sql = " select * from t_product t where t.status=0 and t.status_sys='上架中' ORDER BY t.rank_code desc limit 3";
			List<Map<String, Object>> list = bs.findList(sql);
			return list;
		}
		
		// 查询所有商品
		public List<Map<String,Object>> getCateProductList(String cate_id){
			String sql = " select * from t_product t where t.status=0 and t.status_sys='上架中' and t.cate_code = ? ORDER BY t.rank_code asc";
			List<Map<String, Object>> list = bs.findList(sql,new String[] { cate_id });
			return list;
		}
	
		
		//根据kid查询文件
		public t_product findFileByKid(String kid){
			String sql = " select * from t_product t where t.status=0 and t.kid=? ";
			t_product file = bs.findObj(sql, new Object[]{kid}, t_product.class);
			return file;
		}
		
		// TODO 加入购物车
		@Transactional
		public void addToCart(t_product product, int number) {
			t_member member = SessionTools.getCurrentLoginMember();
			String cookieSN = WebTools.getClientCookieSN();

			t_order_detail order_detail = getCartOd(product.getKid(), member, cookieSN);
			if (order_detail == null) {
				order_detail = new t_order_detail();
				order_detail.setShop_id(product.getShop_id());
				order_detail.setCnt(new BigDecimal(number));
				bs.saveObj(order_detail);
			} else {
				order_detail.setCnt(order_detail.getCnt().add(new BigDecimal(number)));
			}

			order_detail.setCookie_sn(cookieSN);
			if (member != null) {
				order_detail.setMember_id(member.getKid());
			}
			order_detail.setProduct_id(product.getKid());
			order_detail.setType(product.getType());
			order_detail.setPrice(product.getPrice());
			order_detail.setPrice_total(order_detail.getPrice().multiply(order_detail.getCnt()));
			order_detail.setIntegral(product.getIntegral());
			order_detail.setIntegral_total(product.getIntegral() * order_detail.getCnt().intValue());
			order_detail.setIs_print("否");
			order_detail.setStatus_order("购物车");

			bs.updateObj(order_detail);

		}// # addToCart
		
		// TODO 查询购物车中的购物项
		public t_order_detail getCartOd(String productId, t_member member, String cookieSN) {
			if (ChkUtil.isNotNull(member)) {
				String sql = "select * from t_order_detail t where t.status=0 and t.product_id=? and t.member_id=? and t.status_order='购物车' ";
				t_order_detail od = bs.findObj(sql, new String[] { productId, member.getKid() }, t_order_detail.class);
				return od;
			} else {
				String sql = "select * from t_order_detail t where t.status=0 and t.product_id=? and t.cookie_sn=? and t.status_order='购物车' ";
				t_order_detail od = bs.findObj(sql, new String[] { productId, cookieSN }, t_order_detail.class);
				return od;
			}
		}

	// 查询购物车列表项
	public List<Map<String, Object>> selectCartItemList() {
			t_member member = SessionTools.getCurrentLoginMember();
			String cookieSN = WebTools.getClientCookieSN();

			if (ChkUtil.isNotNull(member)) {
				//System.out.println("member");
				String sql = "select t.*,tp.name p_name,tp.kid p_kid,tp.main_file_id,tp.carriage carr,tp.more_carriage mcarr,tp.unit  from t_order_detail t "
						+ " left outer join t_product tp on tp.kid=t.product_id "
						+ " where t.status=0 and t.status_order='购物车' and t.member_id=? ";
				List<Map<String, Object>> list = jt.queryForList(sql, member.getKid());
				return list;
			} else {
				//System.out.println("cookieSN");
				String sql = "select t.*,tp.name p_name,tp.kid p_kid,tp.main_file_id,tp.carriage carr,tp.more_carriage mcarr,tp.unit from t_order_detail t "
						+ " left outer join t_product tp on tp.kid=t.product_id "
						+ " where t.status=0 and t.status_order='购物车' and t.cookie_sn=? ";
				List<Map<String, Object>> list = jt.queryForList(sql, cookieSN);
				return list;
			}

		}
}
