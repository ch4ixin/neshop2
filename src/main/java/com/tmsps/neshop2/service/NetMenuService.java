package com.tmsps.neshop2.service;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.tmsps.neshop2.base.service.BaseService;



@Service
public class NetMenuService extends BaseService{
	
	// 无参查询所有页面导航列表
	public List<Map<String, Object>> selectMenuList() {
		String sql = "select * from t_net_menu t where t.status=0 ";
		
		List<Map<String, Object>> list = bs.findList(sql);
		return list;
	}
	
}
