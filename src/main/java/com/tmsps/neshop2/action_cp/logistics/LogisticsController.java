package com.tmsps.neshop2.action_cp.logistics;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_logistics;
import com.tmsps.neshop2.service.LogisticsService;
import com.tmsps.neshop2.util.json.JsonTools;


/**
 * 物流公司管理
 * 
 * @author 柴鑫
 *
 */
@Controller
@Scope("prototype")
@RequestMapping("/cp/logistics")
public class LogisticsController extends ProjBaseAction{
	
	@Autowired
	private LogisticsService logisticsService;

	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {

		List<Map<String, Object>> list = logisticsService.selectLogisticsListNoParam();

		result.put("list", list);
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public void add(t_logistics logistics) {
		bs.saveObj(logistics);

		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_logistics logistics = bs.findById(kid, t_logistics.class);
		logistics.setStatus(-100);
		bs.updateObj(logistics);
		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		t_logistics logistics = bs.findById(kid, t_logistics.class);
		return JsonTools.toJson(logistics);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(t_logistics logistics) {
		t_logistics logisticsDb = bs.findById(logistics.getKid(), t_logistics.class);
		logisticsDb.setCode(logistics.getCode());
		logisticsDb.setName(logistics.getName());
		logisticsDb.setKd_code(logistics.getKd_code());
		bs.updateObj(logisticsDb);


		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}
	
}
