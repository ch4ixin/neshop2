package com.tmsps.neshop2.action_cp.memberLevel;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_member_level;
import com.tmsps.neshop2.service.MemberLevelService;
import com.tmsps.neshop2.util.json.JsonTools;

@Controller
@Scope("prototype")
@RequestMapping("/cp/memberLevel")
public class MemberLevelController extends ProjBaseAction {
	@Autowired
	private MemberLevelService memberLevelService;

	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {
		List<Map<String, Object>> list = memberLevelService.selectMemderListNoParam(srh, sort_params, page);
		result.put("list", list);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_member_level member_level = bs.findById(kid, t_member_level.class);
		member_level.setStatus(-100);
		bs.updateObj(member_level);

		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}

	@RequestMapping("/add")
	@ResponseBody
	public void add(t_member_level member_level) {
		bs.saveObj(member_level);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		t_member_level member_level = bs.findById(kid, t_member_level.class);
		return JsonTools.toJson(member_level);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(t_member_level member_level) {
		t_member_level member_levelDb = bs.findById(member_level.getKid(), t_member_level.class);
		member_levelDb.setName(member_level.getName());
		member_levelDb.setLevel(member_level.getLevel());
		member_levelDb.setIntegral(member_level.getIntegral());
		member_levelDb.setNote(member_level.getNote());
		bs.updateObj(member_levelDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

}
