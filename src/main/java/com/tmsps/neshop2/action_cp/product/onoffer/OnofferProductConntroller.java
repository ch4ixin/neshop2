package com.tmsps.neshop2.action_cp.product.onoffer;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_product;
import com.tmsps.neshop2.service.ProductOnofferService;
import com.tmsps.neshop2.util.ChkTools;
import com.tmsps.neshop2.util.json.JsonTools;

/**
 * shop 出售中的商品
 * 
 * @author 柴鑫
 *
 */
@Controller
@Scope("prototype")
@RequestMapping("/cp/onoffer")
public class OnofferProductConntroller extends ProjBaseAction {
	@Autowired
	private ProductOnofferService spofs;

	@RequestMapping("/list")
	@ResponseBody
	public String list_data() {

		List<Map<String, Object>> list = spofs.selectOnofferList(srh, sort_params, page);
		JSONObject rows = new JSONObject();
		rows.put("page", page.getPageNumber());
		rows.put("total", page.getTotalRow());
		rows.put("list", list);

		return JsonTools.toJson(rows);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_product fileDB = spofs.findFileByKid(kid);
		fileDB.setStatus(-100);
		bs.updateObj(fileDB);
		this.setTipMsg("文件删除成功！", Tip.Type.success);
	}

	@RequestMapping("/add")
	@ResponseBody
	public void add(t_product product) {
		product.setType("商品");
		bs.saveObj(product);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		t_product product = bs.findById(kid, t_product.class);
		result.put("product", product);
		return JsonTools.toJson(result);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(String kid, String reason, String status_sys, String file_ids, String main_file_id, t_product product) {
		t_product productDb = bs.findById(kid, t_product.class);
		String[] files = file_ids.split(",");

		if (ChkTools.isNotNull(file_ids)) {
			if (file_ids.contains(",") && ChkTools.isNotNull(main_file_id)) {
				productDb.setMain_file_id(main_file_id);
				productDb.setFile_ids(file_ids);
			}
			if (ChkTools.isNotNull(main_file_id)) {
				productDb.setFile_ids(file_ids);
				productDb.setMain_file_id(main_file_id);
			}
			if (ChkTools.isNull(main_file_id)) {
				if (file_ids.contains(",")) {
					productDb.setFile_ids(file_ids.substring(files[0].length() + 1));
				} else {
					productDb.setFile_ids("");
				}
				productDb.setMain_file_id(files[0]);

			}
		} else {
			productDb.setFile_ids(file_ids);
			productDb.setMain_file_id(main_file_id);
		}

		productDb.setCate_code(product.getCate_code());
		productDb.setRank_code(product.getRank_code());
		productDb.setMore_carriage(product.getMore_carriage());
		productDb.setCarriage(product.getCarriage());
		productDb.setStatus_sys(product.getStatus_sys());
		productDb.setName(product.getName());
		productDb.setType(product.getType());
		productDb.setCate_code(product.getCate_code());
		productDb.setStock(product.getStock());
		productDb.setBrand_name(product.getBrand_name());
		productDb.setMarket_price(product.getMarket_price());
		productDb.setLayout_id(product.getLayout_id());
		productDb.setPrice(product.getPrice());
		productDb.setIntegral(product.getIntegral());
		productDb.setCode(product.getCode());
		productDb.setUnit(product.getUnit());
		bs.updateObj(productDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}
	
	@RequestMapping("/edit_content")
	@ResponseBody
	public void edit_content(t_product product) {
		t_product productDb = bs.findById(product.getKid(), t_product.class);
		
		productDb.setContent(product.getContent());
		bs.updateObj(productDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

}
