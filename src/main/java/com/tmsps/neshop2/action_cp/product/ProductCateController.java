package com.tmsps.neshop2.action_cp.product;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_product_cate;
import com.tmsps.neshop2.service.ProductService;
import com.tmsps.neshop2.util.json.JsonTools;

/**
 * 商品分类
 * @author Chaixin
 *
 */
@Controller
@Scope("prototype")
@RequestMapping("/cp/product/cate")
public class ProductCateController extends ProjBaseAction {

	@Autowired
	private ProductService productService;

	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {
		List<Map<String, Object>> list = productService.selectCateList(srh, sort_params, page);
		result.put("list", list);
	}

	@RequestMapping("/add")
	@ResponseBody
	public void add(t_product_cate cate) throws IOException {
		cate.setMain_file_id(productService.getQRcodeFile(req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+"/neshop2/wx/product.htm?cname="+java.net.URLDecoder.decode(new String(cate.getName().getBytes("iso-8859-1"),"utf-8"),"UTF-8")+"&rank_code=DESC"));
		bs.saveObj(cate);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_product_cate cate = bs.findById(kid, t_product_cate.class);
		cate.setStatus(-100);
		bs.updateObj(cate);
		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		t_product_cate cate = bs.findById(kid, t_product_cate.class);
		return JsonTools.toJson(cate);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(t_product_cate cate) throws IOException {
		System.out.println(req.getCharacterEncoding());
		t_product_cate cateDb = bs.findById(cate.getKid(), t_product_cate.class);
		cateDb.setCode(cate.getCode());
		cateDb.setName(cate.getName());
		cateDb.setAd_file_id(cate.getAd_file_id());
		cateDb.setIcon_file_id(cate.getIcon_file_id());
		cateDb.setMain_file_id(productService.getQRcodeFile(req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+"/neshop2/wx/product.htm?cname="+cate.getName()+"&rank_code=DESC"));
		bs.updateObj(cateDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

}
