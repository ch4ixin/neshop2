package com.tmsps.neshop2.action_cp.product.info;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_product;
import com.tmsps.neshop2.service.InfoService;
import com.tmsps.neshop2.util.ChkTools;

/**
 * 
 * @author chaixin
 *
 */
@RestController
@Scope("prototype")
@RequestMapping("/cp/product/info")
public class InfoController extends ProjBaseAction {
	@Autowired
	private InfoService infoService;

	@RequestMapping("/add")
	@ResponseBody
	public void add(t_product cate, String files_ids) {
		if (ChkTools.isNotNull(files_ids)) {
			cate.setFile_ids(files_ids);
			String[] files = files_ids.split(",");
			System.out.println(files[0]);
			cate.setMain_file_id(files[0]);
		}

		bs.saveObj(cate);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/auth_data")
	@ResponseBody
	public void auth_data() {

		List<Map<String, Object>> list = infoService.selectProductAuthList();

		result.put("list", list);
	}

	@RequestMapping("brand")
	@ResponseBody
	public void brand() {
		List<Map<String, Object>> list = infoService.selectBrandList();
		result.put("list", list);
	}

}
