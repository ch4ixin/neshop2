package com.tmsps.neshop2.action_cp.redpacket;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.ne4spring.utils.PKUtil;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_red_packet;
import com.tmsps.neshop2.model.t_redpacket_model;
import com.tmsps.neshop2.service.RedPacketService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.wx.PaymentAPIRedToUser;


@Controller
@Scope("prototype")
@RequestMapping("/cp/redpacket")
public class RedpacketController extends ProjBaseAction {
	@Autowired
	private RedPacketService redPacketService;
	@Autowired
	WxService wxService;

	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {
		List<Map<String, Object>> list =redPacketService.selectRedPacketList(srh, sort_params, page);
		result.put("list", list);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_red_packet redpacket = bs.findById(kid, t_red_packet.class);
		redpacket.setStatus(-100);
		bs.updateObj(redpacket);

		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}
	
	//发放普通红包
	@RequestMapping("/send_redpacket")
	@ResponseBody
	public void send_redpacket(HttpServletRequest req) throws ParseException, UnknownHostException  {
		WxConfig wxConfig = wxService.getShopWxByType("电商");
		String apiclient_cert_p12_path = req.getParameter("path");
		
		Map<String, String> params = new HashMap<>();
		params.put("mch_billno", PKUtil.getOrderNO());
		params.put("re_openid", req.getParameter("openid"));
		params.put("send_name", req.getParameter("send_name"));
		params.put("total_amount", new BigDecimal(100).multiply(new BigDecimal(req.getParameter("total_amount"))).intValue() + ""); //付款金额
		params.put("total_num", new BigDecimal("1").intValue() + "");
		params.put("client_ip", InetAddress.getLocalHost().getHostAddress().toString());
		params.put("act_name", req.getParameter("act_name"));
		params.put("remark", req.getParameter("remark"));
		params.put("wishing", req.getParameter("wishing"));
		Map<String, String> payToUser = new PaymentAPIRedToUser(wxConfig).sendGeneralRedPacket(apiclient_cert_p12_path, params);
		System.err.println(payToUser.get("return_msg"));
		
		t_red_packet redpacket = new t_red_packet();
		redpacket.setMember_id(req.getParameter("kid"));
		redpacket.setMch_billno(params.get("mch_billno"));
		redpacket.setMoney(req.getParameter("total_amount"));
		redpacket.setAct_name(req.getParameter("act_name"));
		redpacket.setWishing(req.getParameter("wishing"));
		redpacket.setNote(payToUser.get("return_msg"));
		redpacket.setType("普通红包");
		bs.saveObj(redpacket);
		
		this.setTipMsg(true, payToUser.get("return_msg"), Tip.Type.success);
	}

	//发放裂变红包
	@RequestMapping("/sendgroupredpack")
	@ResponseBody
	public void sendgroupredpack(HttpServletRequest req) throws ParseException, UnknownHostException  {
		
		
		WxConfig wxConfig = wxService.getShopWxByType("电商");
		String apiclient_cert_p12_path = req.getParameter("path");
		Map<String, String> params = new HashMap<>();
		params.put("mch_billno", PKUtil.getOrderNO());
		params.put("re_openid", req.getParameter("openid"));
		params.put("send_name", req.getParameter("send_name"));
		params.put("total_amount", new BigDecimal(100).multiply(new BigDecimal(req.getParameter("total_amount"))).intValue() + "");
		params.put("total_num", new BigDecimal(req.getParameter("total_num")).intValue() + "");
		params.put("client_ip", InetAddress.getLocalHost().getHostAddress().toString());
		//多加参数
		params.put("amt_type", "ALL_RAND");
		params.put("act_name", req.getParameter("act_name"));
		params.put("remark", req.getParameter("remark"));
		params.put("wishing", req.getParameter("wishing"));
		Map<String, String> payToUser = new PaymentAPIRedToUser(wxConfig).sendFissionRedPacket(apiclient_cert_p12_path, params);
		System.err.println(payToUser.get("return_msg"));
		
		t_red_packet redpacket = new t_red_packet();
		redpacket.setMember_id(req.getParameter("kid"));
		redpacket.setMch_billno(params.get("mch_billno"));
		redpacket.setMoney(req.getParameter("total_amount"));
		redpacket.setAct_name(req.getParameter("act_name"));
		redpacket.setWishing(req.getParameter("wishing"));
		redpacket.setNote(payToUser.get("return_msg"));
		redpacket.setType("裂变红包");
		bs.saveObj(redpacket);
		
		this.setTipMsg(true, payToUser.get("return_msg"), Tip.Type.success);
	}
	
	//发群红包
	@RequestMapping("/sendgroupredpacks")
	@ResponseBody
	public void sendgroupredpacks(String redpmodel_kid,String openids,String path,String total_num) throws ParseException, UnknownHostException  {
		
		t_redpacket_model redpacket_model =bs.findById(redpmodel_kid, t_redpacket_model.class);
		
		WxConfig wxConfig = wxService.getShopWxByType("电商");
		Map<String, String> payToUser = null;
		String[] strArray = null;   
        strArray = openids.split(",");
        
        for(int i=0;i<strArray.length;i++){
        		System.out.println(strArray[i]);
	        	Map<String, String> params = new HashMap<>();
	        	params.put("mch_billno", PKUtil.getOrderNO());
	        	params.put("re_openid", strArray[i]);
	        	params.put("send_name", redpacket_model.getSend_name());
	        	params.put("total_amount", new BigDecimal(100).multiply(new BigDecimal(redpacket_model.getTotal_amount())).intValue() + "");
	        	System.out.println(redpacket_model.getAmt_type());
	        	if("裂变红包".equals(redpacket_model.getAmt_type())){
	        		System.out.println("裂变");
	        		params.put("total_num", new BigDecimal(total_num).intValue() + "");
	        	}else{
	        		params.put("total_num", new BigDecimal("1").intValue() + "");
	        	}
	        	params.put("client_ip", InetAddress.getLocalHost().getHostAddress().toString());
	        	//多加参数
	        	if("裂变红包".equals(redpacket_model.getAmt_type())){
	        		System.out.println("裂变");
	        		params.put("amt_type", "ALL_RAND");
	        	}
	        	params.put("act_name", redpacket_model.getAct_name());
	        	params.put("remark", redpacket_model.getRemark());
	        	params.put("wishing", redpacket_model.getWishing());
	        	
	        	
	        	if("裂变红包".equals(redpacket_model.getAmt_type())){
	        		System.out.println("裂变");
	        		payToUser = new PaymentAPIRedToUser(wxConfig).sendFissionRedPacket(path, params);
	        		System.err.println(payToUser.get("return_msg"));
	        	}else{
	        		payToUser = new PaymentAPIRedToUser(wxConfig).sendGeneralRedPacket(path, params);
	        		System.err.println(payToUser.get("return_msg"));
	        	}
	        	
	        	
	        	t_red_packet redpacket = new t_red_packet();
	        	redpacket.setMember_id(redPacketService.findMemberByKid(strArray[i]).getKid());
	        	redpacket.setMch_billno(params.get("mch_billno"));
	        	redpacket.setMoney(redpacket_model.getTotal_amount());
	        	redpacket.setAct_name(params.get("act_name"));
	        	redpacket.setNote(payToUser.get("return_msg"));
	        	redpacket.setWishing(params.get("wishing"));
	        	if("裂变红包".equals(redpacket_model.getAmt_type())){
	        		redpacket.setType("裂变红包");
	        	}else{
	        		redpacket.setType("普通红包");
	        	}
	        	bs.saveObj(redpacket);
        }
//        payToUser.put("return_msg", "发送失败");
		
		this.setTipMsg(true, payToUser.get("return_msg"), Tip.Type.success);
	}
}
