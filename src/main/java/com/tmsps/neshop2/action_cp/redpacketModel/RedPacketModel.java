package com.tmsps.neshop2.action_cp.redpacketModel;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_redpacket_model;
import com.tmsps.neshop2.service.RedPacketService;
import com.tmsps.neshop2.util.json.JsonTools;

@Controller
@Scope("prototype")
@RequestMapping("/cp/redPacketModel")
public class RedPacketModel extends ProjBaseAction {
	@Autowired
	private RedPacketService redPacketService;

	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {
		List<Map<String, Object>> list = redPacketService.selectRedPacketModelList(srh, sort_params, page);
		result.put("list", list);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_redpacket_model redpacket_model = bs.findById(kid, t_redpacket_model.class);
		redpacket_model.setStatus(-100);
		bs.updateObj(redpacket_model);

		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}

	@RequestMapping("/add")
	@ResponseBody
	public void add(t_redpacket_model redpacket_model) {
		bs.saveObj(redpacket_model);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		t_redpacket_model redpacket_model = bs.findById(kid, t_redpacket_model.class);
		return JsonTools.toJson(redpacket_model);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(t_redpacket_model redpacket_model) {
		t_redpacket_model redpacket_modelDb = bs.findById(redpacket_model.getKid(), t_redpacket_model.class);
		redpacket_modelDb.setAct_name(redpacket_model.getAct_name());  //活动名称
		redpacket_modelDb.setSend_name(redpacket_model.getSend_name());//商户名称
		redpacket_modelDb.setWishing(redpacket_model.getWishing());//祝福语
		redpacket_modelDb.setTotal_amount(redpacket_model.getTotal_amount());//付款金额
		redpacket_modelDb.setAmt_type(redpacket_model.getAmt_type()); //红包类型 
		redpacket_modelDb.setRemark(redpacket_model.getRemark());//备注
		bs.updateObj(redpacket_modelDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

}
