package com.tmsps.neshop2.action_cp.order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4Weixin.api.CustomAPI;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_logistics;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_order;
import com.tmsps.neshop2.model.t_order_detail;
import com.tmsps.neshop2.model.t_order_logistics;
import com.tmsps.neshop2.model.t_shop_wx_config;
import com.tmsps.neshop2.service.ShopOrderService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.KdniaoTrackQueryAPI;
import com.tmsps.neshop2.util.json.JsonTools;

/**
 * 发货
 *
 * @author 柴鑫
 *
 */
@Controller
@Scope("prototype")
@RequestMapping("/cp/order/deliver")
public class OrderLogisticsController extends ProjBaseAction {

	@Autowired
	private ShopOrderService orderService;
	@Autowired
	private WxService wxService;

	// 已付款
	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {
		List<Map<String, Object>> list = orderService.OrderList(srh, sort_params, page);
		result.put("list", list);
	}

	//已收货
	@RequestMapping("/list_Take")
	@ResponseBody
	public void list_Take() {
		List<Map<String, Object>> list = orderService.selectTakeList(srh, sort_params, page);
		result.put("list", list);
	}

	//获取物流公司
	@RequestMapping("/list_logistics")
	@ResponseBody
	public void list() {
		List<Map<String, Object>> list = orderService.selectShopLogisticsParam();
		result.put("list", list);
	}

	//发货中
	@RequestMapping("/list_order")
	@ResponseBody
	public void order_list() {
		List<Map<String, Object>> list = orderService.selectDeliverList(srh, sort_params, page);
		result.put("list", list);
	}

	//根据运单号获取发货信息
	@RequestMapping("/add_form")
	@ResponseBody
	public String add_form(String kid) {
		t_order_logistics orderLogistics = bs.findById(kid, t_order_logistics.class);
		return JsonTools.toJson(orderLogistics);
	}
	
	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_order_logistics orderLogistics = bs.findById(kid, t_order_logistics.class);
		bs.updateObj(orderLogistics);
		this.setTipMsg("订单删除成功！", Tip.Type.success);
	}

	@RequestMapping("/add")
	@ResponseBody
	public void add(t_order_logistics logistics) {
		logistics.setStatus_logistics("发货中");
		logistics.setOrder_id(logistics.getOrder_id());
		logistics.setLogistics_com_code(logistics.getLogistics_com_code());
		logistics.setLogistics_code(logistics.getLogistics_code());
		
		// 修改订单的状态
		t_order order = orderService.orderIdList(logistics.getOrder_id());
//		order.setBest_time(System.currentTimeMillis());
		order.setStatus_order("已发货");
		
		// 修改订单项状态
		t_order_detail order_detail = bs.findById(req.getParameter("kidd"), t_order_detail.class);
		//System.out.println(req.getParameter("kidd"));
		order_detail.setStatus_order("已发货");

		t_logistics tlogistics = orderService.kdCodeList(logistics.getLogistics_com_code());

		// 快递接口
		JSONObject json = kd_api(tlogistics.getKd_code(), logistics.getLogistics_code());

		// 解析后的物流跟踪信息
		JSONArray ja = json.getJSONArray("Traces");
		Iterator<Object> it = ja.iterator();
		List<String> list = new ArrayList<String>();
		while (it.hasNext()) {
			JSONObject ob = (JSONObject) it.next();
			if (ob.getString("AcceptStation") != null) {
				list.add("*" + ob.getString("AcceptStation") + "\n" + "	" + ob.getString("AcceptTime") + "\n");
			}
		}
		String str = Arrays.toString(list.toArray());
		str = str.replace("[", " ").replace(",", "").replace("]", "");
		logistics.setExplain_traces(str);

		String state = json.getString("State");
		if ("2".equals(state) || "2" == state) {
			logistics.setState("在途中");
		} else if ("3".equals(state) || "3" == state) {
			logistics.setState("已签收");
			logistics.setStatus_logistics("已收货");
			order_detail.setStatus_order("已收货");
		} else if ("4".equals(state) || "4" == state) {
			logistics.setState("问题件");
		}

		
		bs.saveObj(logistics);
		order_detail.setLogistics_id(logistics.getKid());;
		bs.updateObj(order_detail);
		logistics.setTraces(json.getString("Traces"));
		
		List<Map<String, Object>> OrderStateList = orderService.fintOrderStateList(order_detail.getOrder_id());
		String swit = "on";
		for(int i =0;i<OrderStateList.size();i++){
			System.out.println(OrderStateList.get(i).get("status_order"));
			if(!"已收货".equals(OrderStateList.get(i).get("status_order"))){
				swit = "off";
			}
		}
		
		if(swit == "on"){
			order.setStatus_order("已收货");
		}
		bs.updateObj(order);
		
		sendTxt(logistics, order, order_detail);
		this.setTipMsg(true, "订单已发货,物流状态:"+logistics.getState(), Tip.Type.success);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(String kid,String kidd) {
		// 根据运单号获取发货信息
		t_order_logistics logistics = bs.findById(kid, t_order_logistics.class);

		// 根据订单号查询订单
		t_order order = orderService.orderIdList(logistics.getOrder_id());
		t_logistics tlogistics = orderService.kdCodeList(logistics.getLogistics_com_code());
		t_order_detail order_detail = bs.findById(kidd, t_order_detail.class);
		//System.out.println(order_detail.getKid());

		// 快递接口
		JSONObject json = kd_api(tlogistics.getKd_code(), logistics.getLogistics_code());

		// 更新快递跟踪信息
		JSONArray ja = json.getJSONArray("Traces");
		Iterator<Object> it = ja.iterator();
		List<String> list = new ArrayList<String>();
		while (it.hasNext()) {
			JSONObject ob = (JSONObject) it.next();
			if (ob.getString("AcceptStation") != null) {
				list.add("*" + ob.getString("AcceptStation") + "\n" + "	" + ob.getString("AcceptTime") + "\n");
			}
		}
		String str = Arrays.toString(list.toArray());
		str = str.replace("[", " ").replace(",", "").replace("]", "");
		
		t_order_logistics logisticsDb = bs.findById(logistics.getKid(), t_order_logistics.class);
		logisticsDb.setLogistics_com_code(logistics.getLogistics_com_code());
		logisticsDb.setLogistics_code(logistics.getLogistics_code());
		logisticsDb.setExplain_traces(str);
		logisticsDb.setTraces(json.getString("Traces"));

		String state = json.getString("State");
		if ("2".equals(state) || "2" == state) {
			logisticsDb.setState("在途中");
		} else if ("3".equals(state) || "3" == state) {
			logisticsDb.setState("已签收");
			logisticsDb.setStatus_logistics("已收货");
			order_detail.setStatus_order("已收货");
			
		} else if ("4".equals(state) || "4" == state) {
			logisticsDb.setState("问题件");
		}
		
		List<Map<String, Object>> OrderStateList = orderService.fintOrderStateList(order_detail.getOrder_id());

		bs.updateObj(order_detail);
		bs.updateObj(logisticsDb);

		String swit = "on";
		for(int i =0;i<OrderStateList.size();i++){
			System.out.println(OrderStateList.get(i).get("status_order"));
			if(!"已收货".equals(OrderStateList.get(i).get("status_order"))){
				swit = "off";
			}
		}
		
		if(swit == "on"){
			order.setStatus_order("已收货");
		}
		bs.updateObj(order);
		this.setTipMsg(true, "更新数据完成", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		t_order_logistics orderLogistics = bs.findById(kid, t_order_logistics.class);
		result.put("orderLogistics", orderLogistics);
		return JsonTools.toJson(orderLogistics);
	}

	@RequestMapping("/edit_order")
	@ResponseBody
	public void edit_order(t_order_logistics logistics) {
		t_order_logistics logisticsDb = bs.findById(logistics.getKid(), t_order_logistics.class);

		logisticsDb.setLogistics_com_code(logistics.getLogistics_com_code());
		logisticsDb.setLogistics_code(logistics.getLogistics_code());

		t_logistics tlogistics = orderService.kdCodeList(logistics.getLogistics_com_code());
		t_order order = orderService.orderIdList(logistics.getOrder_id());
		t_order_detail order_detail = bs.findById(req.getParameter("kidd"), t_order_detail.class);
		//System.out.println(order_detail.getKid());
		
		// 快递接口
		JSONObject json = kd_api(tlogistics.getKd_code(), logistics.getLogistics_code());

		// 解析后的物流跟踪信息
		JSONArray ja = json.getJSONArray("Traces");
		Iterator<Object> it = ja.iterator();
		List<String> list = new ArrayList<String>();
		while (it.hasNext()) {
			JSONObject ob = (JSONObject) it.next();
			if (ob.getString("AcceptStation") != null) {
				list.add("*" + ob.getString("AcceptStation") + "\n" + "	" + ob.getString("AcceptTime") + "\n");
			}
		}
		String str = Arrays.toString(list.toArray());
		str = str.replace("[", " ").replace(",", "").replace("]", "");
		logisticsDb.setExplain_traces(str);

		String state = json.getString("State");
		if ("2".equals(state) || "2" == state) {
			logisticsDb.setState("在途中");
		} else if ("3".equals(state) || "3" == state) {
			logisticsDb.setState("已签收");
			logisticsDb.setStatus_logistics("已收货");
			order_detail.setStatus_order("已收货");
		} else if ("4".equals(state) || "4" == state) {
			logisticsDb.setState("问题件");
		}

		logisticsDb.setTraces(json.getString("Traces"));
		bs.updateObj(order_detail);
		bs.updateObj(logisticsDb);

		List<Map<String, Object>> OrderStateList = orderService.fintOrderStateList(order_detail.getOrder_id());
		String swit = "on";
		for(int i =0;i<OrderStateList.size();i++){
			System.out.println(OrderStateList.get(i).get("status_order"));
			if(!"已收货".equals(OrderStateList.get(i).get("status_order"))){
				swit = "off";
			}
		}
		
		if(swit == "on"){
			order.setStatus_order("已收货");
		}
		bs.updateObj(order);
		this.setTipMsg(true, "物流状态:"+logisticsDb.getState(), Tip.Type.success);
	}

	/**
	 * 快递接口
	 * 
	 * @param kd_code
	 *            快递公司编码
	 * @param logistics_code
	 *            物流单号
	 * @return json
	 */
	public JSONObject kd_api(String kd_code, String logistics_code) {
		KdniaoTrackQueryAPI api = new KdniaoTrackQueryAPI();
		JSONObject json = null;
		String result = null;
		try {
			result = api.getOrderTracesByJson(kd_code, logistics_code.trim());
			json = JsonTools.jsonStrToJsonObject(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}

	private void sendTxt(t_order_logistics logistics, t_order order, t_order_detail order_detail) {
		t_member fxmember = bs.findById(order.getMember_id(), t_member.class);
		t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("电商");
		new Thread(new Runnable() {
			public void run() {
				System.out.println("＝＝＝＝＝＝＝＝＝＝电商客服消息＝＝＝＝＝＝＝＝＝＝");
				String content = "您有一个商品@status,<a href='http://www.sxxlkj.com/neshop2/wx/kuaidi.htm?logisticsId=@logisticsId&orderDetailId=@orderDetailId'>查看物流</a>";
				content = content.replace("@status", order.getStatus_order());
				content = content.replace("@orderDetailId", order_detail.getKid());
				content = content.replace("@logisticsId", logistics.getKid());
				new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),content);
			}
		}).start();
	}

}
