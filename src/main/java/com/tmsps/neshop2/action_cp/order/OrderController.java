package com.tmsps.neshop2.action_cp.order;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_order;
import com.tmsps.neshop2.model.t_order_return;
import com.tmsps.neshop2.service.OrderService;
import com.tmsps.neshop2.util.doc.excel.ExportDataToExcel;
import com.tmsps.neshop2.util.json.JsonTools;

@Controller
@Scope("prototype")
@RequestMapping("/cp/order")
public class OrderController extends ProjBaseAction {

	@Autowired
	private OrderService orderService;

	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {
		List<Map<String, Object>> list = orderService.selectOrderList(srh, sort_params, page);
		result.put("list", list);
	}

	/*
	 * 退款开始
	 */
	// 退货查询
	@RequestMapping("/return_product")
	@ResponseBody
	public void return_product() {
		List<Map<String, Object>> list = orderService.return_selectOrderList(srh, sort_params, page);
		result.put("list", list);
	}

	// 退货成功return_product_success
	@RequestMapping("/return_product_success")
	@ResponseBody
	public void return_product_success() {
		List<Map<String, Object>> list = orderService.return_selectOrderList_success(srh, sort_params, page);
		result.put("list", list);
	}

	@RequestMapping("/return_product_money")
	@ResponseBody
	public void return_product_money(String kid) {
		// 正在退货的kid
		t_order_return order_return = bs.findById(kid, t_order_return.class);
		// 正在退货的order_no
		String order_no = order_return.getOrder_no();
		// 得到订单表
		t_order t_order = orderService.findtOrder(order_no);
		// 如果长在退货的和订单比较，若想等说明没退货
		order_return.setStatus(-100);
		if ("是".equals(t_order.getIs_pay())) {
			order_return.setOrder_status("审核完成");
			order_return.setMoney_status("已退货");
			// order_return.setMessage("未支付不能退货");
		} else {
			order_return.setOrder_status("拒绝退货");
			order_return.setMessage("未支付不能退货");
		}

		bs.updateObj(order_return);
		this.setTipMsg(true, "修改成功!", Tip.Type.success);
	}

	// 退款订单return_money
	@RequestMapping("/return_money")
	@ResponseBody
	public void return_money() {
		List<Map<String, Object>> list = orderService.return_selectOrderMoneyList(srh, sort_params, page);
		result.put("list", list);
	}

	/*
	 * 退款结束
	 */

	// 根据订单号查询订单项
	@RequestMapping("/list_detail")
	@ResponseBody
	public void list_detail(String order_id) {
		List<Map<String, Object>> list = orderService.ListDetail(order_id);
		result.put("list", list);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		System.out.println(kid);
		t_order order = bs.findById(kid, t_order.class);
		System.out.println(order);
		result.put("order", order);
		String memberId = order.getMember_id();
		// String orderId =order.getOrder_no();

		t_member member = bs.findById(memberId, t_member.class);
		result.put("member", member);

		// t_order_logistics orderLogistics = bs.findById(orderId,
		// t_order_logistics.class);
		// result.put("orderLogistics", orderLogistics);
		// String code =orderLogistics.getLogistics_com_code();
		// t_logistics logistics =bs.findById(code, t_logistics.class);
		// result.put("logistics", logistics);

		return JsonTools.toJson(result);
	}

	@RequestMapping("/order_excel")
	@ResponseBody
	public String order_excel(HttpServletRequest request, HttpServletResponse response) {
		String jsonArrayData = request.getParameter("datagridjsondata");

		String filename = null;
		try {
			filename = ExportDataToExcel.createParentAccountWorkbook(request, "数据导出页", jsonArrayData);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			response.getWriter().write(filename);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return JsonTools.toJson(filename);
	}

	
	
	
}
