package com.tmsps.neshop2.action_cp.retail_store;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_retail_order;
import com.tmsps.neshop2.service.OrderService;

/**
 * 分销商业绩
 * @author Chaixin
 */
@Controller
@Scope("prototype")
@RequestMapping("/cp/retail_order")
public class RetailOrderController extends ProjBaseAction {
	@Autowired
	OrderService orderService;
	
	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {
		List<Map<String, Object>> list = orderService.selectMemberOrderList(srh, sort_params, page);
		result.put("list", list);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_retail_order retail_order = bs.findById(kid, t_retail_order.class);
		retail_order.setStatus(-100);
		bs.updateObj(retail_order);

		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}
}
