package com.tmsps.neshop2.action_cp.retail_store;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.ne4Weixin.api.CustomAPI;
import com.tmsps.ne4Weixin.beans.QrCode;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.ne4spring.orm.ORMUtil;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_retailStore;
import com.tmsps.neshop2.model.t_shop_wx_config;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.service.OrderService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.WxCode;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.util.tree.TreeTools;

/**
 * 分销商列表
 * @author Chaixin
 */
@Controller
@Scope("prototype")
@RequestMapping("/cp/retail_store")
public class RetailStoreController extends ProjBaseAction {

	@Autowired
	private MemberService memberService;
	@Autowired
	private WxService wxService;
	@Autowired
	OrderService orderService;
	
	@RequestMapping("/memder_fx")
	@ResponseBody
	public String memder_fx() {
		List<Map<String, Object>> list = memberService.selectMemderFxList();
		return TreeTools.retailListToTree(list, false);
	}
	
	@RequestMapping("/member_order_list")
	@ResponseBody
	public void member_order_list(String retail_id) {
		List<Map<String, Object>> list = orderService.selectMyMemberOrderListt(retail_id);
		result.put("list", list);
	}

	@RequestMapping("/member_list")
	@ResponseBody
	public void member_list(String retail_id) {
		List<Map<String, Object>> list = memberService.my_cousumer_list(retail_id);
		result.put("list", list);
	}

	@RequestMapping("/add")
	@ResponseBody
	public String add(t_retailStore retailStore, String up_code) {
		List<Map<String, Object>> memderFxlist = memberService.selectMemderFxList();
		for(int i=0;i < memderFxlist.size();i++){
			if(retailStore.getMember_id() == memderFxlist.get(i).get("member_id") || retailStore.getMember_id().equals(memderFxlist.get(i).get("member_id"))){
				return (String) memderFxlist.get(i).get("name");
			}
		}
		
		retailStore.setCode(up_code + retailStore.getCode());
		bs.saveObj(retailStore);
		
		QrCode qrCode = WxCode.getQrCode(wxService.getShopWxByType("电商") , WxCode.Retail + retailStore.getKid());
		retailStore.setQr_code(qrCode.getTicket());
		bs.updateObj(retailStore);
		
		t_member fxmember = bs.findById(retailStore.getMember_id(), t_member.class);
		t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("分销");
		new Thread(new Runnable() {
			public void run() {
				System.out.println("＝＝＝＝＝＝＝＝＝＝电商客服消息＝＝＝＝＝＝＝＝＝＝");
				new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),retailStore.getName()+"\n您已成为《悦龙斋科技》分销商! 	请点击“我的客户”下拉获取分销二维码.");
			}
		}).start();


		return "success";
	}
	
	@RequestMapping("/cut")
	@ResponseBody
	public void cut(HttpServletRequest req) throws ParseException {
		String member_id = req.getParameter("memberid");
		String code = req.getParameter("code");
		String name = req.getParameter("name");
		
		List<Map<String, Object>> memderFxlist = memberService.selectMemderFxList();
		for(int i=0;i < memderFxlist.size();i++){
			if(member_id == memderFxlist.get(i).get("member_id") || member_id.equals(memderFxlist.get(i).get("member_id"))){
				t_retailStore retailStoreDb = bs.findById(memderFxlist.get(i).get("kid"), t_retailStore.class);
				logger.info(retailStoreDb.getName());
				retailStoreDb.setMember_id(null);
				bs.updateObj(retailStoreDb);
			}
		}
		
		t_retailStore retailStore = new t_retailStore();
		retailStore.setMember_id(member_id);
		retailStore.setName(name);
		retailStore.setCode(code);
		bs.saveObj(retailStore);
		
		QrCode qrCode = WxCode.getQrCode(wxService.getShopWxByType("电商") , WxCode.Retail + retailStore.getKid());
		retailStore.setQr_code(qrCode.getTicket());
		bs.updateObj(retailStore);
		
		t_member fxmember = bs.findById(retailStore.getMember_id(), t_member.class);
		t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("分销");
		new Thread(new Runnable() {
			public void run() {
				logger.info("＝＝＝＝＝＝＝＝＝＝电商客服消息＝＝＝＝＝＝＝＝＝＝");
				new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),retailStore.getName()+"\n您已成为《悦龙斋科技》分销商! 	请点击“我的客户”下拉获取分销二维码.");
			}
		}).start();

		this.setTipMsg(true, "改绑成功!", Tip.Type.success);
	}
	
	@RequestMapping("/edit_cut")
	@ResponseBody
	public void edit_cut(HttpServletRequest req) throws ParseException {
		String member_id = req.getParameter("memberid");
		String name = req.getParameter("name");
		String kid = req.getParameter("kid");
		
		List<Map<String, Object>> memderFxlist = memberService.selectMemderFxList();
		for(int i=0;i < memderFxlist.size();i++){
			if(member_id == memderFxlist.get(i).get("member_id") || member_id.equals(memderFxlist.get(i).get("member_id"))){
				t_retailStore retailStoreDb = bs.findById(memderFxlist.get(i).get("kid"), t_retailStore.class);
				logger.info(retailStoreDb.getName());
				retailStoreDb.setMember_id(null);
				bs.updateObj(retailStoreDb);
			}
		}
		
		t_retailStore retailStore = bs.findById(kid, t_retailStore.class);
		System.out.println(retailStore.getCode());
		retailStore.setMember_id(member_id);
		retailStore.setName(name);
		QrCode qrCode = WxCode.getQrCode(wxService.getShopWxByType("电商") , WxCode.Retail + retailStore.getKid());
		retailStore.setQr_code(qrCode.getTicket());
		bs.updateObj(retailStore);
		
		t_member fxmember = bs.findById(retailStore.getMember_id(), t_member.class);
		t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("分销");
		new Thread(new Runnable() {
			public void run() {
				logger.info("＝＝＝＝＝＝＝＝＝＝电商客服消息＝＝＝＝＝＝＝＝＝＝");
				new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),retailStore.getName()+"\n您已成为《悦龙斋科技》分销商! 	请点击“我的客户”下拉获取分销二维码.");
			}
		}).start();

		this.setTipMsg(true, "改绑成功!", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		t_retailStore retailStore = (t_retailStore) bs.findById(kid, t_retailStore.class);
		System.out.println(retailStore.getKid());
		return JsonTools.toJson(retailStore);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public String edit(t_retailStore retailStore) {
		List<Map<String, Object>> memderFxlist = memberService.selectMemderFxList();
		for(int i=0;i < memderFxlist.size();i++){
			if(retailStore.getMember_id() == memderFxlist.get(i).get("member_id") || retailStore.getMember_id().equals(memderFxlist.get(i).get("member_id"))){
				return (String) memderFxlist.get(i).get("name");
			}
		}
		
		t_retailStore retailStoreDb = (t_retailStore) bs.findById(retailStore.getKid(), t_retailStore.class);
		retailStoreDb.setCode(retailStore.getCode());
		retailStoreDb.setName(retailStore.getName());
		retailStoreDb.setMember_id(retailStore.getMember_id());
		QrCode qrCode = WxCode.getQrCode(wxService.getShopWxByType("电商") , WxCode.Retail + retailStore.getKid());
		retailStoreDb.setQr_code(qrCode.getTicket());
		bs.updateObj(retailStoreDb);
		
		t_member fxmember = bs.findById(retailStore.getMember_id(), t_member.class);
		t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("分销");
		new Thread(new Runnable() {
			public void run() {
				System.out.println("＝＝＝＝＝＝＝＝＝＝分销客服消息＝＝＝＝＝＝＝＝＝＝");
				new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),retailStore.getName()+"\n您已成为《悦龙斋科技》分销商! 	请点击“我的客户”下拉获取分销二维码.");
			}
		}).start();

		return "success";
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_retailStore retailStore = bs.findById(kid, t_retailStore.class);
		retailStore.setStatus(-100);
		bs.updateObj(retailStore);

		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}
	
	@RequestMapping("/get_qrcode")
	@ResponseBody
	public String get_qrcode(String kid) {
		t_retailStore retailStore = bs.findById(kid, t_retailStore.class);
		return JsonTools.toJson(retailStore.getQr_code());
	}
	
	@RequestMapping("/sync_qrcode")
	@ResponseBody
	public void sync_qrcode() {
		WxConfig config = wxService.getShopWxByType("电商");
		List<Map<String, Object>> list = memberService.selectMemderFxList();
		for (Map<String, Object> tableMap : list) {
			t_retailStore retailStore = ORMUtil.fillMapToBean(t_retailStore.class, tableMap);
			QrCode qrCode = WxCode.getQrCode(config, WxCode.Retail + retailStore.getKid());
			retailStore.setQr_code(qrCode.getTicket());
			bs.updateObj(retailStore);
		}

		this.setTipMsg(true, "已刷新分销二维码!", Tip.Type.success);
	}
	
	@RequestMapping("/uplv")
	@ResponseBody
	public void uplv(t_retailStore retailStore) {
		t_retailStore retailStoreDb = bs.findById(retailStore.getKid(), t_retailStore.class);
		
		retailStoreDb.setCode(retailStore.getCode());
		bs.updateObj(retailStoreDb);
		
		t_member fxmember = bs.findById(retailStoreDb.getMember_id(), t_member.class);
		t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("分销");
		new Thread(new Runnable() {
			public void run() {
				System.out.println("＝＝＝＝＝＝＝＝＝＝分销客服消息＝＝＝＝＝＝＝＝＝＝");
				new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),retailStoreDb.getName()+"\n您已升级,详情请点击《我的分销商》。如需知佣金详情等请向客服索要。");
			}
		}).start();

		this.setTipMsg(true, "升级成功!", Tip.Type.success);
	}
	
	@RequestMapping("/auth_unique")
	@ResponseBody
	public String auth_unique(String code) {
		t_retailStore retailStore = null;

//		if (ChkTools.isNull(kid)) {
			retailStore = memberService.findRetailStoreByCode(code);
		/*} else {
			retailStore = memberService.findRetailStoreByCode(kid, code);
		}*/

		if (retailStore == null) {
			result.put("success", "true");
		} else {
			result.put("fail", "true");
		}

		return JsonTools.toJson(result);
	}
}
