package com.tmsps.neshop2.action_cp.retail_store;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_retail_get_money_list;
import com.tmsps.neshop2.service.RetailService;

/**
 * 分销商佣金体现记录
 * @author 闫师傅
 */
@Controller
@Scope("prototype")
@RequestMapping("/cp/retail/get_moneys")
public class RetailGetMoneysController extends ProjBaseAction {

	@Autowired
	RetailService retailService;
	
	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {
		List<Map<String, Object>> list = retailService.selectUserGetMoneyList(srh, sort_params, page);
		result.put("list", list);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_retail_get_money_list retail_get_money_list = bs.findById(kid, t_retail_get_money_list.class);
		retail_get_money_list.setStatus(-100);
		bs.updateObj(retail_get_money_list);
		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}

}
