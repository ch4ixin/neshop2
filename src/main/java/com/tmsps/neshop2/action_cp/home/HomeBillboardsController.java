package com.tmsps.neshop2.action_cp.home;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_home_billboards;
import com.tmsps.neshop2.service.HomeBillboardService;
import com.tmsps.neshop2.util.json.JsonTools;

/**
 * 首页广告
 */
@Controller
@Scope("prototype")
@RequestMapping("/cp/billboard")
public class HomeBillboardsController extends ProjBaseAction{
	
	@Autowired
	private HomeBillboardService homeBillboardService;

	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data() {
		List<Map<String, Object>> list = homeBillboardService.selectBillboards(srh, sort_params, page);
		result.put("list", list);
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public void add(t_home_billboards homebillboards) {
		bs.saveObj(homebillboards);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_home_billboards homebillboards = bs.findById(kid, t_home_billboards.class);
		homebillboards.setStatus(-100);
		bs.updateObj(homebillboards);
		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		t_home_billboards homebillboards = bs.findById(kid, t_home_billboards.class);
		return JsonTools.toJson(homebillboards);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(t_home_billboards home_billboards) {
		t_home_billboards home_billboardsDb = bs.findById(home_billboards.getKid(), t_home_billboards.class);
		home_billboardsDb.setName(home_billboards.getName());
		home_billboardsDb.setSite(home_billboards.getSite());
		home_billboardsDb.setMain_file_id(home_billboards.getMain_file_id());
		home_billboardsDb.setProduct_id(home_billboards.getProduct_id());
		bs.updateObj(home_billboardsDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}
	
}
