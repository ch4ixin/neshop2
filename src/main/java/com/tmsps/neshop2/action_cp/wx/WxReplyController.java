package com.tmsps.neshop2.action_cp.wx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_shop_wx_reply_join;
import com.tmsps.neshop2.service.WxService;

@Controller
@Scope("prototype")
@RequestMapping("/cp/wx/reply")
public class WxReplyController extends ProjBaseAction {

	@Autowired
	WxService wxService;

	@RequestMapping("/edit_form")
	@ResponseBody
	public void edit_form(String kid) {
		t_shop_wx_reply_join reply = wxService.getReply();

		result.put("reply", reply);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(t_shop_wx_reply_join reply) {
		t_shop_wx_reply_join replyDb = wxService.getReply();
		replyDb.setText(reply.getText());
		bs.updateObj(replyDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/sync")
	@ResponseBody
	public void sync() {

		this.setTipMsg(true, "同步成功!", Tip.Type.success);
	}

}
