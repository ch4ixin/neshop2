package com.tmsps.neshop2.action_cp.wx;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.ne4Weixin.api.MenuAPI;
import com.tmsps.ne4Weixin.beans.BaseBean;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_shop_wx_auth;
import com.tmsps.neshop2.service.WxAuthService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.util.tree.TreeTools;
import com.tmsps.neshop2.util.tree.WxTreeTools;

/**
 * 微信菜单
 * 
 * @author Administrator
 *
 */
@Controller("wxAuth")
@Scope("prototype")
@RequestMapping("/cp/wx/auth")
public class WxAuthController extends ProjBaseAction {

	@Autowired
	private WxAuthService authService;
	@Autowired
	private WxService wxService;

	@RequestMapping("/auth_data")
	@ResponseBody
	public String auth_data() {
		List<Map<String, Object>> list = authService.selectAuthList("电商");
		return TreeTools.turnListToTree(list);
	}
	
	@RequestMapping("/auth_data_f")
	@ResponseBody
	public String auth_data_f() {
		List<Map<String, Object>> list = authService.selectAuthList("分销");
		return TreeTools.turnListToTree(list);
	}

	@RequestMapping("/add")
	@ResponseBody
	public void add(t_shop_wx_auth auth, String up_code) {
		auth.setCode(up_code + auth.getCode());
		auth.setShop_id("电商");
		bs.saveObj(auth);

		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}
	
	@RequestMapping("/add_f")
	@ResponseBody
	public void add_f(t_shop_wx_auth auth, String up_code) {
		auth.setCode(up_code + auth.getCode());
		auth.setShop_id("分销");
		bs.saveObj(auth);

		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		t_shop_wx_auth auth = bs.findById(kid, t_shop_wx_auth.class);
		return JsonTools.toJson(auth);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(t_shop_wx_auth auth) {
		t_shop_wx_auth authDb = bs.findById(auth.getKid(), t_shop_wx_auth.class);
		authDb.setName(auth.getName());
		authDb.setCode(auth.getCode());
		authDb.setUrl(auth.getUrl());
		bs.updateObj(authDb);

		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		t_shop_wx_auth auth = bs.findById(kid, t_shop_wx_auth.class);
		boolean isSubNode = authService.isHaveSubAuth(auth.getCode());
		if (isSubNode) {
			this.setTipMsg(true, "删除失败!存在子节点!", Tip.Type.success);
			return;
		}
		
		bs.deleteObjById(kid, t_shop_wx_auth.class);

		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}
	
	@RequestMapping("/sync")
	@ResponseBody
	public void sync() {
		WxConfig config = wxService.getShopWxByType("电商");

		MenuAPI api = new MenuAPI(config);
		List<Map<String, Object>> list = authService.selectAuthList("电商");
		String json = WxTreeTools.turnListToTree(list);
		logger.info(json);
		BaseBean createMenu = api.createMenu(json);

		if(createMenu.getErrcode()==0){
			this.setTipMsg(true, "同步成功!", Tip.Type.success);
		}else{
			logger.error(createMenu.toJSON());
			this.setTipMsg(true, "同步失败!"+createMenu.getErrmsg(), Tip.Type.error);
		}
	
	}
	
	@RequestMapping("/sync_f")
	@ResponseBody
	public void sync_f() {
		WxConfig config = wxService.getShopWxByType("分销");

		MenuAPI api = new MenuAPI(config);
		List<Map<String, Object>> list = authService.selectAuthList("分销");
		String json = WxTreeTools.turnListToTree(list);
		logger.info(json);
		BaseBean createMenu = api.createMenu(json);

		if(createMenu.getErrcode()==0){
			this.setTipMsg(true, "同步成功!", Tip.Type.success);
		}else{
			logger.error(createMenu.toJSON());
			this.setTipMsg(true, "同步失败!"+createMenu.getErrmsg(), Tip.Type.error);
		}
	
	}
	
	@RequestMapping("/check")
	@ResponseBody
	public String check(){
		WxConfig config = wxService.getShopWxByType("电商");

		MenuAPI api = new MenuAPI(config);
		String json=api.getMenu();
		
		System.out.println(json);
		return JsonTools.toJson(json);
		
	}

	@RequestMapping("/check_f")
	@ResponseBody
	public String check_f(){
		WxConfig config = wxService.getShopWxByType("分销");

		MenuAPI api = new MenuAPI(config);
		String json=api.getMenu();
		
		System.out.println(json);
		return JsonTools.toJson(json);
		
	}
}
