package com.tmsps.neshop2.action_cp.wx;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.ne4Weixin.api.UserAPI;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_shop_wx_config;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.service.SettingService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.qrcode.QRTools;

@Controller
@Scope("prototype")
@RequestMapping("/cp/wx/config")
public class WxConfigController extends ProjBaseAction {

	@Autowired
	WxService wxService;
	@Autowired
	MemberService memberService;
	@Autowired
	public SettingService settingService;

	@RequestMapping("/edit_form")
	@ResponseBody
	public void edit_form(String kid) {
		t_shop_wx_config wx = wxService.getShopWxConfig("电商");
		String url = req.getScheme() + "://" + req.getServerName() + "/neshop2/weixin.htm?wxid=" + wx.getKid();
		result.put("url", url);
		result.put("wx", wx);
	}
	
	@RequestMapping("/edit_form_f")
	@ResponseBody
	public void edit_form_f(String kid) {
		t_shop_wx_config wx = wxService.getShopWxConfig("分销");
		String url = req.getScheme() + "://" + req.getServerName() + "/neshop2/weixin.htm?wxid=" + wx.getKid();
		result.put("url", url);
		result.put("wx", wx);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(t_shop_wx_config wx) {
		t_shop_wx_config wxDb = wxService.getShopWxConfig("电商");
		//wxDb.setDomain(SessionTools.getCurrentShopAdmin().getMobile() + ".ss.vososo.com");
		wxDb.setAes_key(wx.getAes_key());
		wxDb.setAppid(wx.getAppid());
		wxDb.setAppsecret(wx.getAppsecret());
		wxDb.setOriginal_id(wx.getOriginal_id());
		wxDb.setPartner_key(wx.getPartner_key());
		wxDb.setPay_partner(wx.getPay_partner());
		wxDb.setToken(wx.getToken());
//		wxDb.setCmb_mch_id(wx.getCmb_mch_id());
//		wxDb.setCmb_is_invoke(wx.getCmb_is_invoke());
//		wxDb.setCmb_mch_key(wx.getCmb_mch_key());
		bs.updateObj(wxDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}
	
	@RequestMapping("/edit_f")
	@ResponseBody
	public void edit_f(t_shop_wx_config wx) {
		t_shop_wx_config wxDb = wxService.getShopWxConfig("分销");
		//wxDb.setDomain(SessionTools.getCurrentShopAdmin().getMobile() + ".ss.vososo.com");
		wxDb.setAes_key(wx.getAes_key());
		wxDb.setAppid(wx.getAppid());
		wxDb.setAppsecret(wx.getAppsecret());
		wxDb.setOriginal_id(wx.getOriginal_id());
		wxDb.setPartner_key(wx.getPartner_key());
		wxDb.setPay_partner(wx.getPay_partner());
		wxDb.setToken(wx.getToken());
//		wxDb.setCmb_mch_id(wx.getCmb_mch_id());
//		wxDb.setCmb_is_invoke(wx.getCmb_is_invoke());
//		wxDb.setCmb_mch_key(wx.getCmb_mch_key());
		bs.updateObj(wxDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/sync_user")
	@ResponseBody
	public void sync_user() {
		WxConfig wxConfig = wxService.getShopWxByType("电商");
		UserAPI userAPI = new UserAPI(wxConfig);
		List<String> userList = userAPI.getAllUserList();
		memberService.syncAllUser("电商", userAPI, userList);

		this.setTipMsg(true, "同步成功!", Tip.Type.success);
	}

	@RequestMapping("/sync_user_f")
	@ResponseBody
	public void sync_user_f() {
		WxConfig wxConfig = wxService.getShopWxByType("分销");
		UserAPI userAPI = new UserAPI(wxConfig);
		List<String> userList = userAPI.getAllUserList();
		memberService.syncAllUser("分销", userAPI, userList);

		this.setTipMsg(true, "同步成功!", Tip.Type.success);
	}
	
	// 生成微信二维码
	@RequestMapping("/sync_ercode")
	@ResponseBody
	public String sync_ercode() {
		t_shop_wx_config config = wxService.getShopWxConfig("电商");

		String content = "http://" + config.getDomain() + "/wx/pay/pay_for.htm";

		String DATA_PATH = settingService.getVal("DATA_PATH");
		String imgPath = DATA_PATH + File.separator + "qrcode";
		File file = new File(imgPath);
		if (!file.exists() && !file.isDirectory()) {
			file.mkdir();
		}
		imgPath = imgPath + File.separator + config.getKid() + "_wx.png";
		t_shop_wx_config wxConfig = bs.findById(config.getKid(), t_shop_wx_config.class);
		wxConfig.setEr_code(config.getKid() + "_wx");
		bs.updateObj(wxConfig);

		QRTools.getQRCode(content, imgPath);
		//SessionTools.put(SessionTools.WXCONFIG, wxConfig);

		this.setTipMsg(true, "已刷新微信二维码!", Tip.Type.success);
		return wxConfig.getEr_code();
	}

	// 生成微信二维码
	@RequestMapping("/sync_ercode_f")
	@ResponseBody
	public String sync_ercode_f() {
		t_shop_wx_config config = wxService.getShopWxConfig("分销");

		String content = "http://" + config.getDomain() + "/wx/pay/pay_for.htm";

		String DATA_PATH = settingService.getVal("DATA_PATH");
		String imgPath = DATA_PATH + File.separator + "qrcode";
		File file = new File(imgPath);
		if (!file.exists() && !file.isDirectory()) {
			file.mkdir();
		}
		imgPath = imgPath + File.separator + config.getKid() + "_wx.png";
		t_shop_wx_config wxConfig = bs.findById(config.getKid(), t_shop_wx_config.class);
		wxConfig.setEr_code(config.getKid() + "_wx");
		bs.updateObj(wxConfig);

		QRTools.getQRCode(content, imgPath);
		//SessionTools.put(SessionTools.WXCONFIG, wxConfig);

		this.setTipMsg(true, "已刷新微信二维码!", Tip.Type.success);
		return wxConfig.getEr_code();
	}
}
