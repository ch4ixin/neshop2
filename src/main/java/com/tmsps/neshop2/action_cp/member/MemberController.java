package com.tmsps.neshop2.action_cp.member;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.ne4Weixin.api.CustomAPI;
import com.tmsps.ne4spring.utils.MD5Util;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_shop_wx_config;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.json.JsonTools;

@Controller
@Scope("prototype")
@RequestMapping("/cp/member")
public class MemberController extends ProjBaseAction {
	@Autowired
	private MemberService memberService;

	@Autowired
	private WxService wxService;

	@RequestMapping("/list_data")
	@ResponseBody
	public void list_data(String member_id) {
		// System.err.println("member_id->"+member_id);
		List<Map<String, Object>> list = memberService.selectMemderList(srh, sort_params, page , "电商");

		result.put("list", list);
	}
	
	@RequestMapping("/list_fenxiao_data")
	@ResponseBody
	public void list_fenxiao_data(String member_id) {
		// System.err.println("member_id->"+member_id);
		List<Map<String, Object>> list = memberService.selectMemderList(srh, sort_params, page , "分销");

		result.put("list", list);
	}
	
	@RequestMapping("/fx_member_data")
	@ResponseBody
	public void fx_member_data(String member_id) {
		// System.err.println("member_id->"+member_id);
		List<Map<String, Object>> list = memberService.fxMemderList(srh, sort_params, page , "分销");

		result.put("list", list);
	}

	@RequestMapping("/del")
	@ResponseBody
	public void del(String kid) {
		// t_member admin = bs.findById(kid, t_member.class);
		t_member member = bs.findById(kid, t_member.class);
		member.setStatus(-100);
		bs.updateObj(member);

		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}

	@RequestMapping("/add")
	@ResponseBody
	public void add(t_member member) {
		member.setPwd(MD5Util.MD5(member.getPwd()));
		bs.saveObj(member);

		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}

	@RequestMapping("/edit_form")
	@ResponseBody
	public String edit_form(String kid) {
		// System.out.println(kid);
		t_member set = bs.findById(kid, t_member.class);

		result.put("set", set);
		return JsonTools.toJson(set);
	}

	@RequestMapping("/edit")
	@ResponseBody
	public void edit(t_member member) {
		t_member setDb = bs.findById(member.getKid(), t_member.class);
		setDb.setNote(member.getNote());
		setDb.setLevel_id(member.getLevel_id());

		bs.updateObj(setDb);
		this.setTipMsg(true, "保存成功!", Tip.Type.success);
	}
	//给用户发信息
	@RequestMapping("/send_msg")
	@ResponseBody
	public void send_msg(String member_id,String content) {
		t_shop_wx_config wxConfig = wxService.getShopWxConfig("电商");
		t_member member = bs.findById(member_id, t_member.class);
		
		new Thread(new Runnable() {
			public void run() {
				System.out.println("＝＝＝＝＝＝＝＝＝＝电商客服消息＝＝＝＝＝＝＝＝＝＝");
				new CustomAPI(wxService.turnTToWx(wxConfig)).sendTextMsg(member.getOpenid(),content);
			}
		}).start();
		
		this.setTipMsg(true, "发送成功!", Tip.Type.success);
	}
	//给分销发信息
	@RequestMapping("/fx_send_msg")
	@ResponseBody
	public void fx_send_msg(String member_id,String content) {
		t_shop_wx_config wxConfig = wxService.getShopWxConfig("分销");
		t_member member = bs.findById(member_id, t_member.class);
		
		new Thread(new Runnable() {
			public void run() {
				System.out.println("＝＝＝＝＝＝＝＝＝＝分销客服消息＝＝＝＝＝＝＝＝＝＝");
				new CustomAPI(wxService.turnTToWx(wxConfig)).sendTextMsg(member.getOpenid(),content);
			}
		}).start();
		
		this.setTipMsg(true, "发送成功!", Tip.Type.success);
	}
	@RequestMapping("/limit")
	@ResponseBody
	public void limit(String kid) {
		t_member memberDb = bs.findById(kid, t_member.class);

		if ("限制登陆".equals(memberDb.getStatus_sys())) {
			memberDb.setStatus_sys("正常状态");
			bs.updateObj(memberDb);
			this.setTipMsg(true, "解除登陆限制成功!", Tip.Type.success);
		} else {
			memberDb.setStatus_sys("限制登陆");
			bs.updateObj(memberDb);
			this.setTipMsg(true, "限制登陆成功!", Tip.Type.success);
		}

	}

}
