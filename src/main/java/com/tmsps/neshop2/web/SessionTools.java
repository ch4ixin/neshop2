package com.tmsps.neshop2.web;

import com.tmsps.neshop2.model.t_fk_admin;
import com.tmsps.neshop2.model.t_member;

public class SessionTools {

	// 登陆 cp 管理员
	public static String LOGIN_USER = "LOGIN_USER";

	// 当前登陆店铺管理员
	public static String LOGIN_SHOP_ADMIN = "LOGIN_SHOP_ADMIN";
	public static String LOGIN_SHOP = "LOGIN_SHOP";

	// 登陆普通用户
	public static String LOGIN_MEMBER = "LOGIN_MEMBER";

	public static String FX_LOGIN_MEMBER = "FX_LOGIN_MEMBER";

	// 登陆后返回的 URL
	public static String LOGIN_TO_URL = "LOGIN_TO_URL";
	// 图片验证码
	public static String VARIETY_KID = "VARIETY_KID";
	// 短信验证码
	public static String SMS_CODE = "SMS_CODE";
	// 购物车中选中结算的项
	public static final String CART_JSON = "CART_JSON";
	// wx 在 session中订单
	public static final String WX_ORDER_INSN = "WX_ORDER_INSN";

	// wx 当前支付的原因
	public static final String WX_PAY = "WX_PAY";// 0 购买 1 充值 2 扫码支付
	// wx 购买或扫码支付的金额
	public static final String WX_CHARGE = "WX_CHARGE";
	// wx 扫码支付的余额
	public static final String WX_MEMBER_MONEY = "WX_MEMBER_MONEY";
	// wx 订单的优惠卷
	public static final String WX_COUPON = "WX_COUPON";
	// 登陆 cp 管理员
	public static String WXCONFIG = "WXCONFIG";

	// ========= put get ====================
	public static void put(String key, Object val) {
		// TODO 设置session值
		WebTools.getSession().setAttribute(key, val);
	}

	public static Object get(String key) {
		// TODO 获取session值
		return WebTools.getSession().getAttribute(key);
	}

	// ========= 获取各session值方法 ====================
		public static String getVarietyKid() {
			// TODO 获取session值
			return WebTools.getSession().getAttribute(VARIETY_KID).toString();
		}

		public static t_fk_admin getCurrentLoginCpAdmin() {
			t_fk_admin user = (t_fk_admin) WebTools.getSession().getAttribute(LOGIN_USER);
			return user;
		}

		public static String getCurrentLoginCpAdminId() {
			t_fk_admin user = getCurrentLoginCpAdmin();
			return user != null ? user.getKid() : null;
		}


		public static t_member getCurrentLoginMember() {
			t_member member = (t_member) WebTools.getSession().getAttribute(LOGIN_MEMBER);
			return member;
		}
		
		public static t_member getCurrentLoginFxMember() {
			t_member member = (t_member) WebTools.getSession().getAttribute(FX_LOGIN_MEMBER);
			return member;
		}

		public static String getLoginToUrl() {
			String loginToUrl = (String) WebTools.getSession().getAttribute(LOGIN_TO_URL);
			return loginToUrl;
		}
		
		
}
