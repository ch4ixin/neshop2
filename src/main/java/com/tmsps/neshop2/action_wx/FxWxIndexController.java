package com.tmsps.neshop2.action_wx;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_retailStore;
import com.tmsps.neshop2.model.t_retail_get_money_list;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.service.OrderService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.util.tree.TreeTools;
import com.tmsps.neshop2.util.wx.FxWxConfigTools;
import com.tmsps.neshop2.util.wx.PaymentAPIToUser;
import com.tmsps.neshop2.web.SessionTools;
import com.tmsps.neshop2.web.WebTools;

@Controller
@Scope("prototype")
@RequestMapping("/fxwx")
public class FxWxIndexController extends ProjBaseAction {
	
	@Autowired
	OrderService orderService;
	@Autowired
	MemberService memberService;
	@Autowired
	WxService wxService;
	// 我的客户
	@RequestMapping("/my_cousumer")
	@ResponseBody
	public ModelAndView my_cousumer() {
		t_member member =bs.findById(SessionTools.getCurrentLoginFxMember().getKid(), t_member.class);
		t_retailStore retailStore = memberService.get_retailStore(member.getKid());
		ModelAndView mv = null;
		if(retailStore == null){
			System.out.println("不是分销商");
			mv = new ModelAndView("/jsp_wx/fenxiao/not_distributor");
			return mv;
		}
		List<Map<String, Object>> cousumer_list = memberService.my_cousumer_list(retailStore.getKid());
		
		if(cousumer_list.size() == 0){
			mv = new ModelAndView("/jsp_wx/fenxiao/my_c=0");
			System.out.println("没有客户");
			mv.addObject("Qr_code", retailStore.getQr_code());
			return mv;
		}
		
		mv = new ModelAndView("/jsp_wx/fenxiao/my_consumer");
		mv.addObject("cousumer_list", cousumer_list);
		mv.addObject("Qr_code", retailStore.getQr_code());
		return mv;
	}
	
	// 我的分销商
	@RequestMapping("/my_distributor")
	@ResponseBody
	public ModelAndView my_distributor() {
		t_member member =bs.findById(SessionTools.getCurrentLoginFxMember().getKid(), t_member.class);
		t_retailStore retailStore = memberService.get_retailStore(member.getKid());
		ModelAndView mv = null;
		if(retailStore == null){
			System.out.println("不是分销商");
			mv = new ModelAndView("/jsp_wx/fenxiao/not_distributor");
			return mv;
		}
		
		List<Map<String, Object>> distributor_list = memberService.my_distributor_list(retailStore.getCode());
		distributor_list.remove(0);
		distributor_list = TreeTools.wxRetailListToTree(distributor_list, false);
		mv = new ModelAndView("/jsp_wx/fenxiao/my_distributor");
		mv.addObject("distributor_list", distributor_list);
		mv.addObject("code_length", retailStore.getCode().length());
		mv.addObject("retailStore", retailStore);
		return mv;
	}
	
	// 我的业绩
	@RequestMapping("/my_performance")
	@ResponseBody
	public ModelAndView my_performance() {
		t_member member =bs.findById(SessionTools.getCurrentLoginFxMember().getKid(), t_member.class);
		t_retailStore retailStore = memberService.get_retailStore(member.getKid());
		ModelAndView mv = null;
		if(retailStore == null){
			System.out.println("不是分销商");
			mv = new ModelAndView("/jsp_wx/fenxiao/not_distributor");
			return mv;
		}
		
		List<Map<String, Object>> myMemberOrderListt = orderService.selectMyMemberOrderListt(retailStore.getKid());
		List<Map<String, Object>> myMemberOrderList = orderService.selectMyMemberOrderList(retailStore.getKid());
		mv = new ModelAndView("/jsp_wx/fenxiao/my_performance");
		mv.addObject("myList", myMemberOrderList);
		mv.addObject("myList_size", myMemberOrderListt.size());
		mv.addObject("retail", retailStore);
		mv.addObject("retail_lv", retailStore.getCode().length());
		mv.addObject("myListt", myMemberOrderListt);
		return mv;
	}
	
	// 客户订单
	@RequestMapping("/cousumer_order")
	@ResponseBody
	public ModelAndView cousumer_order(String member_id) {
		t_member member =bs.findById(SessionTools.getCurrentLoginFxMember().getKid(), t_member.class);
		t_retailStore retailStore = memberService.get_retailStore(member.getKid());
		List<Map<String, Object>> MemberOrderList = orderService.MemberOrderList(member_id,retailStore.getKid());
		ModelAndView mv = new ModelAndView("/jsp_wx/fenxiao/member_order");
		mv.addObject("myList", MemberOrderList);
		mv.addObject("myList_size", MemberOrderList.size());
		mv.addObject("retailStore", retailStore);
		return mv;
	}
	
	// 分销商业绩
	@RequestMapping("/retail_order")
	@ResponseBody
	public ModelAndView retail_order(String retail_id) {
		List<Map<String, Object>> MemberOrderList = orderService.selectMyMemberOrderListt(retail_id);
		t_retailStore retailStore = bs.findById(retail_id, t_retailStore.class);
		ModelAndView mv = new ModelAndView("/jsp_wx/fenxiao/member_orderr");
		mv.addObject("myList", MemberOrderList);
		mv.addObject("myList_size", MemberOrderList.size());
		mv.addObject("retailStore", retailStore);
		return mv;
	}
	
	// 分销商提现
	@RequestMapping("/deposit")
	@ResponseBody
	public String deposit(String get_money) {
		t_member member =bs.findById(SessionTools.getCurrentLoginFxMember().getKid(), t_member.class);
		t_retailStore retailStore = memberService.get_retailStore(member.getKid());
		//WxConfig wxConfig = wxService.getShopWxByShopId("分销"); //分销微信配置信息
		
		t_retail_get_money_list retail_get_money_list = new t_retail_get_money_list();
		retail_get_money_list.setMember_id(member.getKid());
		retail_get_money_list.setRetail_id(retailStore.getKid());
		retail_get_money_list.setGet_money(new BigDecimal(get_money));
		bs.saveObj(retail_get_money_list);
		
		WxConfig wxConfig = FxWxConfigTools.getWxConfig();
		String apiclient_cert_p12_path = "C:/apiclient_cert.p12";
		Map<String, String> params = new HashMap<>();
		params.put("partner_trade_no", retail_get_money_list.getKid());
		params.put("openid", member.getOpenid());
		params.put("check_name", "NO_CHECK");
		params.put("amount", new BigDecimal(get_money).multiply(new BigDecimal("100")).intValue() + ""); //付款金额
		params.put("desc", "user get money.");
		params.put("spbill_create_ip", WebTools.getIp());

		Map<String, String> payToUser = new PaymentAPIToUser(wxConfig).payToUser(apiclient_cert_p12_path, params);
		logger.info(payToUser.toString());
		
		if("SUCCESS".equals(payToUser.get("result_code"))){
			logger.info(payToUser.get("return_msg"));
			result.put("end", "true");
			result.put("msg", "提现完成");
			
			retailStore.setDeposit_money(retailStore.getDeposit_money().subtract(new BigDecimal(get_money)));
			retailStore.setThen_money(retailStore.getThen_money().add(new BigDecimal(get_money)));
			retail_get_money_list.setReturn_msg("提现完成");
			bs.updateObj(retail_get_money_list);
			bs.updateObj(retailStore);
			return JsonTools.toJson(result);
		}else{
			logger.info(payToUser.get("return_msg"));
			result.put("end", "false");
			result.put("msg", payToUser.get("return_msg"));
			
			retail_get_money_list.setReturn_msg(payToUser.get("err_code_des"));
			bs.updateObj(retail_get_money_list);
			return JsonTools.toJson(result);
		}
	}
	
	// 分销商上级提现
	@RequestMapping("/super_deposit")
	@ResponseBody
	public String super_deposit(String get_money,String retail_id) {
		t_member member =bs.findById(SessionTools.getCurrentLoginFxMember().getKid(), t_member.class);
		t_retailStore retailStore = bs.findById(retail_id, t_retailStore.class);
		//WxConfig wxConfig = wxService.getShopWxByShopId("分销"); //分销微信配置信息
		
		t_retail_get_money_list retail_get_money_list = new t_retail_get_money_list();
		retail_get_money_list.setMember_id(member.getKid());
		retail_get_money_list.setRetail_id(retailStore.getKid());
		retail_get_money_list.setGet_money(new BigDecimal(get_money));
		bs.saveObj(retail_get_money_list);
		
		WxConfig wxConfig = FxWxConfigTools.getWxConfig();
		String apiclient_cert_p12_path = "C:/apiclient_cert.p12";
		Map<String, String> params = new HashMap<>();
		params.put("partner_trade_no", retail_get_money_list.getKid());
		params.put("openid", "oFYh_xLBkaveGMozyGMXgcu3c-54");
		params.put("check_name", "NO_CHECK");
		params.put("amount", new BigDecimal(get_money).multiply(new BigDecimal("100")).intValue() + ""); //付款金额
		params.put("desc", "user get money.");
		params.put("spbill_create_ip", WebTools.getIp());
		
		Map<String, String> payToUser = new PaymentAPIToUser(wxConfig).payToUser(apiclient_cert_p12_path, params);
		logger.info(payToUser.toString());
		
		if("SUCCESS".equals(payToUser.get("result_code"))){
			logger.info(payToUser.get("return_msg"));
			result.put("end", "true");
			result.put("msg", "提现完成");
			
			retailStore.setSuper_deposit_money(retailStore.getSuper_deposit_money().subtract(new BigDecimal(get_money)));
			retailStore.setSuper_then_money(retailStore.getSuper_then_money().add(new BigDecimal(get_money)));
			retail_get_money_list.setReturn_msg("提现完成");
			bs.updateObj(retail_get_money_list);
			bs.updateObj(retailStore);
			return JsonTools.toJson(result);
		}else{
			logger.info(payToUser.get("return_msg"));
			result.put("end", "false");
			result.put("msg", payToUser.get("return_msg"));
			
			retail_get_money_list.setReturn_msg(payToUser.get("err_code_des"));
			bs.updateObj(retail_get_money_list);
			return JsonTools.toJson(result);
		}
	}
}
