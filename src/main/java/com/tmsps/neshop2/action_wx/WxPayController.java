package com.tmsps.neshop2.action_wx;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.ne4Weixin.api.CustomAPI;
import com.tmsps.ne4Weixin.api.PaymentAPI.TradeType;
import com.tmsps.ne4Weixin.utils.XMLParser;
import com.tmsps.ne4spring.utils.ChkUtil;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_order;
import com.tmsps.neshop2.model.t_order_detail;
import com.tmsps.neshop2.model.t_retailStore;
import com.tmsps.neshop2.model.t_retail_order;
import com.tmsps.neshop2.model.t_shop_wx_config;
import com.tmsps.neshop2.service.OrderService;
import com.tmsps.neshop2.service.SettingService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.ChkTools;
import com.tmsps.neshop2.util.OrderTools;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.util.wx.PaymentApi;
import com.tmsps.neshop2.util.wx.PaymentKit;
import com.tmsps.neshop2.web.SessionTools;

@Controller
@Scope("prototype")
@RequestMapping("/wx/pay")
public class WxPayController extends ProjBaseAction {

	@Autowired
	WxService wxService;
	@Autowired
	private OrderService orderService;
	@Autowired
	SettingService settingService;
	private String notify_url = "/wx_pay_back.htm";
	private String notify_url_to_back = "/wx_pay_back_to_back.htm";

	@RequestMapping("/pay_1")
	public ModelAndView pay_1(String oid) {
		t_order order = bs.findById(oid, t_order.class);

		if ("是".equals(order.getIs_pay())) {
			ModelAndView mv = new ModelAndView("redirect:/wx/user/core/order_info.htm");
			mv.addObject("oid", order.getKid());
			return mv;
		}

		t_member user = bs.findById(order.getMember_id(), t_member.class);
		//t_member cox = bs.findById(order.getRecv_member_id(), t_member.class);
		//t_product product = bs.findById(order.getProduct_id(), t_product.class);

		BigDecimal pay_self = user.getMoney();
		if (pay_self.compareTo(ChkTools.getBigDecimal("0")) <= 0) {
			pay_self = null;
		} else if (order.getTotal_price().compareTo(pay_self) <= 0) {
			pay_self = order.getTotal_price();
		}

		BigDecimal pay_wx = order.getTotal_price().subtract(user.getMoney());
		if (pay_wx.compareTo(ChkTools.getBigDecimal("0")) <= 0) {
			pay_wx = null;
		}

		ModelAndView mv = new ModelAndView("/jsp_wx/pay/pay_1");
		mv.addObject("order", order);
		mv.addObject("user", user);
		//mv.addObject("cox", cox);
		//mv.addObject("product", product);
		mv.addObject("backType", "to_back");
		mv.addObject("pay_self", pay_self);
		mv.addObject("pay_wx", pay_wx);
		return mv;
	}

	// 调起微信支付
	@RequestMapping("/to_pay")
	public ModelAndView to_pay(String oid) {
		logger.info("进入支付");
		logger.info(oid);
		t_order order = bs.findById(oid, t_order.class);
		logger.info(order);
		if ("是".equals(order.getIs_pay())) {
			ModelAndView mv = new ModelAndView("/jsp_wx/pay/paid_error");
			mv.addObject("error_info", "支付失败,该订单已经支付!");
			return mv;
		}

		logger.info("微信支付:"+order.toJsonString());
		t_member member = bs.findById(order.getMember_id(), t_member.class);
		t_shop_wx_config wxConfig = bs.findById(req.getParameter("wxid"), t_shop_wx_config.class);

		// 用户微信支付
		BigDecimal pay_wx = order.getEnd_price();
		String openID = member.getOpenid();
		String appid = wxConfig.getAppid();
		String partner = wxConfig.getPay_partner();
		String paternerKey = wxConfig.getPartner_key();

		// 统一下单文档地址：https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_1

		Map<String, String> params = new HashMap<String, String>();
		params.put("appid", appid);
		params.put("mch_id", partner);
		params.put("body", "pay");
		// params.put("out_trade_no", order.getStr("id"));
		params.put("out_trade_no", order.getOrder_no());
		params.put("total_fee", OrderTools.getPayIntToString(pay_wx));

		String ip = req.getRemoteAddr();
		if (ChkUtil.isNull(ip)) {
			ip = "127.0.0.1";
		}

		params.put("spbill_create_ip", ip);
		params.put("trade_type", TradeType.JSAPI.name());
		params.put("nonce_str", System.currentTimeMillis() / 1000 + "");
		// if ("to_back".equals(type)) {
		// params.put("notify_url", "http://" + req.getServerName() +
		// notify_url_to_back);
		// } else {
		params.put("notify_url", "http://" + req.getServerName() + notify_url);
		// }
		params.put("openid", openID);

		String stringA = PaymentKit.packageSign(params, false);
		logger.info(stringA);
		String sign = PaymentKit.createSign(params, paternerKey);
		params.put("sign", sign);
		logger.info("params:" + params);
		String xmlResult = PaymentApi.pushOrder(params);

		logger.info("xmlResult:" + xmlResult);
		Map<String, String> result = XMLParser.getXmlToMap(xmlResult);

		String return_code = result.get("return_code");
		String return_msg = result.get("return_msg");
		if (ChkUtil.isNull(return_code) || !"SUCCESS".equals(return_code)) {
			ModelAndView mv = new ModelAndView("/jsp_wx/pay/paid_error");
			mv.addObject("error_info", return_msg);
			return mv;
		}
		String result_code = result.get("result_code");
		if (ChkUtil.isNull(result_code) || !"SUCCESS".equals(result_code)) {
			ModelAndView mv = new ModelAndView("/jsp_wx/pay/paid_error");
			mv.addObject("error_info", return_msg);
			return mv;
		}
		// 以下字段在return_code 和result_code都为SUCCESS的时候有返回
		String prepay_id = result.get("prepay_id");

		Map<String, String> packageParams = new HashMap<String, String>();
		packageParams.put("appId", appid);
		packageParams.put("timeStamp", System.currentTimeMillis() / 1000 + "");
		packageParams.put("nonceStr", System.currentTimeMillis() + "");
		packageParams.put("package", "prepay_id=" + prepay_id);
		packageParams.put("signType", "MD5");
		String packageSign = PaymentKit.createSign(packageParams, paternerKey);
		packageParams.put("paySign", packageSign);

		String jsonStr = JsonTools.toJson(packageParams);
		logger.info(jsonStr);

		ModelAndView mv = new ModelAndView("/jsp_wx/pay/to_pay");
		mv.addObject("json", jsonStr);
		mv.addObject("order", order);
		return mv;

	}	

	//支付完成后的成功界面
	@RequestMapping("/paid")
	public ModelAndView paid(String oid) {
		logger.info("支付完成:"+oid);
		t_order order = bs.findById(oid, t_order.class);
		if("下单中".equals(order.getStatus_order())){
			order.setStatus_order("已下单");
			order.setPay_way("微信支付");
			order.setIs_pay("是");
			order.setPay_time(System.currentTimeMillis());
			order.setSend_way("普通快递");
			bs.updateObj(order);

			List<Map<String, Object>> list = orderService.ListDetail(order.getKid());
			for(int i=0;i<list.size();i++){
				t_order_detail order_detail = bs.findById(list.get(i).get("kid"), t_order_detail.class);
				order_detail.setStatus_order("已付款");
				bs.updateObj(order_detail);
			}

			t_member member = bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
			long integral = order.getEnd_price().add(BigDecimal.valueOf(member.getIntegral())).intValue();
			member.setIntegral(integral);
			bs.updateObj(member);
			t_shop_wx_config wxConfig = wxService.getShopWxConfig("电商");
			new Thread(new Runnable() {
				public void run() {
					System.out.println("＝＝＝＝＝＝＝＝＝＝电商客服消息＝＝＝＝＝＝＝＝＝＝");
					new CustomAPI(wxService.turnTToWx(wxConfig)).sendTextMsg(member.getOpenid(),"支付成功!我们将会尽快为您发货");
				}
			}).start();

			// TODO 业绩分成
			t_retailStore retailStore = bs.findById(member.getRetail_id(), t_retailStore.class);
			if(ChkTools.isNotNull(retailStore)){
				t_member fxmember = bs.findById(retailStore.getMember_id(), t_member.class);
				t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("分销");

				t_retail_order retail_order = new t_retail_order();
				retail_order.setRetail_id(member.getRetail_id());//分销id
				retail_order.setMember_id(member.getKid());//用户id
				retail_order.setOrder_id(oid);//订单id

				logger.info(retailStore.getCode().length());

				if(retailStore.getCode().length() == 6){
					BigDecimal one_deduct_money = order.getEnd_price().multiply(new BigDecimal(0.05));
					logger.info("一级分销本人提成5%，订单金额："+order.getEnd_price()+";提成金额："+one_deduct_money);
					retail_order.setOne_deduct_money(one_deduct_money);
					retailStore.setTotal_money(retailStore.getTotal_money().add(one_deduct_money));
					retailStore.setDeposit_money(retailStore.getDeposit_money().add(one_deduct_money));
				}else if(retailStore.getCode().length() == 9){
					BigDecimal one_deduct_money = order.getEnd_price().multiply(new BigDecimal(0.02));
					BigDecimal two_deduct_money = order.getEnd_price().multiply(new BigDecimal(0.05));
					logger.info("二级分销上一级提成2%，订单金额："+order.getEnd_price()+";提成金额："+one_deduct_money);
					logger.info("二级分销上二级提成5%，订单金额："+order.getEnd_price()+";提成金额："+two_deduct_money);
					retail_order.setOne_deduct_money(one_deduct_money);
					retail_order.setTwo_deduct_money(two_deduct_money);
					retailStore.setTotal_money(retailStore.getTotal_money().add(two_deduct_money));
					retailStore.setDeposit_money(retailStore.getDeposit_money().add(two_deduct_money));
					retailStore.setSuper_deduct_money(retailStore.getSuper_deduct_money().add(one_deduct_money));//给上级分成业绩
					retailStore.setSuper_deposit_money(retailStore.getSuper_deposit_money().add(one_deduct_money));//给上级分成业绩

					String super_code = retailStore.getCode().substring(0,6);
					t_retailStore super_retail_order = orderService.get_super_retailStore(super_code);
					t_member super_member = bs.findById(super_retail_order.getMember_id(), t_member.class);

					new Thread(new Runnable() {
						public void run() {
							System.out.println("＝＝＝＝＝＝＝＝＝＝分销客服消息＝＝＝＝＝＝＝＝＝＝");
							new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(super_member.getOpenid(),"您的分销商：	“"+retailStore.getName()+"”	\r的客户有一条交易记录,详情请到《我的分销商》查看!");
						}
					}).start();
				}
				bs.saveObj(retail_order);
				bs.updateObj(retailStore);

				new Thread(new Runnable() {
					public void run() {
						System.out.println("＝＝＝＝＝＝＝＝＝＝分销客服消息＝＝＝＝＝＝＝＝＝＝");
						new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),"您的客户：	“"+member.getUser_nick()+"”	\r有一条交易记录,详情请到《我的业绩》查看");
					}
				}).start();
		}
			
			ModelAndView mv = new ModelAndView("/jsp_wx/pay/paid");
			mv.addObject("order", order);
			return mv;
		}else{
			ModelAndView mv = new ModelAndView("/jsp_wx/pay/paid");
			mv.addObject("order", order);
			return mv;
		}
	}
}
