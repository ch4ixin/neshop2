package com.tmsps.neshop2.action_wx;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tmsps.ne4Weixin.api.CustomAPI;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.ne4Weixin.utils.XMLParser;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.model.t_order;
import com.tmsps.neshop2.model.t_pay_no;
import com.tmsps.neshop2.service.OrderService;
import com.tmsps.neshop2.util.ChkTools;
import com.tmsps.neshop2.util.WxConfigTools;
import com.tmsps.neshop2.util.http.HttpTools;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.util.wx.PaymentKit;

@Controller
@Scope("prototype")
public class WxPayBackController extends ProjBaseAction {

	@Autowired
	private OrderService orderService;

	// 微信支付回调页面
	@RequestMapping("/wx_pay_back")
	@ResponseBody
	public String wx_pay_back() {
		return wx_pay(null);
	}

	@RequestMapping("/wx_pay_back_to_back")
	@ResponseBody
	public String wx_pay_back_to_back() {
		return wx_pay("to_back");
	}

	public String wx_pay(String type) {
		Map<String, String> resultXml = new HashMap<String, String>();
		resultXml.put("return_code", "SUCCESS");

		// 支付结果通用通知文档:https://pay.weixin.qq.com/wiki/doc/api/jsapi.php?chapter=9_7
		String xmlMsg = HttpTools.readData(req);
		logger.warn("=============================================");
		logger.warn("支付通知:{}" + xmlMsg);
		logger.warn("=============================================");
		Map<String, String> params = XMLParser.getXmlToMap(xmlMsg);
		String result_code = params.get("result_code");
		// 注意重复通知的情况，同一订单号可能收到多次通知，请注意一定先判断订单状态
		// 避免已经成功、关闭、退款的订单被再次更新

		String out_trade_no = params.get("out_trade_no");
		t_pay_no payNo = orderService.findPayNo(out_trade_no);
		if (payNo == null) {
			return PaymentKit.toXml(resultXml);
		}
		t_order order = bs.findById(payNo.getOrder_id(), t_order.class);

		WxConfig wxConfig = WxConfigTools.getWxConfig();
		String paternerKey = wxConfig.getPayAPI();

		if (PaymentKit.verifyNotify(params, paternerKey)) {
			if (("SUCCESS").equals(result_code)) {
				// 总金额
				String totalFeeStr = params.get("total_fee");
				BigDecimal totalFee = ChkTools.getBigDecimal(totalFeeStr).divide(new BigDecimal("100"));

				// 商户订单号
				// String out_trade_no = params.get("out_trade_no");
				// 微信支付订单号
				String payWxNo = params.get("transaction_id");
				String openid = params.get("openid");
				/*************** 更新订单信息 ***************/
				String pay_method = params.get("trade_type");
				boolean b = false;

				if ("to_back".equals(type)) {
					b = orderService.payToBack(order, payNo, out_trade_no, payWxNo, pay_method, openid, totalFee,
							xmlMsg);
				} else {
					b = orderService.pay(order, payNo, out_trade_no, payWxNo, pay_method, openid, totalFee, xmlMsg);
				}

				if (b) {
					logger.debug(JsonTools.toJson(order));

					new Thread(new Runnable() {
						public void run() {
							logger.info("＝＝＝＝＝＝＝＝＝＝发送客服消息＝＝＝＝＝＝＝＝＝＝");
							new CustomAPI(wxConfig).sendTextMsg(openid, "支付成功!共支付:" + totalFee + "元");
						}
					}).start();
					return PaymentKit.toXml(resultXml);
				} else {
					resultXml.put("return_code", "FAIL");
					return PaymentKit.toXml(resultXml);
				}

			}
		}
		return PaymentKit.toXml(resultXml);

	}

}
