package com.tmsps.neshop2.action_wx;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tmsps.ne4Weixin.action.Ne4WeixinAction;
import com.tmsps.ne4Weixin.api.CustomAPI;
import com.tmsps.ne4Weixin.api.UserAPI;
import com.tmsps.ne4Weixin.beans.UserInfo;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.ne4Weixin.message.in.InBaseMsg;
import com.tmsps.ne4Weixin.message.in.InTextMsg;
import com.tmsps.ne4Weixin.message.in.event.InClickEvent;
import com.tmsps.ne4Weixin.message.in.event.InFollowEvent;
import com.tmsps.ne4Weixin.message.in.event.InLocationEvent;
import com.tmsps.ne4Weixin.message.in.event.InQrCodeEvent;
import com.tmsps.ne4Weixin.message.out.OutTextMsg;
import com.tmsps.ne4spring.base.IBaseService;
import com.tmsps.ne4spring.utils.ChkUtil;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_retailStore;
import com.tmsps.neshop2.model.t_shop_wx_config;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.service.SettingService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.ChkTools;
import com.tmsps.neshop2.util.WxCode;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.util.wx.msg.MyOutTextMsg;
import com.tmsps.neshop2.web.WebTools;

@Controller
@Scope("prototype")
@RequestMapping("/weixin")
public class WxMsgController extends Ne4WeixinAction {

	@Autowired
	protected IBaseService bs;
	@Autowired
	WxService wxService;
	@Autowired
	MemberService memberService;
	@Autowired
	SettingService settingService;

	@Override
	public WxConfig getWxConfig() {
		String wxid = WebTools.getRequest().getParameter("wxid");
		if (ChkUtil.isNotNull(wxid)) {// 根据wxid获取对应的ac
			return wxService.getShopWx(wxid);
		} else {
			log.error("WxConfig is null");
			return null;
		}
	}

	// TODO 公众号接收到消息事件
	@Override
	protected String processInTextMsg(InTextMsg msg) {
		System.out.println("接受到微信消息："+msg.toJson());
		MyOutTextMsg msgOut = new MyOutTextMsg(msg); 
		msgOut.setMsgType("transfer_customer_service");
		return msgOut.toXml();
	}
	
	// TODO 定位 事件
	@Override
	protected String processInLocationEvent(InLocationEvent msg) {
		System.out.println("接受到微信定位消息："+msg.toJson());
		log.info("接受到微信定位消息："+msg.toJson());
		return null;
	}

	// TODO 关注 or 取消关注 事件
	@Override
	protected String processInFollowEvent(InFollowEvent msg) {
		System.out.println("关注 or 取消关注 事件"+msg.toJson());
		log.info(JsonTools.toJson(msg));

		t_shop_wx_config wxConfig = bs.findById(request.getParameter("wxid"), t_shop_wx_config.class);

		String openId = msg.getFromUserName();
		String wx_original_id = msg.getToUserName();// weixinID
		UserInfo userInfo = new UserAPI(getWxConfig()).getUserInfo(openId);
		
		if (InFollowEvent.SUBSCRIBE.equals(msg.getEvent())) {
			memberService.subscribe(userInfo, wx_original_id, wxConfig.getShop_id());
			OutTextMsg msgOut = new OutTextMsg(msg);
			msgOut.setContent("你好！	"+userInfo.getNickname()+"\n\n	\r	 欢迎来到<a href='http://www.sxxlkj.com/neshop2/wx/home.htm?wxid=bNH2Brsj8XVNbysvXbJya'>悦龙斋速购</a>(๑•.•๑)。  \n \n\r一个安心·放心·舒心的购物平台");
			msgOut.setMsgType("text");
			return msgOut.toXml();
		} else if (InFollowEvent.UNSUBSCRIBE.equals(msg.getEvent())) {
			memberService.unsubscribe(userInfo, wx_original_id,wxConfig.getShop_id());
		} else {
		}

		return null;
	}

	// TODO 扫描带参数二维码
	@Override
	protected String processInQrCodeEvent(InQrCodeEvent msg) {
		System.out.println("扫描带参数二维码"+msg.toJson());
		log.info("扫描带参数二维码"+JsonTools.toJson(msg));
		
		t_shop_wx_config wxConfig = bs.findById(request.getParameter("wxid"), t_shop_wx_config.class);
		log.info(JsonTools.toJson(wxConfig));
		
		String openId = msg.getFromUserName();
		String wx_original_id = msg.getToUserName();// weixinID
		UserInfo userInfo = new UserAPI(getWxConfig()).getUserInfo(openId);
		
		if (InFollowEvent.SUBSCRIBE.equals(msg.getEvent())) {
			memberService.subscribe(userInfo, wx_original_id, wxConfig.getShop_id());
		}

		String eventKey = msg.getEventKey();
		if (eventKey.startsWith("qrscene_")) {
			eventKey = eventKey.replace("qrscene_", "");
		}

		// 扫描绑定分销二维码
		if (eventKey.startsWith(WxCode.Retail)) {
			String retail_id = eventKey.replace(WxCode.Retail, "");
			
			t_retailStore retailStore = bs.findById(retail_id, t_retailStore.class);
			
			t_member member = memberService.subscribe(userInfo, wx_original_id, wxConfig.getShop_id());
//			System.out.println(member.getRetail_id() == null || member.getRetail_id() == "");
//			System.out.println(!retail_id.equals(member.getRetail_id()) || retail_id != member.getRetail_id());
			
			if(member.getRetail_id() == null || member.getRetail_id() == ""){
				member.setRetail_id(retail_id);
				bs.updateObj(member);
				
				t_member fxmember = bs.findById(retailStore.getMember_id(), t_member.class);
				t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("分销");
				new Thread(new Runnable() {
					public void run() {
						System.out.println("＝＝＝＝＝＝＝＝＝＝分销客服消息＝＝＝＝＝＝＝＝＝＝");
						new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),"用户： “"+member.getUser_nick()+"”	\r已成为您的客户,请点击《我的客户》查看详情!");
					}
				}).start();
				
				// 给电商用户推送消息
				OutTextMsg msgOut = new OutTextMsg(msg);
				msgOut.setContent("您好！	"+userInfo.getNickname()+"\n\n	\r	 欢迎来到<a href='http://www.sxxlkj.com/neshop2/wx/home.htm?wxid=bNH2Brsj8XVNbysvXbJya'>悦龙斋速购</a>(๑•.•๑)。  \n \n\r一个安心·放心·舒心的购物平台");
				msgOut.setMsgType("text");
				return msgOut.toXml();
			}else if(!retail_id.equals(member.getRetail_id()) || retail_id != member.getRetail_id()){
				// 给电商用户推送消息
				t_retailStore retail =bs.findById(member.getRetail_id(), t_retailStore.class);
				
				if(!ChkTools.isNotNull(retail)){
					member.setRetail_id(retail_id);
					bs.updateObj(member);
					
					t_member fxmember = bs.findById(retailStore.getMember_id(), t_member.class);
					t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("分销");
					new Thread(new Runnable() {
						public void run() {
							System.out.println("＝＝＝＝＝＝＝＝＝＝分销客服消息＝＝＝＝＝＝＝＝＝＝");
							new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),"用户： “"+member.getUser_nick()+"”	\r已成为您的客户,请点击《我的客户》查看详情!");
						}
					}).start();
					
					// 给电商用户推送消息
					OutTextMsg msgOut = new OutTextMsg(msg);
					msgOut.setContent("您好！	"+userInfo.getNickname()+"\n\n	\r	 欢迎来到<a href='http://www.sxxlkj.com/neshop2/wx/home.htm?wxid=bNH2Brsj8XVNbysvXbJya'>悦龙斋速购</a>(๑•.•๑)。  \n \n\r一个安心·放心·舒心的购物平台");
					msgOut.setMsgType("text");
					return msgOut.toXml();
				}else{
					OutTextMsg msgOut = new OutTextMsg(msg);
					String content = "您现在是 nname 的客户. \n	\r	 如需要切换到mname请点 <a href='http://www.sxxlkj.com/neshop2/wx/cut_retail.htm?retail_id=rretail_id&member_id=memberidd&?wxid=bNH2Brsj8XVNbysvXbJya'>这里</a> 即可;";
					content = content.replace("nname", retail.getName());
					content = content.replace("mname", retailStore.getName());
					content = content.replace("rretail_id", retail_id);
					content = content.replace("memberidd", member.getKid());
					msgOut.setContent(content);
					msgOut.setMsgType("text");
					return msgOut.toXml();
				}
			}else{
				// 给电商用户推送消息
				OutTextMsg msgOut = new OutTextMsg(msg);
				msgOut.setContent("您好！	"+userInfo.getNickname()+"\n\n	\r	 欢迎来到<a href='http://www.sxxlkj.com/neshop2/wx/home.htm?wxid=bNH2Brsj8XVNbysvXbJya'>悦龙斋速购</a>(๑•.•๑)。  \n \n\r一个安心·放心·舒心的购物平台");
				msgOut.setMsgType("text");
				return msgOut.toXml();
			}
		}
		// 给电商用户推送消息
		OutTextMsg msgOut = new OutTextMsg(msg);
		msgOut.setContent("您好！	"+userInfo.getNickname()+"\n\n	\r	 欢迎来到<a href='http://www.sxxlkj.com/neshop2/wx/home.htm?wxid=bNH2Brsj8XVNbysvXbJya'>悦龙斋速购</a>(๑•.•๑)。  \n \n\r一个安心·放心·舒心的购物平台");
		msgOut.setMsgType("text");
		return msgOut.toXml();
	}

	@Override
	protected String processInClickEvent(InClickEvent msgBean) {
		System.out.println("接受到点击微信消息："+msgBean.toJson());
		log.info("接受到点击微信消息："+msgBean.toJson());
		return null;
	}

	@Override
	protected String processOtherEvent(InBaseMsg msgBean) {
		System.out.println("接受到其他微信消息："+msgBean.toJson());
		log.info("接受到其他微信消息："+msgBean.toJson());
		return null;
	}
}
