package com.tmsps.neshop2.action_wx;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.tmsps.ne4spring.utils.ChkUtil;
import com.tmsps.neshop2.model.*;
import com.tmsps.neshop2.util.wx.WxJsApiTools;
import com.tmsps.neshop2.web.WebTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4Weixin.api.CustomAPI;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.service.HomeBillboardService;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.service.OrderService;
import com.tmsps.neshop2.service.ProductOnofferService;
import com.tmsps.neshop2.service.ProductService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.ChkTools;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.web.SessionTools;

@Controller
@Scope("prototype")
@RequestMapping("/wx")
public class WxIndexController extends ProjBaseAction {
	
	@Autowired
	private ProductOnofferService spofs;
	@Autowired
	private ProductService productService;
	@Autowired
	private HomeBillboardService homeBillboardService;
	@Autowired
	OrderService orderService;
	@Autowired
	MemberService memberService;
	@Autowired
	WxService wxService;
	
	@RequestMapping("/index")
	public ModelAndView index() {
		List<Map<String, Object>> list = spofs.selectOnofferList(srh, sort_params, page);
		t_member member =bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
		
		ModelAndView mv = new ModelAndView("/jsp_wx/index");
		mv.addObject("product_list", list);
		mv.addObject("member", member);
		return mv;
	}

	@RequestMapping("/home")
	public ModelAndView home() {
		List<Map<String, Object>> list = spofs.selectOnofferList(srh, sort_params, page);
		List<Map<String, Object>> cateList = productService.selectCateList(srh, sort_params, page);
		List<Map<String, Object>> billboards = homeBillboardService.getSiteBillboards();
		ModelAndView mv = new ModelAndView("/jsp_wx/client/home");
		mv.addObject("list",list);
		mv.addObject("cateList",cateList);
		mv.addObject("billboards",billboards);
		return mv;
	}

	@RequestMapping("/product_list")
	@ResponseBody
	public String list_data(int pageNumber) {
		page.setPageNumber(pageNumber);
		page.setPageSize(10);
		JSONObject result = new JSONObject();
		List<Map<String, Object>> list = spofs.selectOnofferList(srh, sort_params, page);
		result.put("list", list);
		result.put("success", true);
		result.put("message", "操作成功");
		return result.toJSONString();
	}

	@RequestMapping("/product")
	@ResponseBody
	public String get_product(String kid) {
		t_product product = bs.findById(kid,t_product.class);
		return JsonTools.toJson(product);
	}
	
	@RequestMapping("/products")
	public ModelAndView products() {
		List<Map<String, Object>> list = spofs.getAllProductList();
		ModelAndView mv = new ModelAndView("/jsp_wx/client/products");
		mv.addObject("product_list", list);
		return mv;
	}
	
	@RequestMapping("/cate_product")
	public ModelAndView product(String cname,String rank_code) {
		t_product_cate product_cate = productService.getNameCate(cname);
		List<Map<String, Object>> list = spofs.upGetAllProductList(cname,rank_code,null);
		ModelAndView mv = new ModelAndView("/jsp_wx/client/cate_product");
		mv.addObject("product_list", list);
		mv.addObject("mv", "cate_product");
		mv.addObject("product_cate", product_cate);
		return mv;
	}

	@RequestMapping("/shop_product")
	public ModelAndView shop_product(String rank_code,String shop_id) {
		t_shop shop = bs.findById(shop_id,t_shop.class);
		List<Map<String, Object>> list = spofs.upGetAllProductList(null,rank_code,shop_id);
		t_product_cate product_cate = new t_product_cate();
		product_cate.setAd_file_id("");
		product_cate.setName("【"+shop.getName()+"】的精选好货");
		ModelAndView mv = new ModelAndView("/jsp_wx/client/cate_product");
		mv.addObject("product_list", list);
		mv.addObject("mv", "shop_product");
		mv.addObject("product_cate", product_cate);
		return mv;
	}
	
	@RequestMapping("/search")
	public ModelAndView search(String pname) {
		List<Map<String, Object>> list = spofs.searchProductList(pname);
		ModelAndView mv = new ModelAndView("/jsp_wx/client/product");
		mv.addObject("product_list", list);
		mv.addObject("pname", pname);
		return mv;
	}

	@RequestMapping("/classify")
	public ModelAndView classify() {
		List<Map<String, Object>> list = productService.selectCateList();
		ModelAndView mv = new ModelAndView("/jsp_wx/client/classify");
		mv.addObject("cateList", list);
		return mv;
	}

	// 分类商品
	@RequestMapping("/cy_product")
	@ResponseBody
	public String cy_product(String name) {
		t_product_cate product_cate = productService.getNameCate(name);
		List<Map<String, Object>> list = spofs.getCateProductList(product_cate.getKid());

		JSONObject result = new JSONObject();
		result.put("list", list);
		result.put("product_cate", product_cate);
		return result.toJSONString();
	}
	
	@RequestMapping("/favourable")
	public ModelAndView favourabl() {
		ModelAndView mv = new ModelAndView("/jsp_wx/favourable");
		return mv;
	}
	
	@RequestMapping("/cart")
	public ModelAndView cart() {
		t_member member = SessionTools.getCurrentLoginMember();
		t_member memberDb = bs.findById(member.getKid(), t_member.class);
		t_member_address address = orderService.getDefaultAddress(memberDb.getKid());
		
		List<Map<String, Object>> cartList = spofs.selectCartItemList();
		BigDecimal sum= BigDecimal.ZERO;
//		BigDecimal carriage= BigDecimal.ZERO;
//		BigDecimal totalsum= BigDecimal.ZERO;
//		BigDecimal more_carriage= BigDecimal.ZERO;
		for(int i=0;i<cartList.size();i++){
			 BigDecimal sig_sum =(BigDecimal) cartList.get(i).get("price_total");
//			 BigDecimal carr =(BigDecimal) cartList.get(i).get("carr");
//			 BigDecimal cnt =(BigDecimal) cartList.get(i).get("cnt");
//			 BigDecimal mcarr =(BigDecimal) cartList.get(i).get("mcarr");
//			 if(cnt != new BigDecimal(1)){
//				 cnt = cnt.subtract(new BigDecimal(1));
//				 more_carriage = mcarr.multiply(cnt);
//			 }
			 sum = sum.add(sig_sum);
//			 carriage = carriage.add(carr.add(more_carriage));
//			 totalsum = sum.add(carriage);
		}
		ModelAndView mv = new ModelAndView("/jsp_wx/client/cart");
		mv.addObject("cartList", cartList);
		mv.addObject("address", address);
		mv.addObject("price_total", sum);
//		mv.addObject("totalsum", totalsum);
//		mv.addObject("carriage", carriage);
		return mv;
	}
	
	@RequestMapping("/user")
	public ModelAndView user() {
		t_member member =bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
		t_member_level member_level = bs.findById(member.getLevel_id(),t_member_level.class);
		List<Map<String, Object>> OrderList = orderService.myOrderList();
		List<Map<String, Object>> stayPayOrder = orderService.myOrderList("下单中");
		List<Map<String, Object>> staySendOrder = orderService.myOrderList("已下单");
		List<Map<String, Object>> finishOrder = orderService.OrderList();
		List<Map<String, Object>> zfinishOrder = orderService.myOrderList("已完成","否");
		ModelAndView mv = new ModelAndView("/jsp_wx/client/user_info");
		mv.addObject("member", member);
		mv.addObject("my_order", OrderList.size());
		mv.addObject("stayPayOrder", stayPayOrder);
		mv.addObject("staySendOrder", staySendOrder);
		mv.addObject("finishOrder", finishOrder);
		mv.addObject("zfinishOrder", zfinishOrder);
		mv.addObject("member_level", member_level.getName());
		return mv;
	}

	@RequestMapping("/orders")
	public ModelAndView orders(String kid) {
		t_product product = bs.findById(kid, t_product.class);
		product.setContent(product.getContent().trim());
		String url = WebTools.getRefererUrl();
		WxJsApiTools jsConfig = WxJsApiTools.getJsApiConfig(url);
		ModelAndView mv = new ModelAndView("/jsp_wx/client/orders");
		mv.addObject("jsConfig", jsConfig);
		mv.addObject("product", product);
		return mv;
	}
	
	@RequestMapping("/edit_orders")
	public ModelAndView edit_orders(String kid,String oid) {
		t_product product = bs.findById(kid, t_product.class);
		t_order_detail order_detail = bs.findById(oid, t_order_detail.class);
		ModelAndView mv = new ModelAndView("/jsp_wx/client/edit_orders");
		mv.addObject("product", product);
		mv.addObject("order_detail", order_detail);
		return mv;
	}
	
	// 添加到购物车
	@RequestMapping("/add_cart")
	@ResponseBody
	public String add_cart(String product_id, String number) {
		t_product product = bs.findById(product_id, t_product.class);
		spofs.addToCart(product, Integer.parseInt(number));

		// 获得用户所有商品的总数量，在页面加入购物车的时候实时更新
		List<Map<String, Object>> productsList = spofs.selectCartItemList();
		BigDecimal total_cnt = BigDecimal.ZERO;
		for (Map<String, Object> map : productsList) {
			BigDecimal sig_cnt = (BigDecimal) (map.get("cnt"));
			total_cnt = total_cnt.add(sig_cnt);
		}

		JSONObject result = new JSONObject();
		result.put("total_cnt", total_cnt);
		return result.toJSONString();
	}
	
	// 修改订单参数
	@RequestMapping("/edit_order")
	@ResponseBody
	public void edit_cart(String kid,String cnt) {
		t_order_detail order_detailDb = bs.findById(kid, t_order_detail.class);
		BigDecimal totalPrice = order_detailDb.getPrice().multiply(new BigDecimal(cnt));
		order_detailDb.setCnt(new BigDecimal(cnt));
		order_detailDb.setPrice_total(totalPrice);
		bs.updateObj(order_detailDb);
	}
	
	//删除订单
	@RequestMapping("/del_order")
	@ResponseBody
	public void del_order(String kid) {
		t_order_detail order_detail = bs.findById(kid, t_order_detail.class);
		order_detail.setStatus(-100);
		bs.updateObj(order_detail);
		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}

	//收货地址列表
	@RequestMapping("/address")
	public ModelAndView address() {
		t_member member = SessionTools.getCurrentLoginMember();
		t_member memberDb = bs.findById(member.getKid(), t_member.class);
		List<Map<String, Object>> addressList = orderService.getMemberAddress(memberDb.getKid());
		ModelAndView mv = new ModelAndView("/jsp_wx/client/address");
		mv.addObject("addressList", addressList);
		return mv;
	}
	
	// 添加新地址
	@RequestMapping("/add_address")
	public ModelAndView add_address() {
		t_member member = SessionTools.getCurrentLoginMember();
		t_member memberDb = bs.findById(member.getKid(), t_member.class);
		ModelAndView mv = new ModelAndView("/jsp_wx/client/add_address");
		mv.addObject("member",memberDb);
		return mv;
	}
	
	// 提交新地址
	@RequestMapping("/submit_address")
	@ResponseBody
	public void submit_address(t_member_address memberAddress) {
		memberAddress.setMember_id(SessionTools.getCurrentLoginMember().getKid());

		if(memberAddress.getIs_default().equals("是")){
			List<Map<String, Object>> addressList = orderService.getMemberAddress(memberAddress.getMember_id());
			 for(int i=0;i<addressList.size();i++){
				 t_member_address memberAddress1 = bs.findById(addressList.get(i).get("kid"), t_member_address.class);
				 memberAddress1.setIs_default("否");
				 bs.updateObj(memberAddress1);
			 }
		}
		
		bs.saveObj(memberAddress);
		this.setTipMsg(true, "添加成功!", Tip.Type.success);
	}
	
	// 地址管理
	@RequestMapping("/edit_from_address")
	public ModelAndView edit_from_address(String kid) {
		t_member_address member_address =bs.findById(kid, t_member_address.class);
		ModelAndView mv = new ModelAndView("/jsp_wx/client/edit_address");
		mv.addObject("member_address", member_address);
		return mv;
	}

	// 更改地址
	@RequestMapping("/edit_address")
	@ResponseBody
	public String edit_address(HttpServletRequest req) throws ParseException {
		t_member_address member_addressDb =bs.findById(req.getParameter("kid"), t_member_address.class);
		member_addressDb.setMember_name(req.getParameter("member_name"));
		member_addressDb.setProvince(req.getParameter("province"));
		member_addressDb.setCity(req.getParameter("city"));
		member_addressDb.setRegion(req.getParameter("region"));
		member_addressDb.setStreet(req.getParameter("street"));
		member_addressDb.setMobile1(req.getParameter("mobile1"));
		
		if(req.getParameter("is_default").equals("是")){
			List<Map<String, Object>> addressList = orderService.getMemberAddress(member_addressDb.getMember_id());
			 for(int i=0;i<addressList.size();i++){
				 t_member_address memberAddress1 = bs.findById(addressList.get(i).get("kid"), t_member_address.class);
				 memberAddress1.setIs_default("否");
				 bs.updateObj(memberAddress1);
			 }
			 member_addressDb.setIs_default("是");
		}

		bs.updateObj(member_addressDb);
		result.put("end", "true");
		result.put("msg", "修改成功");
		return JsonTools.toJson(result);
	}
	
	// 删除地址
	@RequestMapping("/del_address")
	@ResponseBody
	public void del(String kid) {
		t_member_address memberAddress = bs.findById(kid, t_member_address.class);

		memberAddress.setStatus(-100);
		bs.updateObj(memberAddress);
		this.setTipMsg(true, "删除成功!", Tip.Type.success);
	}
	
	// 生成订单
	@RequestMapping("/to_cart")
	public ModelAndView to_cart(String order_id) {
		t_order order = null;
		if (ChkTools.isNotNull(order_id)) {
			order = bs.findById(order_id, t_order.class);
		} else {
			order = orderService.turnCartItemToOrder();
			if(ChkUtil.isNotNull(order)){
				return new ModelAndView("redirect:/wx/to_cart.htm?order_id=" + order.getKid());
			}else{
				return new ModelAndView("redirect:/wx/cart.htm");
			}
		}

		return new ModelAndView("redirect:/wx/order_detail.htm?kid=" + order.getKid());
//		ModelAndView mv = new ModelAndView("redirect:/wx/pay/to_pay.htm?oid=" + order_id +"&wxid=bNH2Brsj8XVNbysvXbJya");
//		return mv;
	}

	// 生成订单
	@RequestMapping("/to_pay")
	public ModelAndView to_pay(String order_id,String address_id) {
		t_member member = SessionTools.getCurrentLoginMember();
		t_member memberDb = bs.findById(member.getKid(), t_member.class);
		t_member_address address;
		if(ChkUtil.isNotNull(address_id)){
			address = bs.findById(address_id,t_member_address.class);
		}else{
			address = orderService.getDefaultAddress(memberDb.getKid());
		}
		t_order order = bs.findById(order_id,t_order.class);
		if(ChkUtil.isNotNull(address)){
			order.setAddress(address.getProvince()+address.getCity()+address.getRegion()+address.getStreet());
			order.setName(address.getMember_name());
			order.setPhone(address.getMobile1());
			bs.updateObj(order);
		}

		ModelAndView mv = new ModelAndView("redirect:/wx/pay/to_pay.htm?oid=" + order_id +"&wxid=bNH2Brsj8XVNbysvXbJya");
		return mv;
	}

	// 立即购买下单
	@RequestMapping("/buy_now")
	@ResponseBody
	public ModelAndView buy_now(String product_id, int number) {
		t_order order =  orderService.buyNowToOrder(product_id,number);
		return new ModelAndView("redirect:/wx/order_detail.htm?kid=" + order.getKid());
	}
	
	/**
	 * 我的订单
	 */
	@RequestMapping("/order_list")
	public ModelAndView order_list() {
		List<Map<String, Object>> OrderList = orderService.myOrderList();
		List<Map<String, Object>> stayPayOrder = orderService.myOrderList("下单中");
		List<Map<String, Object>> staySendOrder = orderService.myOrderList("已下单");
		List<Map<String, Object>> finishOrder = orderService.OrderList();
		List<Map<String, Object>> zfinishOrder = orderService.myOrderList("已完成");
		ModelAndView mv = new ModelAndView("/jsp_wx/client/order_list");
		mv.addObject("orderList", OrderList);
		mv.addObject("stayPayOrder", stayPayOrder);
		mv.addObject("staySendOrder", staySendOrder);
		mv.addObject("finishOrder", finishOrder);
		mv.addObject("zfinishOrder", zfinishOrder);
		return mv;
	}

	//取消订单
	@RequestMapping("/cancel_order")
	@ResponseBody
	public void cancel_order(String kid) {
		t_order order = bs.findById(kid, t_order.class);
		order.setStatus(-100);
		order.setStatus_order("已取消");
		
		bs.updateObj(order);
		this.setTipMsg(true, "已取消!", Tip.Type.success);
	}
	
	// 订单详情
	@RequestMapping("/order_detail")
	public ModelAndView order_detail(String kid) {
		t_member member = SessionTools.getCurrentLoginMember();
		t_order order = bs.findById(kid, t_order.class);
		t_member_address address = orderService.getDefaultAddress(member.getKid());
//		t_order_detail orderDetail = bs.findById(kid, t_order_detail.class);
//		t_product product =bs.findById(orderDetail.getProduct_id(), t_product.class);
//		t_order_logistics orderLogistics = bs.findById(orderDetail.getLogistics_id(), t_order_logistics.class);
		
	   SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	   String time = formatter.format(order.getCreated());
	   String pay_time = formatter.format(order.getPay_time());
	   String best_time = formatter.format(order.getBest_time());
	   List<Map<String, Object>> detailList = orderService.ListDetail(order.getKid());
		List<Map<String, Object>> addressList = orderService.getMemberAddress(member.getKid());

		ModelAndView mv = new ModelAndView("/jsp_wx/client/order_detail");
//		if(orderDetail.getLogistics_id()==null){
//			mv.addObject("logistics", "卖家未发货");
//		}
		mv.addObject("order", order);
		mv.addObject("time", time);
		mv.addObject("pay_time", pay_time);
		mv.addObject("best_time", best_time);
		mv.addObject("detailList", detailList);
		mv.addObject("addressList", addressList);
		mv.addObject("address", address);
		return mv;
	}
	
	// 物流详情
	@RequestMapping("/kuaidi")
	public ModelAndView kuaidi_list(String logisticsId,String orderDetailId) {
		t_order_logistics orderLogistics = bs.findById(logisticsId, t_order_logistics.class);
		t_order_detail orderDetail = bs.findById(orderDetailId, t_order_detail.class);
		t_product product =bs.findById(orderDetail.getProduct_id(), t_product.class);
		t_logistics logistics =orderService.getOrderLogistics(orderLogistics.getLogistics_com_code());
		
		ModelAndView mv = new ModelAndView("/jsp_wx/client/kuaidi");
		mv.addObject("orderLogistics", orderLogistics);
		mv.addObject("orderDetail", orderDetail);
		mv.addObject("logistics", logistics);
		mv.addObject("product", product);
		return mv;
	}
	
	// 确认收货
	@RequestMapping("/confirm_take")
	@ResponseBody
	public ModelAndView confirm_take(String logisticsId,String kid) {
		t_order_logistics orderLogistics = bs.findById(logisticsId, t_order_logistics.class);
		t_order_detail orderDetail = bs.findById(kid, t_order_detail.class); 
		t_product product =bs.findById(orderDetail.getProduct_id(), t_product.class);
		t_logistics logistics =orderService.getOrderLogistics(orderLogistics.getLogistics_com_code());
		t_order order =bs.findById(orderDetail.getOrder_id(), t_order.class);
		orderDetail.setStatus_order("已完成");
		bs.updateObj(orderDetail);
		
		order.setStatus_order("已完成");
		bs.updateObj(order);
		
		t_member member = bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
		new Thread(new Runnable() {
			t_shop_wx_config wxConfig = wxService.getShopWxConfig("电商");
			public void run() {
				System.out.println("＝＝＝＝＝＝＝＝＝＝电商客服消息＝＝＝＝＝＝＝＝＝＝");
				new CustomAPI(wxService.turnTToWx(wxConfig)).sendTextMsg(member.getOpenid(),"您的一个订单已完成,祝您生活愉快。 详情点击《我的订单详情》");
			}
		}).start();

		ModelAndView mv = new ModelAndView("/jsp_wx/client/kuaidi");
//		ModelAndView mv = new ModelAndView("/jsp_wx/client/order_finish");
		mv.addObject("orderLogistics", orderLogistics);
		mv.addObject("orderDetail", orderDetail);
		mv.addObject("product", product);
		mv.addObject("logistics", logistics);
//		mv.addObject("order", order);
		return mv;
	}
	
	// 切换分销商
	@RequestMapping("/cut_retail")
	@ResponseBody
	public ModelAndView cut_retail(String retail_id,String member_id) {
		t_member memberDb =bs.findById(member_id, t_member.class);//当前客户
		
		t_retailStore yretailStore =bs.findById(memberDb.getRetail_id(), t_retailStore.class);//原分销商
		t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("分销"); //分销微信配置信息
		
		if(ChkTools.isNotNull(yretailStore)){
			t_member ymember =bs.findById(yretailStore.getMember_id(), t_member.class);//原分销商用户
			
			new Thread(new Runnable() {
				public void run() {
					System.out.println("给"+ymember.getUser_nick()+"发送客服消息");
					new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(ymember.getOpenid(),"您的客户： “"+memberDb.getUser_nick()+"”	\r已与您取消客户关系,请点击《我的客户》查看详情!");
				}
			}).start();
		}
		
		t_retailStore retailStore =bs.findById(retail_id, t_retailStore.class); //当前分销商
		t_member fxmember = bs.findById(retailStore.getMember_id(), t_member.class); //当前分销用户
		
		new Thread(new Runnable() {
			public void run() {
				System.out.println("给"+fxmember.getUser_nick()+"发送客服消息");
				new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),"用户： “"+memberDb.getUser_nick()+"”	\r已成为您的客户,请点击《我的客户》查看详情!");
			}
		}).start();
		
		memberDb.setRetail_id(retail_id);
		bs.updateObj(memberDb);
		
		ModelAndView mv = new ModelAndView("/jsp_wx/fenxiao/cut_retail");
		mv.addObject("name", retailStore.getName());
		return mv;
	}
	
	// 订单退货/退款
	@RequestMapping("/order_return")
	public ModelAndView order_return(String orderDetailId) {
		t_order_detail orderDetail = bs.findById(orderDetailId, t_order_detail.class);
		t_order order = bs.findById(orderDetail.getOrder_id(), t_order.class);
		t_product product =bs.findById(orderDetail.getProduct_id(), t_product.class);
		t_order_return order_return = orderService.get_orderReturn(orderDetail.getKid());
		
		if(order_return == null){
			ModelAndView mv = new ModelAndView("/jsp_wx/client/order_return");
			mv.addObject("product", product);
			mv.addObject("orderDetail", orderDetail);
			mv.addObject("order_no", order.getOrder_no());
			if(ChkTools.isNotNull(orderDetail.getLogistics_id())){
				mv.addObject("return_type", "退货");
			}else{
				mv.addObject("return_type", "退款");
			}
			
			return mv;
		}else{
			return new ModelAndView("redirect:/wx/check_oreturn.htm?kid=" + order_return.getKid());
		}
	}
	
	// 提交退款/退款
	@RequestMapping("/submit_oreturn")
	@ResponseBody
	public String submit_oreturn(HttpServletRequest req) throws ParseException {
		t_order_return order_return = new t_order_return();
		order_return.setMember_id(SessionTools.getCurrentLoginMember().getKid());
		order_return.setProduct_id(req.getParameter("product_id"));
		order_return.setorder_detail_id(req.getParameter("order_detail_id"));
		order_return.setOrder_no(req.getParameter("order_no"));
		order_return.setNote(req.getParameter("note"));
		order_return.setProblem(req.getParameter("problem"));
		order_return.setReturn_money(new BigDecimal(req.getParameter("return_money")));
		order_return.setReturn_type(req.getParameter("return_type"));
		order_return.setOrder_status("申请审核中");
		order_return.setMoney_status("未退款");
		
		bs.saveObj(order_return);
		result.put("end", "true");
		result.put("msg", order_return.getKid());
		return JsonTools.toJson(result);
	}
	
	// 订单退货/退款  信息
	@RequestMapping("/check_oreturn")
	@ResponseBody
	public ModelAndView check_oreturn(String kid) {
		t_order_return order_return = bs.findById(kid, t_order_return.class);
		ModelAndView mv = new ModelAndView("/jsp_wx/client/check_oreruen");
		mv.addObject("order_return", order_return);
		return mv;
	}

	@RequestMapping("/order_eval")
	public ModelAndView order_eval(String oid) {
		t_order order = bs.findById(oid, t_order.class);
		List<Map<String, Object>> detailList = orderService.ListDetail(order.getKid());
		t_order_discuzz discuzz = orderService.getDiscuzz(order.getKid());
		ModelAndView mv = new ModelAndView("/jsp_wx/client/order_eval");
		mv.addObject("order", order);
		mv.addObject("discuzz", discuzz);
		mv.addObject("detailList", detailList);
		return mv;
	}

	@RequestMapping("/eval_submit")
	public ModelAndView eval_submit(t_order_discuzz discuzz) {
		t_order_discuzz discuzzDb = orderService.getDiscuzz(discuzz.getOrder_id());
		if (discuzzDb == null) {
			t_order order = bs.findById(discuzz.getOrder_id(), t_order.class);
			t_member cox = bs.findById(order.getMember_id(), t_member.class);
			// 保存评分
			orderService.saveDiscuzz(order, cox, discuzz);
		}

		return new ModelAndView("redirect:/wx/order_eval.htm?oid=" + discuzz.getOrder_id());
	}

}
