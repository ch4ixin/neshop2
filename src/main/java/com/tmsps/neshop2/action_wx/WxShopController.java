package com.tmsps.neshop2.action_wx;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4Weixin.api.CustomAPI;
import com.tmsps.ne4Weixin.beans.BaseBean;
import com.tmsps.ne4spring.utils.MD5Util;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.*;
import com.tmsps.neshop2.service.*;
import com.tmsps.neshop2.util.ChkTools;
import com.tmsps.neshop2.util.KdniaoTrackQueryAPI;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.util.wx.WxJsApiTools;
import com.tmsps.neshop2.web.SessionTools;
import com.tmsps.neshop2.web.WebTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@Scope("prototype")
@RequestMapping("/wx/shop")
public class WxShopController extends ProjBaseAction {

    @Autowired
    private ProductService productService;
    @Autowired
    OrderService orderService;
    @Autowired
    private ShopOrderService shopOrderService;
    @Autowired
    MemberService memberService;
    @Autowired
    WxService wxService;

    /**
     * 商户认证页面
     *
     * @return
     */
    @RequestMapping("/to_cox")
    public ModelAndView to_cox() {
        t_member member = SessionTools.getCurrentLoginMember();
        t_member memberDb = bs.findById(member.getKid(), t_member.class);
        if (ChkTools.isNotNull(memberDb.getShop_id())) {
            return new ModelAndView("redirect:/wx/shop/product.htm");
        }

        String url = WebTools.getRefererUrl();
        WxJsApiTools jsConfig = WxJsApiTools.getJsApiConfig(url);
        ModelAndView mv = new ModelAndView("/jsp_wx/shop/to_cox");
        mv.addObject("jsConfig", jsConfig);
        mv.addObject("memberDb", memberDb);
        return mv;
    }

    /**
     * 商户认证提交
     *
     * @param req
     * @return
     */
    @RequestMapping("/register")
    @ResponseBody
    public String register(HttpServletRequest req) {
        t_member member = SessionTools.getCurrentLoginMember();
        JSONObject result = new JSONObject();
        String shop_name = req.getParameter("shop_name");
        String shop_type = req.getParameter("shop_type");
        String name = req.getParameter("name");
        String mobile = req.getParameter("mobile");
        String city = req.getParameter("city");
        String province = req.getParameter("province");
        String user_id_p = req.getParameter("user_id_p");
        String user_id_n = req.getParameter("user_id_n");
        String shop_license = req.getParameter("shop_license");
        String shop_permit = req.getParameter("shop_permit");
        t_shop shop = new t_shop();
        shop.setType(shop_type);
        shop.setName(shop_name);
        shop.setLink_mobile(mobile);
        shop.setLevel("初级店铺");
        shop.setUser_id_n(user_id_n);
        shop.setUser_id_p(user_id_p);
        shop.setShop_license(shop_license);
        shop.setShop_permit(shop_permit);
        shop.setProvince(province);
        shop.setCity(city);
        shop.setVerify_status("无需审核");
        bs.saveObj(shop);

        t_shop_admin shopAdmin = new t_shop_admin();
        shopAdmin.setName(name);
        shopAdmin.setShop_id(shop.getKid());
        shopAdmin.setMobile(shop.getLink_mobile());
        shopAdmin.setType("负责人");
        shopAdmin.setPwd(MD5Util.MD5("123456"));
        bs.saveObj(shopAdmin);

        shop.setAdmin_id(shopAdmin.getKid());
        bs.updateObj(shop);

        t_member memberDb = bs.findById(member.getKid(), t_member.class);
        memberDb.setShop_id(shop.getKid());
        bs.updateObj(memberDb);

        result.put("end", "true");
        result.put("msg", "认证成功!");
        return result.toJSONString();
    }// #register

    /**
     * 商户编辑
     * @param shop
     * @return result
     */
    @RequestMapping("/edit")
    @ResponseBody
    public String edit(t_shop shop) {
        JSONObject result = new JSONObject();
        t_shop shopDb = bs.findById(shop.getKid(),t_shop.class);
//        shopDb.setType(shop.getType());
//        shopDb.setName(shop.getName());
//        shopDb.setLink_mobile(shop.getLink_mobile());
//        shopDb.setLevel(shop.getLevel());
//        shopDb.setUser_id_n(shop.getUser_id_n());
//        shopDb.setUser_id_p(shop.getUser_id_n());
        shopDb.setLogo_file_id(shop.getLogo_file_id());
//        shopDb.setShop_license(shop.getShop_license());
//        shopDb.setShop_permit(shop.getShop_permit());
//        shopDb.setProvince(shop.getProvince());
//        shopDb.setCity(shop.getCity());
//        shopDb.setVerify_status(shop.getVerify_status());
        bs.updateObj(shopDb);

        result.put("end", "true");
        result.put("msg", "编辑成功!");
        return result.toJSONString();
    }// #register

    /**
     * 商品管理
     * @return
     */
    @RequestMapping("/product")
    public ModelAndView product() {
        page.setPageSize(100);
        sort_params.put("created","desc");
        List<Map<String, Object>> cateList = productService.selectCateList();
        srh.put("status_sys","上架中");
        List<Map<String, Object>> onShelfList = productService.selectShopProductList(srh, sort_params, page);
        srh.put("status_sys","已下架");
        List<Map<String, Object>> offShelfList = productService.selectShopProductList(srh, sort_params, page);
        ModelAndView mv = new ModelAndView("/jsp_wx/shop/product");
        mv.addObject("cateList", cateList);
        mv.addObject("onShelfList", onShelfList);
        mv.addObject("offShelfList", offShelfList);
        return mv;
    }

    @RequestMapping("/product_list")
    @ResponseBody
    public String list_data(int pageNumber,String shelf,String name) {
        if(ChkTools.isNotNull(name)){
            srh.put("name",name);
        }
        page.setPageNumber(pageNumber);
        page.setPageSize(10);
        srh.put("status_sys",shelf);
        JSONObject result = new JSONObject();
        List<Map<String, Object>> list = productService.selectShopProductList(srh, sort_params, page);
        result.put("list", list);
        result.put("success", true);
        result.put("message", "操作成功");
        return result.toJSONString();
    }

    /**
     * 订单管理
     *
     * @return
     */
    @RequestMapping("/order")
    public ModelAndView order() {
        t_member member = SessionTools.getCurrentLoginMember();
        t_member memberDb = bs.findById(member.getKid(), t_member.class);
        t_shop shop = bs.findById(memberDb.getShop_id(),t_shop.class);
        String url = WebTools.getRefererUrl();
        WxJsApiTools jsConfig = WxJsApiTools.getJsApiConfig(url);
        List<Map<String, Object>> OrderList = orderService.shopOrderList();
        List<Map<String, Object>> stayPayOrder = orderService.shopOrderList("下单中");
        List<Map<String, Object>> staySendOrder = orderService.shopOrderList("已付款");
        List<Map<String, Object>> finishOrder = orderService.shopFinishOrderList();
        List<Map<String, Object>> logisticsList = shopOrderService.selectShopLogisticsParam();
//        List<Map<String, Object>> zfinishOrder = orderService.shopOrderList("已完成");
        ModelAndView mv = new ModelAndView("/jsp_wx/shop/order");
        mv.addObject("orderList", OrderList);
        mv.addObject("stayPayOrder", stayPayOrder);
        mv.addObject("staySendOrder", staySendOrder);
        mv.addObject("finishOrder", finishOrder);
        mv.addObject("logisticsList", logisticsList);
        mv.addObject("jsConfig", jsConfig);
        mv.addObject("shop", shop);
//        mv.addObject("zfinishOrder", zfinishOrder);
        return mv;
    }

    /**
     * 店铺管理
     *
     * @return
     */
    @RequestMapping("/store")
    public ModelAndView shop() {
        String url = WebTools.getRefererUrl();
        WxJsApiTools jsConfig = WxJsApiTools.getJsApiConfig(url);
        t_member memberDb = bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
        t_shop shop = bs.findById(memberDb.getShop_id(), t_shop.class);
        ModelAndView mv = new ModelAndView("/jsp_wx/shop/store");
        mv.addObject("shop", shop);
        mv.addObject("jsConfig", jsConfig);
        return mv;
    }

    /**
     * 商品发布
     *
     * @return
     */
    @RequestMapping("/product_add")
    @ResponseBody
    public String product_add(t_product product) {
        t_member memberDb = bs.findById(SessionTools.getCurrentLoginMember().getKid(), t_member.class);
        product.setShop_id(memberDb.getShop_id());
        product.setMain_file_id(product.getFile_ids().split(",")[0]);
        product.setContent("");
        bs.saveObj(product);
        result.put("end", "true");
        result.put("msg", "发布成功");
        return JsonTools.toJson(result);
    }

    /**
     * 商品编辑
     * @return
     */
    @RequestMapping("/product_edit")
    public ModelAndView product_edit(String kid) {
        t_product product = bs.findById(kid,t_product.class);
        List<Map<String, Object>> cateList = productService.selectCateList();
        ModelAndView mv = new ModelAndView("/jsp_wx/shop/product_edit");
        mv.addObject("product", product);
        mv.addObject("cateList", cateList);
        return mv;
    }

    /**
     * 商品编辑提交
     *
     * @return
     */
    @RequestMapping("/product_edit_submit")
    @ResponseBody
    public String product_edit_submit(t_product product) {
        t_product productDb = bs.findById(product.getKid(), t_product.class);
        productDb.setName(product.getName());
        productDb.setCate_code(product.getCate_code());
        productDb.setCarriage(product.getCarriage());
        productDb.setMore_carriage(product.getMore_carriage());
        productDb.setPrice(product.getPrice());
        productDb.setMarket_price(product.getMarket_price());
        productDb.setUnit(product.getUnit());
        productDb.setStatus_sys(product.getStatus_sys());
        productDb.setFile_ids(product.getFile_ids());
        productDb.setMain_file_id(product.getMain_file_id());
        bs.updateObj(productDb);
        result.put("end", "true");
        result.put("msg", "修改成功");
        return JsonTools.toJson(result);
    }

    /**
     * 商品上下架
     *
     * @return
     */
    @RequestMapping("/product_upper_or_lower")
    @ResponseBody
    public String product_upper_or_lower(t_product product) {
        t_product productDb = bs.findById(product.getKid(), t_product.class);
        productDb.setStatus_sys(product.getStatus_sys());
        bs.updateObj(productDb);
        result.put("end", "true");
        result.put("msg", "修改成功");
        return JsonTools.toJson(result);
    }

    /**
     * 商品上删除
     *
     * @return
     */
    @RequestMapping("/product_del")
    @ResponseBody
    public String product_del(String kid) {
        t_product product = bs.findById(kid, t_product.class);
        product.setStatus(-100);
        bs.updateObj(product);
        result.put("end", "true");
        result.put("msg", "删除成功");
        return JsonTools.toJson(result);
    }

    @RequestMapping("/deliver")
    @ResponseBody
    public String deliver(t_order_logistics logistics) {
        JSONObject result = new JSONObject();
        logistics.setStatus_logistics("发货中");
        logistics.setOrder_id(logistics.getOrder_id());
        logistics.setLogistics_com_code(logistics.getLogistics_com_code());
        logistics.setLogistics_code(logistics.getLogistics_code());

        // 修改订单的状态
        t_order order = shopOrderService.orderIdList(logistics.getOrder_id());
        order.setStatus_order("已发货");

        // 修改订单项状态
        t_order_detail order_detail = bs.findById(req.getParameter("order_detail_id"), t_order_detail.class);

        logger.info(order_detail.toJsonString());
        order_detail.setStatus_order("已发货");

        t_logistics tlogistics = shopOrderService.kdCodeList(logistics.getLogistics_com_code());

        // 快递接口
        JSONObject json = kd_api(tlogistics.getKd_code(), logistics.getLogistics_code());

        // 解析后的物流跟踪信息
        JSONArray ja = json.getJSONArray("Traces");
        Iterator<Object> it = ja.iterator();
        List<String> list = new ArrayList<String>();
        while (it.hasNext()) {
            JSONObject ob = (JSONObject) it.next();
            if (ob.getString("AcceptStation") != null) {
                list.add("*" + ob.getString("AcceptStation") + "\n" + "	" + ob.getString("AcceptTime") + "\n");
            }
        }
        String str = Arrays.toString(list.toArray());
        str = str.replace("[", " ").replace(",", "").replace("]", "");
        logistics.setExplain_traces(str);

        String state = json.getString("State");
        if ("2".equals(state) || "2" == state) {
            logistics.setState("在途中");
        } else if ("3".equals(state) || "3" == state) {
            logistics.setState("已签收");
            logistics.setStatus_logistics("已收货");
            order_detail.setStatus_order("已收货");
        } else if ("4".equals(state) || "4" == state) {
            logistics.setState("问题件");
        }


        bs.saveObj(logistics);
        logger.info(logistics.getKid());
        order_detail.setLogistics_id(logistics.getKid());
        logistics.setTraces(json.getString("Traces"));

        List<Map<String, Object>> OrderStateList = shopOrderService.fintOrderStateList(order_detail.getOrder_id());
        String swit = "on";
        for(int i =0;i<OrderStateList.size();i++){
            System.out.println(OrderStateList.get(i).get("status_order"));
            if(!"已收货".equals(OrderStateList.get(i).get("status_order"))){
                swit = "off";
            }
        }

        if(swit == "on"){
            order.setStatus_order("已收货");
        }
        bs.updateObj(order_detail);
        bs.updateObj(order);

        sendTxt(logistics, order, order_detail);
        this.setTipMsg(true, "订单已发货,物流状态:"+logistics.getState(), Tip.Type.success);
        result.put("end", "true");
        result.put("msg", "发货成功，物流状态："+logistics.getState());
        result.put("logistics_id", logistics.getKid());
        return JsonTools.toJson(result);
    }

    // 物流详情
    @RequestMapping("/kuaidi")
    public ModelAndView kuaidi_list(String orderDetailId) {
        t_order_detail orderDetail = bs.findById(orderDetailId, t_order_detail.class);
        t_order_logistics orderLogistics = bs.findById(orderDetail.getLogistics_id(), t_order_logistics.class);
        t_product product =bs.findById(orderDetail.getProduct_id(), t_product.class);
        t_logistics logistics =orderService.getOrderLogistics(orderLogistics.getLogistics_com_code());

        ModelAndView mv = new ModelAndView("/jsp_wx/shop/kuaidi");
        mv.addObject("orderLogistics", orderLogistics);
        mv.addObject("orderDetail", orderDetail);
        mv.addObject("logistics", logistics);
        mv.addObject("product", product);
        return mv;
    }

    /**
     * 发送用户消息
     * @return
     */
    @RequestMapping("/sendTextMsg")
    @ResponseBody
    public String sendTextMsg(String member_id, String content) {
        t_shop_wx_config wxConfig = wxService.getShopWxConfig("电商");
        t_member member = bs.findById(member_id, t_member.class);
        logger.info("＝＝＝＝＝＝＝＝＝＝电商客服消息＝＝＝＝＝＝＝＝＝＝");
        BaseBean baseBean = new CustomAPI(wxService.turnTToWx(wxConfig)).sendTextMsg(member.getOpenid(),content);
        logger.info(baseBean.toJSON());
        return baseBean.toJSON();
    }// #sendTextMsg

    private void sendTxt(t_order_logistics logistics, t_order order, t_order_detail order_detail) {
        t_member fxmember = bs.findById(order.getMember_id(), t_member.class);
        t_shop_wx_config fxWxConfig = wxService.getShopWxConfig("电商");
        new Thread(new Runnable() {
            public void run() {
                System.out.println("＝＝＝＝＝＝＝＝＝＝电商客服消息＝＝＝＝＝＝＝＝＝＝");
                String content = "您有一个商品@status,<a href='http://www.sxxlkj.com/neshop2/wx/kuaidi.htm?logisticsId=@logisticsId&orderDetailId=@orderDetailId'>查看物流</a>";
                content = content.replace("@status", order.getStatus_order());
                content = content.replace("@orderDetailId", order_detail.getKid());
                content = content.replace("@logisticsId", logistics.getKid());
                new CustomAPI(wxService.turnTToWx(fxWxConfig)).sendTextMsg(fxmember.getOpenid(),content);
            }
        }).start();
    }

    /**
     * 快递接口
     *
     * @param kd_code
     *            快递公司编码
     * @param logistics_code
     *            物流单号
     * @return json
     */
    public JSONObject kd_api(String kd_code, String logistics_code) {
        KdniaoTrackQueryAPI api = new KdniaoTrackQueryAPI();
        JSONObject json = null;
        String result = null;
        try {
            result = api.getOrderTracesByJson(kd_code, logistics_code.trim());
            json = JsonTools.jsonStrToJsonObject(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }
}