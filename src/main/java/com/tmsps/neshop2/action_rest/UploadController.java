package com.tmsps.neshop2.action_rest;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4Weixin.api.MediaApi;
import com.tmsps.ne4Weixin.utils.HttpClient;
import com.tmsps.neshop2.util.WxConfigTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.model.t_fk_file;
import com.tmsps.neshop2.service.SettingService;
import com.tmsps.neshop2.util.DateTools;
import com.tmsps.neshop2.util.file.FileTools;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.util.qrcode.QRTools;

@Controller
@Scope("prototype")
public class UploadController extends ProjBaseAction {

	@Autowired
	public SettingService settingService;

	@RequestMapping("/upload_form")
	public ModelAndView list(String area_parent_id) {
		ModelAndView mv = new ModelAndView("/jsp_cp/file/list");
		return mv;
	}

	/**
	 * 保存(网络)文件到服务器数据库
	 * @param upfile
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	private t_fk_file save_file(MultipartFile upfile) throws IllegalStateException, IOException{
		String suffix = FileTools.getSuffix(upfile.getOriginalFilename());
		String filename = System.currentTimeMillis() + "." + suffix;
		int year = DateTools.getYear();
		int month = DateTools.getMonth();
		String folder = year + File.separator + month;
		String folder_url = year + "/" + month;
		// 保存文件
		t_fk_file tf = new t_fk_file();
		tf.setFile_name(upfile.getOriginalFilename());
		tf.setNew_file_name(filename);
		tf.setFolder(folder);
		tf.setFolder_url(folder_url);
		tf.setContent_type(upfile.getContentType());
		tf.setSize(upfile.getSize());
		// 获取保存路径
		String DATA_PATH = settingService.getVal("DATA_PATH");

		File newFile = new File(DATA_PATH + File.separator + folder + File.separator + tf.getNew_file_name());
		if (!newFile.getParentFile().exists()) {
			newFile.getParentFile().mkdirs();
		}
		upfile.transferTo(newFile);
		bs.saveObj(tf);
		
		return tf;
	}
	
	/**
	 * 百度编辑器文件上传代理方法(公共方法)
	 * @param upfile
	 * @param type
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	private String upload_baidu_proxy(MultipartFile upfile,String type) throws IllegalStateException, IOException{
		t_fk_file tf = save_file(upfile);
		Map<String, Object> end = new HashMap<String, Object>();
		String file_name = upfile.getOriginalFilename();
		// 发送保存结果
		end.put("original", file_name);
		end.put("name", tf.getFile_name());
		//type 为img时 为图片上传(url 为图片展示功能)  为download是为文件上传(url为文件的下载功能)
		end.put("url", req.getContextPath()+type+"/"+tf.getKid()+".htm");
		end.put("size", upfile.getSize());
		end.put("type", file_name.substring(file_name.lastIndexOf(".")));
		end.put("state", "SUCCESS");
		logger.info(end);
		return JsonTools.toJson(end);
	}
	
	
	//百度编辑器图片上传
	@RequestMapping("/upload_baidu_image")
	public void upload_baidu_image(@RequestParam MultipartFile upfile) throws IllegalStateException, IOException {
		resp.setCharacterEncoding("utf-8");
		resp.getWriter().print(upload_baidu_proxy(upfile,"/img"));
		resp.getWriter().flush();
	}
	
	//百度编辑器文件上传
	@RequestMapping("/upload_baidu_file")
	public void upload_baidu_file(@RequestParam MultipartFile upfile) throws IllegalStateException, IOException {
		resp.setCharacterEncoding("utf-8");
		resp.getWriter().print(upload_baidu_proxy(upfile,"download"));
		resp.getWriter().flush();
	}
	
	@RequestMapping("/upload")
	public void upload(@RequestParam MultipartFile file) throws IllegalStateException, IOException {
		System.out.println(file);
		
		resp.setCharacterEncoding("utf-8");
		t_fk_file tf = save_file(file);
		Map<String, String> end = new HashMap<String, String>();
		// 发送保存结果
		end.put("file_id", tf.getKid());
		end.put("file_name", tf.getFile_name());
		end.put("content_type", tf.getContent_type());
		resp.getWriter().print(JsonTools.toJson(end));
		resp.getWriter().flush();
	}

	@RequestMapping("/getQRcodeFile")
	@ResponseBody
	public void getQRcodeFile(String cname) throws IOException{
		System.out.println(cname);
		resp.setCharacterEncoding("utf-8");
		int year = DateTools.getYear();
		int month = DateTools.getMonth();
		String folder = year + File.separator + month;
		String folder_url = year + "/" + month;
		
		// 获取保存路径
		String DATA_PATH = settingService.getVal("DATA_PATH");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		Date d=new Date();
		String str=sdf.format(d);
		String imgPath = DATA_PATH + File.separator + folder + File.separator + str+".png"; 
		String content= "http://192.168.1.104:8080/neshop2/wx/product.htm?cname="+cname+"&rank_code=DESC&test=ds";
		File file =QRTools.getQRCodeFile(content, imgPath);

		t_fk_file tf = new t_fk_file();
		// 保存文件
		tf.setFile_name(file.getName());
		tf.setNew_file_name(file.getName());
		tf.setFolder(folder);
		tf.setFolder_url(folder_url);
		tf.setContent_type("image/jpeg");
		tf.setSize(file.length());
		bs.saveObj(tf);

		Map<String, String> end = new HashMap<String, String>();
		// 发送保存结果
		end.put("file_id", tf.getKid());
		end.put("file_name", tf.getFile_name());
		end.put("content_type", tf.getContent_type());
		resp.getWriter().print(JsonTools.toJson(end));
		resp.getWriter().flush();
	}


	@RequestMapping("/wx_upload")
	@ResponseBody
	public String wx_upload(String media_id) throws FileNotFoundException {
		logger.info(media_id);
		String filename = System.currentTimeMillis() + ".jpg";

		int year = DateTools.getYear();
		int month = DateTools.getMonth();
		String folder = year + File.separator + month;
		String folder_url = year + "/" + month;

		// 获取保存路径
		String DATA_PATH = settingService.getVal("DATA_PATH");
		File newFile = new File(DATA_PATH + File.separator + folder + File.separator + filename);
		if (!newFile.getParentFile().exists()) {
			newFile.getParentFile().mkdirs();
		}

		OutputStream out = new FileOutputStream(newFile);
		new MediaApi(WxConfigTools.getWxConfig()).getTempMedia(media_id, out);
		logger.info(newFile.getName());
		// 保存文件
		t_fk_file tf = new t_fk_file();
		tf.setFile_name(filename);
		tf.setNew_file_name(filename);
		tf.setFolder(folder);
		tf.setFolder_url(folder_url);
		tf.setContent_type("image/jpeg");
		tf.setSize(newFile.length());

		bs.saveObj(tf);

		Map<String, String> end = new HashMap<String, String>();

		// 发送保存结果
		end.put("file_id", tf.getKid());
		end.put("file_name", tf.getFile_name());
		return JsonTools.toJson(end);
	}
}
