package com.tmsps.neshop2.action_member.order;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_order_return;
import com.tmsps.neshop2.service.OrderService;
import com.tmsps.neshop2.web.SessionTools;

/**
 * 订单管理
 * 
 * @author 冯晓东
 *
 */
@Controller("memOrder")
@Scope("prototype")
@RequestMapping("/member/order")
public class OrderController extends ProjBaseAction {

	@Autowired
	private OrderService orderService;

	@RequestMapping("/list")
	public ModelAndView list() {

		  List<Map<String,Object>> orderList= orderService.selectOrderList();
		  List<Map<String,Object>> order= orderService.selectByOrderId();                            
		  List<Map<String, Object>> orderProduct=orderService.selectByOrder();//待付款的产品
			
			int size = orderProduct.size();
		 
		ModelAndView mv = new ModelAndView("/jsp_member/order/list");
		
		mv.addObject("order",order);
		mv.addObject("orderList",orderList);
		mv.addObject("size",size);
		return mv;
	}

	// 我的订单 -待付款
	@RequestMapping("/noConfirm_order")
	public ModelAndView noConfirm_order() {
		List<Map<String, Object>> list = orderService.noConfirmList();//待付款的订单号
		List<Map<String, Object>> orderProduct=orderService.selectByOrder();//待付款的产品
		
		int size = orderProduct.size();
		
		ModelAndView mv = new ModelAndView("/jsp_member/order/noConfirm_order");
		mv.addObject("list", list);
		mv.addObject("orderProduct",orderProduct);
		mv.addObject("size", size);
		return mv;
	}

	// 我的订单--待发货

	@RequestMapping("/noSendGoods")
	public ModelAndView noSendGoods() {
		
		ModelAndView mv = new ModelAndView("/jsp_member/order/noSendGoods");
		
		return mv;
	}

	@RequestMapping("/del")
	@ResponseBody
	public String del(String product_id) {

		/*
		 * t_order_return member = bs.findById(kids, t_order_return.class);
		 * member.setMessage(message); member.setOrder_status("审核通过");
		 * bs.updateObj(member);
		 */

		return "succ";

	}

	// 退货退款及维修 提交页面
	@RequestMapping("/return")
	public ModelAndView returt_succ(String order_no, String product_id, String shop_id, BigDecimal cnt) {

		List<Map<String, Object>> list = orderService.selectReturnList(order_no, product_id, shop_id);

		ModelAndView mv = new ModelAndView("/jsp_member/order/return");
		mv.addObject("product_id", product_id);
		mv.addObject("shop_id", shop_id);
		mv.addObject("order_no", order_no);
		mv.addObject("list", list);
		mv.addObject("cnt", cnt);
		return mv;
	}

	// 退货退款及维修 提交成功页面
	@RequestMapping("/return_succ")
	public ModelAndView return_succ(String product_id, String main_file_id, String order_no, String back_type,
			BigDecimal tui_goods_number, String back_reason, String back_pay, String back_postscript) {

		t_member member = SessionTools.getCurrentLoginMember();

		// 保存数据
		t_order_return table = new t_order_return();

		//table.setShop_picture(main_file_id);
		table.setMoney_status("未退款");
		table.setOrder_status("申请审核中 ");
		table.setProduct_id(product_id);
		table.setMember_id(member.getKid());
		table.setOrder_no(order_no);
		table.setReturn_type(back_type);
		table.setNumber(tui_goods_number);
		table.setProblem(back_reason);
		//table.setReturn_money(back_pay);
		table.setNote(back_postscript);
		bs.saveObj(table);

		ModelAndView mv = new ModelAndView("/jsp_member/order/return_succ");

		return mv;
	}

	// 退货退款及维修订单列表

	@RequestMapping("/back")
	public ModelAndView back_list() {

		List<Map<String, Object>> backList = orderService.SelectBackList();

		ModelAndView mv = new ModelAndView("/jsp_member/order/back");
		mv.addObject("backList", backList);
		return mv;
	}

	// 退货时进行唯一性判断,退了的就不能再退
	@RequestMapping("/back_only")
	@ResponseBody
	public String back_only(String product_id) {
		t_order_return backList = orderService.getOrderReturnList();

		if (backList.getProduct_id().equals(product_id)) {
			return "succ";
		}
		return "false";

	}

	// 退货退款及维修状态表

	@RequestMapping("/back_status")
	public ModelAndView back_status(String product_id) {

		List<Map<String, Object>> backList = orderService.SelectBackById(product_id);

		ModelAndView mv = new ModelAndView("/jsp_member/order/back_status");
		mv.addObject("backList", backList);
		return mv;
	}

	// 订单详情
	@RequestMapping("/order")
	public ModelAndView order(String product_id,String order_no) {
        
        List<Map<String, Object>>list=orderService.CheckList(product_id,order_no);
       
		ModelAndView mv = new ModelAndView("/jsp_member/order/order");
        
		mv.addObject("list",list);
		mv.addObject("order_no",order_no);
		return mv;
	}

}
