package com.tmsps.neshop2.action_member.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.ne4spring.utils.ChkUtil;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.web.SessionTools;
import com.tmsps.neshop2.web.WebTools;

@Controller("loginMember")
@Scope("prototype")
public class LoginController extends ProjBaseAction {

	@Autowired
	MemberService memService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(String login_to_url) {
		if(ChkUtil.isNotNull(login_to_url)){
			SessionTools.put(SessionTools.LOGIN_TO_URL, login_to_url);
		}
		
		ModelAndView mv = new ModelAndView("/jsp_member/login/login");
		return mv;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login_post(String username, String password) {
		t_member member = memService.findMember(username, password);
		if (member != null) {
			SessionTools.put(SessionTools.LOGIN_MEMBER, member);
			
			String loginToUrl = SessionTools.getLoginToUrl();
			if (ChkUtil.isNotNull(loginToUrl)) {
				SessionTools.put(SessionTools.LOGIN_TO_URL, null);
				return new ModelAndView("redirect:" + loginToUrl);
			} else {
				return new ModelAndView("redirect:/member/frame/frame.htm");
			}

		} else {
			this.setTipMsg(false, "登陆失败,账号密码错误!", Tip.Type.error);
			return login(null);
		}

	}// #login

	// 询问登陆情况
	@RequestMapping("/login_act_ajax")
	@ResponseBody
	public String login_act_ajax() {
		t_member member = SessionTools.getCurrentLoginMember();

		if (member != null) {
			member.setPwd(null);
			result.put("is_login", "true");
		} else {
			result.put("is_login", "false");
		}

		result.put("member", member);
		return JsonTools.toJson(result);
	}

	@RequestMapping("/login_check_yzm")
	@ResponseBody
	public String login_check_yzm() {
		t_member member = SessionTools.getCurrentLoginMember();

		if (member != null) {
			member.setPwd(null);
			result.put("islogin", 0);
		} else {
			result.put("islogin", 1);
		}

		result.put("error", 0);
		result.put("message", "");
		result.put("member", member);
		return JsonTools.toJson(result);
	}

	@RequestMapping("/logout")
	public String logout() {
		WebTools.getSession().invalidate();
		super.setTipMsg("退出成功！", Tip.Type.success);
		return "redirect:/index.htm";
	}// #logout_cp

}
