package com.tmsps.neshop2.action_member.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.service.MemberService;

@Controller
@Scope("prototype")
public class PwdController extends ProjBaseAction {

	@Autowired
	MemberService memService;

	@RequestMapping(value = "/get_pwd", method = RequestMethod.GET)
	public ModelAndView login() {

		ModelAndView mv = new ModelAndView("/jsp_member/login/get_pwd");
		return mv;
	}

}
