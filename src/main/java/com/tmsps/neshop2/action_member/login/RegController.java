package com.tmsps.neshop2.action_member.login;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.ne4spring.utils.MD5Util;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.base.tip.Tip;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.service.SmsService;
import com.tmsps.neshop2.util.ChkTools;
import com.tmsps.neshop2.web.SessionTools;

@Controller("regMember")
@Scope("prototype")
public class RegController extends ProjBaseAction {

	@Autowired
	private SmsService smsSevice;

	@RequestMapping("/reg")
	public ModelAndView reg() {

		ModelAndView mv = new ModelAndView("/jsp_member/login/reg");
		return mv;
	}

	@RequestMapping("/submit")
	public ModelAndView submit(String mobile, String pwd, String mobile_code) {
		if (!mobile_code.equals(SessionTools.get(SessionTools.SMS_CODE))) {
			return new ModelAndView("redirect:/reg.htm");
		}
		t_member member = new t_member();
		member.setUname(mobile);
		member.setMobile(mobile);
		member.setPwd(MD5Util.MD5(pwd));
		bs.saveObj(member);
		this.setTipMsg("注册成功!", Tip.Type.success);
		ModelAndView mv = new ModelAndView("/jsp_member/login/login");
		return mv;
	}

	@RequestMapping("/sms_member")
	@ResponseBody
	public String sms_member(String mobile) {
		if (ChkTools.isNotNull(mobile)) {
			String code = RandomStringUtils.randomNumeric(4);
			Boolean result = smsSevice.sendSms(mobile, "reg", code);
			SessionTools.put(SessionTools.SMS_CODE, code);
			if (result) {
				this.setTipMsg("发送成功!", Tip.Type.success);
				return "ok";
			} else {
				this.setTipMsg("发送失败!", Tip.Type.error);
				return "no";
			}

		}
		return "no";
	}

}
