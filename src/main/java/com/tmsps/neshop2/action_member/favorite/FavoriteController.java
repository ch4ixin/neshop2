package com.tmsps.neshop2.action_member.favorite;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.service.MemberFavoriteService;

/**
 * 收藏的 店铺/商品 管理
 * 
 * @author 冯晓东
 *
 */
@Controller("memFavorite")
@Scope("prototype")
@RequestMapping("/member/favorite")
public class FavoriteController extends ProjBaseAction {

	@Autowired
	private MemberFavoriteService MFService;
	

	@RequestMapping("shop_list")
	public ModelAndView shop_list() {
		
	     List<Map<String,Object>> shopList	=MFService.selectShopList();
	
		ModelAndView mv = new ModelAndView("/jsp_member/favorite/shop_list");
		mv.addObject("shopList",shopList);
		return mv;
	}

	@RequestMapping("product_list")
	public ModelAndView product_list() {
            
		List<Map<String, Object>> collectionList = MFService.selectCollectionList();
	     
		
		ModelAndView mv = new ModelAndView("/jsp_member/favorite/product_list");
		mv.addObject("collectionList", collectionList);
		return mv;
	}

}
