package com.tmsps.neshop2.action_member.frame;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.service.MemberFavoriteService;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.service.OrderService;
import com.tmsps.neshop2.web.SessionTools;

/**
 * 账户余额跟积分管理
 * 
 * @author 张杰2016 8.12
 *
 */
@Controller("frameMember")
@Scope("prototype")
@RequestMapping("/member/frame")
public class FrameController extends ProjBaseAction {

	@Autowired
	private MemberFavoriteService MFService;
	@Autowired
	MemberService memService;
	@Autowired
	private OrderService orderService;


	@RequestMapping("/frame")
	public ModelAndView frame() {
         
		List<Map<String, Object>> order=orderService.selectOrderList();
		List<Map<String, Object>> orderList = orderService.selectByOrderId();
		List<Map<String, Object>> collectionList = MFService.selectCollectionList();
		t_member member = SessionTools.getCurrentLoginMember();
		
		
		ModelAndView mv = new ModelAndView("/jsp_member/frame/frame");
		mv.addObject("collectionList", collectionList);
		mv.addObject("member",member);
		mv.addObject("order",order);
		mv.addObject("orderList",orderList);
		return mv;
	}

	@RequestMapping("/user")
	public ModelAndView user() {
		t_member member = SessionTools.getCurrentLoginMember();
		ModelAndView mv = new ModelAndView("/jsp_member/frame/user");
		mv.addObject("member", member);
		return mv;
	}

	// 根据memberId查询支付金额明细数据
	@RequestMapping("/balance")
	public ModelAndView balance() {
		t_member member = SessionTools.getCurrentLoginMember();
		List<Map<String, Object>> payMoney = memService.findToMoney(member.getKid());
		ModelAndView mv = new ModelAndView("/jsp_member/frame/balance");
		mv.addObject("member", member);
		mv.addObject("payMoney", payMoney);
		return mv;
	}

	// 根据memberId查询积分明细数据
	@RequestMapping("/integral")
	public ModelAndView integral() {
		t_member member = SessionTools.getCurrentLoginMember();
		List<Map<String, Object>> inte = memService.findToIntegral(member.getKid());

		ModelAndView mv = new ModelAndView("/jsp_member/frame/integral");
		mv.addObject("member", member);
		mv.addObject("inte", inte);
		return mv;
	}
}
