package com.tmsps.neshop2.action_member.member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.ne4spring.utils.MD5Util;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.web.SessionTools;

/**
 * 用户信息
 * 
 * @author 冯晓东
 *
 */
@Controller("memMember")
@Scope("prototype")
@RequestMapping("/member/member")
public class MemberController extends ProjBaseAction {

	@Autowired
	MemberService memService;

	@RequestMapping("/security")
	public ModelAndView security() {
		ModelAndView mv = new ModelAndView("/jsp_member/member/security");
		return mv;
	}

	@RequestMapping("/set_pwd")

	public ModelAndView set_pwd() {
		t_member member = SessionTools.getCurrentLoginMember();

		ModelAndView mv = new ModelAndView("/jsp_member/member/set_pwd");
		mv.addObject("member", member);
		return mv;
	}

	@RequestMapping("/user")
	public ModelAndView user() {
		t_member member = SessionTools.getCurrentLoginMember();
		member = bs.findById(member.getKid(), t_member.class);

		ModelAndView mv = new ModelAndView("/jsp_member/member/user");
		mv.addObject("member", member);
		return mv;
	}

	@RequestMapping("/user_deit")
	public ModelAndView user_deit(String username, String kid, String province, String city, int sex) {

		t_member member = bs.findById(kid, t_member.class);
		member.setUser_sex(sex);
		member.setUser_province(province);
		member.setUser_city(city);
		member.setUser_nick(username);

		bs.updateObj(member);
		ModelAndView mv = new ModelAndView("/jsp_member/member/edit_message");
		mv.addObject("member", member);
		return mv;
	}

	@RequestMapping("/deit")
	public ModelAndView deit(String new_pwd, String old_pwd) {

		String pwd = MD5Util.MD5(old_pwd);
		t_member member = SessionTools.getCurrentLoginMember();
		t_member m = memService.findUser(member.getKid());

		if (pwd.equals(m.getPwd())) {

			t_member new_member = bs.findById(member.getKid(), t_member.class);
			new_member.setPwd(MD5Util.MD5(new_pwd));
			bs.updateObj(new_member);

			ModelAndView mv = new ModelAndView("/jsp_member/login/login");
			return mv;

		} else {

			ModelAndView mv = new ModelAndView("/jsp_member/member/set_pwd");
			return mv;
		}

	}

}
