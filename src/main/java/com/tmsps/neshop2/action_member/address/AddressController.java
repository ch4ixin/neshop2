package com.tmsps.neshop2.action_member.address;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.tmsps.neshop2.base.action.ProjBaseAction;
import com.tmsps.neshop2.model.t_member_address;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.web.SessionTools;

/**
 * 我的地址管理
 * 
 * @author 张杰 8.11
 *
 */
@Controller("memAddress")
@Scope("prototype")
@RequestMapping("/member/address")
public class AddressController extends ProjBaseAction {

	@Autowired
	MemberService memService;

	@RequestMapping("list")
	public ModelAndView list() {
		String memberId = SessionTools.getCurrentLoginMember().getKid();
		List<Map<String, Object>> addr = memService.selectCartList(memberId);
		ModelAndView mv;
		if (addr.size() > 0) {
			mv = new ModelAndView("/jsp_member/address/list");
			mv.addObject("addr", addr);
		} else {
			mv = new ModelAndView("/jsp_member/address/new");
		}
		return mv;
	}

	// 删除address信息
	@RequestMapping("/del_to_address")
	@ResponseBody
	public String del_to_address(String address_id) {

		t_member_address address = memService.findToCartAddress(address_id);
		address.setStatus(-100);
		bs.updateObj(address);
		JSONObject end = new JSONObject();
		end.put("error", 0);
		end.put("message", "删除成功");
		return JsonTools.toJson(end);
	}

	// 添加address信息
	@RequestMapping("/add_to_address")
	@ResponseBody
	public String add_to_address(String address) {

		memService.addToCartAddress(address);
		JSONObject end = new JSONObject();
		end.put("error", 0);
		end.put("message", "保存成功");
		return JsonTools.toJson(end);
	}

	// 添加空address的信息
	@RequestMapping("/editMessage_to_address")
	@ResponseBody
	public String editMessage_to_address(String address) {
		memService.addToCartAddress1(address);
		JSONObject end = new JSONObject();
		end.put("error", 0);
		end.put("message", "保存成功");
		return JsonTools.toJson(end);
	}

	// 修改address信息
	@RequestMapping("/editMeg_to_address")
	@ResponseBody
	public String editMeg_to_address(String address) {
		JSONObject json = JsonTools.jsonStrToJsonObject(address);
		t_member_address addr = memService.findToCartAddress(json.getString("address_id"));
		addr.setProvince(json.getString("province"));
		addr.setCity(json.getString("city"));
		addr.setRegion(json.getString("district"));
		addr.setStreet(json.getString("address"));
		addr.setMember_name(json.getString("consignee"));
		addr.setStatus(0);
		addr.setMobile1(json.getString("mobile"));
		addr.setMobile2(json.getString("mobile2"));
		bs.updateObj(addr);
		JSONObject end = new JSONObject();
		end.put("error", 0);
		end.put("message", "修改成功");
		return JsonTools.toJson(end);
	}

	@RequestMapping("/get_address")
	@ResponseBody
	public String get_address(String address_id) {
		t_member_address addr = memService.findToCartAddress(address_id);
		
		return JsonTools.toJson(addr);
	}
}
