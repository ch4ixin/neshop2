package com.tmsps.neshop2.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tmsps.neshop2.util.ChkTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.tmsps.ne4Weixin.api.OAuthAPI;
import com.tmsps.ne4Weixin.beans.OAuthToken;
import com.tmsps.ne4Weixin.beans.UserInfo;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.ne4spring.base.IBaseService;
import com.tmsps.neshop2.model.t_member;
import com.tmsps.neshop2.model.t_shop_wx_config;
import com.tmsps.neshop2.service.MemberService;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.json.JsonTools;
import com.tmsps.neshop2.web.SessionTools;

/**
 * 分页拦截器，定义拦截路径 /wx
 * 
 * @author uninf
 * 
 */
public class WxInterceptor implements HandlerInterceptor {

	@Autowired
	IBaseService bs;
	@Autowired
	WxService wxService;
	@Autowired
	MemberService memberService;
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		t_member member = SessionTools.getCurrentLoginMember();

		logger.info("member={}", member);
		if (member == null) {
			String test = request.getParameter("test");
			String code = request.getParameter("code");

			member = memberService.findMemberByWx(test);
			if (ChkTools.isNotNull(member)) {
				SessionTools.put(SessionTools.LOGIN_MEMBER, member);
				return true;
			}

			if (code == null) {
				// 跳转到登陆页面
				WxConfig wxConfig = wxService.getShopWxByType("电商");
				String referer = OAuthAPI.getRefererUrl(request);
				String oAuthUrl = new OAuthAPI(wxConfig).getOAuthUrl(referer, OAuthAPI.SNSAPI_USERINFO, null);
				
				logger.info("code为空,重定向={}", oAuthUrl);
				response.sendRedirect(oAuthUrl);

				return false;
			} else {
				t_shop_wx_config config = wxService.getShopWxConfig("电商");
				WxConfig wxConfig = wxService.turnTToWx(config);
				OAuthAPI api = new OAuthAPI(wxConfig);
				OAuthToken token = api.getToken(code);
				logger.info("code={},token={}", code, JsonTools.toJson(token));
				String openid = token.getOpenid();

//				member = memberService.findMemberByWx(openid);
				UserInfo userInfo = api.getUserInfo(token.getAccess_token(), openid);
				logger.info("openid={},userinfo={}", openid, JsonTools.toJson(userInfo));
				t_member memberDb = memberService.subscribe(userInfo, config.getOriginal_id(), config.getType());
				
				SessionTools.put(SessionTools.LOGIN_MEMBER, memberDb);
				logger.info("设置session={}", JsonTools.toJson(SessionTools.getCurrentLoginMember()));
			}

			return true;
		}
		return true;

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}

}
