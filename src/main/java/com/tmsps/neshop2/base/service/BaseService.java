package com.tmsps.neshop2.base.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import com.tmsps.ne4spring.base.IBaseService;

public class BaseService {
	protected Logger logger = Logger.getLogger(getClass());

	@Autowired
	protected JdbcTemplate jt;
	@Autowired
	protected IBaseService bs;
}
