package com.tmsps.neshop2.model;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 商品分类表
 */

@Table
public class t_product_cate extends DataModel {

	private int code;// 编码
	private String name;
	private String main_file_id;// 商品主图片
	private String ad_file_id;// 主广告
	private String icon_file_id;// 小图标
	private String icon;// 图标 class中的值

	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMain_file_id() {
		return main_file_id;
	}

	public void setMain_file_id(String main_file_id) {
		this.main_file_id = main_file_id;
	}

	public String getAd_file_id() {
		return ad_file_id;
	}

	public void setAd_file_id(String ad_file_id) {
		this.ad_file_id = ad_file_id;
	}

	public String getIcon_file_id() {
		return icon_file_id;
	}

	public void setIcon_file_id(String icon_file_id) {
		this.icon_file_id = icon_file_id;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}
