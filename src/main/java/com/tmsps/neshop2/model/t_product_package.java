package com.tmsps.neshop2.model;

import java.math.BigDecimal;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 套餐对应的商品表
 */

@Table
public class t_product_package extends DataModel {

	private String shop_id;// 店铺id

	private String name;
	private BigDecimal price;// 价格
	private int integral;// 所需积分
	private String main_file_id;// 套餐主图片
	private String file_ids;// 套餐图片集合,逗号分隔
	private String cate_code;// 分组code
	private int stock;// 库存量

	private String content_app;// 详细描述 - App端
	private String content;// 详细描述

	private String status_sys;// 状态 : 上架中 or 已下架
	private String product_id_dest;// 最终套餐对应的商品 id

	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public int getStatus() {
		return status;
	}

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}

	public String getProduct_id_dest() {
		return product_id_dest;
	}

	public void setProduct_id_dest(String product_id_dest) {
		this.product_id_dest = product_id_dest;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public long getCreated() {
		return created;
	}

	public String getContent_app() {
		return content_app;
	}

	public void setContent_app(String content_app) {
		this.content_app = content_app;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getIntegral() {
		return integral;
	}

	public void setIntegral(int integral) {
		this.integral = integral;
	}

	public String getMain_file_id() {
		return main_file_id;
	}

	public void setMain_file_id(String main_file_id) {
		this.main_file_id = main_file_id;
	}

	public String getFile_ids() {
		return file_ids;
	}

	public void setFile_ids(String file_ids) {
		this.file_ids = file_ids;
	}

	public String getCate_code() {
		return cate_code;
	}

	public void setCate_code(String cate_code) {
		this.cate_code = cate_code;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStatus_sys() {
		return status_sys;
	}

	public void setStatus_sys(String status_sys) {
		this.status_sys = status_sys;
	}

}