package com.tmsps.neshop2.model;

import java.math.BigDecimal;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 商品表
 */

@Table
public class t_product extends DataModel {
	private int show_id;// 数据库自增长id
	private String shop_id;//店铺ID
	private String code;// 货号
	private String name;
	private String brand_name; // 商品品牌
	private String layout_id; // 关联版式

	private BigDecimal market_price; // 市场价格
	private BigDecimal carriage; // 运费
	private BigDecimal more_carriage; // 多件续运费
	private String brief;// 商品简述
	private BigDecimal price;// 价格
	private int integral;// 所需积分
	private String main_file_id;// 商品主图片
	private String file_ids;// 商品图片集合,逗号分隔
	private String cate_code;// 分组code
	private String cate_code_shop;// 店铺分组
	private int stock;// 库存量
	private String reason; // 下架原因

	private String content_app;// 详细描述 - App端
	private String content;// 详细描述
	private String unit;// 规格（单位）

	private String status_sys;// 状态 : 上架中 or 已下架
	private String type;// 类型: 商品 or 套餐
	private int rank_code;// 排序代码

	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public int getStatus() {
		return status;
	}

	public String getLayout_id() {
		return layout_id;
	}

	public void setLayout_id(String layout_id) {
		this.layout_id = layout_id;
	}

	public BigDecimal getMarket_price() {
		return market_price;
	}

	public void setMarket_price(BigDecimal market_price) {
		this.market_price = market_price;
	}

	public String getBrand_name() {
		return brand_name;
	}

	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getMain_file_id() {
		return main_file_id;
	}

	public void setMain_file_id(String main_file_id) {
		this.main_file_id = main_file_id;
	}

	public String getFile_ids() {
		return file_ids;
	}

	public void setFile_ids(String file_ids) {
		this.file_ids = file_ids;
	}

	public String getCate_code() {
		return cate_code;
	}

	public void setCate_code(String cate_code) {
		this.cate_code = cate_code;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getCate_code_shop() {
		return cate_code_shop;
	}

	public void setCate_code_shop(String cate_code_shop) {
		this.cate_code_shop = cate_code_shop;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStatus_sys() {
		return status_sys;
	}

	public void setStatus_sys(String status_sys) {
		this.status_sys = status_sys;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getContent_app() {
		return content_app;
	}

	public void setContent_app(String content_app) {
		this.content_app = content_app;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getIntegral() {
		return integral;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setIntegral(int integral) {
		this.integral = integral;
	}

	public int getShow_id() {
		return show_id;
	}

	public void setShow_id(int show_id) {
		this.show_id = show_id;
	}

	public String getType() {
		return type;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getCarriage() {
		return carriage;
	}

	public void setCarriage(BigDecimal carriage) {
		this.carriage = carriage;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public BigDecimal getMore_carriage() {
		return more_carriage;
	}

	public void setMore_carriage(BigDecimal more_carriage) {
		this.more_carriage = more_carriage;
	}

	public int getRank_code() {
		return rank_code;
	}

	public void setRank_code(int rank_code) {
		this.rank_code = rank_code;
	}

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}
}