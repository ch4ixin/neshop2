package com.tmsps.neshop2.model;

import java.math.BigDecimal;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 店铺的物流设置
 *
 * @author 闫无恙
 */

@Table
public class t_shop_logistics extends DataModel {

	private String shop_id;
	private String logistics_codes;// 物流公司的 code 集合, 逗号分隔
	private BigDecimal free_money;// 免运费额度
	private BigDecimal freight_money;//运费金额

	// 打印备注信息将出现在打印订单的下方位置，用于注明店铺简介或发货、
	// 退换货相关规则等；内容不要超过100字。
	private String note;// 备注信息
	private String seal_file_id;// 印章图片

	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public void setCreated(long created) {
		this.created = created;
	}

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}

	public String getLogistics_codes() {
		return logistics_codes;
	}

	public void setLogistics_codes(String logistics_codes) {
		this.logistics_codes = logistics_codes;
	}

	public BigDecimal getFree_money() {
		return free_money;
	}

	public void setFree_money(BigDecimal free_money) {
		this.free_money = free_money;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSeal_file_id() {
		return seal_file_id;
	}

	public void setSeal_file_id(String seal_file_id) {
		this.seal_file_id = seal_file_id;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getFreight_money() {
		return freight_money;
	}

	public void setFreight_money(BigDecimal freight_money) {
		this.freight_money = freight_money;
	}

	
}
