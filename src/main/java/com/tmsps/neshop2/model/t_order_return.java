package com.tmsps.neshop2.model;

import java.math.BigDecimal;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 订单表--退款/货表
 */

@Table
public class t_order_return extends DataModel {

	private String member_id;
	private String note;// 退货说明
	private String product_id;// 商品的id
	private String  problem; //退货原因
	private String order_no;// 原单号订单号
	private BigDecimal number;//  退货的数量
	private String return_type;// 退换类型: 退货 or 退款
	private BigDecimal return_money;// 退款类型： 退款至账户余额 or 原支付方式返回
	private String order_detail_id;  //order_detail_id   shop_picture修改为order_detail_id
	private String message;  //商店给客户的是否退货留言

	private String money_status ;//退款状态 ： 未退款   or 已退款
	

	private String  order_status; //订单状态 审核完成  or 申请审核中  or 拒绝退货
	
	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;
	

	// ======= get / set ()=====================
  
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getMoney_status() {
		return money_status;
	}

	public void setMoney_status(String money_status) {
		this.money_status = money_status;
	}
	
	public String getOrder_status() {
		return order_status;
	}

	public void setOrder_status(String order_status) {
		this.order_status = order_status;
	}
	
	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}
	
	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public String getorder_detail_id() {
		return order_detail_id;
	}

	public void setorder_detail_id(String order_detail_id) {
		this.order_detail_id = order_detail_id;
	}
	
	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public BigDecimal getNumber() {
		return number;
	}

	public void setNumber(BigDecimal tui_goods_number) {
		this.number = tui_goods_number;
	}

	public String getReturn_type() {
		return return_type;
	}

	public void setReturn_type(String return_type) {
		this.return_type = return_type;
	}

	public BigDecimal getReturn_money() {
		return return_money;
	}

	public void setReturn_money(BigDecimal return_money) {
		this.return_money = return_money;
	}

	public String getOrder_detail_id() {
		return order_detail_id;
	}

	public void setOrder_detail_id(String order_detail_id) {
		this.order_detail_id = order_detail_id;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

   
	

}