package com.tmsps.neshop2.model;
import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 订单发货
 *
 * @author 闫无恙
 */

@Table
public class t_order_logistics extends DataModel {
	private String order_id;
	private String logistics_com_code;// 物流公司编号
	private String logistics_code;// 物流公司的发货编号
	private String status_logistics;// 发货状态: 发货中 or 已收货
	private String shop_admin_id;// 发货人
	private String traces;// 物流信息
	private String state;	// 物流状态：在途中, 已签收, 问题件
	private String explain_traces;// 解析后的 快递跟踪信息

	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public void setCreated(long created) {
		this.created = created;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getLogistics_com_code() {
		return logistics_com_code;
	}

	public void setLogistics_com_code(String logistics_com_code) {
		this.logistics_com_code = logistics_com_code;
	}

	public String getLogistics_code() {
		return logistics_code;
	}

	public void setLogistics_code(String logistics_code) {
		this.logistics_code = logistics_code;
	}

	public String getStatus_logistics() {
		return status_logistics;
	}

	public void setStatus_logistics(String status_logistics) {
		this.status_logistics = status_logistics;
	}

	public String getShop_admin_id() {
		return shop_admin_id;
	}

	public void setShop_admin_id(String shop_admin_id) {
		this.shop_admin_id = shop_admin_id;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTraces() {
		return traces;
	}

	public void setTraces(String traces) {
		this.traces = traces;
	}

	public String getExplain_traces() {
		return explain_traces;
	}

	public void setExplain_traces(String explain_traces) {
		this.explain_traces = explain_traces;
	}

}
