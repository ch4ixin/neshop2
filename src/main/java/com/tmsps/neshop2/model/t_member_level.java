package com.tmsps.neshop2.model;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

@Table
public class t_member_level extends DataModel {

	// ========== 系统字段 ========================
	private String name;//名称
	private String level;//等级
	private String integral;//积分
	private String note;//描述
	
	@PK
	private String kid; //代表一条数据      	唯一
	private int status; // 状态   0  -100
	private long created = System.currentTimeMillis(); // 数据的创建时间

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getIntegral() {
		return integral;
	}

	public void setIntegral(String integral) {
		this.integral = integral;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
}

	

	
	
	
	
	