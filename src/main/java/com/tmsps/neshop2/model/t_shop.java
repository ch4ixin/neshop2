package com.tmsps.neshop2.model;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 店铺表
 */

@Table
public class t_shop extends DataModel {

	private String type;// 类型: 个人店铺 or 企业店铺
	private String name;// 店铺名称
	private String admin_id;// 超级管理员kid
	private String link_man;// 负责人
	private String link_mobile;// 负责人手机号
	private String level;//店铺等级
	private String logo_file_id;// 企业logo
	private String user_id_p;// 负责人身份证正面
	private String user_id_n;// 负责人身份证反面
	private String shop_license;//企业营业执照
	private String shop_permit;//企业许可证
	private String latitude;// 维度
	private String longitude;// 经度
	private String province;// 省份
	private String city;// 城市
	private String address;// 地址

	private String verify_status;// 审核状态:待审核 or 已审核 or 审核失败 or 无需审核
	private String verify_note;// 审核备注

	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getAdmin_id() {
		return admin_id;
	}

	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}

	public String getVerify_status() {
		return verify_status;
	}

	public void setVerify_status(String verify_status) {
		this.verify_status = verify_status;
	}

	public String getVerify_note() {
		return verify_note;
	}

	public void setVerify_note(String verify_note) {
		this.verify_note = verify_note;
	}

	public String getLink_man() {
		return link_man;
	}

	public void setLink_man(String link_man) {
		this.link_man = link_man;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getLink_mobile() {
		return link_mobile;
	}

	public void setLink_mobile(String link_mobile) {
		this.link_mobile = link_mobile;
	}

	public String getUser_id_p() {
		return user_id_p;
	}

	public void setUser_id_p(String user_id_p) {
		this.user_id_p = user_id_p;
	}

	public String getUser_id_n() {
		return user_id_n;
	}

	public void setUser_id_n(String user_id_n) {
		this.user_id_n = user_id_n;
	}

	public String getShop_license() {
		return shop_license;
	}

	public void setShop_license(String shop_license) {
		this.shop_license = shop_license;
	}

	public String getShop_permit() {
		return shop_permit;
	}

	public void setShop_permit(String shop_permit) {
		this.shop_permit = shop_permit;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLogo_file_id() {
		return logo_file_id;
	}

	public void setLogo_file_id(String logo_file_id) {
		this.logo_file_id = logo_file_id;
	}
}