package com.tmsps.neshop2.model;

import java.math.BigDecimal;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 订单表
 */

@Table
public class t_order extends DataModel {

	private String member_id;
//	private String shop_id;
	private String note;// 备注

	private String order_no;// 订单号
	private BigDecimal product_price;// 商品总价
	private BigDecimal logistics_price;//物流总价
	private BigDecimal total_price;// 总共金额
	private BigDecimal member_money_pay;// 余额支付金额
	private BigDecimal end_price;// 实际支付金额

	private int total_integral;// 总共积分
	private int end_integral;// 实际积分
	
	private String address;// 地址信息
	private String phone;// 电话信息
	private String name;// 姓名

	private long best_time;// 最佳送货时间
	private String status_order;// 状态:下单中 or 已下单 or 已发货 or 已收货 or 已完成 or 已退单 or 已取消
	private String is_pay = "否";// 是否支付 : 是 or 否
	private String pay_type;// 支付类型:货到付款 or 在线支付
	private String pay_way;// 具体支付方式:支付宝 or 微信支付

	private String cost_product_handle;// 缺货处理:等待所有商品备齐后再发 or 取消订单 or 与店主协商
	// 配送
	private String send_way;// 配送方式:普通快递 or EMS 等
	
	private String main_file_id;// 商品主图片
	private int product_sum;// 商品数量

	// 评价相关
	private String is_discuzz = "否";// 是否评价 : 是 or 否

	// ========== 支付相关字段 ========================
	private String out_trade_no;
	private String pay_wx_no;
	private String pay_user_openid;
	private String pay_method;
	private String pay_message;// 微信支付的xml消息
	// 退单信息
	private String back_message;// 微信支付的xml消息
//	private long pay_time = 0;
	private long pay_time;
	
	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================
	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public BigDecimal getTotal_price() {
		return total_price;
	}

	public void setTotal_price(BigDecimal total_price) {
		this.total_price = total_price;
	}

	public BigDecimal getMember_money_pay() {
		return member_money_pay;
	}

	public void setMember_money_pay(BigDecimal member_money_pay) {
		this.member_money_pay = member_money_pay;
	}

	public int getTotal_integral() {
		return total_integral;
	}

	public void setTotal_integral(int total_integral) {
		this.total_integral = total_integral;
	}

	public int getEnd_integral() {
		return end_integral;
	}

	public void setEnd_integral(int end_integral) {
		this.end_integral = end_integral;
	}

	public String getStatus_order() {
		return status_order;
	}

	public void setStatus_order(String status_order) {
		this.status_order = status_order;
	}

	public String getIs_pay() {
		return is_pay;
	}

	public void setIs_pay(String is_pay) {
		this.is_pay = is_pay;
	}

	public String getPay_type() {
		return pay_type;
	}

	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}

	public String getPay_way() {
		return pay_way;
	}

	public void setPay_way(String pay_way) {
		this.pay_way = pay_way;
	}

	public String getCost_product_handle() {
		return cost_product_handle;
	}

	public void setCost_product_handle(String cost_product_handle) {
		this.cost_product_handle = cost_product_handle;
	}

	public String getSend_way() {
		return send_way;
	}

	public void setSend_way(String send_way) {
		this.send_way = send_way;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getPay_wx_no() {
		return pay_wx_no;
	}

	public void setPay_wx_no(String pay_wx_no) {
		this.pay_wx_no = pay_wx_no;
	}

	public String getPay_user_openid() {
		return pay_user_openid;
	}

	public void setPay_user_openid(String pay_user_openid) {
		this.pay_user_openid = pay_user_openid;
	}

	public String getPay_method() {
		return pay_method;
	}

	public void setPay_method(String pay_method) {
		this.pay_method = pay_method;
	}

	public String getPay_message() {
		return pay_message;
	}

	public void setPay_message(String pay_message) {
		this.pay_message = pay_message;
	}

	public String getBack_message() {
		return back_message;
	}

	public void setBack_message(String back_message) {
		this.back_message = back_message;
	}

	public long getPay_time() {
		return pay_time;
	}

//	public String getShop_id() {
//		return shop_id;
//	}

//	public void setShop_id(String shop_id) {
//		this.shop_id = shop_id;
//	}

	public void setPay_time(long pay_time) {
		this.pay_time = pay_time;
	}

	public String getMain_file_id() {
		return main_file_id;
	}

	public void setMain_file_id(String main_file_id) {
		this.main_file_id = main_file_id;
	}

	public int getProduct_sum() {
		return product_sum;
	}

	public void setProduct_sum(int i) {
		this.product_sum = i;
	}

	public long getBest_time() {
		return best_time;
	}

	public void setBest_time(long best_time) {
		this.best_time = best_time;
	}


	public BigDecimal getProduct_price() {
		return product_price;
	}

	public void setProduct_price(BigDecimal product_price) {
		this.product_price = product_price;
	}

	public BigDecimal getLogistics_price() {
		return logistics_price;
	}

	public void setLogistics_price(BigDecimal logistics_price) {
		this.logistics_price = logistics_price;
	}

	public BigDecimal getEnd_price() {
		return end_price;
	}

	public void setEnd_price(BigDecimal end_price) {
		this.end_price = end_price;
	}

	public String getIs_discuzz() {
		return is_discuzz;
	}

	public void setIs_discuzz(String is_discuzz) {
		this.is_discuzz = is_discuzz;
	}
}