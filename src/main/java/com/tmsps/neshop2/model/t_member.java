package com.tmsps.neshop2.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 会员信息表
 * 
 * @author
 *
 */

@Table
public class t_member extends DataModel {

	private String status_sys; // 状态:正常状态 or 限制登陆
	private String shop_id; // 商户id
	private String source; //用户来源
	private String wx_original_id;//微信公众号id
	private String uname;// 用户名
	private String pwd;// 密码
	private String openid;// 账号
	private String user_nick;
	private int user_sex;//性别
	private String user_language;
	private String user_city;//城市
	private String user_province;//省
	private String user_country;//国家
	private String user_headimgurl;//头像
	private Long subscribe_time;//关注时间
	private String unionid;
	private String remark;
	private Integer groupid;
	private String user_origin;
	private String user_origin_key;
	private int points;
	private int balance;
	private String cardNum;
	private int consumption;
	private long integral;// 积分
	private BigDecimal money;// 余额
	private String mobile;// 手机号
	private String name;// 姓名
	private Timestamp reg_time;
	private String level_id;// 当前的会员级别
	private Date birthday;
	private String note;//备注
	private String retail_id;// 分销商ID

	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}
	
	public java.lang.String getStatus_sys() {
		return status_sys;
	}

	public void setStatus_sys(java.lang.String status_sys) {
		this.status_sys = status_sys;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getUser_nick() {
		return user_nick;
	}

	public void setUser_nick(String user_nick) {
		this.user_nick = user_nick;
	}

	public int getUser_sex() {
		return user_sex;
	}

	public void setUser_sex(int user_sex) {
		this.user_sex = user_sex;
	}

	public String getUser_language() {
		return user_language;
	}

	public void setUser_language(String user_language) {
		this.user_language = user_language;
	}

	public String getUser_city() {
		return user_city;
	}

	public void setUser_city(String user_city) {
		this.user_city = user_city;
	}

	public String getUser_province() {
		return user_province;
	}

	public void setUser_province(String user_province) {
		this.user_province = user_province;
	}

	public long getIntegral() {
		return integral;
	}

	public void setIntegral(long integral) {
		this.integral = integral;
	}

	public String getUser_country() {
		return user_country;
	}

	public void setUser_country(String user_country) {
		this.user_country = user_country;
	}

	public String getUser_headimgurl() {
		return user_headimgurl;
	}

	public void setUser_headimgurl(String user_headimgurl) {
		this.user_headimgurl = user_headimgurl;
	}

	public String getUser_origin() {
		return user_origin;
	}

	public void setUser_origin(String user_origin) {
		this.user_origin = user_origin;
	}

	public String getUname() {
		return uname;
	}

	public String getLevel_id() {
		return level_id;
	}

	public void setLevel_id(String level_id) {
		this.level_id = level_id;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getUser_origin_key() {
		return user_origin_key;
	}

	public void setUser_origin_key(String user_origin_key) {
		this.user_origin_key = user_origin_key;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getCardNum() {
		return cardNum;
	}

	public void setCardNum(String cardNum) {
		this.cardNum = cardNum;
	}

	public int getConsumption() {
		return consumption;
	}

	public void setConsumption(int consumption) {
		this.consumption = consumption;
	}

	public Timestamp getReg_time() {
		return reg_time;
	}

	public void setReg_time(Timestamp reg_time) {
		this.reg_time = reg_time;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getWx_original_id() {
		return wx_original_id;
	}

	public void setWx_original_id(String wx_original_id) {
		this.wx_original_id = wx_original_id;
	}

	public Long getSubscribe_time() {
		return subscribe_time;
	}

	public void setSubscribe_time(Long subscribe_time) {
		this.subscribe_time = subscribe_time;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getGroupid() {
		return groupid;
	}

	public void setGroupid(Integer groupid) {
		this.groupid = groupid;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getRetail_id() {
		return retail_id;
	}

	public void setRetail_id(String retail_id) {
		this.retail_id = retail_id;
	}


	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}
}
