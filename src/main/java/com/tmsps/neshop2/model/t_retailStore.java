package com.tmsps.neshop2.model;

import java.math.BigDecimal;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 分销表
 */

@Table
public class t_retailStore extends DataModel {
	
	private String code;// 编码 每3位一级
	private String name;// 分销商名称
	private String member_id;// 用户ID
	private String qr_code;//二维码 
	private BigDecimal total_money = new BigDecimal(0);//佣金总金额 
	private BigDecimal deposit_money = new BigDecimal(0);//未提现金额
	private BigDecimal then_money = new BigDecimal(0);//已提现金额
	private BigDecimal super_deduct_money = new BigDecimal(0);//给上级分成的总佣金
	private BigDecimal super_deposit_money = new BigDecimal(0);//上级未提的佣金
	private BigDecimal super_then_money = new BigDecimal(0);//上级已提的佣金
	
	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;
	

	// ======= get / set ()=====================

	public int getStatus() {
		return status;
	}


	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMember_id() {
		return member_id;
	}


	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}


	public String getQr_code() {
		return qr_code;
	}


	public void setQr_code(String qr_code) {
		this.qr_code = qr_code;
	}


	public BigDecimal getTotal_money() {
		return total_money;
	}


	public void setTotal_money(BigDecimal total_money) {
		this.total_money = total_money;
	}


	public BigDecimal getDeposit_money() {
		return deposit_money;
	}


	public void setDeposit_money(BigDecimal deposit_money) {
		this.deposit_money = deposit_money;
	}


	public BigDecimal getThen_money() {
		return then_money;
	}


	public void setThen_money(BigDecimal then_money) {
		this.then_money = then_money;
	}


	public BigDecimal getSuper_deduct_money() {
		return super_deduct_money;
	}


	public void setSuper_deduct_money(BigDecimal super_deduct_money) {
		this.super_deduct_money = super_deduct_money;
	}


	public BigDecimal getSuper_deposit_money() {
		return super_deposit_money;
	}


	public void setSuper_deposit_money(BigDecimal super_deposit_money) {
		this.super_deposit_money = super_deposit_money;
	}


	public BigDecimal getSuper_then_money() {
		return super_then_money;
	}


	public void setSuper_then_money(BigDecimal super_then_money) {
		this.super_then_money = super_then_money;
	}
	
}