package com.tmsps.neshop2.model;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 店铺认证表
 */

@Table
public class t_shop_auth extends DataModel {

	private String shop_id;

	private String auth_id;// 认证人身份证号
	private String img_id1;// 手持身份证照片
	private String img_id2;// 身份证正面
	private String img_id3;// 身份证反面

	// 开户银行信息
	private String bank_code;// *银行卡号：
	private String bank_address;// *开户银行地址：
	private String bank_mobile;// *提现电话：

	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public long getCreated() {
		return created;
	}

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}

	public String getAuth_id() {
		return auth_id;
	}

	public void setAuth_id(String auth_id) {
		this.auth_id = auth_id;
	}

	public String getImg_id1() {
		return img_id1;
	}

	public void setImg_id1(String img_id1) {
		this.img_id1 = img_id1;
	}

	public String getImg_id2() {
		return img_id2;
	}

	public void setImg_id2(String img_id2) {
		this.img_id2 = img_id2;
	}

	public String getImg_id3() {
		return img_id3;
	}

	public void setImg_id3(String img_id3) {
		this.img_id3 = img_id3;
	}

	public String getBank_code() {
		return bank_code;
	}

	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}

	public String getBank_address() {
		return bank_address;
	}

	public void setBank_address(String bank_address) {
		this.bank_address = bank_address;
	}

	public String getBank_mobile() {
		return bank_mobile;
	}

	public void setBank_mobile(String bank_mobile) {
		this.bank_mobile = bank_mobile;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}