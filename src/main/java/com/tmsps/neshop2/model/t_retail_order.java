package com.tmsps.neshop2.model;

import java.math.BigDecimal;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 分销商的客户订单记录
 */

@Table
public class t_retail_order extends DataModel {

	private String retail_id;
	private String member_id;
	private String order_id;
	private BigDecimal one_deduct_money;
	private BigDecimal two_deduct_money;
	private BigDecimal three_deduct_money;
	
	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================
	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public String getRetail_id() {
		return retail_id;
	}

	public void setRetail_id(String retail_id) {
		this.retail_id = retail_id;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getOne_deduct_money() {
		return one_deduct_money;
	}

	public void setOne_deduct_money(BigDecimal one_deduct_money) {
		this.one_deduct_money = one_deduct_money;
	}

	public BigDecimal getTwo_deduct_money() {
		return two_deduct_money;
	}

	public void setTwo_deduct_money(BigDecimal two_deduct_money) {
		this.two_deduct_money = two_deduct_money;
	}

	public BigDecimal getThree_deduct_money() {
		return three_deduct_money;
	}

	public void setThree_deduct_money(BigDecimal three_deduct_money) {
		this.three_deduct_money = three_deduct_money;
	}
	
	
}