package com.tmsps.neshop2.model;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

@Table
public class t_red_packet extends DataModel  {
		// ========== 系统字段 ========================
		private String money;//红包金额
		private String act_name;//活动名称
		private String note;//描述
		private String mch_billno;//单号
		private String member_id;//用户
		private String type;//类型 裂变 or 普通
		private String wishing;//祝福语
		
		@PK
		private String kid; //代表一条数据      	唯一
		private int status; // 状态   0  -100
		private long created = System.currentTimeMillis(); // 数据的创建时间

		@NotMap
		private static final long serialVersionUID = 1L;
		
		// ======= get / set ()=====================

		public String getMoney() {
			return money;
		}

		public void setMoney(String money) {
			this.money = money;
		}

		public String getNote() {
			return note;
		}

		public void setNote(String note) {
			this.note = note;
		}

		public String getKid() {
			return kid;
		}

		public void setKid(String kid) {
			this.kid = kid;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public long getCreated() {
			return created;
		}

		public void setCreated(long created) {
			this.created = created;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}

		public String getAct_name() {
			return act_name;
		}

		public void setAct_name(String act_name) {
			this.act_name = act_name;
		}

		public String getMch_billno() {
			return mch_billno;
		}

		public void setMch_billno(String mch_billno) {
			this.mch_billno = mch_billno;
		}

		public String getMember_id() {
			return member_id;
		}

		public void setMember_id(String member_id) {
			this.member_id = member_id;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getWishing() {
			return wishing;
		}

		public void setWishing(String wishing) {
			this.wishing = wishing;
		}
	

}
