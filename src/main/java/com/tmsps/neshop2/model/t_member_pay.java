package com.tmsps.neshop2.model;

import java.math.BigDecimal;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 扫码支付 刷卡支付表
 * 
 * @author Administrator
 *
 */
@Table
public class t_member_pay extends DataModel {

	private String shop_id;
	private String member_id;
	private BigDecimal money;// 余额支付
	private BigDecimal total_money;// 总共支付
	private BigDecimal member_money;// 余额支付

	private String note;// 说明 扫码支付 or 刷卡支付

	private String isprint;// 是否打印 null or yes
	private String trade_no;// 支付单号
	private String pay_type;// 支付类型:支付宝 or 财付通 or 微支付 or ......
	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;
	// ======= get / set ()=====================

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}

	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getIsprint() {
		return isprint;
	}

	public void setIsprint(String isprint) {
		this.isprint = isprint;
	}

	public String getTrade_no() {
		return trade_no;
	}

	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}

	public String getPay_type() {
		return pay_type;
	}

	public void setPay_type(String pay_type) {
		this.pay_type = pay_type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BigDecimal getTotal_money() {
		return total_money;
	}

	public void setTotal_money(BigDecimal total_money) {
		this.total_money = total_money;
	}

	public BigDecimal getMember_money() {
		return member_money;
	}

	public void setMember_money(BigDecimal member_money) {
		this.member_money = member_money;
	}

}
