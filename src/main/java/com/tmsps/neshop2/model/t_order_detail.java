package com.tmsps.neshop2.model;

import java.math.BigDecimal;

import com.tmsps.ne4spring.annotation.NotMap;
import com.tmsps.ne4spring.annotation.PK;
import com.tmsps.ne4spring.annotation.Table;
import com.tmsps.ne4spring.orm.model.DataModel;

/**
 * 订单项 表
 */

@Table
public class t_order_detail extends DataModel {

	private String cookie_sn;// cookie码,代表设备的cookie
	private String shop_id;
	private String order_id;

	private String member_id;// 发起人
	private String product_id;
	private String type;// 类型:商品 or 套餐
	private String name;// 商品名称
	private BigDecimal price;// 单价
	private BigDecimal cnt;// 数量
	private BigDecimal price_total;// 总价
	private int integral;// 所需积分
	private int integral_total;// 共消耗积分

	private String is_print = "否";// 是否打印 : 是 or 否
	private String status_order;// 状态:购物车 or 下单中 or 已付款 or 已发货 or 已完成 or 已退单 or 已取消

	private String logistics_id;// 发货单id
	// ========== 系统字段 ========================

	@PK
	private String kid;
	private int status;
	private long created = System.currentTimeMillis();

	@NotMap
	private static final long serialVersionUID = 1L;

	// ======= get / set ()=====================

	public int getStatus() {
		return status;
	}

	public String getShop_id() {
		return shop_id;
	}

	public void setShop_id(String shop_id) {
		this.shop_id = shop_id;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getMember_id() {
		return member_id;
	}

	public void setMember_id(String member_id) {
		this.member_id = member_id;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getCnt() {
		return cnt;
	}

	public void setCnt(BigDecimal cnt) {
		this.cnt = cnt;
	}

	public BigDecimal getPrice_total() {
		return price_total;
	}

	public String getCookie_sn() {
		return cookie_sn;
	}

	public void setCookie_sn(String cookie_sn) {
		this.cookie_sn = cookie_sn;
	}

	public void setPrice_total(BigDecimal price_total) {
		this.price_total = price_total;
	}

	public int getIntegral() {
		return integral;
	}

	public void setIntegral(int integral) {
		this.integral = integral;
	}

	public int getIntegral_total() {
		return integral_total;
	}

	public String getLogistics_id() {
		return logistics_id;
	}

	public void setLogistics_id(String logistics_id) {
		this.logistics_id = logistics_id;
	}

	public void setIntegral_total(int integral_total) {
		this.integral_total = integral_total;
	}

	public String getStatus_order() {
		return status_order;
	}

	public void setStatus_order(String status_order) {
		this.status_order = status_order;
	}

	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}

	public long getCreated() {
		return created;
	}

	public void setCreated(long created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getIs_print() {
		return is_print;
	}

	public void setIs_print(String is_print) {
		this.is_print = is_print;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}