//package com.tmsps.neshop2.util.wx.card;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.util.HashMap;
//import java.util.Map;
//
//import com.alibaba.fastjson.JSONObject;
//import com.tmsps.ne4Weixin.api.BaseAPI;
//import com.tmsps.ne4Weixin.config.WxConfig;
//import com.tmsps.ne4Weixin.utils.HttpClient;
//import com.tmsps.ne4Weixin.utils.PaymentUtil;
//import com.tmsps.ne4Weixin.utils.XmlHelper;
//import com.tmsps.neshop2.util.json.JsonTools;
//
//import net.sf.json.JSONString;
//
///**
// * 刷卡支付 API
// * 
// * @author 冯晓东
// *
// */
//public class PaymentAPICard extends BaseAPI {
//	public PaymentAPICard(WxConfig config) {
//		super(config);
//	}
//
//	//获得ACCESS_TOKEN	参数	appID  Secret
//	private static String WXACCESSTOKENURL = "http://mp.weixin.qq.com/debug/";
//	
//	//上传LOGO	参数	 access_token  buffer(文件的数据流json)
//	private static String WXCARDLOGOURL = "https://api.weixin.qq.com/cgi-bin/media/uploadimg";
//	
//	//创建card	参数	access_token	POST数据
//	private static String WXCREATECARDURL = "https://api.weixin.qq.com/card/create";
//	
//	//创建二维码	参数	access_token	POST数据		返回的show_qrcode_url为二维码显示地址
//	private static String WXTWODIMENSIONALURL = "https://api.weixin.qq.com/card/qrcode/create";
//	
//	//设置测试白名单	access_token	POST数据
//	private static String WXWHILELISTURL = "https://api.weixin.qq.com/card/testwhitelist/set";
//	
//	//核销卡券(线下)	access_token	POST数据
//	private	static String 	WXOFFLINECANCELURL = "https://api.weixin.qq.com/card/code/get";
//	
//	//核销卡券(线上)	access_token	POST数据
//	private	static String 	WXONLINECANCELURL = "https://api.weixin.qq.com/card/code/decrypt";
//	
//	/**
//	 * 生成订单
//	 * 
//	 * @return
//	 */
//	public Map<String, String> scanQrcode(Map<String, String> params) {
//
////		params.put("access_token", config.getJsAccessToken());
////		params.put("buffer", "http://mmbiz.qpic.cn/mmbiz/iaL1LJM1mF9aRKPZJkmG8xXhiaHqkKSVMMWeN3hLut7X7hicFNjakmxibMLGWpXrEXB33367o7zHN0CwngnQY7zb7g/0");
////		params.put("color","Color010");
////		params.put("title", "132元双人火锅套餐");
////		params.put("secret", config.getSecret());
//		params.put("appid", "wx09640ee4d72db498");
//		params.put("secret", "24ee47fe2a97ff00246ad6afca6ae923");
//		System.err.println(PaymentUtil.toXml(params));
//		
//		String body = HttpClient.postXML(WXCREATECARDURL, PaymentUtil.toXml(params));
//		System.err.println("body"+body);
//		return XmlHelper.of(body).toMap();
//	}
//
//	public static void main(String[] args) throws FileNotFoundException {
//		/*WxService s = SpringTools.getBean(WxService.class);
//		WxConfig wxConfig = s.getShopWxByDomain("dd.ss.vososo.com");*/
//		/*WxConfig wxConfig = new WxConfig();
//		System.err.println(JsonTools.toJson(wxConfig));
//		String access_token = wxConfig.getAccessToken();
//		System.err.println("access_token"+access_token);
//		PaymentAPICard api = new PaymentAPICard(wxConfig);
//		File img = new File("C:/Users/Administrator/Desktop/0.jpg");
//		FileInputStream fis = new FileInputStream("C:/Users/Administrator/Desktop/0.jpg");;
//		
//		params.put("access_token", access_token);
//		params.put("buffer", fis+"");
//
//		Map<String, String> end = api.scanQrcode(params);
//		System.err.println(end);*/
////		params.put("secret", "24ee47fe2a97ff00246ad6afca6ae923");
//		Map<String, String> params = new HashMap<>();
//		params.put("access_token", "adrn6ppdU13zHlxWOQQ9UzEQBMffuP-AGQVkRIlC8pAB8HmKvWxOBx6j3o5-NiX-Tf9W2J1nTlRsx1xwsp1NljEPZP8nyfjWw7XShVMrz-Ytr0GovZtbuy8RTIL12Gc4UUTiAHABBA");
//		File img = new File("C:/Users/Administrator/Desktop/0.jpg");
//		FileInputStream fis = new FileInputStream("C:/Users/Administrator/Desktop/0.jpg");
//		params.put("buffer", fis+"");
//		JSONObject json = new JSONObject();
//		System.err.println(PaymentUtil.toXml(params));
//		String body = HttpClient.postXML(WXCREATECARDURL, PaymentUtil.toXml(params));
//		Map<String, String> end = XmlHelper.of(body).toMap();
//		System.err.println(end);
//
//	}
//}
