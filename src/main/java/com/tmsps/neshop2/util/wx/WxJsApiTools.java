package com.tmsps.neshop2.util.wx;

import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.ne4Weixin.encrypt.SHA1;
import com.tmsps.ne4spring.utils.PKUtil;
import com.tmsps.neshop2.util.WxConfigTools;

import java.util.HashMap;
import java.util.Map;

public class WxJsApiTools {

	private String appId;
	private long timestamp;
	private String nonceStr;
	private String signature;

	public static WxJsApiTools getJsApiConfig(String url) {
		WxJsApiTools jsConfig = new WxJsApiTools();
		WxConfig wxConfig = WxConfigTools.getWxConfig();
		jsConfig.setAppId(wxConfig.getAppid());
		jsConfig.setTimestamp(System.currentTimeMillis());
		jsConfig.setNonceStr(PKUtil.getPK());

		String jsAccessToken = wxConfig.getJsAccessToken();

		Map<String, String> params = new HashMap<String, String>();
		params.put("noncestr", jsConfig.getNonceStr());
		params.put("jsapi_ticket", jsAccessToken);
		params.put("timestamp", jsConfig.getTimestamp() + "");
		params.put("url", url);
		try {
			String packageSign = PaymentKit.packageSign(params, false);
			String sign = SHA1.getSHA1HexString(packageSign);
			jsConfig.setSignature(sign);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsConfig;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

}
