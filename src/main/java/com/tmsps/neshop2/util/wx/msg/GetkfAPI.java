package com.tmsps.neshop2.util.wx.msg;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4Weixin.api.BaseAPI;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.ne4Weixin.utils.HttpClient;
import com.tmsps.neshop2.service.WxService;
import com.tmsps.neshop2.util.wx.FxWxConfigTools;

/**
 * 
 * @author zhangwei 396033084@qq.com 
 */
public class GetkfAPI extends BaseAPI {
	
	@Autowired
	WxService wxService;
	
	private static String GETKFLIST = BaseURL.concat("/cgi-bin/customservice/getkflist");
	
	public GetkfAPI(WxConfig config) {
		super(config);
	}
	
	/**
	 * 获取客服基本信息
	 * @param 
	 * @return
	 */
	public KfInfo getKfInfo() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("access_token", config.getAccessToken());
		JSONObject result = JSON.parseObject(HttpClient.httpGet(GETKFLIST, params));
		System.out.println(result.toJSONString());
		return JSON.parseObject(result.toJSONString(), KfInfo.class);
	}
	
	public static void main(String[] args) {
		new GetkfAPI(FxWxConfigTools.getWxConfig()).getKfInfo();
	}

}
