package com.tmsps.neshop2.util.wx;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import com.tmsps.ne4Weixin.api.BaseAPI;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.ne4Weixin.utils.PaymentUtil;
import com.tmsps.ne4Weixin.utils.XmlHelper;
import com.tmsps.ne4spring.utils.PKUtil;
import com.tmsps.neshop2.util.WxConfigTools;

/**
 * 微信红包付款 API 到用户
 * 
 * @author 张可
 * 发放红包调用接口模块
 */
public class PaymentAPIRedToUser extends BaseAPI {

	// 文档地址：https://pay.weixin.qq.com/wiki/doc/api/tools/cash_coupon.php?chapter=13_4&index=3
	private static final String GeneralRedPacketURL = "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack";
	
	// 文档地址：https://pay.weixin.qq.com/wiki/doc/api/tools/cash_coupon.php?chapter=13_5&index=4
	private static final String FissionRedPacketURL = "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendgroupredpack";

	
	/**
	 *  微信配置信息
	 * @param config
	 */
	public PaymentAPIRedToUser(WxConfig config) {
		super(config);
	}
	
	/**
	 * 普通红包
	 * @param apiclient_cert_p12_path
	 * @param params
	 * @return
	 */
	public Map<String, String> sendGeneralRedPacket(String apiclient_cert_p12_path, Map<String, String> params) {
		Map<String, String> result = commonPayRedToUser(apiclient_cert_p12_path, params, GeneralRedPacketURL);
		return result;
	}

	/**
	 * 裂变红包, 最少3个, 金额最少3块
	 * @param apiclient_cert_p12_path
	 * @param params
	 * @return
	 */
	public Map<String, String> sendFissionRedPacket(String apiclient_cert_p12_path, Map<String, String> params) {
		Map<String, String> result = commonPayRedToUser(apiclient_cert_p12_path, params, FissionRedPacketURL);
		return result;
	}

	

	/**
	 * 通用发红包参数
	 * 
	 * @param apiclient_cert_p12_path
	 *            证书路径 API安全中心 证书下载
	 * @param params
	 *            企业付款参数,不含 mch_appid,mchid,nonce_str,sign四个参数
	 * @return
	 */

	private Map<String, String> commonPayRedToUser(String apiclient_cert_p12_path, Map<String, String> params, String WXPAYURL) {
		String body = "";
		try {
			KeyStore keyStore = KeyStore.getInstance("PKCS12");
			FileInputStream instream = new FileInputStream(new File(apiclient_cert_p12_path));
			try {
				keyStore.load(instream, config.getMch_id().toCharArray());
			} finally {
				instream.close();
			}

			// Trust own CA and all self-signed certs
			SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(keyStore, config.getMch_id().toCharArray())
					.build();
			// Allow TLSv1 protocol only
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, new String[] { "TLSv1" },
					null, SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
			CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

			params.put("wxappid", config.getAppid());
			params.put("mch_id", config.getMch_id());
			params.put("nonce_str", System.currentTimeMillis() + "");
			String sign = PaymentUtil.createSign(params, config.getPayAPI());
			params.put("sign", sign);
			System.err.println(params);
			HttpPost httppost = new HttpPost(WXPAYURL);
			RequestConfig config = RequestConfig.custom().setConnectionRequestTimeout(10000).setConnectTimeout(10000)
					.setSocketTimeout(10000).build();
			httppost.setEntity(new StringEntity(PaymentUtil.toXml(params),
					ContentType.create("application/xml", Charset.forName("utf-8"))));
			httppost.setConfig(config);

			CloseableHttpResponse response = null;
			try {
				response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				body = EntityUtils.toString(entity, "UTF-8");
			} catch (IOException e) {
				log.error(e.toString());
				e.printStackTrace();
			}

		} catch (Exception e) {
			log.error(e.toString());
			e.printStackTrace();
		}

		return XmlHelper.of(body).toMap();
	}// #commonPayRedToUser

	
	public static void main(String[] args) {
		//发放普通红包
		WxConfig wxConfig = WxConfigTools.getWxConfig();
		WxConfig FxwxConfig = FxWxConfigTools.getWxConfig();
		String apiclient_cert_p12_path = "C:/apiclient_cert.p12";
		Map<String, String> params = new HashMap<>();
		params.put("mch_billno", PKUtil.getPK()); //商户订单号
		params.put("re_openid", "oQZUa1g2LPhM42tnKm_IBzp2SsWs"); //用户openid
		params.put("send_name", "悦龙斋"); //商户名称
		params.put("total_amount", new BigDecimal("3000").intValue() + ""); //付款金额
		params.put("total_num", new BigDecimal("1").intValue() + ""); //红包发放总人数
		params.put("client_ip", "192.168.62.111");//Ip地址
		params.put("act_name", "满10返0.01"); //活动名称
		params.put("remark", "快来抢");//备注
		params.put("wishing", "六一快乐"); //红包祝福语
		Map<String, String> payToUser = new PaymentAPIRedToUser(wxConfig).sendGeneralRedPacket(apiclient_cert_p12_path, params);
		System.err.println(payToUser);
	}
	
//	public static void main(String[] args) {
//		//发放 裂变 红包
//		WxConfig wxConfig = WxConfigTools.getWxConfig();
//		String apiclient_cert_p12_path = "D:/apiclient_cert.p12";
//		Map<String, String> params = new HashMap<>();
//		params.put("mch_billno", PKUtil.getPK());
//		params.put("re_openid", "oFYh_xOcjWqeScFFBnGIMf9N4gPw");
//		params.put("send_name", "悦龙斋");
//		params.put("total_amount", new BigDecimal("200").intValue() + "");
//		params.put("total_num", new BigDecimal("2").intValue() + "");
//		//多加参数
//		params.put("amt_type", "ALL_RAND");
//		params.put("wishing", "六一快乐");
//		params.put("act_name", "满10返0.01");
//		params.put("client_ip", "192.168.62.111");
//		params.put("remark", "快来抢");
//		Map<String, String> payToUser = new PaymentAPIRedToUser(wxConfig).sendFissionRedPacket(apiclient_cert_p12_path, params);
//		System.err.println(payToUser);
//	}
	
//	public static void main(String[] args) {
//	//发放 裂变 红包
//	WxConfig wxConfig = WxConfigTools.getWxConfig();
//	String apiclient_cert_p12_path = "C:/apiclient_cert.p12";
//	Map<String, String> params = new HashMap<>();
//	params.put("mch_billno", PKUtil.getPK());
//	params.put("re_openid", "oFYh_xOcjWqeScFFBnGIMf9N4gPw");
//	params.put("send_name", "悦龙斋");
//	params.put("total_amount", new BigDecimal("200").intValue() + "");
//	params.put("total_num", new BigDecimal("2").intValue() + "");
//	//多加参数
//	params.put("amt_type", "ALL_RAND");
//	params.put("wishing", "六一快乐");
//	params.put("act_name", "满10返0.01");
//	params.put("client_ip", "192.168.62.111");
//	params.put("remark", "快来抢");
//	Map<String, String> payToUser = new PaymentAPIRedToUser(wxConfig).sendFissionRedPacket(apiclient_cert_p12_path, params);
//	System.err.println(payToUser);
//}


}
