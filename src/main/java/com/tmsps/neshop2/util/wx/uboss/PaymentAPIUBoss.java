//package com.tmsps.neshop2.util.wx.uboss;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import com.tmsps.ne4Weixin.api.BaseAPI;
//import com.tmsps.ne4Weixin.config.WxConfig;
//import com.tmsps.ne4Weixin.utils.HttpClient;
//import com.tmsps.ne4Weixin.utils.PaymentUtil;
//import com.tmsps.ne4Weixin.utils.XmlHelper;
//import com.tmsps.neshop2.model.t_shop_wx_config;
//import com.tmsps.neshop2.service.WxService;
//import com.tmsps.neshop2.util.SpringTools;
//import com.tmsps.neshop2.util.json.JsonTools;
//
///**
// * 刷卡支付 API
// * 
// * @author 冯晓东
// *
// */
//public class PaymentAPIUBoss extends BaseAPI {
//	public PaymentAPIUBoss(WxConfig config) {
//		super(config);
//	}
//
//	// 文档地址：http://gitlab.ulaiber.com/uboss/mdocs/wikis/uchang-shuakazhifu
//	// private static String WXSCANPAYURL =
//	// "http://api.mch.stage.uline.cc/wechat/orders";
//	private static String WXPAYURL = "http://api.cmbxm.mbcloud.com/wechat/orders";
//	private static String WXSCANPAYURL = "http://api.cmbxm.mbcloud.com/wechat/orders/micropay";
//	private static String WXREFUNDSURL = "http://api.cmbxm.mbcloud.com/wechat/refunds";
//	private static String WXQueryURL = "http://api.cmbxm.mbcloud.com/wechat/orders/query";
//
//	/**
//	 * 普通生成订单
//	 * 
//	 * @param config
//	 * 
//	 * @return
//	 */
//	public Map<String, String> mkOrder(Map<String, String> params, t_shop_wx_config config) {
//
//		// params.put("appid", config.getAppid());
//		params.put("mch_id", config.getCmb_mch_id());
//		params.put("nonce_str", System.currentTimeMillis() + "");
//		String sign = PaymentUtil.createSign(params, config.getCmb_mch_key());// 招行key
//		params.put("sign", sign);
//		System.err.println(PaymentUtil.toXml(params));
//
//		String body = HttpClient.postXML(WXPAYURL, PaymentUtil.toXml(params));
//
//		return XmlHelper.of(body).toMap();
//	}
//	
//	/**
//	 * 扫码枪
//	 * @param params
//	 * @param config
//	 * @return
//	 */
//	public Map<String, String> mkScanOrder(Map<String, String> params, t_shop_wx_config config) {
//		
//		// params.put("appid", config.getAppid());
//		params.put("mch_id", config.getCmb_mch_id());
//		params.put("nonce_str", System.currentTimeMillis() + "");
//		String sign = PaymentUtil.createSign(params, config.getCmb_mch_key());// 招行key
//		params.put("sign", sign);
//		System.err.println(PaymentUtil.toXml(params));
//		
//		String body = HttpClient.postXML(WXSCANPAYURL, PaymentUtil.toXml(params));
//		
//		return XmlHelper.of(body).toMap();
//	}
//	
//	/**
//	 * 查询订单
//	 * 
//	 * @param config
//	 * 
//	 * @return
//	 */
//	public Map<String, String> queryOrder(String out_trade_no, t_shop_wx_config config) {
//
//		Map<String, String> params = new HashMap<>();
//		params.put("mch_id", config.getCmb_mch_id());
//		params.put("sub_appid", config.getAppid());
//		params.put("nonce_str", System.currentTimeMillis() + "");
//		params.put("out_trade_no", out_trade_no);
//		String sign = PaymentUtil.createSign(params, config.getCmb_mch_key());// 招行key
//		params.put("sign", sign);
//		System.err.println(PaymentUtil.toXml(params));
//
//		String body = HttpClient.postXML(WXQueryURL, PaymentUtil.toXml(params));
//
//		return XmlHelper.of(body).toMap();
//	}
//	
//	/**
//	 * 退款交易
//	 * @param params
//	 * @param config
//	 * @return
//	 */
//	public Map<String, String> refundsOrder(Map<String, String> params, t_shop_wx_config config) {
//		
//		// params.put("appid", config.getAppid());
//		params.put("mch_id", config.getCmb_mch_id());
//		params.put("nonce_str", System.currentTimeMillis() + "");
//		String sign = PaymentUtil.createSign(params, config.getCmb_mch_key());// 招行key
//		params.put("sign", sign);
//		System.err.println(PaymentUtil.toXml(params));
//		
//		String body = HttpClient.postXML(WXREFUNDSURL, PaymentUtil.toXml(params));
//		
//		return XmlHelper.of(body).toMap();
//	}
//
//	public static void main(String[] args) {
//		WxService s = SpringTools.getBean(WxService.class);
//		String shopId = "Qwx9zdZVhwfus5t5oL1Jc9";
//		t_shop_wx_config config = s.getShopWxConfig(shopId);
//		WxConfig wxConfig = s.getShopWxByShopId(shopId);
//		System.err.println(JsonTools.toJson(wxConfig));
//		PaymentAPIUBoss api = new PaymentAPIUBoss(wxConfig);
//		Map<String, String> params = new HashMap<>();
//		params.put("body", "image");
//		params.put("out_trade_no", System.currentTimeMillis() + "");
//		params.put("total_fee", "1");
//		params.put("spbill_create_ip", "127.0.0.1");
//		params.put("trade_type", "JSAPI");
//		params.put("notify_url", "http://" + config.getDomain() + "/wx_pay_back.htm");
//		params.put("openid", "o_uoMw5V3h3A1-skTVFoUghLW_mQ");
//		// params.put("product_id", "o_uoMw5V3h3A1-skTVFoUghLW_mQ");
//		// params.put("sub_openid", "");
//		// params.put("sub_appid", "gh_");
//
//		Map<String, String> end = api.mkOrder(params, config);
//		System.err.println(end);
//
//	}
//}
