package com.tmsps.neshop2.util.wx.msg;

import com.tmsps.ne4Weixin.message.in.InBaseMsg;

public class MyOutTextMsg extends MyOutBaseMsg {
	private static final long serialVersionUID = 1L;

	private String content;

	public MyOutTextMsg() {
		this.MsgType = "text";
	}

	public MyOutTextMsg(InBaseMsg inMsg) {
		super(inMsg);
		this.MsgType = "text";
	}

	@Override
	protected void subXml(StringBuilder sb) {
//		if (null == content) {
//			throw new NullPointerException("content is null");
//		}
//		sb.append("<Content><![CDATA[").append(content).append("]]></Content>\n");
	}
	
	public String getContent() {
		return content;
	}
	
	public MyOutTextMsg setContent(String content) {
		this.content = content;
		return this;
	}

}
