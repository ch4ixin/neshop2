package com.tmsps.neshop2.util.wx.msg;

import java.io.Serializable;

import com.tmsps.ne4Weixin.message.in.InBaseMsg;

public abstract class MyOutBaseMsg implements Serializable {
	private static final long serialVersionUID = 1L;

	// 接收方帐号（收到的OpenID）
	protected String ToUserName;
	// 开发者微信号
	protected String FromUserName;
	// 消息创建时间
	protected String CreateTime;
	/**
	 * 被动响应消息类型 
	 * 1：text 文本消息 
	 * 2：image 图片消息 
	 * 3：voice 语音消息 
	 * 4：video 视频消息 
	 * 5：music 音乐消息
	 * 6：news 图文消息
	 */
	protected String MsgType;
	
	public MyOutBaseMsg() {}
	
	public MyOutBaseMsg(InBaseMsg inMsg) {
		this.ToUserName = inMsg.getFromUserName();
		this.FromUserName = inMsg.getToUserName();
		this.CreateTime = now();
	}
	//createTime 
	public String now() {
		return String.valueOf(System.currentTimeMillis() / 1000);
	}
	
	protected abstract void subXml(StringBuilder sb);
	
	/**
	 * 转换xml
	 */
	public String toXml() {
		StringBuilder sb = new StringBuilder();
		sb.append("<xml>\n");
		sb.append("<ToUserName><![CDATA[").append(ToUserName).append("]]></ToUserName>\n");
		sb.append("<FromUserName><![CDATA[").append(FromUserName).append("]]></FromUserName>\n");
		sb.append("<CreateTime>").append(CreateTime).append("</CreateTime>\n");
		sb.append("<MsgType><![CDATA[").append(MsgType).append("]]></MsgType>\n");//
//		sb.append("<TransInfo><KfAccount><![CDATA[").append("kf2001@gh_413322536fe7").append("]]></KfAccount></TransInfo>");
		subXml(sb);
		sb.append("</xml>");
		return sb.toString();
	}
	
	protected String nullToBlank(String str) {
		return null == str ? "" : str;
	}

	public String getToUserName() {
		return ToUserName;
	}

	public void setToUserName(String toUserName) {
		ToUserName = toUserName;
	}

	public String getFromUserName() {
		return FromUserName;
	}

	public void setFromUserName(String fromUserName) {
		FromUserName = fromUserName;
	}

	public String getCreateTime() {
		return CreateTime;
	}

	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	public String getMsgType() {
		return MsgType;
	}

	public void setMsgType(String msgType) {
		MsgType = msgType;
	}
	
}
