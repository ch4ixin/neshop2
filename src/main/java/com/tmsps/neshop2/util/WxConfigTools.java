package com.tmsps.neshop2.util;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.tmsps.ne4Weixin.config.WxConfig;

public class WxConfigTools {

	private static Logger log = LoggerFactory.getLogger(WxConfigTools.class);
	private static Cache<String, WxConfig> cache = CacheBuilder.newBuilder().build();

	private WxConfigTools() {
	}

	public static WxConfig getWxConfig() {
		WxConfig wc = null;
		try {
			wc = cache.get("default", new Callable<WxConfig>() {
				@Override
				public WxConfig call() throws Exception {
					log.warn("not find cache and new it");

					String appid = "wxed401bfa4468749d";
					String secret = "ad96799426c03e8b7a61ac2149d4e4fd";
					String encodingaeskey = "puBWK6AGasBEpT9FvbMQcdltfKIgUWvALHFDEMqi5jV";
					String token = "7eD3u4AzTcOcbXaqaSoPNN";
					String mch_id = "1484142022";
					String payAPI = "ddfklsdAS12dkljsklAKLJDIONV12kii";

					WxConfig wxConfig = new WxConfig(appid, secret, encodingaeskey, true, token, mch_id, payAPI, 3600);

					return wxConfig;
				}
			});
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return wc;
	}

}
