package com.tmsps.neshop2.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tmsps.ne4Weixin.api.TagAPI;
import com.tmsps.ne4Weixin.api.UserTagAPI;
import com.tmsps.ne4Weixin.beans.UserTag;
import com.tmsps.ne4Weixin.config.WxConfig;
import com.tmsps.neshop2.util.json.JsonTools;

/**
 * 微信标签
 * 
 * @author Administrator
 *
 */
public class WxTagTools {

	// 获取微信服务器标签的id
	public static int getTag(WxConfig wxConfig, String name) {
		TagAPI api = new TagAPI(wxConfig);
		JSONArray list = api.listTag();
		for (Object object : list) {
			Map<String, Object> map = JsonTools.objToMap(object);
			if (name.equals(map.get("name"))) {
				return Integer.parseInt(map.get("id") + "");
			}
		}
		UserTag usertag = api.addTag(name);
		return usertag.getId();
	}

	// 更改多个人员的标签
	public static JSONObject getMemberTag(WxConfig wxConfig, int tagId, List<String> openids) {
		UserTagAPI api = new UserTagAPI(wxConfig);
		api.cancleMemberTag(tagId, openids);
		return api.addTagToMember(tagId, openids);
	}
	
	// 更改1个人员的标签
	public static JSONObject getMemberTag(WxConfig wxConfig, String name, String openid) {
		int tagId = getTag(wxConfig, name);
		UserTagAPI api = new UserTagAPI(wxConfig);
		List<String> openids = new ArrayList<>();
		openids.add(openid);
		api.cancleMemberTag(tagId, openids);
		return api.addTagToMember(tagId, openids);
	}
}
