package com.tmsps.neshop2.util.doc.excel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.alibaba.fastjson.JSONObject;

public class AppUtil {

	public static final String codeSuccess = "Y";
	public static final String codeFailure = "N";
	public static final String codeException = "E";
	public static final String msgSuccess = "操作成功！";
	public static final String msgFailure = "没有相关信息！";
	public static final String msgException = "系统异常";
	public static final String msgpwoer = "没有权限,请升级";
	public static List<Map<String,Object>> spfllist =null; //商品分类集合
	public static List<Map<String,Object>> mgclist = new  ArrayList<Map<String,Object>>() ; //敏感词
	public static final String msgcount = "次数已到";
	 
	public static final String stip = "http://www.boaov.org:8081"; //试题ip
	public static final String headUpload = "headUpload";// 头像
	public static String evidenceUpload = "evidenceUpload";
	public static final String topicimgs = "topicimgs";// 头像
	public static final String shopimgs = "shopimgs";// 头像 
// 	public static final String IP=  "http://192.168.1.160:8080"; 
    public static final String IP="http://123.56.186.183:8081"; 
	
	//public static final String pictureUpload = "pictureUpload";// 用户图像
	public static final String pictureUpload = "pictureUpload/"+new SimpleDateFormat("yyyy").format(new Date())+ "/"+new SimpleDateFormat("yyyyMM").format(new Date())+ "/"+new SimpleDateFormat("yyyyMMdd").format(new Date());// 用户图像
	public static final String Excel = "Excel";
	
	 
	public static String platform="";
	public static final String erweimapth="http://www.boaov.org:8081/boaojiaoyu/tmunber!htmlreg.do?inviteCode=";
	

 
//public static final String erweimapth="http://192.168.1.150:8080/JiangJ/tmunber!htmlreg.do?inviteCode=";
	
    
	//public static final String passwordtalk = Md5Encrypt.md5("123456"); 
	/** 编码格式 */
	public static final String ENCODING_HEADERS = "encoding:UTF-8";
	/** 分页的每一页条数 */
	public static final int pagerows = 20;
	public static final int pageindex = 1;

	// 是否显示 System.out.println 打印
	public static final boolean isShowSystemOutPrintLn = false; 
	// 是否显示 System.err.println 打印
	public static final boolean isShowSystemErrPrintLn = false;
	

	public static final int pagePhotorows = 40; 
	// 树显示的级数
	public static final int treeshowjibie = 3; 
	/** 权限默认值 */
	public static final int setFriendpower = 1;
	public static final int setLookPower = 1;
	/** 全局常量 */
	public static final String PWD = "123456"; 
	public static final String DECKEY = "tadd1234567890125648934961safwsdaE";

	public static JSONObject getJson() {
		JSONObject json = new JSONObject();
		json.put("code", codeSuccess);
		json.put("data", new JSONObject());
		json.put("msg", AppUtil.msgSuccess);
		json.put("check", "");
		return json;
	}

	public static JSONObject getJsonByRequest(HttpServletRequest request) {
		JSONObject json = new JSONObject();
		json = JSONObject.parseObject(request.getParameter("json"));
		return json;
	}

	public static String setJson(JSONObject json) {
		String str = json.toString().replace(":null", ":\"\"");
		return str;
	}

	/**
	 * 极光推送
	 */
	public static final String master_secret = "88bdf85d8b28d7ffcca1cb6e";
	public static final String appKey = "0e97a6c84c421486af3a30f7";
	public static final String maxRetryTimes = "864000";

	// --------------------------------------------

	public AppUtil(HttpServletRequest request,HttpServletResponse response) {
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		response.setCharacterEncoding("utf-8");
	}

	public static Map<String, String> qihaotime = new HashMap<String, String>();

	/**
	 * 
	 * @param qihao
	 *            格式"20150901001";
	 * @return
	 */
	public static Date GetQihaotimeByQihao(String qihao) {

		Date returnval = new Date();
		try {
			if (qihaotime.size() <= 0) {

				qihaotime.put("001", "00:06:00");
				qihaotime.put("002", "00:11:00");
				qihaotime.put("003", "00:16:00");
				qihaotime.put("004", "00:21:00");
				qihaotime.put("005", "00:26:00");
				qihaotime.put("006", "00:31:00");
				qihaotime.put("007", "00:36:00");
				qihaotime.put("008", "00:41:00");
				qihaotime.put("009", "00:46:00");

				qihaotime.put("010", "00:51:00");
				qihaotime.put("011", "00:56:00");
				qihaotime.put("012", "01:01:00");
				qihaotime.put("013", "01:06:00");
				qihaotime.put("014", "01:11:00");
				qihaotime.put("015", "01:16:00");
				qihaotime.put("016", "01:21:00");
				qihaotime.put("017", "01:26:00");
				qihaotime.put("018", "01:31:00");
				qihaotime.put("019", "01:36:00");

				qihaotime.put("020", "01:41:00");
				qihaotime.put("021", "01:46:00");
				qihaotime.put("022", "01:51:00");
				qihaotime.put("023", "01:56:00");
				qihaotime.put("024", "10:01:00");
				qihaotime.put("025", "10:11:00");
				qihaotime.put("026", "10:21:00");
				qihaotime.put("027", "10:31:00");
				qihaotime.put("028", "10:41:00");
				qihaotime.put("029", "10:51:00");

				qihaotime.put("030", "11:01:00");
				qihaotime.put("031", "11:11:00");
				qihaotime.put("032", "11:21:00");
				qihaotime.put("033", "11:31:00");
				qihaotime.put("034", "11:41:00");
				qihaotime.put("035", "11:51:00");
				qihaotime.put("036", "12:01:00");
				qihaotime.put("037", "12:11:00");
				qihaotime.put("038", "12:21:00");
				qihaotime.put("039", "12:31:00");

				qihaotime.put("040", "12:41:00");
				qihaotime.put("041", "12:51:00");
				qihaotime.put("042", "13:01:00");
				qihaotime.put("043", "13:11:00");
				qihaotime.put("044", "13:21:00");
				qihaotime.put("045", "13:31:00");
				qihaotime.put("046", "13:41:00");
				qihaotime.put("047", "13:51:00");
				qihaotime.put("048", "14:01:00");
				qihaotime.put("049", "14:11:00");

				qihaotime.put("050", "14:21:00");
				qihaotime.put("051", "14:31:00");
				qihaotime.put("052", "14:41:00");
				qihaotime.put("053", "14:51:00");
				qihaotime.put("054", "15:01:00");
				qihaotime.put("055", "15:11:00");
				qihaotime.put("056", "15:21:00");
				qihaotime.put("057", "15:31:00");
				qihaotime.put("058", "15:41:00");
				qihaotime.put("059", "15:51:00");

				qihaotime.put("060", "16:01:00");
				qihaotime.put("061", "16:11:00");
				qihaotime.put("062", "16:21:00");
				qihaotime.put("063", "16:31:00");
				qihaotime.put("064", "16:41:00");
				qihaotime.put("065", "16:51:00");
				qihaotime.put("066", "17:01:00");
				qihaotime.put("067", "17:11:00");
				qihaotime.put("068", "17:21:00");
				qihaotime.put("069", "17:31:00");

				qihaotime.put("070", "17:41:00");
				qihaotime.put("071", "17:51:00");
				qihaotime.put("072", "18:01:00");
				qihaotime.put("073", "18:11:00");
				qihaotime.put("074", "18:21:00");
				qihaotime.put("075", "18:31:00");
				qihaotime.put("076", "18:41:00");
				qihaotime.put("077", "18:51:00");
				qihaotime.put("078", "19:01:00");
				qihaotime.put("079", "19:11:00");

				qihaotime.put("080", "19:21:00");
				qihaotime.put("081", "19:31:00");
				qihaotime.put("082", "19:41:00");
				qihaotime.put("083", "19:51:00");
				qihaotime.put("084", "20:01:00");
				qihaotime.put("085", "20:11:00");
				qihaotime.put("086", "20:21:00");
				qihaotime.put("087", "20:31:00");
				qihaotime.put("088", "20:41:00");
				qihaotime.put("089", "20:51:00");

				qihaotime.put("090", "21:01:00");
				qihaotime.put("091", "21:11:00");
				qihaotime.put("092", "21:21:00");
				qihaotime.put("093", "21:31:00");
				qihaotime.put("094", "21:41:00");
				qihaotime.put("095", "21:51:00");
				qihaotime.put("096", "22:01:00");
				qihaotime.put("097", "22:05:00");
				qihaotime.put("098", "22:10:00");
				qihaotime.put("099", "22:15:00");

				qihaotime.put("100", "22:20:00");
				qihaotime.put("101", "22:25:00");
				qihaotime.put("102", "22:30:00");
				qihaotime.put("103", "22:35:00");
				qihaotime.put("104", "22:40:00");
				qihaotime.put("105", "22:45:00");
				qihaotime.put("106", "22:50:00");
				qihaotime.put("107", "22:55:00");
				qihaotime.put("108", "23:00:00");
				qihaotime.put("109", "23:05:00");

				qihaotime.put("110", "23:10:00");
				qihaotime.put("111", "23:15:00");
				qihaotime.put("112", "23:20:00");
				qihaotime.put("113", "23:25:00");
				qihaotime.put("114", "23:30:00");
				qihaotime.put("115", "23:35:00");
				qihaotime.put("116", "23:40:00");
				qihaotime.put("117", "23:45:00");
				qihaotime.put("118", "23:50:00");
				qihaotime.put("119", "23:55:00");
				qihaotime.put("120", "00:01:00"); // 第二天　

			}
			// 20150901001
			if (qihao.length() < 10)
				return returnval;
			String no = qihao.substring(8, 11);
			if (no.equals("120")) {
				// 日期需要+1天
				Date dt = CommonUtil.ConvertToDate(qihao.substring(0, 4) + "-" + qihao.substring(4, 6) + "-" + qihao.substring(6, 8));
				return CommonUtil.ConvertToDate(new SimpleDateFormat("yyyy-MM-dd").format(CommonUtil.DataAddDays(dt, 1)) + " 00:01:00");
			} else {
				return CommonUtil.ConvertToDate(qihao.substring(0, 4) + "-" + qihao.substring(4, 6) + "-" + qihao.substring(6, 8) + " " + qihaotime.get(no));
			}

		} catch (Exception e) {
			// e.printStackTrace();
		}
		return returnval;
	}
	
	//boindex列表的一级缓存 
	public static  Map<String,List<Map<String,Object>>>  listboindexcache= new HashMap<String,List<Map<String,Object>>>();
	

}
