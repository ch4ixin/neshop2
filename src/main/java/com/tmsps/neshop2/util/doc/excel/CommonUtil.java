package com.tmsps.neshop2.util.doc.excel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CommonUtil {

	public static Date ConvertToDate(Object o, Date defaultValue) {

		if (o == null)
			return defaultValue;
		if (o instanceof Date) {
			return (Date) o;
		}

		try {
			return new Date(o.toString());
		} catch (Exception e) {
			return defaultValue;
		}
	}

	/**
	 * 日期字符格式 ,yyyy-MM-dd 或 yyyyMMdd 所有
	 * 
	 * @param datastr
	 * @param datafmtstr
	 * @param defaultvalue
	 * @return
	 */
	public static Date ConvertToDate(String datastr, String datafmtstr, Date defaultvalue) {

		SimpleDateFormat format = new SimpleDateFormat(datafmtstr);
		Date date = null;
		try {
			date = format.parse(datastr);
		} catch (ParseException e) {
			return defaultvalue;
		}
		return date;
	}

	/**
	 * 日期字符格式 ,yyyy-MM-dd 或 yyyyMMdd 所有
	 * 
	 * @param datastr
	 * @param datafmtstr
	 * @param defaultvalue
	 * @return
	 */
	public static Date ConvertToDate(String datastr, String datafmtstr) {

		return ConvertToDate(datastr, datafmtstr, new Date());
	}

	public static Date ConvertToDate(Object o) {
		return ConvertToDate(o, new Date());
	}

	public static int ConvertToInt(Object o, int defaultValue) {

		if (o == null)
			return defaultValue;
		try {
			return Integer.parseInt(o.toString());
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public static int ConvertToInt(Object o) {
		return ConvertToInt(o, 0);
	}

	/**
	 * 转成String类型
	 * 
	 * @param o
	 *            传入参数
	 * @param defaultValue
	 *            出现转换异常，默认返回值
	 * @return String
	 */
	public static String ConvertToString(Object o, String defaultValue) 
	{

		if (o == null || "".equals(o))
			return defaultValue;
		try {
			return o.toString().toString();
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public static String ConvertToString(Object o) {

		if (o == null)
			return "";
		try {
			return o.toString().trim();
		} catch (Exception e) {
			return "";
		}

	}

	public static double ConvertToDouble(Object o) {

		if (o == null)
			return 0.0;
		try {
			return Double.valueOf(0);
		} catch (Exception e) {
			return 0.0;
		}
	}

	public static long ConvertToLong(Object o, long defaultValue) {

		if (o == null)
			return defaultValue;
		try {
			return Long.parseLong(o.toString());
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public static long ConvertToLong(Object o) {
		return ConvertToLong(o, -1);
	}

	public static float ConvertToFloat(Object o, float defaultValue) {

		if (o == null)
			return defaultValue;
		try {
			return Float.parseFloat(o.toString());
		} catch (Exception e) {
			return defaultValue;
		}
	}

	public static float ConvertToFloat(Object o) {
		return ConvertToFloat(o, 0f);
	}

	/**
	 * 得到主键
	 * 
	 * @return
	 */
	public static String GetEnterId() {
		// 根据时间和4为随机数生成ID
		// String.format("%04d",(int)(Math.random()*10000))随机数不满4位时填充0
		String id = new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date()).toString() + String.format("%04d", (int) (Math.random() * 10000));
		return id;
	}

	/**
	 * 得到当天日期格式yyy/MM/dd HH:mm:ss 的 字号串
	 * 
	 * @return
	 */
	public static String GetNowTimeFormat() {
		return GetNowDateaddFormat("yyyy/MM/dd HH:mm:ss", 0);
	}

	/**
	 * 得到当天日期格式yyy-MM-dd的 字号串
	 * 
	 * @return
	 */
	public static String GetNowDateFormat() {
		return GetNowDateaddFormat("yyyy-MM-dd", 0);
	}

	/**
	 * 日期格式化 ，
	 * 
	 * @param formatString
	 *            格式化字符串 例如 yyyy-MM-dd HH:mm:ss
	 * @param days
	 *            与当前天 +- 天数
	 * @return
	 */
	public static String GetNowDateaddFormat(String formatString, int days) {
		if (days == 0) {
			String robTime = new SimpleDateFormat(formatString).format(new Date());
			return robTime;
		} else {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, days);
			String robTime = new SimpleDateFormat(formatString).format(c.getTime());
			return robTime;
		}
	}
	
	public static Date DataAddDays(Date  day, int days) {
		if (days == 0) {
		  return day;
		} else {
			Calendar c =  Calendar.getInstance();
			c.set(day.getYear(), day.getMonth(), day.getDay(),day.getHours(),day.getMinutes(),day.getSeconds());
			 c.add(Calendar.DAY_OF_MONTH, days);
			 return c.getTime();
		}
	}

	/**
	 * 得到当天日期格式yyy-MM-dd HH:mm:ss 的 字号串
	 * 
	 * @return
	 */
	public static String GetNowTimeFormat2() {
		return GetNowDateaddFormat("yyy-MM-dd HH:mm:ss", 0);
	}

	/**
	 * 左填充
	 * 
	 * @param str
	 * @param len
	 * @param rep
	 * @return
	 */
	public static String PadLeft(String str, int len, char rep) {
		return stringFill2(str, len, rep, true);
	}

	/**
	 * 右填充
	 * 
	 * @param str
	 * @param len
	 * @param rep
	 * @return
	 */
	public static String PadRight(String str, int len, char rep) {
		return stringFill2(str, len, rep, false);
	}

	/**
	 * 自动填充字符串
	 * 
	 * @param source
	 * @param fillLength
	 * @param fillChar
	 * @param isLeftFill
	 * @return
	 */
	private static String stringFill2(String source, int fillLength, char fillChar, boolean isLeftFill) {
		if (source == null || source.length() >= fillLength)
			return source;

		char[] c = new char[fillLength];
		char[] s = source.toCharArray();
		int len = s.length;
		if (isLeftFill) {
			int fl = fillLength - len;
			for (int i = 0; i < fl; i++) {
				c[i] = fillChar;
			}
			System.arraycopy(s, 0, c, fl, len);
		} else {
			System.arraycopy(s, 0, c, 0, len);
			for (int i = len; i < fillLength; i++) {
				c[i] = fillChar;
			}
		}
		return String.valueOf(c);
	}

	/**
	 * 返回两个时间相差天数 bdate-smdate
	 * 
	 * @param smdate
	 * @param bdate
	 * @return
	 * @throws ParseException
	 */
	public static int daysBetween(Date smdate, Date bdate, int defaultvalue) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			smdate = sdf.parse(sdf.format(smdate));
			bdate = sdf.parse(sdf.format(bdate));
			Calendar cal = Calendar.getInstance();
			cal.setTime(smdate);
			long time1 = cal.getTimeInMillis();
			cal.setTime(bdate);
			long time2 = cal.getTimeInMillis();
			long between_days = (time2 - time1) / (1000 * 3600 * 24);

			return Integer.parseInt(String.valueOf(between_days));
		} catch (Exception e) {
			return defaultvalue;
		}
	}

}
