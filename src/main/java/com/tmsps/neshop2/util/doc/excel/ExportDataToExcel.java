package com.tmsps.neshop2.util.doc.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONArray;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import com.tmsps.neshop2.util.doc.excel.AppUtil;
import com.tmsps.neshop2.util.doc.excel.CommonUtil;

public class ExportDataToExcel {
 
	/**
	 * 导出excel 
	 * @param request
	 * @param sheetName
	 * @param lstmpjsonstr
	 * @return
	 * @throws Exception
	 */
	public static String createParentAccountWorkbook(
			HttpServletRequest request, String sheetName,
			String lstmpjsonstr) throws Exception {

		  JSONArray jsonArray = JSONArray.parseArray(lstmpjsonstr); 
		  
	        List<Map<String,Object>> mapListJson = (List)jsonArray;
	        
	        for (int i = 0; i < mapListJson.size(); i++) 
	        {
	            Map<String,Object> obj=mapListJson.get(i);  
	        }  
	        return createParentAccountWorkbook(request,sheetName,mapListJson);
	}
	
	
	//
	public static String createParentAccountWorkbook(
			HttpServletRequest request, String sheetName,
			List<Map<String, Object>> lsmp) throws Exception {

		HSSFWorkbook workbook = null;
		HSSFSheet sheet = null;
		HSSFRow row = null;
		try 
		{
			// 创建Excel的工作sheet,对应到一个excel文档的tab
			workbook = new HSSFWorkbook();
			sheet = workbook.createSheet(sheetName);  
			ExportExcelUtil exportExcel = new ExportExcelUtil(workbook, sheet);
			HSSFCellStyle cellStyle = exportExcel.createCellFontStyle(workbook,200, "normal", "center");

			int rowNum = -1;
			for (Map<String, Object> mp : lsmp) {
				rowNum++;
				if (rowNum == 0) {
					// 第一行 ，；列名
					exportExcel.createExcelRow(workbook, sheet, rowNum, 750, 1,
							null, 400, "bold", "center");
					row = sheet.getRow(rowNum);
					int colNum = 0;
					for (Map.Entry<String, Object> entry : mp.entrySet()) {
						exportExcel.createCell(workbook, sheet, row, colNum++,
								"right",
								CommonUtil.ConvertToString(entry.getKey()),
								1600, cellStyle);
					}  
					rowNum++;
				}

				// 数据行
				exportExcel.createExcelRow(workbook, sheet, rowNum, 750, 1,
						null, 400, "bold", "center");
				row = sheet.getRow(rowNum);
				int colNum = 0;
				for (Map.Entry<String, Object> entry : mp.entrySet()) {
					exportExcel.createCell(workbook, sheet, row, colNum++,
							"right",
							CommonUtil.ConvertToString(entry.getValue()), 1600,
							cellStyle);
				}
			}

			String folder = Constant.EXPORT_EXCEL_PATH;
			// 取得互联网程序的绝对地址
			String realPath = request.getRealPath("");
			File filedir = new File(realPath + File.separator + folder);
			if (!filedir.exists())
				filedir.mkdir();
			String filename = "";
			UUID uuid = UUID.randomUUID();
			String uuidStr = uuid.toString();
			uuidStr = uuidStr.replaceAll("-", "");
			filename = uuidStr + ".xls";
			System.out.println(realPath + File.separator + folder
					+ File.separator + filename);
			OutputStream out = new FileOutputStream(realPath + File.separator
					+ folder + File.separator + filename);
			workbook.write(out);
			out.flush();
			out.close();
			return request.getContextPath() + "/" + folder + "/" + filename;
		} catch (Exception e) {
			throw e;
			// throw new ServiceException("家长登录账号信息生成失败！");
		}
	}

	//
	// public static HSSFWorkbook createTeacherAccountWorkbook(String sheetName,
	// List<TeacherVo> teacherList, String className) {
	// HSSFWorkbook workbook = null;
	// HSSFSheet sheet = null;
	// HSSFRow row = null;
	// try {
	// workbook = new HSSFWorkbook();
	// // 创建Excel的工作sheet,对应到一个excel文档的tab
	// sheet = workbook.createSheet(sheetName);
	// ExportExcelUtil exportExcel = new ExportExcelUtil(workbook, sheet);
	// int rowNum = 0;
	// exportExcel.createExcelRow(workbook, sheet, rowNum, 600, 10, className +
	// "老师登录账号信息", 400, "bold", "center");
	// HSSFCellStyle cellStyle = exportExcel.createCellFontStyle(workbook, 200,
	// "normal", "center");
	// for (TeacherVo teacher : teacherList) {
	// rowNum++;
	// exportExcel.createExcelRow(workbook, sheet, rowNum, 750, 1, null, 400,
	// "bold", "center");
	// row = sheet.getRow(rowNum);
	// int colNum = 0;
	// exportExcel.createCell(workbook, sheet, row, colNum++, "right", "工号：",
	// 1600, cellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "left",
	// teacher.getCode(), 2620, cellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "right", "姓名：",
	// 1600, cellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "left",
	// teacher.getName(), 2620, cellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "right", "老师电话：",
	// 2600, cellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "left",
	// teacher.getPhone(), 3000, cellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "right", "登录账号：",
	// 2600, cellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "left",
	// teacher.getUserName(), 2200, cellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "right", "初始密码：",
	// 2600, cellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "left", "123456",
	// 1750, cellStyle);
	// }
	// return workbook;
	// } catch (Exception e) {
	// throw new ServiceException("老师登录账号信息生成失败！");
	// }
	// }
	//
	// @SuppressWarnings("static-access")
	// public static HSSFWorkbook createResultTempBook(String sheetName,
	// List<StudentVo> dataList,
	// String courseNames, String resultName, String resultType, String courses,
	// String classId, String grade, String term) {
	// HSSFWorkbook workbook = null;
	// HSSFSheet sheet = null;
	// HSSFRow row = null;
	// try {
	// workbook = new HSSFWorkbook();
	// // 创建Excel的工作sheet,对应到一个excel文档的tab
	// sheet = workbook.createSheet(sheetName);
	// ExportExcelUtil exportExcel = new ExportExcelUtil(workbook, sheet);
	// int rowNum = 0;
	// //exportExcel.createExcelRow(workbook, sheet, rowNum, 600, 10, className
	// + "考试成绩模板", 400, "bold", "center");
	// exportExcel.createExcelRow(workbook, sheet, rowNum, 500, 1, null, 400,
	// "bold", "center");
	// HSSFCellStyle cellStyleHead = exportExcel.createCellFontStyle(workbook,
	// 240, "bold", "center");
	// HSSFCellStyle cellStyle = exportExcel.createCellFontStyle(workbook, 200,
	// "normal", "center");
	// HSSFCellStyle noeditCellStyle = exportExcel.createCellFontStyle(workbook,
	// 200, "normal", "center");
	// noeditCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
	// noeditCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
	// noeditCellStyle.setFillBackgroundColor(HSSFColor.YELLOW.index);
	//
	// String[] courseNms = courseNames.split(",");
	// String[] courseIds = courses.split(",");
	// int len = courseIds.length;
	//
	// // 取得规则
	// int endVal = 8 + len * 2 - 1;
	// HSSFDataValidation validate = exportExcel.setDataValidation((short) 1,
	// (short) dataList.size(), (short) 8, (short) endVal);
	// // 设定规则
	// validate.createErrorBox("输入值类型或大小有误", "数值型，请输入0~999之间的数值");
	// validate.setShowErrorBox(true);
	// sheet.addValidationData(validate);
	//
	// row = sheet.getRow(rowNum);
	// int headColNum = 0;
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center",
	// "班级编号", 3500, cellStyleHead);
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center",
	// "学生编号", 3500, cellStyleHead);
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center",
	// "学生学号", 3500, cellStyleHead);
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center",
	// "学生姓名", 3500, cellStyleHead);
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center",
	// "学年", 3500, cellStyleHead);
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center",
	// "学期", 3500, cellStyleHead);
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center",
	// "考试类型", 3500, cellStyleHead);
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center",
	// "考试名称", 6800, cellStyleHead);
	//
	// for (int i = 0; i < courseNms.length; i++) {
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center", "",
	// 3500, cellStyleHead);
	// exportExcel.createCell(workbook, sheet, row, headColNum++, "center",
	// courseNms[i], 5000, cellStyleHead);
	// }
	//
	// for (StudentVo student : dataList) {
	// rowNum++;
	// exportExcel.createExcelRow(workbook, sheet, rowNum, 350, 1, null, 400,
	// "bold", "center");
	// row = sheet.getRow(rowNum);
	// int colNum = 0;
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center", classId,
	// 3500, noeditCellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center",
	// student.getId().toString(), 3500, noeditCellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center",
	// student.getRecordCode(), 3500, noeditCellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center",
	// student.getName(), 3500, noeditCellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center", grade,
	// 3500, noeditCellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center", term,
	// 3500, noeditCellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center",
	// resultType, 3500, noeditCellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center",
	// resultName, 6800, noeditCellStyle);
	// for (int i = 0; i < courseNms.length; i++) {
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center",
	// courseIds[i], 3500, noeditCellStyle);
	// exportExcel.createCell(workbook, sheet, row, colNum++, "center", "",
	// 5000, cellStyle);
	// }
	// }
	// sheet.setColumnHidden(0, true);
	// sheet.setColumnHidden(1, true);
	// sheet.setColumnHidden(4, true);
	// sheet.setColumnHidden(5, true);
	// sheet.setColumnHidden(6, true);
	// for (int i = 0; i < len; i++) {
	// sheet.setColumnHidden(i*2 + 8, true);
	// }
	// exportExcel.createSummaryRow(workbook, sheet, endVal + 1,
	// "*注：黄色区域的数据为成绩导入时系统所需数据，请勿对其中内容做任何修改；另外请勿随意添加/删除任何行或列，否则将会导致成绩数据无法正常导入或导入错误。",
	// 300, "bold", "right");
	// return workbook;
	// } catch (Exception e) {
	// throw new ServiceException("班级考试成绩模板生成失败！");
	// }
	// }
	
//	public static void main(String[] args) {
//		String jsonArrayData = request.getParameter("datagridjsondata");
//		
//		String filename = ExportDataToExcel.createParentAccountWorkbook(request, "数据导出页", jsonArrayData);
//		
//		response.getWriter().write(filename);
//	}

}
