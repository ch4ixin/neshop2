package com.tmsps.neshop2.util.doc.excel;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;
import org.apache.poi.ss.util.CellRangeAddressList;

public class ExportExcelUtil {
	private HSSFWorkbook workbook = null;
	private HSSFSheet sheet = null;
	
	public ExportExcelUtil(HSSFWorkbook workbook) {
		this.workbook = workbook;
	}
	
	public ExportExcelUtil(HSSFWorkbook workbook, HSSFSheet sheet) {
		this.workbook = workbook;
		this.sheet = sheet;
	}
	
	/** 
     * 创建通用的Excel空白行信息 
     * @param workbook 如果为空 则没有样式 
     * @param sheet (创建sheet) 
     * @param rowNO 报表的单行行号(创建第几行) 
     * @param rowHeight 报表的单行行高 
     * @param colNum 报表的总列数 (合并) 
     * @param colWidth 报表的列宽
     */  
	public void createExcelRow(HSSFWorkbook workbook, HSSFSheet sheet, int rowNum, int rowHeight, int colNum) {  
		createExcelRow(workbook, sheet, rowNum, -1, colNum, null, -1, null, null);  
	}
	
	/** 
     * 创建通用的Excel带标题行信息 
     * @param workbook 如果为空 则没有样式 
     * @param sheet (创建sheet) 
     * @param rowNO 报表的单行行号(创建第几行) 
     * @param rowHeight 报表的单行行高 
     * @param colNum 报表的总列数 (合并) 
     * @param fontCaption 报表行中显示的字符 
     */  
	public void createExcelRow(HSSFWorkbook workbook, HSSFSheet sheet, int rowNum, int rowHeight, int colNum, String fontCaption) {  
		createExcelRow(workbook, sheet, rowNum, -1, colNum, fontCaption, -1, null, null);  
	}
	
	/** 
     * 创建通用的Excel行信息 
     * @param workbook 如果为空 则没有样式 
     * @param sheet (创建sheet) 
     * @param rowNum 报表的单行行号(创建第几行) 
     * @param rowHeight 报表的单行行高 
     * @param colNum 报表的总列数 (合并) 
     * @param fontCaption 报表行中显示的字符 
     * @param fontSize 字体的大小 (字体大小 默认 200) 
     * @param fontWeight 报表表头显示的字符 
     * @param align 字体水平位置 (center中间  right右  left左) 
     */  
	@SuppressWarnings("deprecation")
	public void createExcelRow(HSSFWorkbook workbook, HSSFSheet sheet, int rowNum, int rowHeight, int colNum, String fontCaption, int fontSize, String fontWeight, String align) {  
        if(colNum < 0) {  
        	colNum = 100;    
        }
        //创建第一行
        HSSFRow row = sheet.createRow(rowNum);
        //设置行高
        row.setHeight((short) (rowHeight < 1 ? 300 : rowHeight));
        //设置第一行
        HSSFCell cell = row.createCell(0);
        //定义单元格为字符串类型  
        cell.setCellType(HSSFCell.ENCODING_UTF_16);
        cell.setCellValue(new HSSFRichTextString(fontCaption));
        //指定合并区域
        sheet.addMergedRegion(new Region(rowNum, (short) 0, rowNum, (short) (colNum - 1)));
        //设定样式 
        HSSFCellStyle cellStyle = createCellFontStyle(workbook, fontSize, fontWeight, align);
        if (cellStyle != null) {  
        	cell.setCellStyle(cellStyle);  
        }
        for (int i = 1; i < colNum; i++) {
        	HSSFCell newCell = row.createCell(i);
        	newCell.setCellStyle(cellStyle);
        }
	}
	
	/** 
	 * 创建通用的Excel最后一行的信息 (创建合计行 (最后一行)) 
	 * @param workbook 如果为空 则没有样式 
     * @param sheet  
     * @param colNum 报表的总列数 (合并) 
     * @param fontCaption 报表行中显示的字符 
     * @param fontSize 字体的大小 (字体大小 默认 200) 
     * @param fontWeight 报表表头显示的字符 
     * @param align 字体水平位置 (center中间  right右  left左) 
     * @param colNum 报表的列数 (需要合并到的列索引) 
     *  
     */  
	@SuppressWarnings("deprecation")
	public void createSummaryRow(HSSFWorkbook workbook, HSSFSheet sheet, int colNum, String fontCaption, int fontSize, String fontWeight, String align) {
		HSSFCellStyle cellStyle = createCellFontStyle(workbook, fontSize, fontWeight, align);
		HSSFFont font = workbook.createFont();
        font.setFontHeightInPoints((short) 12);
        font.setColor(HSSFColor.RED.index);
		cellStyle.setFont(font);
		
		HSSFRow lastRow = sheet.createRow((short) (sheet.getLastRowNum() + 1));
		lastRow.setHeight((short) 800);
		HSSFCell sumCell = lastRow.createCell(0);
		
		sumCell.setCellValue(new HSSFRichTextString(fontCaption));
		if(cellStyle != null){sumCell.setCellStyle(cellStyle);}
		sheet.addMergedRegion(new Region(sheet.getLastRowNum(), (short) 0, sheet.getLastRowNum(), (short) (colNum - 1)));// 指定合并区域  
	}
	
	/** 
	 * 设置字体样式   (字体为宋体 ，上下居中对齐，可设置左右对齐，字体粗细，字体大小 ) 
	 * @param workbook 如果为空 则没有样式 
	 * @param fontSize 字体大小 默认 200 
	 * @param fontWeight 字体粗细 ( 值为bold 为加粗) 
	 * @param align 字体水平位置 (center中间  right右  left左) 
	 */  
	public HSSFCellStyle createCellFontStyle(HSSFWorkbook workbook, int fontSize, String fontWeight, String align) {
		if(workbook == null) {
			return null;
		}
		
		HSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 指定单元格居中对齐
		if(align != null && align.equalsIgnoreCase("left")) {
			cellStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT); // 指定单元格居中对齐  
		}  
		if(align != null && align.equalsIgnoreCase("right")) {
			cellStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT); // 指定单元格居中对齐
		}
		
		cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);// 指定单元格垂直居中对齐
		cellStyle.setWrapText(true);// 指定单元格自动换行
		cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框
		cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框
		cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框
		cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框
		
		// 单元格字体  
		HSSFFont font = workbook.createFont();
		if(fontWeight != null && fontWeight.equalsIgnoreCase("normal")) {
			font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
		} else {
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		}  
	    
		font.setFontName("宋体");
		font.setFontHeight((short) (fontSize < 1 ? 200 : fontSize));
		cellStyle.setFont(font);
		return cellStyle;
	}
	
	/** 
	 * 创建内容单元格  
	 * @param workbook HSSFWorkbook 
     * @param row  HSSFRow 
     * @param columnNumber short型的列索引 
     * @param alignType  对齐方式  (默认居中对齐) 
     * @param value  列值 
     */  
	public void createCell(HSSFWorkbook workbook, HSSFSheet sheet, HSSFRow row, int columnNumber, String alignType, String value, int colWidth, HSSFCellStyle cellstyle) {
		HSSFCell cell = row.createCell(columnNumber);
		cell.setCellType(HSSFCell.ENCODING_UTF_16);
		cell.setCellValue(new HSSFRichTextString(value));
		
		short align = HSSFCellStyle.ALIGN_CENTER_SELECTION;
		if (alignType != null && !"".equals(alignType)) {
			if ("left".equals(alignType)) {
				align = HSSFCellStyle.ALIGN_LEFT;
			} else if ("right".equals(alignType)) {
				align = HSSFCellStyle.ALIGN_RIGHT;
			} else {
				align = HSSFCellStyle.ALIGN_CENTER;
			}
		}
		cellstyle.setAlignment(align);
		cell.setCellStyle(cellstyle);
		sheet.setColumnWidth(columnNumber, colWidth);
	}
	
	public static HSSFDataValidation setDataValidation(int firstRow, int endRow, int firstCol, int endCol) {
		// 加载下拉列表内容
		DVConstraint constraint = DVConstraint.createNumericConstraint(DVConstraint.ValidationType.INTEGER, DVConstraint.OperatorType.BETWEEN, "0", "999");
		// 设置数据有效性加载在哪个单元格上。
		// 四个参数分别是：起始行、终止行、起始列、终止列
		CellRangeAddressList regions = new CellRangeAddressList((short) firstRow, (short) endRow, (short) firstCol, (short) endCol);
		// 数据有效性对象
		HSSFDataValidation data_validation = new HSSFDataValidation(regions, constraint);
		return data_validation;
	}
		
	
	/**
	 * @return the workbook
	 */
	public HSSFWorkbook getWorkbook() {
		return workbook;
	}
	/**
	 * @param workbook the workbook to set
	 */
	public void setWorkbook(HSSFWorkbook workbook) {
		this.workbook = workbook;
	}
	/**
	 * @return the sheet
	 */
	public HSSFSheet getSheet() {
		return sheet;
	}
	/**
	 * @param sheet the sheet to set
	 */
	public void setSheet(HSSFSheet sheet) {
		this.sheet = sheet;
	}
}
