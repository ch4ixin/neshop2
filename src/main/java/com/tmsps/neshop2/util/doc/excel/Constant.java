package com.tmsps.neshop2.util.doc.excel;
/**********************************************************************
 * 文件名称: Constant.java
 * 系统名称: 系统常量类
 * 模块名称: 
 * 软件版权: 静思美页（北京）科技有限公司
 * 功能说明: 系统常量类
 * 系统版本: V1.0	
 * 修改记录:   修改日期    	   修改人员       修改说明
 * v1.0.0.1 
 **********************************************************************/
public class Constant {

	/** 系统内置角色 */
	public static final String ROLE_SUPERMANAGER = "ROLE_SUPERMANAGER";
	public static final String ROLE_OPERATOR = "ROLE_OPERATOR"; 
	
 
	/** 编码格式 */
	public static final String ENCODING_HEADERS = "encoding:UTF-8";
	
	/** 返回结果 */
	public static final String RESULT_FAILURE = "-1";
	public static final String RESULT_ERROR = "0";
	public static final String RESULT_SUCCESS = "1";
	public static final String SYSTEM_ERROR = "{\"code\": 1, \"msg\":\"系统异常\"}";
	public static final String PARAMETER_ERROR = "{\"code\": 1, \"msg\":\"参数错误\"}";
	
	/** 全局常量 */
	public static final String PWD = "123456";
	public static final String KName = "neighborhoodName";
	public static final String CName = "classesName";
	
	/** 星期 */
	public static final String MONDAY = "周一";
	public static final String TUESDAY = "周二";
	public static final String WEDNESDAY = "周三";
	public static final String THURSDAY = "周四";
	public static final String FRIDAY = "周五";
	public static final String SATURDAY = "周六";
	public static final String SUNDAY = "周日";
	 
	// 验证码
	public static final String SESSION_SECURITY_CODE = "secCode";
	// EXCEL中需输入整形
	public static final String CELL_DATA_TYPE_INT = "int";
	// EXCEL中需输入数字
	public static final String CELL_DATA_TYPE_NUM = "number";
	// EXCEL中需输入数字或字母
	public static final String CELL_DATA_TYPE_LEDIG = "letterOrDigit";
	// EXCEL中日期
	public static final String CELL_DATA_TYPE_DATE = "date";
	// EXCEL中输入长度
	public static final String CELL_DATA_LENGTH = "length";
	// 导入文件暂存路劲
	public static final String EXPORT_EXCEL_PATH = "exportExcel"; 
 
	// 空字符串
	public static final String NULL_VAL = "NULL_VAL"; 
	
	public static final String SMS_SERVER_URL = "http://sms.chanzor.com:8001/sms.aspx?action=send";
	public static final String SMS_ACCOUNT = "zhixuejiaoyu";
	public static final String SMS_NOTICE_ACCOUNT = "zhixuejiaoyu-tz";
	public static final String SMS_PWD = "152413";
	// 在职的老师
	public static final String TEACHER_WORING = "working";
	public static final String TEACHER_STATUS_ING = "0";
//	// 是否是班主任
//	public static final String IS_MASTER = "YES";
//	public static final String IS_NOT_MASTER = "NO";
//	//考勤状态码
//	//1. 到课
//	public static final int CHECK_ARRIVAL = 1;
//	//2. 旷课
//	public static final int CHECK_ABSENT = 2;
//	//3. 缺勤
//	public static final int CHECK_NON_ARRIVAL = 3;
//	//4. 请假
//	public static final int CHECK_LEAVEL = 4;
//	//5. 迟到
//	public static final int CHECK_LATE = 5;
//	//6. 其他
//	public static final int CHECK_OTHERS = 6;
	
	/** 消息推送状态 */
	public static final int PUSH_OPENED = 1;
	public static final int PUSH_CLOSED = 2;

	/** 排序 */
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";
	
	/** PUSH PARAMS */
	public static final String TEACHER_MASTER_SECRET = "teacher_master_secret";
	public static final String PARENT_MASTER_SECRET = "parent_master_secret";
	public static final String PARENT_APPKEY = "parent_appKey";
	public static final String TEACHER_APPKEY = "teacher_appKey";
	public static final String MAXRETRYTIMES = "maxRetryTimes";
	
	/** AttendanceNotice content */
	public static final String SIGNIN_CONTENT = "您的孩子已安全入园，请放心！";
	public static final String SIGNOUT_CONTENT = "您的孩子已离园，祝宝贝开心！";
	public static final String UNSIGNIN_CONTENT = "您的孩子还未入园，请填写未到原因！";
	public static final String UNSIGNOUT_CONTENT = "您的孩子未签退！";
	
	/** Notice title */
	public static final String DIRECTOR_TITLE = "全园通知";
	public static final String RECIPE_TITLE = "本周食谱";
	public static final String DAYREPORT_TITLE = "每日一报";
	public static final String WEEKREPORT_TITLE = "每周一报";
	public static final String DOCTOR_TITLE = "服药提醒";
	public static final String HONOR_TITLE = "荣誉榜";
	public static final String TEACHER_TITLE = "本班通知";
	public static final String BIRTH_TITLE = "生日提醒";
	public static final String ACTIVITY_TITLE = "活动通知";
	public static final String ATTENDANCE_TITLE = "签到/签退";
	public static final String APPRAISE_TITLE = "家长月评";
	public static final String PARENT_TITLE = "未签到原因";
	public static final String COURSE_TITLE = "课程表";
	
	/** Notice type */
	public static final Integer DIRECTOR_TYPE = 2;
	public static final Integer RECIPE_TYPE = 3;
	public static final Integer DAYREPORT_TYPE = 4;
	public static final Integer WEEKREPORT_TYPE = 5;
	public static final Integer DOCTOR_TYPE = 6;
	public static final Integer HONOR_TYPE = 7;
	public static final Integer TEACHER_TYPE = 8;
	public static final Integer BIRTH_TYPE = 9;
	public static final Integer ACTIVITY_TYPE = 10;
	public static final Integer ATTENDANCE_TYPE = 11;
	public static final Integer APPRAISE_TYPE = 12;
	public static final Integer PARENT_TYPE = 13;
	public static final Integer COURSE_TYPE = 14;
	
	/** Notice json key */
	public static final String NOTICE_MSGID = "msgid";
	public static final String NOTICE_TYPE = "type";
	public static final String NOTICE_TITLE = "title";
	public static final String NOTICE_CONTENT = "content";
	
	
	/**
	 * 网络资源登录
	 */
	public static final String WLZY_ADDRESS="www.zxjy360.com";
	public static final int WLZY_PROT=8020;
	public static final int WLZY_WEB_PROT=8081;
}
