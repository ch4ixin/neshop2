	var uplv_form_panel = Ext.create("Ext.form.Panel", {
		url : getServerHttp() + "/cp/retail_store/uplv.htm",
		buttonAlign : "center",
		bodyStyle : "padding: 10px;",
		defaultType : "textfield",
		items : [ {
			id : "kid",
			name : "kid",
			hidden : true
		}, {
			id : "code",
			fieldLabel : "编号",
			name : "code",
			allowBlank : false,
//			readOnly : true,
			validator : function() {
				var error = true;
				var kid = Ext.getCmp("kid").getValue();
				if (kid == null || kid == "") {
					return true;
				}
				
				if (codeLength != this.value.length) {
					error = "长度必须为:" + codeLength;
					return error;
				}

				Ext.Ajax.request({
					url : getServerHttp() + '/cp/retail_store/auth_unique.htm',
					params : {
						code : this.value
					},
					scope : true,
					async : false,
					method : 'POST',
					success : function(response) {
						var result = Ext.JSON.decode(response.responseText);
						if ("true" == result.fail) {
							error = "该编号己经存在,请重新输入！";
						}
					}
				});
				return error;
			}//end_validator
		}, {
			fieldLabel : "商户名称",
			name : "name",
			allowBlank : false
			//hidden : true
		}, {
			fieldLabel : "openID",
			name : "member_id",
			readOnly : true,
			allowBlank : false,
			hidden : true
		}],
		buttons : [ {
			text : "保存",
			formBind : true, // only enabled once the form is valid
			disabled : true,
			handler : function() {
				var form = this.up("form").getForm();
				if (form.isValid()) {
					form.submit({
						waitMsg : "保存中...",
						success : function(form, action) {
							Ext.Msg.alert("提示", action.result.tip.msg);
							uplv_form_panel_win.close();
							dataStore.load();
						},
						failure : function(form, action) {
							Ext.Msg.alert("提示", "升级失败,请及时联络管理员。");
						}
					});
				}
			}
		} ]
	});
	

	var uplv_form_panel_win = Ext.create("Ext.Window", {
		title : "分销商升级",
		closeAction : "hide",
		buttonAlign : "center",
		closeAction : "hide",
		layout : "fit", // 窗口布局类型
		maximizable : true, // 设置是否可以最大化
		items : [uplv_form_panel],
		modal:true
	});

	function myUplv(kid) {
		Ext.Ajax.request({
			url : getServerHttp() + "/cp/retail_store/edit_form.htm?kid=" + kid,
			success : function(response) {
				var json = Ext.JSON.decode(response.responseText);
				uplv_form_panel.getForm().reset();
				uplv_form_panel.getForm().setValues(json);
				uplv_form_panel_win.show();
				
				//alert(edit_form_panel.getForm().findField("kid").getValue());
				codeLength = Ext.getCmp("code").getValue().length - 3;
			},
			failure : function(response) {
				Ext.Msg.alert("提示", "操作失败!");
			}
		});
	}//#myUplv
