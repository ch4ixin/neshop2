	var memberDataStore = Ext.create('Ext.data.Store', {
		remoteSort : true,
		autoLoad : true,
		pageSize : 10,
		sorters : {
			property : 'created',
			direction : 'DESC'
		},
		proxy : { 
			type : "ajax",
			url : getServerHttp()+"/cp/member/fx_member_data.htm",
			reader : {
				type : 'json',
				root : 'list',
	            totalProperty : 'page.totalRow'
			}
		}
	}); //#memberDataStore

	//校验 code 长度
	var codeLength = 0;
	
	var edit_form_panel = Ext.create("Ext.form.Panel", {
		url : getServerHttp() + "/cp/retail_store/edit.htm",
		buttonAlign : "center",
		width : 500,
		height : 140,
		bodyStyle : "padding: 10px;",
		defaultType : "textfield",
		items : [ {
			id : "kid",
			name : "kid",
			hidden : true
		}, {
			id : "code",
			fieldLabel : "编号",
			name : "code",
			//allowBlank : false,
			readOnly : true,
//			validator : function() {
//				var error = true;
//				var kid = Ext.getCmp("kid").getValue();
//				if (kid == null || kid == "") {
//					return true;
//				}
//				
//				if (codeLength != this.value.length) {
//					error = "长度必须为:" + codeLength;
//					return error;
//				}
//
//				Ext.Ajax.request({
//					url : '/cp/retail_store/auth_unique.htm',
//					params : {
//						kid : kid,
//						code : this.value
//					},
//					scope : true,
//					async : false,
//					method : 'POST',
//					success : function(response) {
//						var result = Ext.JSON.decode(response.responseText);
//						if ("true" == result.fail) {
//							error = "该名称己经存在,请重新输入！";
//						}
//					}
//				});
//				return error;
//			}//end_validator
		}, {
			fieldLabel : "商户名称",
			name : "name",
			allowBlank : false
			//hidden : true
		}, {
			xtype:"fieldcontainer" , layout:"hbox" , 
			items:[{xtype:"textfield" ,fieldLabel:"绑定用户" , name : "mname"},//,readOnly: true
            		{xtype:"displayfield" ,value:"* 必须先点击下方(用户)绑定用户才能执行保存操作"}]
		}, {
			fieldLabel : "openID",
			name : "member_id",
			readOnly : true,
			allowBlank : false,
			hidden : true
		}],
		buttons : [ {
			text : "保存",
			formBind : true, //only enabled once the form is valid
			disabled : true,
			handler : function() {
				var form = this.up("form").getForm();
				if (form.isValid()) {
					form.submit({
						failure : function(form, action) {
							console.log(form.getValues());
							if(action.response.responseText == "success"){
								Ext.Msg.alert("提示", "添加分销商信息成功！");
								edit_form_panel_win.close();
								dataStore.load();
							}else{
								Ext.MessageBox.confirm("提示", "该用户已经绑定分销商:"+action.response.responseText+",是否更改绑定?", function (btn) {
									if(btn == 'yes'){
									    Ext.MessageBox.show({
									        title: '提示',
									        msg: '	请稍后……',
									        icon: Ext.MessageBox.INFO,
									        progressText: '更改绑定中……',
									        progress: true,
									        closable: false,
									    });
										Ext.Ajax.request({
											url : getServerHttp() + "/cp/retail_store/edit_cut.htm?memberid="+form.getValues().member_id+"&kid="+form.getValues().kid+"&name="+form.getValues().name,
											success : function(response) {
												var json = Ext.JSON.decode(response.responseText);
												Ext.Msg.alert("提示", json.tip.msg);
												edit_form_panel_win.close();
												dataStore.load();
											},
											failure : function(response) {
												Ext.Msg.alert("提示", "改绑失败!");
											}
										});
									}else if(btn == 'no'){
										add_form_panel_win.close();
									}
								});
							}
						}
					});
				}
			}
		} ]
	});
	
	var sm = new Ext.selection.CheckboxModel({
		checkOnly : true,
		singleSelect : true,
		mode: "SINGLE"
	});

	var find_member_panel1 = Ext.create('Ext.grid.Panel', {
		tbar : edit_form_panel,
		store : memberDataStore,
		selModel : sm,
		buttonAlign : "center",
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			store : memberDataStore, // same store GridPanel is using
			dock : 'bottom',
			displayInfo : true,
			buttons : [{
				text : "绑定用户",
				formBind : false, // only enabled once the form is valid
				handler : function() {
					myAdddd();
				}
			}]
		} ],
		columns : [{
			text : '头像',
			dataIndex : 'user_headimgurl',
			flex : 1,
			renderer : function(value, cellmeta, record, rowIndex, columnIndex, store) {
				var img_url = record.data["user_headimgurl"];
				return '<img width=25 height=25 src='+img_url+'>';
			}
		},{
			text : '昵称',
			dataIndex : 'user_nick',
			flex : 1
		}, {
			text : '关注时间',
			dataIndex : 'created',
			flex : 1,
			renderer : function(val) {
				if (val != '') {
					return Ext.Date.format(new Date(val), "Y-m-d H:i:s");
				}
			}
		}]
	});


	var edit_form_panel_win = Ext.create("Ext.Window", {
		title : "分销商编辑",
		closeAction : "hide",
		buttonAlign : "center",
		closeAction : "hide",
		width : 700, // 宽度
		layout : "fit", // 窗口布局类型
		maximizable : true, // 设置是否可以最大化
		items : [find_member_panel1],
		modal:true
	});

	function myEdit(kid) {
		Ext.Ajax.request({
			url : getServerHttp() + "/cp/retail_store/edit_form.htm?kid=" + kid,
			success : function(response) {
				var json = Ext.JSON.decode(response.responseText);
				edit_form_panel.getForm().reset();
				edit_form_panel.getForm().setValues(json);
				edit_form_panel_win.show();
				
				//alert(edit_form_panel.getForm().findField("kid").getValue());
				codeLength = Ext.getCmp("code").getValue().length;
			},
			failure : function(response) {
				Ext.Msg.alert("提示", "操作失败!");
			}
		});
	}//#myEdit
	
	//添加按钮方法
	function myAdddd() {
		var selectedData = find_member_panel1.getSelectionModel().getSelection();
		/*console.log(selectedData[0].data.user_nick);
		console.log(selectedData[0].data.kid);*/
		if (selectedData == "") {
			Ext.Msg.alert("提示", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	请选择用户 ( ｰ̀εｰ́ )");
		} else {
			edit_form_panel.getForm().findField('mname').setValue(selectedData[0].data.user_nick);
			edit_form_panel.getForm().findField('member_id').setValue(selectedData[0].data.kid);
		}
		
	}
