var send_form_panel = Ext.create("Ext.form.Panel", {
	url : getServerHttp() + "/cp/member/fx_send_msg.htm",
	buttonAlign : "center",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	items : [{
		xtype : 'textareafield',
		fieldLabel : "消息内容",
		name : "content"
	},{
		fieldLabel : "用户id",
		name : "member_id",
		allowBlank : false,
		hidden : true
	}],
	buttons : [ {
		text : "发送",
		formBind : true, // only enabled once the form is valid
		disabled : true,
		handler : function() {
			var form = this.up("form").getForm();
			if (form.isValid()) {
				form.submit({
					waitMsg : "保存中...",
					success : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
						edit_form_panel_win.close();
						dataStore.load();
					},
					failure : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
					}
				});
			}
		}
	} ]
});

var send_form_panel_win = Ext.create("Ext.Window", {
	title : "客服消息发送",
	closeAction : "hide",
	items : send_form_panel,
	modal:true
});

function myfxSend(kid) {
	send_form_panel.getForm().reset();
	send_form_panel.getForm().findField('member_id').setValue(kid);;
	send_form_panel_win.show();
}// #myEdit
