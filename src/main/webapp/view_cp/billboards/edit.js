var edit_form_panel = Ext.create("Ext.form.Panel", {
		url : getServerHttp()+"/cp/billboard/edit.htm",
		buttonAlign : "cnter",
		bodyStyle : "padding: 10px;",
		defaultType : "textfield",
		items : [ {
			fieldLabel : "名称",
			name : "name",
			allowBlank : false
		}, {
			fieldLabel : "位置",
			name : "site"
		},{
			xtype : "panel",
			html : '<div><img id="logo_img1" src="../../resource/img/product.png" width="120" height="120"></div>'
					+ '<div id="uploader1" class="wu-example">'
					+ '<div id="thelist1" class="uploader-list">'
					+ '</div>'
					+ '<div class="btns">'
					+ '<div id="picker1">'
					+ '上传图片(500*500px)'
					+ '</div>'
					+ '</div>' + '</div>',
			width : '100%',
			height : 170,
			border : false,
			style : {
				marginBottom : '10px',
				marginLeft : '130px',
			}
		},{
			name :"main_file_id",
			hidden :true
		},{
			fieldLabel : "链接商品",
			name : "product_id",
			allowBlank : false,
			xtype : "combobox",
			emptyText : '请选择链接的商品',
			store : pdataStore,
			displayField : 'name',
			valueField : 'kid',
			editable : false,
			style : {
				display : 'table',
				marginTop : '5px'
			}
		}, {
			fieldLabel :"id",
			name :"kid",
			hidden :true
		} ],
		buttons : [ {
			text : "保存",
			formBind : true, //only enabled once the form is valid
			disabled : true,
			handler : function() {
				var form = this.up("form").getForm();
				if (form.isValid()) {
					form.submit({
						waitMsg : "保存中...",
						success : function(form, action) {
							Ext.Msg.alert("提示", action.result.tip.msg);
							edit_form_panel_win.close();
							dataStore.load();
						},
						failure : function(form, action) {
							Ext.Msg.alert("提示", action.result.tip.msg);
						}
					});
				}
			}
		} ]
	});

	var edit_form_panel_win = Ext.create("Ext.Window", {
		title : "商品分类编辑",
		closeAction : "hide",
		items : edit_form_panel,
		modal:true
	});

	var isEditInit = false;
	function myEdit(kid) {
		//alert(kid);
		Ext.Ajax.request({
			url : getServerHttp()+"/cp/billboard/edit_form.htm?kid=" +kid,
			success : function(response) {
				var json = Ext.JSON.decode(response.responseText);
				edit_form_panel.getForm().reset();
				edit_form_panel.getForm().setValues(json);
				$("#logo_img1").attr("src","/neshop2/img/"+json.main_file_id+".htm?mw=120&mh=120");
				edit_form_panel_win.show();
				if(isEditInit==false){
					initUploader(1);
					isEditInit = true;
				}
			},
			failure : function(response) {
				Ext.Msg.alert("提示", "操作失败!");
			}
		});
	}//#myEdit