var edit_form_panel;
var edit_form_panel_win;
var orderStore;
// 设置orderStore列表数据源参数
orderStore = Ext.create('Ext.data.Store',
    {
        remoteSort : true,
        autoLoad : true,
        sorters :
        {
            property : 'name',
            direction : 'ASC'
        },
        proxy :
        {
            type : "ajax",
            url : getServerHttp() + '/cp/order/deliver/list_logistics.htm',
            reader :
            {
                type : 'json',
                root : 'list',
            }
        }
    }
    ); // #comboxStore

edit_form_panel = Ext.create("Ext.form.Panel",
    {
        url :  getServerHttp() + "/cp/order/deliver/add.htm",
        buttonAlign : "center",
        bodyStyle : "padding: 10px;",
        defaultType : "textfield",
        width : 400,
        items : [
            {
                fieldLabel : "订单号",
                name : "order_id",
                readOnly : true
            },
            {
                id : "roleCombo",
                xtype : 'combo',
                editable : false,
                minChars : 1,
                fieldLabel : "物流公司",
                name : "logistics_com_code",
                emptyText : '请选择',
                store : orderStore,
                displayField : 'name',
                valueField : 'code',
                hiddenName : 'logistics_com_code',
                multiSelect : false,
                mode : 'remote',
                queryParam : 'logistics_codes',
                triggerAction : 'all',
                selectOnFocus : true,
                forceSelection : true,
                allowBlank : false
            },
            {
                fieldLabel : "运单编号",
                name : "logistics_code",
                allowBlank : false
            },
            {
                fieldLabel : "kidd",
                name : "kidd",
                hidden : true
            }
        ],
        buttons : [
            {
                text : "发货",
                formBind : true, // only enabled once the form is valid
                disabled : true,
                handler : function ()
                {
                    var form = this.up("form").getForm();
                    if (form.isValid())
                    {
                        form.submit(
                        {
                            waitMsg : "操作中...",
                            success : function (form, action)
                            {
                                Ext.Msg.alert("提示", action.result.tip.msg);
                                edit_form_panel_win.close();
                                dai_dataStore.load();
                                //location = "javascript:location.reload()"; // 窗口关闭后刷新当前页面
                            },
                            failure : function ()
                            {
                                Ext.Msg.alert("提示", "发货失败!");
                            }
                        }
                        );
                    }

                }
            }
        ]
    }
    );

edit_form_panel_win = Ext.create("Ext.Window",
    {
        title : "发货信息",
        closeAction : "hide",
        items : edit_form_panel,
        modal:true,
    }
    );

function myAdd(order_no,kid)
{
    edit_form_panel.getForm().reset();
    edit_form_panel.getForm().findField('order_id').setValue(order_no);
    edit_form_panel.getForm().findField('kidd').setValue(kid);
    edit_form_panel_win.show();
}
