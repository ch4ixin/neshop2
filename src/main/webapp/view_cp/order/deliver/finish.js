var dataStore;
var find1_form_panel;
var find1_form_panel_win;

// 设置dataStore列表数据源参数
shou_dataStore = Ext.create('Ext.data.Store', {
	remoteSort : true,
	autoLoad : true,
	pageSize : 20,
	sorters : {
		property : 'created',
		direction : 'DESC'
	},
	proxy : {
		type : "ajax",
		url : getServerHttp() + "/cp/order/deliver/list_Take.htm",
		reader : {
			type : 'json',
			root : 'list',
			totalProperty : 'page.totalRow'
		}
	},
	listeners : {
		'beforeload' : function(store, op, options) {
			var params = shou_searchFormPanel.getForm().getValues();
			Ext.apply(shou_dataStore.proxy.extraParams, params);
		}
	}
}); // #dataStore


var shou_searchFormPanel = Ext.create('Ext.form.Panel', {
	frame : true,
	title : "搜索条件",
	style : {
		marginBottom : '5px'
	},
	bodyStyle : {
		padding : '10px'
	},
	buttonAlign : "center",
	collapsible : true,
	defaultType : "textfield",
	items : [ {
		xtype : "container",
		layout : "hbox",
		items : [ {
			xtype : "textfield",
			fieldLabel : "订单号",
			name : "srh.order_no"
		}]
	} ],
	buttons : [ {
		text : "搜索",
		icon : jcapp.getIcon("magnifier.png"),
		handler : function() {
			shou_dataStore.load({
			//params:params()
			});
			shou_dataStore.sync();
		}
	}, {
		text : "重置",
		icon : jcapp.getIcon("arrow_refresh.png"),
		handler : function() {
			this.up('form').getForm().reset();
			shou_dataStore.load({});
			shou_dataStore.sync();
		}
	} ]
}); //#fa_searchFormPanel

finishListPanel = Ext.create('Ext.grid.Panel', {
	title : '已收货',
	tbar : shou_searchFormPanel,
	dockedItems : [ {
		xtype : 'pagingtoolbar',
		store : shou_dataStore, // same store GridPanel is using
		dock : 'bottom',
		displayInfo : true
	} ],
	store : shou_dataStore, 
    viewConfig:{  
        enableTextSelection:true  
    },
	columns : [ {
		text : '订单号',
		dataIndex : 'order_id',
		flex : 1
	}, {
		text : '商品名称',
		dataIndex : 'pname',
		flex : 1
	}, {
		text : '数量',
		dataIndex : 'cnt',
		flex : 0.5
	}, {
		text : '规格(单位)',
		dataIndex : 'unit',
		flex : 0.5
	}, {
		text : '下单用户昵称',
		dataIndex : 'mname',
		flex : 1
	}, {
		text : '收件人',
		dataIndex : 'rname',
		flex : 1
	}, {
		text : '电话',
		dataIndex : 'rphone',
		flex : 1
	}, {
		text : '地址',
		dataIndex : 'address',
		flex : 1
	}, {
		text : '物流公司',
		dataIndex : 'name',
		flex : 1
	}, {
		text : '运单编号',
		dataIndex : 'logistics_code',
		flex : 1
	}, {
		text : '发货时间',
		dataIndex : 'created',
		flex : 1,
		renderer : function(val) {
			if (val != '') {
				return Ext.Date.format(new Date(val), "Y-m-d H:i:s");
			}
		}
	}, {
		text : '订单状态',
		dataIndex : 'status_logistics',
		flex : 1
	}, {
		text : '操作者',
		dataIndex : 'shop_admin_id',
		flex : 1,
		hidden : true
	}, {
		xtype : "actioncolumn",
		align : "center",
		text : '操作',
		items : [ {
			xtype : 'button',
			tooltip : '查看',
			icon : jcapp.getIcon("look.png"),
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				myFind1(rec.get('kid'));
			}
		}/*, {
            xtype : "container"
        }, {
			xtype : 'button',
			tooltip : '删除',
			icon : jcapp.getIcon("delete.png"),
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				myDel(rec.get('kid'));
			}
		} */]

	} ]
}); // #finishListPanel

find1_form_panel = Ext.create("Ext.form.Panel", {
	buttonAlign : "cnter",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	items : [ {
		xtype : 'displayfield',
		fieldLabel : "发货时间",
		name : "created",
		renderer : function(val) {
			if (val != '') {
				return Ext.Date.format(new Date(val), "Y-m-d H:i:s");
			}
		},
	}, {
		xtype : 'textarea',
		fieldLabel : "物流跟踪",
		width : 750,
		height : 350,
		name : "explain_traces",
		readOnly : true
	}, {
		xtype : 'displayfield',
		fieldLabel : "快递状态",
		name : "state",
		readOnly : true,
		hidden : false
	} ]
});//#find1_form_panel

find1_form_panel_win = Ext.create("Ext.Window", {
	title : "物流信息",
	closeAction : "hide",
	modal:true,
	items : find1_form_panel
});// #find1_form_panel_win

function myFind1(kid) {
	Ext.Ajax.request({
		url :getServerHttp() + "/cp/order/deliver/add_form.htm?kid="
				+ kid,
		success : function(response) {
			var json = Ext.JSON.decode(response.responseText);
			find1_form_panel.getForm().reset();
			find1_form_panel.getForm().setValues(json);
			find1_form_panel_win.show();
		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});
} // #myFind1

function myDel(kid) {
	Ext.Msg.confirm("提示:", "确定删除选定的记录?", function(e) {
		if (e == "yes") {
			Ext.Ajax.request({
				url :getServerHttp() + "/shop/order/deliver/del.htm?kid=" + kid,
				success : function(response) {
					var json = Ext.decode(response.responseText);
					Ext.Msg.alert("提示", json.tip.msg);
					shou_dataStore.load();
				},
				failure : function(response) {
					Ext.Msg.alert("提示", "操作失败!");
				}
			});
		}
	});
}//#myDel
