var cateStore = Ext.create('Ext.data.Store', {
	remoteSort : true,
	autoLoad : true,
	pageSize : 50,
	sorters : {
		property : 'code',
		direction : 'asc'
	},
	proxy : {
		type : "ajax",
		url : getServerHttp() + "/cp/product/cate/list_data.htm",
		reader : {
			type : 'json',
			root : 'list',
			totalProperty : 'page.totalRow'
		}
	}
});

onemenuStore = getStore('t_product_cate'); // 一级菜单
twomenuStore = getStore('t_product_cate'); // 二级菜单
threemenuStore = getStore('t_product_cate');// 三级菜单
var productStore = Ext.create('Ext.data.Store', {
	fields : [ 'name', 'kid' ],
	autoLoad : true,
	remoteSort : true,
	sorters : {
		property : 'name',
		direction : 'ASC'
	},
	proxy : {
		url : getServerHttp() + "/cp/product/info/brand.htm",
		type : "ajax",
		reader : {
			type : 'json',
			root : 'list'
		}
	}
});

var img_src = '../../../../resource/img/plus.png';

add_form_panel = Ext.create("Ext.form.Panel", {

	title : '商品信息',
	height : 130,
	width : 550,
	frame : true,
	layout : 'form',
	buttonAlign : "center",
	border :false,
	items : [ {

		items : [ {
			fieldLabel : "商品名称",
			labelWidth : 130,
			xtype : "textfield",
			name : 'name',
			allowBlank : false,
			style : 'margin-left:5px',
		},{
			xtype : "panel",
			name : 'file_ids',
			html : '<div style="height:160px;margin-bottom:5px;">' + '<span id="picker1"style="vertical-align:middle;color:#666666;display:block;margin-right:75px;float:left">商品图片<b style="color:red;">*</b>:</span>'
				+ '<div style="position:relative;float:left">'
				+ '<span  style="display:block;margin-bottom:5px;color:red">&nbsp;&nbsp;*可上传五张图片(第一张默认为主图片)</span>'
				+ '<div style="margin-right:15px;float:left;width:104px;height:104px;border-radius:4%;"><div style="position:relative;"><img class="img-square" src="'+img_src+'"><div class="closeLayer" onClick="cancelUpload(this)" onmouseover="showClose(this)" onmouseout="hideClose(this)"><img src="../../../../resource/img/close.png" hidden></div></div><div class="picker">上传图片</div><input name="file_id" type="hidden"/></div>'
				+ '<div style="margin-right:15px;float:left;width:104px;height:104px;border-radius:4%;"><div style="position:relative;"><img class="img-square" src="'+img_src+'"><div class="closeLayer" onClick="cancelUpload(this)" onmouseover="showClose(this)" onmouseout="hideClose(this)"><img src="../../../../resource/img/close.png" hidden></div></div><div class="picker">上传图片</div><input name="file_id" type="hidden"/></div>'
				+ '<div style="margin-right:15px;float:left;width:104px;height:104px;border-radius:4%;"><div style="position:relative;"><img class="img-square" src="'+img_src+'"><div class="closeLayer" onClick="cancelUpload(this)" onmouseover="showClose(this)" onmouseout="hideClose(this)"><img src="../../../../resource/img/close.png" hidden></div></div><div class="picker">上传图片</div><input name="file_id" type="hidden"/></div>'
				+ '<div style="margin-right:15px;float:left;width:104px;height:104px;border-radius:4%;"><div style="position:relative;"><img class="img-square" src="'+img_src+'"><div class="closeLayer" onClick="cancelUpload(this)" onmouseover="showClose(this)" onmouseout="hideClose(this)"><img src="../../../../resource/img/close.png" hidden></div></div><div class="picker">上传图片</div><input name="file_id" type="hidden"/></div>'
				+ '<div style="margin-right:15px;float:left;width:104px;height:104px;border-radius:4%;"><div style="position:relative;"><img class="img-square" src="'+img_src+'"><div class="closeLayer" onClick="cancelUpload(this)" onmouseover="showClose(this)" onmouseout="hideClose(this)"><img src="../../../../resource/img/close.png" hidden></div></div><div class="picker">上传图片</div><input name="file_id" type="hidden"/></div>'
				+ '</div>'
				+ '<input name="file_ids" type="hidden">'+ '</div>',
			border : false,
		},{
			fieldLabel : "商品分类",
			labelWidth : 130,
			style : 'margin-left:5px',
			name : "cate_code",
			allowBlank : false,
			xtype : "combobox",
			emptyText : '请选择分类',
			store : cateStore,
			displayField : 'name',
			valueField : 'kid',
			editable : false
		},
		{
			fieldLabel : "运费",
			labelWidth : 130,
			xtype : 'numberfield',
			style : 'margin-left:5px',
			name : 'carriage',
			allowDecimals : true, // 允许小数点
			minValue : 0,
			value : '0',
			allowNegative : false
		}, 
		{
			fieldLabel : "续件运费",
			labelWidth : 130,
			xtype : 'numberfield',
			style : 'margin-left:5px',
			name : 'more_carriage',
			allowDecimals : true, // 允许小数点
			minValue : 0,
			value : '0',
			allowNegative : false
		}, {
			fieldLabel : "商品价格",
			xtype : 'numberfield',
			labelWidth : 130,
			name : 'price',
			value : '0',
			minValue : 0,
			allowDecimals : true, // 允许小数点
			allowNegative : false,
			style : 'margin-left:5px',
		// 不允许负数
		}, {
			fieldLabel : "市场价格",
			xtype : 'numberfield',
			labelWidth : 130,
			name : 'market_price',
			value : '0',
			minValue : 0,
			allowDecimals : true, // 允许小数点
			allowNegative : false,
			style : 'margin-left:5px',
		// 不允许负数
		}, {
			fieldLabel : "规格(单位)",
			labelWidth : 130,
			xtype : "textfield",
			name : 'unit',
			style : 'margin-left:5px',
		}, {
			fieldLabel : "排列位置",
			labelWidth : 130,
			xtype : "textfield",
			name : 'rank_code',
			style : 'margin-left:5px',
		}, {
			xtype : "radiogroup",
			fieldLabel : "是否上架",
			style : 'margin-left:5px',
			labelWidth : 130,
			width : 300,
			items : [ {
				name : "status_sys",
				boxLabel : "上架",
				inputValue : "上架中",
				checked : true
			}, {
				name : "status_sys",
				boxLabel : "下架",
				inputValue : "已下架"
			} ]
		}, {
			xtype : "textarea",
			fieldLabel : '多行',
			name : 'content',
			hidden : true
		} ]
	} ],
	buttons : [{
		text : "下一步",
		formBind : true, // only enabled once the form is valid
		disabled : true,
		handler : function() {
			tabs.setActiveTab(add_content_panel);
		}
	} ]

});

//显示close图标
function showClose(obj){
	var $img = $(obj).find('img');
	var $img_square = $(obj).parent().find(".img-square");
	if($img_square.attr('src') == img_src){
		return;
	}
	if($img.is(':hidden')){
		$img.show();
		$(obj).css('cursor','pointer');
	}
	
}

//关闭close图标
function hideClose(obj){
	var $img = $(obj).find('img');
	if($img.is(':visible')){
		$img.hide(); 
		$(obj).css('cursor','default');
	}
	
}

//删除上传图片
function cancelUpload(obj){
	
	var $parent = $(obj).parent().parent();
	var $img_square = $(obj).parent().find(".img-square");
	var file_id = $parent.find('input[name="file_id"]').val();
	for(var key in files){
		if(files[key] == file_id){
			//删除uploader中的缓存文件
			uploader1.removeFile(uploader1.getFile(files[key]),true);
			//将5个上传图片按钮对应位置的file_id清空
			files[key] = null;
			//将上传图片后返回的服务器file_id存储中的 key属性值删除
			delete file_ids[key];
		}
	}
	
	$img_square.attr('src',img_src);
	
	var $img = $(obj).find('img');
	$img.hide();
	$(obj).css('cursor','default');
	
	
}
