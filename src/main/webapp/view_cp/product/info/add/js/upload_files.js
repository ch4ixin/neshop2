var files = {};
//var $img = $('#logo_img');
//var $msg = $('#msg');

var uploader1;
var file_ids = {};
function initUploader() {
	
		var thumbnailWidth =120; 
		
		var thumbnailHeight=120;
		
		$(function() {
	//		$list1 = $('#thelist1');
	//		$btn1 = $('#ctlBtn');
	
			var state = 'pending';
	
			uploader1 = WebUploader.create({
				// 选完文件后，是否自动上传。
				auto : true,
				//禁用拖拽功能
				disableGlobalDnd : true,
				
				// swf文件路径
				swf : 'http://cdn.staticfile.org/webuploader/0.1.5/Uploader.swf',
				
				duplicate : true,
				// 文件接收服务端。
				server : '/neshop2/upload.htm',
				// 选择文件的按钮。可选。
				pick : {
					id:'.picker',
					label : '<span style="display:block;width:70px;">上传图片</span>', 
					multiple:false
				},
				// 内部根据当前运行是创建，可能是input元素，也可能是flash.
				
				// 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
				resize : false,
				accept:{
		          extensions:'gif,jpg,jpeg,png',//可接受的文件后缀名
		          mimeTypes:'image/*'
			    }
			});
	
			// 当有文件被添加进队列的时候
			uploader1.on('fileQueued', function(file) {
				//获取点击事件按钮的元素id
				var uploaderId = '#rt_'+file.source.ruid;
				var $s_parent = $(uploaderId).parent().parent();
				var $img = $s_parent.find('.img-square');
				var old_file_id = files[uploaderId];
				if(old_file_id != null){
					console.log('old_id++++++++++++'+old_file_id);
					uploader1.removeFile(uploader1.getFile(old_file_id),true);
				}
				files[uploaderId] = file.id;
				$s_parent.find('input[name="file_id"]').val(file.id);
				uploader1.makeThumb( file, function( error, src ) {
			        if ( error ) {
			            $img.replaceWith('<span>不能预览</span>');
			            return;
			        }
			   
			        $img.attr( 'src', src );
			    }, thumbnailWidth, thumbnailHeight );
			});
			// 文件上传过程中创建进度条实时显示。
			uploader1.on('uploadProgress',function(file, percentage) {
								
						var $li = $('#' + file.id), 
						$percent = $li.find('.progress .progress-bar');

						// 避免重复创建
						if (!$percent.length) {
							$percent = $(
									'<div class="progress progress-striped active">'
											+ '<div class="progress-bar" role="progressbar" style="width: 0%">'
											+ '</div>' + '</div>')
									.appendTo($li).find('.progress-bar');
						}

						$li.find('p.state').text('上传中');

						$percent.css('width', percentage * 100 + '%');
			});
	
			uploader1.on('uploadSuccess',function(file, response) {
					var uploaderId = '#rt_'+file.source.ruid;
					var server_file = $.parseJSON(response._raw);
					file_ids[uploaderId] = $.parseJSON(response._raw).file_id;					
			});
			
			uploader1.on('uploadError', function(file) {
				$('#' + file.id).find('p.state').text('上传出错');
			});
	
			uploader1.on('uploadComplete', function(file) {
				$('#' + file.id).find('.progress').fadeOut();
			});
		});
}