function getStore(table){
	 return Ext.create('Ext.data.Store', {
			fields : [ 'name', 'kid' ],
			autoLoad : true,
			remoteSort : true,
			sorters : {
				property : 'name', 
				direction : 'ASC'
			},
			proxy : {
				url : getServerHttp() + "/menu/all_data_by_field.htm?field=code&table="+table,
				type : "ajax",
				reader : {
					type : 'json',
					root : 'list'
				}
			}
		});
}