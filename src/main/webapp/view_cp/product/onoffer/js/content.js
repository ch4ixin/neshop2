var ue_context;
var edit_content_panel = Ext.create("Ext.form.Panel",{
					url : getServerHttp() + '/cp/onoffer/edit_content.htm',
					buttonAlign : "center",
					defaultType : "textfield",
					items : [{
								fieldLabel : "kid",
								name : "kid",
								hidden : true
							},{
								xtype : 'textarea',
								id : 'memo',
								//width : 300,
								//height : 300,
								name : 'content',
								fieldLabel : '商品描述',
								//hidden : true
							},{
								xtype : "panel",
								html : '<iframe src= "js/baidu_edit.html?V=1" width="100%" height="100%" marginwidth="0" framespacing="0" marginheight="0" frameborder="0" ></iframe>',
								width : '100%',
								height : 420,
								border : false
							}],
					buttons : [{
								text : "保存",
								formBind : true, // only enabled once the
								// form is valid
								disabled : true,
								handler : function() {
									// console.log(UE.getEditor("editor"))

									var form = this.up("form").getForm();
									
									is_submiting = true;
									var content = ue_context;
									form.findField("content").setValue(content);
									//将上传的图片返回的kid转换成字符串(eg:91Fg6b8m1BJJzeCne6pp3W,KwtKrFMr2Fz4P1XDmjC1s9,GZEsqGQMgVCkw2bYAfJw4C)
									if (form.isValid()) {
										form.submit({
											waitMsg : "保存中...",
											success : function(form, action) {
												Ext.Msg.alert("提示",action.result.tip.msg);
											},
											failure : function(form, action) {
												Ext.Msg.alert("提示","保存失败！");
											}
										});
									}

								}
							} ]
				});


var edit_content_panel_win = Ext.create("Ext.Window", {
	title : "商品描述编辑 ",
	closeAction : "hide",
	items : edit_content_panel,
	width : 1050,
	modal:true
});

function myContent(kid) {
	Ext.Ajax.request({
		url : getServerHttp() + "/cp/onoffer/edit_form.htm?kid=" + kid,
		success : function(response) {
			var json = Ext.JSON.decode(response.responseText);
			//console.log(json.product);
			edit_content_panel.getForm().reset();
			edit_content_panel.getForm().setValues(json.product);
	
			edit_content_panel_win.show();
			//console.log(window.opener);
		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});
}// #myEdit