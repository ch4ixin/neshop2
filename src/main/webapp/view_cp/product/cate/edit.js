var edit_form_panel = Ext.create("Ext.form.Panel", {
		url : getServerHttp()+"/cp/product/cate/edit.htm",
		buttonAlign : "cnter",
		bodyStyle : "padding: 10px;",
		defaultType : "textfield",
		items : [ {
			fieldLabel : "编号",
			name : "code",
			allowBlank : false
		}, {
			fieldLabel : "分类名称",
			name : "name"
		},{
			xtype : "panel",
			html : '<div><img id="logo_img2" src="../../../resource/img/product.png" width="120" height="120"></div>'
				+ '<div id="uploader" class="wu-example">'
				+ '<div id="thelist2" class="uploader-list">'
				+ '</div>'
				+ '<div class="btns">'
				+ '<div id="picker2">'
				+ '上传图标'
				+ '</div>'
				+ '</div>' + '</div>',
			width : '100%',
			height : 170,
			border : false,
			style : {
				marginBottom : '10px',
				paddingLeft : '130px',
			}
		},{
			name :"icon_file_id",
			hidden :true
		},{
			xtype : "panel",
			html : '<div><img id="logo_img1" src="../../../resource/img/product.png" width="120" height="120"></div>'
				+ '<div id="uploader" class="wu-example">'
				+ '<div id="thelist1" class="uploader-list">'
				+ '</div>'
				+ '<div class="btns">'
				+ '<div id="picker1">'
				+ '上传广告'
				+ '</div>'
				+ '</div>' + '</div>',
			width : '100%',
			height : 170,
			border : false,
			style : {
				marginBottom : '10px',
				paddingLeft : '130px',
			}
		},{
			name :"ad_file_id",
			hidden :true
		}, {
			fieldLabel :"id",
			name :"kid",
			hidden :true
		} ],
		buttons : [ {
			text : "保存",
			formBind : true, //only enabled once the form is valid
			disabled : true,
			handler : function() {
				var form = this.up("form").getForm();
				if (form.isValid()) {
					form.submit({
						waitMsg : "保存中...",
						success : function(form, action) {
							Ext.Msg.alert("提示", action.result.tip.msg);
							edit_form_panel_win.close();
							dataStore.load();
						},
						failure : function(form, action) {
							Ext.Msg.alert("提示", action.result.tip.msg);
						}
					});
				}
			}
		} ]
	});

	var edit_form_panel_win = Ext.create("Ext.Window", {
		title : "商品分类编辑",
		closeAction : "hide",
		items : edit_form_panel,
		modal:true
	});

	isInit = false;
	function myEdit(kid) {
		//alert(kid);
		Ext.Ajax.request({
			url : getServerHttp()+"/cp/product/cate/edit_form.htm?kid=" +kid,
			success : function(response) {
				var json = Ext.JSON.decode(response.responseText);
				edit_form_panel.getForm().reset();
				edit_form_panel.getForm().setValues(json);
				$("#logo_img1").attr("src","/neshop2/img/"+json.ad_file_id+".htm?mw=120&mh=120");
				console.log("/neshop2/img/"+json.ad_file_id+".htm?mw=120&mh=120");
				$("#logo_img2").attr("src","/neshop2/img/"+json.icon_file_id+".htm?mw=120&mh=120");
				console.log("/neshop2/img/"+json.icon_file_id+".htm?mw=120&mh=120");
				edit_form_panel_win.show();
				if(isInit==false){
					initUploader(1);
					initUploader(2);
					isInit = true;
				}
			},
			failure : function(response) {
				Ext.Msg.alert("提示", "操作失败!");
			}
		});
	}//#myEdit