<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>
<% 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragrma","no-cache"); 
response.setDateHeader("Expires",0); 
%>
<!DOCTYPE html >
<html>
<head>
<base href="${basePath}resource/mobile/" />
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="0">
<title>全部商品</title>
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<link rel="icon" href="../../../../resource/img/favicon.ico" type="image/x-ico" />
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/index.css"/>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/catalog.css"/>
<link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
<link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
<script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
<script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
<script src="${path }/resource/js/fastclick.js"></script>
<script src="${path }/resource/jqweui/js/swiper.min.js"></script>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/vant.css"/>
<!-- <script type="text/javascript" src="js/common.js"></script> -->
<style type="text/css">
.goods_nav{width:30%; float:right; right:5px; overflow:hidden; position:fixed;margin-top:25px; z-index:9999999}
.category2 dt{margin-left:1%;}
.category2 dd{margin-left:2%;width: 98%;}
/*.category2 dt a { width: 42%;float: left;padding: 3%;border: 1px solid #f4f4f4;margin-bottom: 1%;}*/
.pic_i{width:100%;height:120px;}
/*.category2 dt a {display: block;font-size: 16px;line-height: 20px;color: #999;background-color: #fff;margin-left:1%;}*/
.category2 dt {width: 98%;}
/*.title_box {font-size: 14px;background: #ffffff;color: #3d4145;padding: 5px;text-align: justify;overflow: hidden;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;}*/
.title_box{font-size: 12px;width: 100%;margin: auto;color: #3d4145;height: 30px;background: #ffffff;padding: 5px;overflow: hidden;text-align: left;line-height: 16px;-webkit-line-clamp: 2;-webkit-box-orient: vertical;display: -webkit-box;}
.price_box{background: #ffffff;}
.price_box span i{color: #FF0000}
.vf_3{
	background: url(${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/pub_main1.png) no-repeat 0 -72px;background-size: auto 400px;
}
.item{border-radius: 6px}
/*.pic_box{border-radius: 6px;border: 2px solid #f8b551}*/
.box{width: 50%;float: left;overflow: hidden;margin: 0px}
.index_pro{width: 95%;overflow: hidden;margin: auto;border: 1px solid #f4f4f4;margin-bottom: 6px;border-radius: 6px;background: #fff;}
.price {font-size: .9em;margin-right: 6px;}
.btns{position: absolute;display: block;right: 15px;height: 25px;width: 25px;background: #f23;border-radius: 50%;bottom: 8px;}
.price_pro {width: 100%;display: block;font-size: 16px;color: #ff2333;height: 30px;line-height: 30px;text-align: left;}
.btns img {display: block;width: 25px;height: 25px;}
.product_kuang {margin: auto;overflow: hidden;position: relative;border-radius: 6px;}
.product_kuang img {width: 90% !important;height: 142px;margin: auto;margin-left:5%;padding-top: 6%;}
.goods_name {font-size: 14px;width: 85%;color: #3d4145;height: 32px;overflow: hidden;text-align: left;line-height: 16px;margin: 10px;-webkit-line-clamp: 2;-webkit-box-orient: vertical;display: -webkit-box;}
.price {width: 100%;margin: auto;position: relative;padding-left: 10px;font-size: 0.9em;}
.summary {padding: 8px;text-align: right;background-color: white;}
.weui-popup__modal{
    min-height: 45%;
    max-height: 80%;
    overflow-y: visible;
    border-radius: 20px 20px 0 0;
    background: #FFFFFF;
}
.weui-popup__modal .modal-content{
    padding-top: 2.2rem;
}
.toolbar{
    background: #FFFFFF;
}
.weui-cells{
    font-size: 14px;
}
.weui-cell__ft{
    display: block;
    float: right;
}
.weui-count .weui-count__btn{
    border-radius: 0;
    border: none;
    width: 28px;
    height: 28px;
}
.weui-count__btn.weui-count__decrease{
    color: #323233;
    background-color: #f7f8fa;
}
.weui-count__btn.weui-count__increase{
    color: #323233;
    background-color: #f7f8fa;
}
.weui-count .weui-count__btn:after, .weui-count .weui-count__btn:before{
    background: #5f5f9e;
    vertical-align: middle;
}
.weui-btn{
    font-size: 14px;
    height: 40px;
    border-radius: 0;
}
.weui-btn+.weui-btn{
    margin-top: 0;
}
.weui-btn:after{
    border:none
}
</style>
</head>
<body style="width: 100%;height: 100%;overflow: hidden;">
  <div class="container">
    <div class="category-box">
      <div class="category1" style="outline: none;" tabindex="5000">
        <ul class="clearfix" style="padding-bottom:50px;">
           	<!-- <li class="cur" style="margin-top:46px"></li> -->
          <c:forEach items="${cateList }" var="one">
            <li>${one.name }</li>
          </c:forEach>
        </ul>
      </div>
      <div class="category2" style=" outline: none;" tabindex="5001">
        <dl style="display: none; margin-top:46px; padding-bottom:50px;display: block;">
        </dl>
      </div>
        <div id="show_product" class='weui-popup__container popup-bottom'>
            <div class="weui-popup__overlay"></div>
            <div class="weui-popup__modal">
                <div class="toolbar">
                    <div class="toolbar-inner">
                        <a href="javascript:;" class="picker-button close-popup"><i role="button" tabindex="0" class="van-icon van-icon-cross van-popup__close-icon van-popup__close-icon--top-right"><!----></i></a>
                        <h1 class="title"></h1>
                    </div>
                </div>
                <div class="modal-content">
                    <div class="weui-cells">
                        <div class="weui_cell weui-cell_swiped">
                            <div class="weui-cell__bd">
                                <div class="weui-cell">
                                    <div class="weui-cell__hd"><a id="popup_product_link" href=""><img id="popup_product_img" src="" alt="" style="width:90px;margin-right:5px;display:block"></a></div>
                                    <div class="weui-cell__bd">
                                        <p name="popup_product_name"></p>
                                        <p name="popup_product_kid" hidden></p>
                                    </div>
                                    <span style="color: #ff2333;font-size: 18px">￥<span class="price" name="popup_product_price"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="van-sku-stepper-stock">
                        <div style="float: left">数量</div>
                        <div class="weui-cell__ft">
                            <div class="weui-count" style="text-align: right">
                                <a class="weui-count__btn weui-count__decrease"></a>
                                <input class="weui-count__number" type="number" value="1" />
                                <a class="weui-count__btn weui-count__increase"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="summary">
                    共计 <strong name="popup_total_price"></strong> 元
                </p>
                <div class="van-sku-actions" style="margin-top: 20px;">
                    <button class="weui-btn weui-btn_primary" style="background: #f85;border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;" onclick="add_cart()" >添加购物车</button>
                    <button class="weui-btn weui-btn_primary" style="background: #ff2333;border-top-right-radius: 20px;
    border-bottom-right-radius: 20px;" onclick="buy_now()" >立即购买</button>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="v_nav">
    	<div class="vf_nav">
			<ul>
				<li><a href="${path }/wx/home.htm"><i class="vf_1"></i><span>首页</span></a></li>
				<li><a href="${path }/wx/classify.htm" style="color: #FF2233;"><i class="vf_3"></i><span>全部商品</span></a></li>
                <li><a href="${path }/wx/cart.htm"><i class="vf_4"></i><span>购物车</span></a></li>
                <li><a href="${path }/wx/user.htm"><i class="vf_5"></i><span>我的</span></a></li>
			</ul>
		</div>
  </div>
  <script type="text/javascript">
   	$(function(){
  		/*给第一个li添加样式和模拟点击事件  */
  		$(".clearfix li").eq(0).addClass("cur").css("margin-top","46px");
  		$(".clearfix li").eq(0).trigger('click');
  	})
  	//点击li以后，发送ajax,获得下级分类
  	$(".clearfix li").on("click",function(){
  		$.showLoading();
  		var oneName = $(this).html().trim();
  		$.ajax({
  			url:"${path}/wx/cy_product.htm?name="+oneName,
  			dataType:"json",
  			success:function(result){
  				/*清空div，再添加dl  */
  				$(".category2").empty();
  				console.log(result.product_cate.ad_file_id);
  				var ad_file_id = result.product_cate.ad_file_id;
  				if(ad_file_id === undefined || ad_file_id === null || ad_file_id === ""){
                    $dl = $('<dl style="display: none; margin-top:46px;display: block;"></dl>');
                }else{
                    $dl = $('<dl style="display: none; margin-top:46px;display: block;"><span>'+
                        '<a href="${path}/wx/cate_product.htm?cname='+oneName+'&rank_code=DESC">'+
                        '<img src="${path}/img/'+ad_file_id+'.htm"></a>' +
                        '</span></dl>');
  				}
  				$(".category2").append($dl);
				
  				var $dt = '<dt>@products</dt><dd><div class="fenimg"></div></dd>';
 				var products = '';
  				
 				/*添加dt,二级分类  */
  				var plist = result.list;
  				<%--for(var i=0;i<plist.length;i++){--%>
  				<%--	//products += '<a href=${path}/wx/orders.htm?kid='+pkid+'> '+pName+'</a>';--%>
  				<%--	products += '<a href=${path}/wx/orders.htm?kid='+plist[i].kid+' class="item">'--%>
		  			<%--			+'<div class="pic_box">'--%>
		  			<%--			+'<div class="active_box">'--%>
		  			<%--			+'</div>'--%>
		  			<%--			+'<img class="pic_i" src=${path}/img/'+plist[i].main_file_id+'.htm>'--%>
		  			<%--			+'</div>'--%>
		  			<%--			+'<div class="title_box">'--%>
		  			<%--			+''+plist[i].name+'</div>'--%>
		  			<%--			+'<div class="price_box">'--%>
		  			<%--			+'<span class="new_price">'--%>
		  			<%--			+'<i>￥'+plist[i].price+'</i>'--%>
		  			<%--			+'<s style="color:#999;font-size:13px;margin-left:15px;">￥'+plist[i].market_price+'</s>'--%>
		  			<%--			+'</span>'--%>
		  			<%--			+'</div>';--%>
  				<%--	//console.log(products);--%>
  				<%--	//在dl之后，添加兄弟dt--%>
  				<%--} --%>
                for(var i=0;i<plist.length;i++){
                    products +='<div class="box">'
                        +'<div class="index_pro">'
                        +'<a href=${path}/wx/orders.htm?kid='+plist[i].kid+'>'
                        +'<div class="product_kuang">'
                        +'<img src=${path}/img/'+plist[i].main_file_id+'.htm>'
                        +'</div>'
                        +'<div class="goods_name">'
                        +''+plist[i].name+'</div>'
                        +'</a>'
                        +'<div class="price" onclick="show_product(' + "'" +plist[i].kid+ "'" +')">'
                        +'<div class="btns">'
                        +'<img src="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/images/index_flow.png" >'
                        +'</div>'
                        +'<span class="price_pro">￥'
                        +'<i>'+plist[i].price+'</i>'
                        +'<s style="color:#999;font-size:12px;margin-left:2px;">￥'+plist[i].market_price+'</s>'
                        +'</span>'
                        +'</div>'
                        +'</div>'
                        +'</div>';
                    //console.log(products);
                    //在dl之后，添加兄弟dt
                }
                $dt = $dt.replace('@products',products);
  				//console.log($dt);
				$(".category2 dl").after($dt);
				$.hideLoading();
  			},
  			error:function(){
  				alert("加载失败");
  			}
  		})
  	})
    function show_product(kid){
        $.showLoading();
        $('.weui-count__number').val(1)
        var ajaxTimeOut = $.ajax({
            type : "GET",
            url: "${path}/wx/product.htm",
            timeout : 60000, //超时时间设置，单位毫秒
            data: {kid: kid},
            dataType: "json",
            success: function(data){
                $.hideLoading();
                console.log(data);
                $("#popup_product_img").attr("src","${path}/img/"+data.main_file_id+".htm");
                $('#popup_product_link').attr('href','${path}/wx/orders.htm?kid='+data.kid);
                $("p[name=popup_product_name]").html(data.name+"（"+data.unit+"）");
                $("span[name=popup_product_price]").html(data.price);
                $("strong[name=popup_total_price]").html(data.price);
                $("p[name=popup_product_kid]").html(data.kid);
                $("#show_product").popup();
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                $.hideLoading();
                $.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
            },
            complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                if(status=='timeout'){//超时,status还有success,error等值的情况
                    ajaxTimeOut.abort(); //取消请求
                    $.hideLoading();
                    $.toptip('😣 网络好像出问题了，请重试', 'warning');
                }
            },
            failure : function(response) {
                $.hideLoading();
                $.toptip('😣 好像出问题了，请重试', 'warning');
            }
        });
    }

    // 加购数量减少
    $('#divice').click(function(){
        var c=$('.van-stepper__input').val();
        if(c>1){
            c--;
            $('.van-stepper__input').val(c);
        }
    });
    // 加购数量增加
    $('#add').click(function(){
        var c=$('.van-stepper__input').val();
        c++;
        $('.van-stepper__input').val(c);
    });

    $('.search-type li').click(function() {
        $(this).addClass('cur').siblings().removeClass('cur');
        $('#searchtype').val($(this).attr('num'));
    });
    $('#searchtype').val($(this).attr('0'));

    var MAX = 99, MIN = 1;
    $('.weui-count__decrease').click(function (e) {
        var $input = $(e.currentTarget).parent().find('.weui-count__number');
        var number = parseInt($input.val() || "0") - 1
        if (number < MIN){
            number = MIN;
        }else{
            var price = $("span[name=popup_product_price]").html();
            $("strong[name=popup_total_price]").html(number*price);
        }
        $input.val(number)
    })
    $('.weui-count__increase').click(function (e) {
        var $input = $(e.currentTarget).parent().find('.weui-count__number');
        var number = parseInt($input.val() || "0") + 1
        if (number > MAX){
            number = MAX;
            $.toast("最多只能买99件哦！", "text");
        }else {
            var price = $("span[name=popup_product_price]").html();
            $("strong[name=popup_total_price]").html(number*price);
        }
        $input.val(number)
    })

    function add_cart(){
        var kid = $("p[name=popup_product_kid]").html();
        var number = $('.weui-count__number').val();

        $.showLoading();
        var ajaxTimeOut = $.ajax({
            type : "POST",
            url:"${path}/wx/add_cart.htm",
            data:{product_id:kid,number:number},
            timeout : 60000, //超时时间设置，单位毫秒
            dataType: "json",
            success: function(data){
                $.closePopup()
                $.hideLoading();
                $.toast("已添加购物车");
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                $.hideLoading();
                $.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
            },
            complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                if(status=='timeout'){//超时,status还有success,error等值的情况
                    ajaxTimeOut.abort(); //取消请求
                    $.hideLoading();
                    $.toptip('😣 网络好像出问题了，请重试', 'warning');
                }
            },
            failure : function(response) {
                $.hideLoading();
                $.toptip('😣 好像出问题了，请重试', 'warning');
            }
        });
    }

    function buy_now(){
        var kid = $("p[name=popup_product_kid]").html();
        var number = $('.weui-count__number').val();
        window.location.href="${path }/wx/buy_now.htm?product_id="+kid+"&number="+number;
    }
  </script>
  <script src="themesmobile/68ecshopcom_mobile/js/category.js"></script>
  <script src="themesmobile/68ecshopcom_mobile/js/jquery.nicescroll.min.js"></script>
</body>
</html>