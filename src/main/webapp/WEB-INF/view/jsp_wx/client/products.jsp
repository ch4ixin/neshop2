<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.weui-grid__icon {width: 100%;height: 120PX;}
		.weui-title {
		    font-size: 14px;
		    background: #ffffff;
		    color: #3d4145;
		    padding: 5px;
		    text-align: justify;
		    overflow: hidden;
		    text-overflow: ellipsis;
		    overflow: hidden;
		    white-space: nowrap;
		}
		.p_span {
		    background: #ffffff;
		}
		.weui-grid {
		    padding: 3px 3px;
		}
		.weui-grid:after {
		    border-bottom: 0px solid #d9d9d9; 
		}
		.weui-grid:before {
		    border-right: 0px solid #d9d9d9;
		}
	</style>
  </head>
  <body>
		<div class="weui-grids">
			<c:forEach items="${product_list}" var="list">
				<a href="${path}/wx/orders.htm?kid=${list.kid}" class="weui-grid js_grid">
				    <div class="weui-grid__icon">
				      <img src="${path}/img/${list.main_file_id}.htm" alt="">
				    </div>
				    <p class="weui-title">${list.name}</p>
					<p class="p_span"><%-- ${list.name}  --%><span style="color:#FF0000;" class="sspan">￥${list.price}</span><s style="color:#999;font-size:12px;margin-left:4px;">￥${list.market_price}</s></p>
				  </a>
			</c:forEach>
		</div>
 	   <script>
		  $(function() {
	  	  $.showLoading();
		 	document.onreadystatechange = function(){   
		        if(document.readyState=="complete"){   
			        $.hideLoading();
		        } 
			}
	        FastClick.attach(document.body);
		  });
		</script>

  </body>

</html>
