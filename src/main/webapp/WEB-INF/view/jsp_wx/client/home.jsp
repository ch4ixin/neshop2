<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>
<% 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragrma","no-cache"); 
response.setDateHeader("Expires",0); 
%> 
<!DOCTYPE html >
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">
<title>首页</title>
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/index.css"/>
<%--<script type="text/javascript" src="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/js/jquery.js"></script>--%>
<%--<script type="text/javascript" src="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/js/TouchSlide.1.1.js"></script>--%>
<%--<script type="text/javascript" src="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/js/jquery.more.js"></script>--%>
<link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
<link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
<script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
<script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
<script src="${path }/resource/js/fastclick.js"></script>
<script src="${path }/resource/jqweui/js/swiper.min.js"></script>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/vant.css"/>
<%--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>--%>
<%--<script src="https://cdn.jsdelivr.net/npm/vant@2.9/lib/vant.min.js"></script>--%>
<style type="text/css">
	.scrollimg{position:relative; overflow:hidden; margin:0px auto; /* 设置焦点图最大宽度 */}
	.scrollimg .hd{ position: absolute;bottom:0px;text-align: center;width: 100%;}
	.scrollimg .hd li{display: inline-block;width: .4em;height: .4em;margin: 0 .4em;-webkit-border-radius: .8em;-moz-border-radius: .8em;-ms-border-radius: .8em;-o-border-radius: .8em; border-radius: .8em;background: #FFF;filter: alpha(Opacity=60);opacity: .6;box-shadow: 0 0 1px #ccc; text-indent:-100px; overflow:hidden; }
	.scrollimg .hd li.on{ filter: alpha(Opacity=90);opacity: .9;background: #f8f8f8;box-shadow: 0 0 2px #ccc; }
	.scrollimg .bd{position:relative; z-index:0;}
	.scrollimg .bd li{position:relative; text-align:center;}
	.scrollimg .bd li img{background:url(${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/loading.gif) center center no-repeat;  vertical-align:top; width:100%;/* 图片宽度100%，达到自适应效果 */}
	.scrollimg .bd li a{-webkit-tap-highlight-color:rgba(0,0,0,0);}  /* 去掉链接触摸高亮 */
	.scrollimg .bd li .tit{display:block; width:100%;  position:absolute; bottom:0; text-indent:10px; height:28px; line-height:28px; background:url(${path }/resource/mobile/themesmobile/68ecshopcom_mobile/images/focusBg.png) repeat-x; color:#fff;  text-align:left;}
	.scroll_hot .bd ul li {width: 50%;}
	.scroll_hot .bd ul li .product_kuang {margin: auto;overflow: hidden;position: relative;border-radius: 6px}/*border: 1px solid #f8b551;*/
	.scroll_hot .bd ul li .product_kuang img {width: 90% !important;height:167px;margin: auto;padding-top: 6%;}
	.scroll_hot .hd .on {color: #D6505B;}
	.scroll_hot .bd ul li .goods_name {font-size:13px; padding: 0;width:88%; margin:10px; color:#3d4145;height:32px; overflow:hidden;text-align:left; line-height:16px;-webkit-line-clamp: 2;-webkit-box-orient: vertical;display: -webkit-box;}
	/*.vf_5 {*/
	/*    background: url(../images/pub_goods.png) no-repeat;*/
	/*    background-size: auto 400px;*/
	/*}*/
	.vf_1{background: url(${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/pub_main1.png) no-repeat 0 -1px;background-size: auto 400px;}
	.swiper-container{background:#ffffff;}
	.swiper-container-horizontal>.swiper-pagination-bullets{bottom:-4px;}
	.swiper-slide img{width:100%}
	.summary {padding: 8px;text-align: right;background-color: white;}
	.price {font-size: .9em;margin-right: 6px;}
	.swiper-pagination-bullet{
		background: #282828;
	}
	.swiper-pagination-bullet-active{
		background: #ff2333;
	}
	.weui-popup__modal{
		min-height: 45%;
		max-height: 80%;
		overflow-y: visible;
		border-radius: 20px 20px 0 0;
		background: #FFFFFF;
	}
	.weui-btn{
		font-size: 14px;
		height: 40px;
		border-radius: 0;
	}
	.weui-cells{
		font-size: 14px;
	}
	.weui-count .weui-count__btn{
       border-radius: 0;
		border: none;
		width: 28px;
		height: 28px;
	}
    .weui-count__btn.weui-count__decrease{
		color: #323233;
		background-color: #f7f8fa;
	}
	.weui-count__btn.weui-count__increase{
		color: #323233;
		background-color: #f7f8fa;
	}
	.weui-count .weui-count__btn:after, .weui-count .weui-count__btn:before{
		background: #5f5f9e;
		vertical-align: middle;
	}
	.weui-cell__ft{
		display: block;
		float: right;
	}
	.weui-btn+.weui-btn{
		margin-top: 0;
	}
	.weui-btn:after{
		border:none
	}
	.weui-popup__modal .modal-content{
		padding-top: 2.2rem;
	}
	.toolbar{
		background: #FFFFFF;
	}
</style>
<body>
<div id="page" class="showpage">
	<div>
		<div id="fake-search" class="index_search">
			<div class="index_search_mid">
				<span><img src="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/xin/sousuo.jpg"></span>
				<input  type="text" id="search_text" class="search_text" value="请输入您要搜索的商品"/>
			</div>
		</div>
		<div class="swiper-container" >
			<div class="swiper-wrapper">
				<c:forEach items="${billboards }" var="b">
					<div class="swiper-slide">
						<a href='${path}/wx/orders.htm?kid=${b.product_id}' >
							<img src='${path }/img/${b.main_file_id }.htm' alt=""/></a>
					</div>
				</c:forEach>
			</div>
			<div class="swiper-pagination"></div><!--分页器-->
		</div>
		<div class="entry-list clearfix">
			<nav>
				<ul style="margin-bottom: 10px">
					<c:forEach items="${cateList}" var="cl">
						<li><a href="${path }/wx/cate_product.htm?cname=${cl.name}&rank_code=DESC"><img alt="${cl.name}" src="${path }/img/${cl.icon_file_id}.htm" /><span>${cl.name}</span></a></li>
					</c:forEach>
				</ul>
			</nav>
		</div>
		<div>
			<img src="${path}/resource/img/tuijian.jpg" style="height: 40px;display: block;margin: auto;">
		</div>
		<section class="index_floor">
			<div class="floor_body1" >
				<div id="scroll_hot" class="scroll_hot">
					<div class="bd">
						<ul id="product_list" >
							<c:forEach items="${list}" var="l">
								<li>
									<div class="index_pro">
										<a href="${path}/wx/orders.htm?kid=${l.kid}" title="${l.name }">
											<div class="product_kuang">
												<img src="${path}/img/${l.main_file_id}.htm"></div>
											<div class="goods_name">${l.name }</div>
										</a>
										<div class="price" onclick="show_product('${l.kid}')">
											<div class="btns">
												<img src="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/images/index_flow.png" alt="">
											</div>
											<span class="price_pro">￥${l.price }<s style="color:#999;font-size:12px;margin-left:2px;">￥${l.market_price }</s></span>
										</div>
									</div>
								</li>
							</c:forEach>
						</ul>
					</div>
<%--					<div class="hd"><ul></ul></div>--%>
				</div>
			</div>
		</section>
		<div class="weui-loadmore" id="loading_more">
			<i class="weui-loading"></i>
			<span class="weui-loadmore__tips">正在加载</span>
		</div>
		<footer>
<%--			<div class="footer" id="no_more">--%>
<%--				<p class="mf_o4"><a href="http://www.sxxlkj.com" style="color: #586c94;font-size: 10px;">中科讯龙</a> 提供技术支持</p>--%>
<%--			</div>--%>
			<div class="weui-loadmore weui-loadmore_line weui-loadmore_dot"  id="no_more">
				<span class="weui-loadmore__tips"></span>
			</div>
			<div style="height:50px; line-height:50px; clear:both;"></div>
			<div class="v_nav">
				<div class="vf_nav">
					<ul>
						<li><a href="${path }/wx/home.htm" style="color: #FF2233;"><i class="vf_1"></i><span>首页</span></a></li>
						<li><a href="${path }/wx/classify.htm"><i class="vf_3"></i><span>全部商品</span></a></li>
						<li><a href="${path }/wx/cart.htm"><i class="vf_4"></i><span>购物车</span></a></li>
						<li><a href="${path }/wx/user.htm"><i class="vf_5"></i><span>我的</span></a></li>
					</ul>
				</div>
			</div>
		</footer>
		<a href="javascript:goTop();" class="gotop"><img src="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/topup.png"></a>
		<div id="show_product" class='weui-popup__container popup-bottom'>
			<div class="weui-popup__overlay"></div>
			<div class="weui-popup__modal">
				<div class="toolbar">
					<div class="toolbar-inner">
						<a href="javascript:;" class="picker-button close-popup"><i role="button" tabindex="0" class="van-icon van-icon-cross van-popup__close-icon van-popup__close-icon--top-right"><!----></i></a>
						<h1 class="title"></h1>
					</div>
				</div>
				<div class="modal-content">
					<div class="weui-cells">
							<div class="weui_cell weui-cell_swiped">
								<div class="weui-cell__bd">
									<div class="weui-cell">
										<div class="weui-cell__hd"><a id="popup_product_link" href=""><img id="popup_product_img" src="" alt="" style="width:90px;margin-right:5px;display:block"></a></div>
										<div class="weui-cell__bd">
											<p name="popup_product_name"></p>
											<p name="popup_product_kid" hidden></p>
										</div>
										<span style="color: #ff2333;font-size: 18px">￥<span class="price" name="popup_product_price"></span></span>
									</div>
								</div>
							</div>
					</div>
					<div class="van-sku-stepper-stock">
						<div style="float: left">数量</div>
						<div class="weui-cell__ft">
							<div class="weui-count" style="text-align: right">
								<a class="weui-count__btn weui-count__decrease"></a>
								<input class="weui-count__number" type="number" value="1" />
								<a class="weui-count__btn weui-count__increase"></a>
							</div>
						</div>
					</div>

				</div>
				<p class="summary">
					共计 <strong name="popup_total_price"></strong> 元
				</p>
				<div class="van-sku-actions" style="margin-top: 20px;">
					<button class="weui-btn weui-btn_primary" style="background: #f85;border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;" onclick="add_cart()" >添加购物车</button>
					<button class="weui-btn weui-btn_primary" style="background: #f23;border-top-right-radius: 20px;
    border-bottom-right-radius: 20px;" onclick="buy_now()" >立即购买</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="search_hide" class="search_hide">
	<h2> <span class="close"><img src="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/close.png"></span>搜索</h2>
	<div id="mallSearch" class="search_mid">
		<div id="search_tips" style="display:none;"></div>
		<ul class="search-type">
			<li  class="cur"  num="0">商品</li>
		</ul>
		<div class="searchdotm">
			<form class="mallSearch-form" method="get" name="searchForm" id="searchForm" action="${path }/wx/search.htm" onSubmit="">
				<div class="mallSearch-input">
					<div id="s-combobox-135">
						<input aria-haspopup="true" role="combobox" class="s-combobox-input" name="pname" id="pname" tabindex="9" accesskey="s" onkeyup="" autocomplete="off"  value="请输入关键词" onFocus="if(this.value=='请输入关键词'){this.value='';}else{this.value=this.value;}" onBlur="if(this.value=='')this.value='请输入关键词'"  type="text">
					</div>
					<input type="submit" value="" class="button"  >
				</div>
			</form>
		</div>
	</div>
	<section class="mix_recently_search"><h3>热门搜索</h3>
		<ul>
			<li>
				<a href="${path }/wx/search.htm?pname=商务汾阳王">商务汾阳王</a>
			</li>
			<li>
				<a href="${path }/wx/search.htm?pname=福益德亚麻籽油">福益德亚麻籽油</a>
			</li>
			<li>
				<a href="${path }/wx/search.htm?pname=汾酒股份20年">汾酒股份20年</a>
			</li>
			<li>
				<a href="${path }/wx/search.htm?pname=老闫家芝麻饼">老闫家芝麻饼</a>
			</li>
			<li>
				<a href="${path }/wx/search.htm?pname=老闫家芝麻饼">东杏老酒</a>
			</li>
		</ul>
	</section>
</div>
<script type="text/javascript" language="JavaScript" >
	$(document).ready(function() {
		//首先将.gotop隐藏
		$(".gotop").hide();
		//当滚动条的位置处于距顶部600像素以下时，跳转链接出现，否则消失
		$(function() {
			$(window).scroll(function() {
				if ($(window).scrollTop() > 600) {
					$(".gotop").fadeIn(200);
				} else {
					$(".gotop").fadeOut(200);
				}
			});
		});
	});
	function goTop(){
		$('html,body').animate({'scrollTop':0},600);
		return false;
	}

	var mySwiper = new Swiper('.swiper-container', {
		autoplay: 3000,//可选选项，自动滑动
		autoplayDisableOnInteraction : false,
		parallax : true,
		pagination : '.swiper-pagination',
		paginationClickable :true,
		loop:true,   // 形成环路  最后一张跳转到第一张
	});

	$(function() {
		FastClick.attach(document.body);
		$('#no_more').hide();
		$.showLoading();
		document.onreadystatechange = function(){
			if(document.readyState=="complete"){
				$.hideLoading();
			}
		}
		$('#search_text').click(function(){
			$(".showpage").children('div').hide();
			$("#search_hide").css('position','fixed').css('top','0px').css('width','100%').css('z-index','999').show();
		})
		$('#get_search_box').click(function(){
			$(".showpage").children('div').hide();
			$("#search_hide").css('position','fixed').css('top','0px').css('width','100%').css('z-index','999').show();
		})
		$("#search_hide .close").click(function(){
			$(".showpage").children('div').show();
			$("#search_hide").hide();
		})
	});

	function show_product(kid){
		$.showLoading();
		$('.weui-count__number').val(1)
		var ajaxTimeOut = $.ajax({
			type : "GET",
			url: "${path}/wx/product.htm",
			timeout : 60000, //超时时间设置，单位毫秒
			data: {kid: kid},
			dataType: "json",
			success: function(data){
				$.hideLoading();
				console.log(data);
				$("#popup_product_img").attr("src","${path}/img/"+data.main_file_id+".htm");
				$('#popup_product_link').attr('href','${path}/wx/orders.htm?kid='+data.kid);
				$("p[name=popup_product_name]").html(data.name+"（"+data.unit+"）");
				$("span[name=popup_product_price]").html(data.price);
				$("strong[name=popup_total_price]").html(data.price);
				$("p[name=popup_product_kid]").html(data.kid);
				$("#show_product").popup();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				$.hideLoading();
				$.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
			},
			complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
				if(status=='timeout'){//超时,status还有success,error等值的情况
					ajaxTimeOut.abort(); //取消请求
					$.hideLoading();
					$.toptip('😣 网络好像出问题了，请重试', 'warning');
				}
			},
			failure : function(response) {
				$.hideLoading();
				$.toptip('😣 好像出问题了，请重试', 'warning');
			}
		});

	}

	// 加购数量减少
	$('#divice').click(function(){
		var c=$('.van-stepper__input').val();
		if(c>1){
			c--;
			$('.van-stepper__input').val(c);
		}
	});
	// 加购数量增加
	$('#add').click(function(){
		var c=$('.van-stepper__input').val();
		c++;
		$('.van-stepper__input').val(c);
	});

	$('.search-type li').click(function() {
		$(this).addClass('cur').siblings().removeClass('cur');
		$('#searchtype').val($(this).attr('num'));
	});
	$('#searchtype').val($(this).attr('0'));

	var MAX = 99, MIN = 1;
	$('.weui-count__decrease').click(function (e) {
		var $input = $(e.currentTarget).parent().find('.weui-count__number');
		var number = parseInt($input.val() || "0") - 1
		if (number < MIN){
			number = MIN;
		}else{
			var price = $("span[name=popup_product_price]").html();
			$("strong[name=popup_total_price]").html(number*price);
		}
		$input.val(number)
	})
	$('.weui-count__increase').click(function (e) {
		var $input = $(e.currentTarget).parent().find('.weui-count__number');
		var number = parseInt($input.val() || "0") + 1
		if (number > MAX){
			number = MAX;
			$.toast("最多只能买99件哦！", "text");
		}else {
			var price = $("span[name=popup_product_price]").html();
			$("strong[name=popup_total_price]").html(number*price);
		}
		$input.val(number)
	})

	var loading = false;
	var pageNumber = 1;
	var now_kid = "${list[list.size()-1].kid}";
	$(document.body).infinite(1000).on("infinite", function() {
		if(loading) return;
		loading = true;
		setTimeout(function() {
			var ajaxTimeOut = $.ajax({
				type : "GET",
				url: "${path}/wx/product_list.htm",
				timeout : 60000, //超时时间设置，单位毫秒
				data: {pageNumber: pageNumber+1},
				dataType: "json",
				success: function(data){
					pageNumber = pageNumber+1;
					if(data.list.length > 0){
						if(data.list[data.list.length-1].kid === now_kid){
							$('#loading_more').hide();
							$('#no_more').show();
							$(document.body).destroyInfinite();
						}
						for (var i=0;i<data.list.length;i++){
							var product = '<li><div class="index_pro">'+
							'			<a href="${path}/wx/orders.htm?kid='+data.list[i].kid+'" title="'+data.list[i].name+'">'+
							'				<div class="product_kuang">'+
							'					<img src="${path}/img/'+data.list[i].main_file_id+'.htm"></div>'+
							'				<div class="goods_name">'+data.list[i].name+'</div>'+
							'			</a>'+
							'			<div class="price" onclick="show_product(&kid&)">'+
							'				<div class="btns">'+
							'					<img src="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/images/index_flow.png" >'+
							'				</div>'+
							'				<span class="price_pro">￥'+data.list[i].price+'<s style="color:#999;font-size:12px;margin-left:2px;">￥'+data.list[i].market_price+'</s></span>'+
							'			</div>'+
							'		</div></li>';
							$("#product_list").append(product.replace("&kid&","'"+data.list[i].kid+"'"));
						}
						now_kid = data.list[data.list.length-1].kid;
					}else{
						$('#loading_more').hide();
						$('#no_more').show();
						$(document.body).destroyInfinite();
					}
					loading = false;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					$('#loading_more').hide();
					$('#no_more').show();
					$(document.body).destroyInfinite();
					$.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
				},
				complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
					if(status=='timeout'){//超时,status还有success,error等值的情况
						ajaxTimeOut.abort(); //取消请求
						$('#loading_more').hide();
						$('#no_more').show();
						$(document.body).destroyInfinite();
						$.toptip('😣 网络好像出问题了，请重试', 'warning');
					}
				},
				failure : function(response) {
					$('#loading_more').hide();
					$('#no_more').show();
					$(document.body).destroyInfinite();
					$.toptip('😣 好像出问题了，请重试', 'warning');
				}
			});
			loading = false;
		}, 500);
	});

	function add_cart(){
		var kid = $("p[name=popup_product_kid]").html();
		var number = $('.weui-count__number').val();

		$.showLoading();
		var ajaxTimeOut = $.ajax({
			type : "POST",
			url:"${path}/wx/add_cart.htm",
			data:{product_id:kid,number:number},
			timeout : 60000, //超时时间设置，单位毫秒
			dataType: "json",
			success: function(data){
				$.closePopup()
				$.hideLoading();
				$.toast("已添加购物车");
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				$.hideLoading();
				$.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
			},
			complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
				if(status=='timeout'){//超时,status还有success,error等值的情况
					ajaxTimeOut.abort(); //取消请求
					$.hideLoading();
					$.toptip('😣 网络好像出问题了，请重试', 'warning');
				}
			},
			failure : function(response) {
				$.hideLoading();
				$.toptip('😣 好像出问题了，请重试', 'warning');
			}
		});
	}

	function buy_now(){
		var kid = $("p[name=popup_product_kid]").html();
		var number = $('.weui-count__number').val();
		window.location.href="${path }/wx/buy_now.htm?product_id="+kid+"&number="+number;
	}
</script>
</body>
</html>