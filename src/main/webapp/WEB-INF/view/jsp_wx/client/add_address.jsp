<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>新增收货地址</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
    <link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
    <link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
    <script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
    <script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
    <script src="${path }/resource/js/fastclick.js"></script>
    <script src="${path }/resource/jqweui/js/city-picker.min.js"></script>
	<style type="text/css">
		body,html{
			background-color: #f4f4f4;
		}
		div{font-size: 14px;}
		.weui-btn {width: 90%;font-size: 16px;}
        .weui-switch-cp__input:checked~.weui-switch-cp__box, .weui-switch:checked{
            border-color: #ff2333;
            background-color: #ff2333
        }
        .weui-cells{
           margin: 10px;
           border-radius: 8px;
        }
        .weui-btn_warn{
            background: #ff2333;
        }
        .weui-btn:after{
            display: none;
        }
        .weui-cells:after, .weui-cells:before{
            display: none;
        }
	</style>
  </head>
  <body ontouchstart>
    <div class="weui-cells weui-cells_form" style="line-height: 24px">
      <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">收货人</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" name="consignee" placeholder="真实姓名" value="${member.user_nick}">
        </div>
      </div>
      <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">联系电话</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" type="tel" name="mobile1" placeholder="手机号码">
        </div>
      </div>
      <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">所在地区</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" id="start" type="text" value="" placeholder="选择省/市/区">
          <input class="weui_input" type="hidden" name="province" value=""/>
		  <input class="weui_input" type="hidden" name="city" value=""/>
		  <input class="weui_input" type="hidden" name="region" value=""/>
        </div>
      </div>
	  <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">详细地址</label></div>
	    <div class="weui-cell__bd">
	      <input class="weui-input" type="text" name="address" placeholder="请填写详细地址">
	    </div>
	  </div>
    </div>
    <div class="weui-cells weui-cells_form">
      <div class="weui-cell weui-cell_switch">
        <div class="weui-cell__bd">设置为默认地址</div>
        <div class="weui-cell__ft">
          <input class="weui-switch" type="checkbox" aria-checked="false" onclick="onOff()">
          <input class="weui_input" type="hidden" name="is_default" value="是"/>
        </div>
      </div>
    </div>
    <br>
	<a href="javascript:submit();" class="weui-btn weui-btn_warn" style="font-size: 16px;background: #ff2333;border-radius: 26px">保存</a>
   	<script>
	  $(function() {
	    FastClick.attach(document.body);
	  });
	</script>
	<script>
      $("#start").cityPicker({
        title: "选择收货城市",
        onChange: function (picker, values, displayValues) {
          console.log(values, displayValues);
          console.log(displayValues);
	      $("input[name=province]").val(displayValues[0]);
	      $("input[name=city]").val(displayValues[1]);
	      $("input[name=region]").val(displayValues[2]);
        }
      });
      
      function onOff(){
	      console.log($('input[type=checkbox]').is(':checked'));
	      if($('.weui-switch').is(':checked') == true){
	    	  $("input[name=is_default]").val("是");
	      }else{
	    	  $("input[name=is_default]").val("否");
	      }
      }
      
      function submit() {
    		var member_name = $("input[name='consignee']").val(); // 收货人姓名
    		var province = $("input[name='province']").val(); // 省份
    		var city = $("input[name='city']").val(); // 城市
    		var region = $("input[name='region']").val(); // 区域
    		var street = $("input[name='address']").val();//街道
    		var mobile1 = $("input[name='mobile1']").val();//联系方式1
    		var is_default = $("input[name='is_default']").val();//联系方式1
    		
    		var phoneNum = /^1[345789]\d{9}$/;
    	    if (phoneNum.test(mobile1)) {
    	    	if(member_name == ''){
    	    		$.toptip('请输入真实姓名', 'warning');
    	    		return false;
    	    	}
    	    	if(city == ''){
    	    		$.toptip('请选择城市', 'warning');
    	    		return false;
    	    	}
    	        $.ajax({
        			type : "POST",
        			url : "${path}/wx/submit_address.htm",
        			data : "member_name=" + member_name + "&province=" + province + "&city=" + city + "&region=" + region + "&street=" + street+ "&mobile1=" + mobile1+ "&is_default=" + is_default+ "&kid=" + '${member_address.kid}',
        			dateType : 'json',
        			success : function(msg) {
        					$.toast("新增成功");
                            sessionStorage.setItem('refresh', 'true');
                            history.go(-1);
        					
        			}
        		}); 
       		}else{
       			$.toptip('请输入正确的手机号码！', 'warning');
       			return false;
       		}
    	}
	</script>

  </body>

</html>
