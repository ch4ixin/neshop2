<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>订单详情</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	  <link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
	  <link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
	  <script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
	  <script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
	  <script src="${path }/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.weui-cell__ft {color: #ff2333;}
		.weui-media-box {padding: 5px;}
		.demos-header {padding: 50px 0;background-color: #EBEBEB;}
		.icon-box {margin-bottom: 25px;margin-top: 25px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
		.icon-box i {margin: auto;text-align: center;}
		.demos-title span {color: #DBDBDB}
		.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;margin: 0 15%;}
		.weui-navbar__item{font-size: 14px;padding: 10px 0}
		.weui-navbar__item{color: #353535}
		.weui-navbar__item.weui-bar__item--on{color: #ff2333;background-color: #F4F4F4;border-top: 2px solid #ff2333;font-weight: 600;}
		.weui-tab__bd .weui-tab__bd-item{background: #F4F4F4}
		.weui-panel__bd .weui-media-box{height:83px;margin: 8px 10px 0px 10px;background: #fff;border-radius: 8px;padding:0 15px 0 0;}
		.weui-navbar+.weui-tab__bd{padding-top: 40px}
		.demos-header{background: #F4F4F4}
		.weui-panel:first-child{padding-top: 8px;margin: 0}
		.weui-navbar:after{display: none}
		.weui-navbar__item:after{display: none}
		.weui-panel{background: none}
		.weui-panel:after, .weui-panel:before{display: none}
		.weui-media-box:before{display: none}
		.icon{margin: 0 auto;width: 150px}
		.back{width: 25%;font-size: 14px;border-radius: 26px;border: 1px solid #ff2333;background: #f7f8fa;color: #ff2333;}
		.weui-media-box_appmsg .weui-media-box__hd {margin-left: .8em;}
		.evaluate{border-radius: 26px;color: #ff2333;border: 1px solid #ff2333;display: inline-block;padding: 0 1em;line-height: 2.1em;font-size: 11px;position: relative;right: -10px}
		.evaluate a{color: #ff2333}
		.evaluate .weui-media-box_appmsg,.evaluate .weui-panel__bd .weui-media-box{display: none}
	</style>
  </head>
  <body ontouchstart>
  <div id="order_lists" class="weui-popup__container">
	  <div class="weui-popup__overlay"></div>
	  <div class="weui-popup__modal">
			   <div class="weui-tab">
				  <div class="weui-navbar">
				    <a class="weui-navbar__item weui-bar__item--on cur" href="#tab1" id="tabb1">全部 </a>
				    <a class="weui-navbar__item" href="#tab2" id="tabb2">待付款</a>
				    <a class="weui-navbar__item" href="#tab3" id="tabb3">待发货</a>
				    <a class="weui-navbar__item" href="#tab4" id="tabb4">待收货</a>
				    <a class="weui-navbar__item" href="#tab5" id="tabb5">已完成</a>
				  </div>
				  <div class="weui-tab__bd">
				  	<div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
						<div class="weui-panel weui-panel_access">
							<div class="weui-panel__bd">
								<c:if test="${orderList=='[]'}" >
									<header class='demos-header'>
										<div class="icon-box">
											<img src="${path}/resource/img/zanwudingdan.png" class="icon">
										</div>
										<h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
										<a href="${path }/wx/home.htm" class="weui-btn back">返回首页</a>
									</header>
								</c:if>
								<c:if test="${orderList!='[]'}" >
									<div class="weui-cells__title">共${orderList.size()}条</div>
									<c:forEach items="${orderList }" var="ol">
										<a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
											<div class="weui-media-box__hd">
												<img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
											</div>
											<div class="weui-media-box__bd">
												<c:if test="${ol.status_order=='下单中'}" >
													<h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">待付款</span></h4>
												</c:if>
												<c:if test="${ol.status_order=='已下单'}" >
													<h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品<span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">待发货</span></h4>
												</c:if>
												<c:if test="${ol.status_order=='已发货' }" >
													<h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">待收货</span></h4>
												</c:if>
												<c:if test="${ol.status_order=='已收货'}" >
													<h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">待收货</span></h4>
												</c:if>
												<c:if test="${ol.status_order=='已完成'}" >
													<h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">已完成</span></h4>
												</c:if>
												<c:if test="${ol.status_order=='已退单'}" >
													<h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">已退款</span></h4>
												</c:if>
												<c:if test="${ol.status_order=='已取消'}" >
													<h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">已取消</span></h4>
												</c:if>
												<p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>
											</div>
											<div class="weui-cell__ft">￥${ol.total_price }</div>
										</a>
									</c:forEach>
									<div class="weui-loadmore weui-loadmore_line weui-loadmore_dot" id="no_more">
										<span class="weui-loadmore__tips"></span>
									</div>
								</c:if>
							</div>
						</div>
				    </div>
				    <div id="tab2" class="weui-tab__bd-item">
				      <div class="weui-panel weui-panel_access">
					  <div class="weui-panel__bd">
					  <c:if test="${stayPayOrder=='[]'}" >
				           <header class='demos-header'>
				              <div class="icon-box">
									<img src="${path}/resource/img/zanwudingdan.png" class="icon">
						      </div>
						       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
							   <a href="${path }/wx/home.htm" class="weui-btn back">返回首页</a>
					    </header>
					  </c:if>
					  <c:if test="${stayPayOrder!='[]'}" >
						  <div class="weui-cells__title">共${stayPayOrder.size()}条</div>
				          <c:forEach items="${stayPayOrder }" var="ol">
						    <a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
						      <div class="weui-media-box__hd">
						        <img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
						      </div>
						      <div class="weui-media-box__bd">
						        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">待付款</span></h4>
						        <p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>	
						      </div>
							  <div class="weui-cell__ft">￥${ol.total_price }</div>
						    </a>
					       </c:forEach>
						  <div class="weui-loadmore weui-loadmore_line weui-loadmore_dot" id="no_more">
							  <span class="weui-loadmore__tips"></span>
						  </div>
					  </c:if>
					  </div>
					</div>
				    </div>
				    <div id="tab3" class="weui-tab__bd-item">
				            <div class="weui-panel weui-panel_access">
					  <div class="weui-panel__bd">
					  <c:if test="${staySendOrder=='[]'}">
				           <header class='demos-header'>
				              <div class="icon-box">
									<img src="${path}/resource/img/zanwudingdan.png" class="icon">
						      </div>
						       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
							   <a href="${path }/wx/home.htm" class="weui-btn back">返回首页</a>
					    </header>
					  </c:if>
					  <c:if test="${staySendOrder!='[]'}" >
						  <div class="weui-cells__title">共${staySendOrder.size()}条</div>
				          <c:forEach items="${staySendOrder }" var="ol">
						    <a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
						      <div class="weui-media-box__hd">
						        <img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
						      </div>
						      <div class="weui-media-box__bd">
							        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">待发货</span></h4>
						        	<p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>	
						      </div>
						      <div class="weui-cell__ft">￥${ol.total_price }</div>
						    </a>
					       </c:forEach>
						  <div class="weui-loadmore weui-loadmore_line weui-loadmore_dot" id="no_more">
							  <span class="weui-loadmore__tips"></span>
						  </div>
					  </c:if>
					  </div>
					</div>
				    </div>
				   <div id="tab4" class="weui-tab__bd-item">
				            <div class="weui-panel weui-panel_access">
					  <div class="weui-panel__bd">
					  <c:if test="${finishOrder=='[]'}" >
				           <header class='demos-header'>
				              <div class="icon-box">
								  <img src="${path}/resource/img/zanwudingdan.png" class="icon">
						      </div>
						       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
							   <a href="${path }/wx/home.htm" class="weui-btn back">返回首页</a>
					    </header>
					  </c:if>
					  <c:if test="${finishOrder!='[]'}" >
						  <div class="weui-cells__title">共${finishOrder.size()}条</div>
				          <c:forEach items="${finishOrder }" var="ol">
						    <a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
						      <div class="weui-media-box__hd">
						        <img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
						      </div>
						      <div class="weui-media-box__bd">
						        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品<span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">待收货</span></h4>
						        <p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>	
						      </div>
						      <div class="weui-cell__ft">￥${ol.total_price }</div>
						    </a>
					       </c:forEach>
						  <div class="weui-loadmore weui-loadmore_line weui-loadmore_dot" id="no_more">
							  <span class="weui-loadmore__tips"></span>
						  </div>
					  </c:if>
					  </div>
					</div>
				    </div>
				     <div id="tab5" class="weui-tab__bd-item">
				            <div class="weui-panel weui-panel_access">
							  <div class="weui-panel__bd">
							  <c:if test="${zfinishOrder=='[]'}" >
						           <header class='demos-header'>
						              <div class="icon-box">
										  <img src="${path}/resource/img/zanwudingdan.png" class="icon">
								      </div>
								       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
									   <a href="${path }/wx/home.htm" class="weui-btn back">返回首页</a>
							      </header>
							  </c:if>
							  <c:if test="${zfinishOrder!='[]'}" >
								  <div class="weui-cells__title">共${zfinishOrder.size()}条</div>
						          <c:forEach items="${zfinishOrder }" var="ol">
								    <a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
								      <div class="weui-media-box__hd">
								        <img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
								      </div>
								      <div class="weui-media-box__bd">
								        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品<span style="color:#ff6a00;position: absolute;right: 16px;top: 6px;font-size: 12px">已完成</span></h4>
								        <p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>	
								      </div>
									  <div>
										<div class="weui-cell__ft" style="text-align: right;margin: 22px 0 0 0">￥${ol.total_price }</div>
										  <c:if test="${ol.is_discuzz=='是'}" >
											  <object class="evaluate"><a href="${path }/wx/order_eval.htm?oid=${ol.kid}">我的评价</a></object>
										  </c:if>
										  <c:if test="${ol.is_discuzz!='是'}" >
											  <object class="evaluate"><a href="${path }/wx/order_eval.htm?oid=${ol.kid}">立即评价</a></object>
										  </c:if>
									  </div>
								    </a>
							       </c:forEach>
								  <div class="weui-loadmore weui-loadmore_line weui-loadmore_dot" id="no_more">
									  <span class="weui-loadmore__tips"></span>
								  </div>
							  </c:if>
							  </div>
							</div>
				    </div>
				  </div>
				</div>
	  </div>
	</div>
 	   <script>

		   console.log(location.hash);
		   if(location.hash === "#tab1"){
			   $("#tabb1").trigger("click");
		   }
		   if(location.hash === "#tab2"){
			   $("#tabb2").trigger("click");
		   }
		   if(location.hash === "#tab3"){
			   $("#tabb3").trigger("click");
		   }
		   if(location.hash === "#tab4"){
			   $("#tabb4").trigger("click");
		   }
		   if(location.hash === "#tab5"){
			   $("#tabb5").trigger("click");
		   }
		  $(function() {
		 	$("#order_lists").popup();
			  document.addEventListener('visibilitychange', function() {
				  console.log(document.visibilityState)
				  // alert(document.visibilityState);
				  if(document.visibilityState === 'visible'){
					  history.go(0);
				  }
			  });
		    FastClick.attach(document.body);
		  });
		</script>
  </body>

</html>
