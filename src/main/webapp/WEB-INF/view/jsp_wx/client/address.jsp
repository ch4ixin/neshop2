<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
  	<link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
  	<link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
  	<script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
  	<script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
  	<script src="${path }/resource/js/fastclick.js"></script>
<%--  	<script src="${path }/resource/jqweui/js/city-picker.min.js"></script>--%>
	  <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/vant.css"/>
	<style type="text/css">
		body,html{background-color: #f3f3f3;}
		#add_address{position:absolute;bottom:60px;width: 100%;font-size: 16px;}
		#add_addresss{position:absolute;bottom:10px;width: 100%;font-size: 16px;}
		.weui-cells{
			padding: 12px;
			background-color: #fff;
			border-radius: 8px;
		}
		.weui-btn {width: 90%;font-size: 16px;}
		.demos-header {padding: 50px 0;background-color: #EBEBEB;}
		.demos-header{background: #F4F4F4}
		.icon-box {margin-bottom: 25px;margin-top: 25px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
		.icon-box i {margin: auto;text-align: center;}
		.icon{margin: 0 auto;width: 150px}
		.demos-title span {color: #DBDBDB}
		.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;margin: 0 15%;}
		.van-address-item__edit{font-size: 23px;margin-left: .2em;margin-right: .2em;}
		.weui-btn:after{display: none}
	</style>
  </head>
  <body ontouchstart>
    <div class="weui-cells__title"></div>
	<c:if test="${addressList=='[]'}" >
		<header class='demos-header'>
			<div class="icon-box">
				<i class="empty-list__icon" style="">
					<img src="${path}/resource/img/noAddress.png" width="150" height="150" alt="">
				</i>
			</div>
			<p class="demos-title" style="color:black;margin-bottom: 25px;">您还没有收货地址哦！</p>
		</header>
	</c:if>
	<c:if test="${addressList!='[]'}" >
		<div class="weui-cells weui-cells_radio" style="border-radius: 8px;margin: 10px">
			<c:forEach  items="${addressList}" var="a">
				  <a href="${path}/wx/edit_from_address.htm?kid=${a.kid}" class="weui-media-box weui-media-box_appmsg">
					<div class="weui-media-box__bd" >
					  <h4 class="weui-media-box__title" style="font-size: 12px;">${a.mobile1 } ${a.member_name } 收</h4>
					   <p style="font-size: 12px;"><span>${a.province } ${a.city } ${a.region } ${a.street }</span></p>
					</div>
					<div class="weui-cell__ft">
					  <c:if test="${a.is_default=='是' }">
						  <i class="weui-icon-success"></i>
					  </c:if>
					  <c:if test="${a.is_default=='否' }">
						  <i class="van-icon van-icon-edit van-address-item__edit"></i>
					  </c:if>
					</div>
				  </a>
			</c:forEach>
		</div>
	</c:if>
<%--     <a href="${path }/wx/order_detail.htm" id="add_address" class="weui-btn weui-btn_default" >返回订单</a>--%>
     <a href="${path }/wx/add_address.htm" id="add_addresss" class="weui-btn weui-btn_primary" style="background: #ff2333;width: -webkit-fill-available;border-radius: 26px;margin: 0 16px">新增收货地址</a>
 	<script>
	  $(function() {
		  console.log(sessionStorage.getItem('refresh'));
		  window.addEventListener('pageshow', function(event) {
			  if(event.persisted) { // ios 有效, android 和 pc 每次都是 false
				  location.reload();
			  } else { // ios 除外
				  if(sessionStorage.getItem('refresh') === 'true') {
					  location.reload();
				  }
			  }
			  sessionStorage.removeItem('refresh');
		  });
	    FastClick.attach(document.body);
	  });
	</script>
  </body>
</html>
