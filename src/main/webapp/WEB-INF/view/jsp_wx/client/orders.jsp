<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>${product.name }</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	  <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>
	  <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/index.css"/>
	  <link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
	  <link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
	  <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/vant.css"/>
	  <script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
	  <script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
	  <script src="${path }/resource/js/fastclick.js"></script>
	  <script src="${path }/resource/jqweui/js/swiper.min.js"></script>
	  <script src="http://res.wx.qq.com/open/js/jweixin-1.6.0.js"></script>
	  <script src="${path }/resource/js/clipboard.min.js"></script>
	  <style type="text/css">
		body,html{background-color: #f4f4f4;}
		.swiper-container{background:#ffffff;}
		.weui-cell__ft p{font-size: 15px;color: #FF6B2F;}
		.weui-cell_switch {padding-top: 10px;padding-bottom: 10px;}
		.swiper-container-horizontal>.swiper-pagination-bullets{bottom:-4px;}
		.weui-cells {font-size: 14px;}
		.swiper-slide{position: relative;height: 375px;}
		.swiper-slide img{ position: absolute;top: 0;bottom: 0;left: 0;right: 0;margin: auto;max-width: 100%;max-height: 100%;width: 100%}
		.swiper-pagination-bullet-active{background: #ff2333;}
		.fanhui_img{width:30px; height:30px; background:rgba(12,12,12,0.5); border-radius:50%;position:absolute;top:6px;bottom:0;left:6px;z-index:100;text-align:center;line-height:30px;}
		.fanhui_img img{width:12px;padding-top:4px;}
		.weui-cells{margin:0;}
		.count-box{ width:50%; background:#ffffff; position:absolute; top:1px; right:10px;}
		.count-box-top{width:100%; height:45px; padding-top:10px; text-align:center;}
		.count-box-top span{width: 30px; height: 32px; border:1px solid #ccc; display:inline-block; text-align: center; line-height: 30px; font-size: 18px; font-weight: bold; color: #3d4145;}
		.count-box-top .count-inp{border:1px solid #ccc; width:50px; font-weight:normal;line-height:30px;}
		.weui-cells:before{border-top: 0px solid #d9d9d9;}
		.weui-cells:after{border-bottom:none}
		.weui-c{padding: 20px 15px;}
		.my_divv{width: 100%;position: fixed;bottom: 0;background: #fff}
		.my_divv a{float:left;height: 50px;line-height: 50px;}
		.weui-btn+.weui-btn {margin-top:0;}
		.weui-btn {border-radius: 0px;padding: 0}
		.weui-btn:after { border-radius: 0px;border:none}
		.weui-photo-browser-modal.weui-photo-browser-modal-visible {z-index: 999;}
		.weui-panel__bd img{width:100%;}
		.detail_price{padding: 10px 10px 5px 10px;color: #ff2333;font-size: 22px;line-height:24px;}
		.weui-btn_default{background: #FFFFFF;}
		.weui-cell:before{border-top:none}
		.weui-popup__modal{background: #FFFFFF;border-radius: 16px 16px 0 0;}
		.service_item{padding: 0 16px;height: 100%;}
		.service_info{padding-top: 24px;}
		.service_title{font-size: 14px;font-weight: 500;color: #323233;}
		.service_content{margin-top: 8px;font-size: 13px;color: #969799;line-height: 18px;}
		.weui-cell__ft{
			display: block;
			float: right;
		}
		.weui-count .weui-count__btn{
			border-radius: 0;
			border: none;
			width: 28px;
			height: 28px;
		}
		.weui-count__btn.weui-count__decrease{
			color: #323233;
			background-color: #f7f8fa;
		}
		.weui-count__btn.weui-count__increase{
			color: #323233;
			background-color: #f7f8fa;
		}
		.weui-count .weui-count__btn:after, .weui-count .weui-count__btn:before{
			background: #5f5f9e;
			vertical-align: middle;
		}
		.good_share{position: absolute;right: 5%;top: 8px;}
		.share-pop-header{padding: 20px 0 4px;font-size: 14px;line-height: 20px;text-align: center;}
		.share-pop-content{position: relative;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: start;-webkit-align-items: flex-start;align-items: flex-start;padding: 16px 8px;overflow-x: scroll;}
		.share-pop__item{border: 0;padding: 0 0px;height: auto;line-height: auto;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-orient: vertical;-webkit-box-direction: normal;-webkit-flex-direction: column;flex-direction: column;-webkit-box-align: center;-webkit-align-items: center;align-items: center;-webkit-flex-shrink: 0;flex-shrink: 0;width: 80px;background-color: #fff;}
		.share-pop__item-icon{width: 48px;height: 48px;}
		.share-pop__item-text{color: #646566;font-size: 12px;margin-top: 8px;height: 16px;line-height: 16px;white-space: nowrap;}
		#share-guide{position: fixed;top: 0;left: 0;width: 100%;height: 100%;background-color: rgba(0,0,0,.8);z-index: 2000;padding-top: 140px;box-sizing: border-box;color: #fff;text-align: center;line-height: 30px}
		.share-guide__arrow{position: absolute;top: 12px;right: 33px;background: url(${path}/resource/img/share-arrow.png) no-repeat;width: 66px;height: 117px;background-size: contain;}
		.share-guide__title{font-size: 20px;font-weight: 500;}
	</style>
  </head>
  <body ontouchstart>
   <%-- <div class="fanhui_img" onclick="${path }/wx/home.htm"><img src="../../../neshop2/resource/img/fanhui.png"></div> --%>
		<div class="swiper-container" id="pb1">
		  <div class="swiper-wrapper">
		    <div class="swiper-slide"><img onclick="javascript:show_img('${path}/img/${product.main_file_id}.htm');" src="${path}/img/${product.main_file_id}.htm" alt=""></div>
		  </div>
		  <div class="swiper-pagination"></div>
		</div>
		<div class="weui-cells weui-cells_form">
			<div class="detail_price">
				<p>￥${product.price }<s style="color:#999;font-size:12px;margin-left:2px;">￥${product.market_price }</s></p>
			</div>
		  <div class="weui-cell" style="position: relative;padding: 10px 58px 10px 15px">
		    <div class="weui-cell__bd" style="font-size: 16px;font-weight: 500;width: 70%;">${product.name }</div>
			<a href="javascript:" class="good_share open-popup" style="color: rgb(100, 101, 102); font-size: 18px; display: block;line-height: 10px" data-target="#popup_share" >
                   <div class="tee-view vant-tee van-icon van-icon-share "></div>
				   <p style="font-size: 12px;line-height: 15px;margin-left: -2px">分享</p>
			</a>
		  </div>
		</div>
		<div class="weui-cells weui-cells_form" style="margin-top: 8px">
		  <div class="weui-cell">
		    <div class="weui-cell__hd"><label class="weui-label" style="color: #999">运费(首件)</label></div>
		    <div class="weui-cell__bd" style="width: 50%;background: #ffffff;position: absolute;top: 15%;right:-35%;font-size: 15px;color: #ff2333;">
		      <input class="weui-input" readonly="readonly" value="¥ ${product.carriage }">
		    </div>
		   </div>
		  <div class="weui-cell">
		    <div class="weui-cell__hd"><label class="weui-label" style="color: #999">运费(续件)</label></div>
		    <div class="weui-cell__bd" style="width: 50%;background: #ffffff;position: absolute;top: 15%;right:-35%;font-size: 15px;color: #ff2333;">
		      <input class="weui-input" readonly="readonly" value="¥ ${product.more_carriage }">
		    </div>
		   </div>
		   <div class="van-sku-stepper-stock">
		    <div style="float: left;width: 150px;color: #999">数量(${product.unit })</div>
<%--		    <div class="weui-cell__bd">--%>
<%--    			  <div class="count-box">--%>
<%--                      <div class="count-box-top" style="text-align: right;">--%>
<%--                          <span id="divice">-</span>--%>
<%--                          <span class="count-inp"><input  id="number" style="border:0;width:50px; text-align:center;" type="Number" value="1"></span>--%>
<%--                          <span id="add">+</span>--%>
<%--                      </div>--%>
<%--                  </div>--%>
<%--		    </div>--%>
			   <div class="weui-cell__ft">
				   <div class="weui-count" style="text-align: right">
					   <a class="weui-count__btn weui-count__decrease"></a>
					   <input class="weui-count__number" type="number" value="1" id="number" style="position: relative;top:-6px"/>
					   <a class="weui-count__btn weui-count__increase"></a>
				   </div>
			   </div>
		  </div>
			<div class="weui-panel__ft">
				<a href="javascript:" class="weui-cell weui-cell_access weui-cell_link open-popup" data-target="#popup_service">
					<div class="weui-cell__bd" style="color: #323233">精选好货 · 厂家发货配送 · 收货后结算</div>
					<span class="weui-cell__ft"></span>
				</a>
			</div>
		</div>
   		<c:if test="${product.content !=''}">
			<div class="weui-panel weui-panel_access" id="pcontent">
				<div class="weui-panel__hd">商品详情</div>
				<div class="weui-panel__bd">
						${product.content}
				</div>
				<div class="weui-loadmore">
					<i class="weui-loading"></i>
					<span class="weui-loadmore__tips">正在加载</span>
				</div>
			</div>
		</c:if>
       <a href="javascript:goTop();" class="gotop"><img src="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/topup.png"></a>
	   <footer>
<%--		   <div class="footer">--%>
<%--			   <p class="mf_o4"><a href="http://www.sxxlkj.com" style="color: #586c94;font-size: 10px;">中科讯龙</a> 提供技术支持</p>--%>
<%--		   </div>--%>
			<div class="weui-loadmore weui-loadmore_line weui-loadmore_dot" id="no_more">
				<span class="weui-loadmore__tips"></span>
			</div>
		   <div style="height:50px; line-height:50px; clear:both;"></div>
	   </footer>
		<div class="my_divv">
			<a href="${path }/view_cp/wx/kefu.html" style="width: 14%" class="weui-btn weui-btn_default" >
				<div class="van-icon van-icon-chat-o icon-btn__icon" style="height: 18px;line-height: 18px;position:absolute;top:10px;left: 22px"></div>
				<div class="" style="font-size: 12px;line-height: 14px;color: #646566;position:absolute;bottom: 2px;left: 18px">客服</div></a>
			<a href="${path }/wx/cart.htm" style="width: 14%" class="weui-btn weui-btn_default">
				<div class="tee-view vant-tee van-icon van-icon-shopping-cart-o icon-btn__icon" style="position:absolute;top:10px;left: 20px"></div>
				<div class="" style="font-size: 12px;line-height: 14px;color: #646566;position:absolute;bottom: 2px;left: 10px" >购物车</div></a>
			<a href="javascript:add_cart();"  style="background: #f85;color: #fff;width: 35%;font-size: 14px; border-radius: 26px 0 0 26px;height: 40px;line-height: 40px;margin-top: 5px;margin-left: 1%" class="weui-btn weui-btn_default">加入购物车</a>
			<a href="javascript:buy_now()" style="width: 35%;background: #ff2333;font-size: 14px; border-radius: 0 26px 26px 0;height: 40px;line-height: 40px;margin-top: 5px;margin-right: 1%" class="weui-btn weui-btn_primary">立即购买</a>
		</div>
	   <div id="popup_service" class='weui-popup__container popup-bottom'>
		   <div class="weui-popup__overlay"></div>
		   <div class="weui-popup__modal">
              <div class="service_item">
				  <div class="service_info">
					  <div class="service_title">精选好货</div>
					  <div class="service_content">严格把关挑选，正品保障</div>
				  </div>
				  <div class="service_info">
					  <div class="service_title">厂家发货配送</div>
					  <div class="service_content">由平台指定厂家或供应商提供发货和配送服务</div>
				  </div>
				  <div class="service_info">
					  <div class="service_title">收货后结算</div>
					  <div class="service_content">该店铺交易由平台提供资金存管服务，当符合以下条件时，资金自动结算给商家：买家确认收货或到达约定的自动确认收货日期。交易资金未经平台存管的情形（储值型、电子卡券等）不在本服务范围内。</div>
				  </div>
				  <div class="service_info">
					  <div class="service_content">该店铺交易由平台提供资金存管服务，当符合以下条件时，资金自动结算给商家：买家确认收货或到达约定的自动确认收货日期。交易资金未经平台存管的情形（储值型、电子卡券等）不在本服务范围内。</div>
				  </div>
				  <div class="service_content" style="text-align: center;margin-top: 20px">
					  <a href="javascript:;" class="weui_btn close-popup" style="width: 90%;height: 40px;background: #f44;border: none;border-radius: 26px;color: #ffffff;text-align: center;line-height: 40px;display: inline-block;margin-bottom: 4px;font-size: 14px;">我知道了</a>
				  </div>

			  </div>
		   </div>
	   </div>

   <div id="popup_share" class='weui-popup__container popup-bottom'>
	   <div class="weui-popup__overlay"></div>
	   <div class="weui-popup__modal">
			   <div class="tee-view share-pop-header">
				   <span class="tee-text">立即分享给好友</span>
			   </div>
			   <div class="tee-view share-pop-content">
				   <div class="tee-view share-item" >
					   <a href="javascript:" class="share-pop__item open-popup" data-target="#share-guide">
						  <div class="tee-view share-pop__item-icon">
							  <img src="${path}/resource/img/share-icon-wechat.png" alt="" style="width: 100%;height: 100%">
						  </div>
						   <span class="tee-text share-pop__item-text">分享给好友</span>
						   <span class="tee-text share-pop__item-desc"></span>
					 </a>
				   </div>
				   <div class="tee-view share-item" >
					   <a href="javascript:" class="share-pop__item open-popup" data-target="#share-guide">
						   <div class="tee-view share-pop__item-icon">
							   <img src="${path}/resource/img/share-sheet-wechat-moments.png" alt="" style="width: 100%;height: 100%">
						   </div>
						   <span class="tee-text share-pop__item-text">分享到朋友圈</span>
						   <span class="tee-text share-pop__item-desc"></span>
					   </a>
				   </div>
				   <div class="tee-view share-item" >
					   <div class="share-pop__item close-popup" id="copyBtn">
						   <div class="tee-view share-pop__item-icon">
							   <img src="${path}/resource/img/share-copy.png" alt="" style="width: 100%;height: 100%">
						   </div>
						   <span class="tee-text share-pop__item-text">复制链接</span>
						   <span class="tee-text share-pop__item-desc"></span>
					   </div>
				   </div>
			   </div>
			   <div class="service_content" style="text-align: center;margin-top: 20px">
				   <a href="javascript:;" class="weui_btn close-popup" style="width: 90%;height: 40px;background: #f44;border: none;border-radius: 26px;color: #ffffff;text-align: center;line-height: 40px;display: inline-block;margin-bottom: 4px;font-size: 14px;">取消</a>
			   </div>
	   </div>
   </div>

   <div id="share-guide" class="tee-view weui-popup__container close-popup">
	   <div class="tee-view share-guide__arrow"></div>
	   <div class="tee-view share-guide__title">立即分享给好友吧</div>
	   <div class="tee-view share-guide__sub" style="font-size: 14px">点击屏幕右上角将本页面分享给好友</div>
   </div>


	<script>

		$("#copyBtn").click(function(){
			var url = "【" +"${product.name}" + "】" + "这个我很喜欢，推荐给你，点击查看" +'👉'+ "http://zs.sxxlkj.com/neshop2/wx/orders.htm?kid=${product.kid}";
			console.log(url)
			var successful;
			if (navigator.userAgent.match(/(iPhone|iPod|iPad|iOS)/i)) { //ios
				var copyDOM = document.createElement('div');  //要复制文字的节点
				copyDOM.innerHTML = url;
				document.body.appendChild(copyDOM);
				var range = document.createRange();
				// 选中需要复制的节点
				range.selectNode(copyDOM);
				// 执行选中元素
				window.getSelection().addRange(range);
				// 执行 copy 操作
				successful = document.execCommand('copy');
				// 移除选中的元素
				window.getSelection().removeAllRanges();
				$(copyDOM).hide()
			}else{
				var oInput = document.createElement('input')
				oInput.value = url;
				document.body.appendChild(oInput)
				oInput.select() // 选择对象
				successful = document.execCommand('Copy') // 执行浏览器复制命令
				oInput.className = 'oInput'
				oInput.style.display = 'none'
				oInput.remove()
			}
			if(successful){
				$.toast("复制成功",{time:2000},function(){
					$("#popup_share").hide();
				});
			}else{
				$.toast("复制失败",{time:2000},function(){
					$("#popup_share").hide();
				});
			}
		});


	$(function(){
		wx.config({
			debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
			appId: '${jsConfig.appId}', // 必填，公众号的唯一标识
			timestamp: ${jsConfig.timestamp}, // 必填，生成签名的时间戳
			nonceStr: '${jsConfig.nonceStr}', // 必填，生成签名的随机串
			signature: '${jsConfig.signature}',// 必填，签名，见附录1
			jsApiList: ['checkJsApi','updateAppMessageShareData','updateTimelineShareData'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
		});

		FastClick.attach(document.body);
		// $.showLoading();
		document.onreadystatechange = function(){
			if(document.readyState=="complete"){
				$('.weui-loadmore').hide();
				$('#no_more').show();
				// $.hideLoading();
			}
		}

		//console.log('${product.file_ids}');
		file_ids = '${product.file_ids}';
		file_idss= file_ids.split(",");
		
		for (var i = 1; i < file_idss.length; i++) {
			//alert(file_idss[i]);
			$('.swiper-wrapper').append('<div class="swiper-slide"><img onclick="javascript:show_img(\'${path}/img/'+file_idss[i]+'.htm\');" src="${path}/img/'+file_idss[i]+'.htm" alt=""></div>');
		}

		wx.ready(function () {   //需在用户可能点击分享按钮前就先调用
			wx.checkJsApi({
				jsApiList: [
					'updateAppMessageShareData',
					'updateTimelineShareData',
				],
				success: function (res) {
					//alert(JSON.stringify(res));
				}
			});

			wx.updateAppMessageShareData({
				title: '${product.name} ￥${product.price} ', // 分享标题
				desc: '精选好货，为您严格把关挑选，厂家发货配送服务，正品保障。', // 分享描述
				link: 'http://zs.sxxlkj.com/neshop2/wx/orders.htm?kid=${product.kid}', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
				imgUrl: 'http://zs.sxxlkj.com/neshop2/img/${product.main_file_id}.htm', // 分享图标
				success: function () {
					// 设置成功
				}
			})
			wx.updateTimelineShareData({
				title: '${product.name} ￥${product.price} ', // 分享标题
				desc: '精选好货，为您严格把关挑选，厂家发货配送服务，正品保障。', // 分享描述
				link: 'http://zs.sxxlkj.com/neshop2/wx/orders.htm?kid=${product.kid}', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
				imgUrl: 'http://zs.sxxlkj.com/neshop2/img/${product.main_file_id}.htm', // 分享图标
				success: function () {
					// 设置成功
				}
			})
		});
	})
	$(document).ready(function() {
		//首先将.gotop隐藏
		$(".gotop").hide();
		//当滚动条的位置处于距顶部600像素以下时，跳转链接出现，否则消失
		$(function() {
			$(window).scroll(function() {
				if ($(window).scrollTop() > 600) {
					$(".gotop").fadeIn(200);
				} else {
					$(".gotop").fadeOut(200);
				}
			});
		});
	});
	function goTop(){
		$('html,body').animate({'scrollTop':0},600);
		return false;
	}

	function show_img(img) {
		$.photoBrowser({
			items: [
				img
			],
			onSlideChange: function(index) {
				console.log(this, index);
			},
			onOpen: function() {
				console.log("onOpen", this);
			},
			onClose: function() {
				console.log("onClose", this);
			}
		}).open();
	};
	
	var mySwiper = new Swiper('.swiper-container', {
		autoplay:2000,//可选选项，自动滑动
		autoplayDisableOnInteraction : false,
		parallax : true,
		pagination : '.swiper-pagination',
		paginationClickable :true,
	});

	$('.weui-count__decrease').click(function(){
	   var c=$('.weui-count__number').val();
	   if(c>1){
		c--;
		$('.weui-count__number').val(c);
	}
	});

	$('.weui-count__increase').click(function(){
		var c=$('.weui-count__number').val();
		c++;
		$('.weui-count__number').val(c);
	});

	function add_cart(){
		$.showLoading();
		var ajaxTimeOut = $.ajax({
			type : "POST",
			url:"${path}/wx/add_cart.htm",
			data:{product_id:'${product.kid}',number:$("#number").val()},
			timeout : 60000, //超时时间设置，单位毫秒
			dataType: "json",
			success: function(data){
				$.closePopup()
				$.hideLoading();
				$.toast("已添加购物车");
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				$.hideLoading();
				$.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
			},
			complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
				if(status=='timeout'){//超时,status还有success,error等值的情况
					ajaxTimeOut.abort(); //取消请求
					$.hideLoading();
					$.toptip('😣 网络好像出问题了，请重试', 'warning');
				}
			},
			failure : function(response) {
				$.hideLoading();
				$.toptip('😣 好像出问题了，请重试', 'warning');
			}
		});
	}

	function buy_now(){
		window.location.href="${path }/wx/buy_now.htm?product_id=${product.kid}&number="+$("#number").val();
	}

	// 分享朋友圈 已废弃
	function weixinShareTimeline(){
		WeixinJSBridge.invoke('shareTimeline',{
			"img_url":'http://zs.sxxlkj.com/neshop2/img/${product.main_file_id}.htm',
			//"img_width":"640",
			//"img_height":"640",
			"link":'http://zs.sxxlkj.com/neshop2/wx/orders.htm?kid=${product.kid}',
			"desc": '精选好货，严格把关挑选，正品保障',
			"title":'${product.name}'
		});
	}

	// 分享微信好友 已废弃
	function weixinSendAppMessage(){
		WeixinJSBridge.invoke('sendAppMessage',{
			//"appid":appId,
			"img_url":'http://zs.sxxlkj.com/neshop2/img/${product.main_file_id}.htm',
			//"img_width":"640",
			//"img_height":"640",
			"link":'http://zs.sxxlkj.com/neshop2/wx/orders.htm?kid=${product.kid}',
			"desc":'精选好货，严格把关挑选，正品保障',
			"title":'${product.name}'
		});
	}

	// 关闭分享遮罩
	$("#share-guide").click(function(){
			$("#share-guide").hide();
	});

	</script>
  </body>

</html>
