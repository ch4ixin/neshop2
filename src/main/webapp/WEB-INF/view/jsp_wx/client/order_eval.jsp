<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
<head>
    <title>订单评价</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
    <link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
    <link rel="stylesheet" type="text/css" href="${path}/resource/webuploader/webuploader2/webuploader.css">
    <script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
    <script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
    <script type="text/javascript" src="${path}/resource/style4wx_1/extend/jquery.raty.min.js"></script>
    <script type="text/javascript" src="${path}/resource/webuploader/webuploader.min.js"></script>
    <script src="${path }/resource/js/fastclick.js"></script>
    <style type="text/css">
        body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #f4f4f4;}
        .weui-cell{
            padding: 8px 12px;
        }
        .weui-cells{
            border-radius: 8px;
            width: 96%;
            margin: auto;
            line-height: 2.47;
        }
        .weui-cells:before{
            border-top: none
        }
        .weui-btn{
            background: #ff2333;
            font-size: 14px;
            line-height: 40px;
        }
        .weui-textarea{
            font-size: 14px;
        }
        .weui-uploader__file {
            width: 100px;
            height: 100px;
            position: relative;
            margin-right: 20px;
        }
        .closeLayer {
            z-index: 99;
            float: right;
            width: 23px;
            height: 23px;
            position: absolute;
            top: -17px;
            right: -12px;

        }
        .closeLayer img{
            width: 20px;
            height: 20px;
        }
        .weui-uploader__files{
            margin-top: 10px;

        }
        .weui-form-preview__btn{
            background: #ffffff;
            line-height: 40px;
            border-radius: 26px;
        }
        .weui-form-preview__btn:after{
            border-left: none;
        }
        .weui-form-preview__ft:before{
            border-top: none;
        }
        .weui-form-preview__ft:after{
            border-top: none;
        }
    </style>
</head>
<body ontouchstart>
<form id="discuzzForm" action="${path}/wx/eval_submit.htm" method="post">
    <input name="order_id" value="${order.kid}" type="hidden" />
    <input name="score" value="" type="hidden" />
    <div class="weui-cells__title"></div>
    <div class="weui-cells weui-cells_form">
        <c:forEach items="${detailList}" var="dl">
            <div class="weui-cell">
                <div class="weui-cell__hd"><img src="${path}/img/${dl.main_file_id}.htm" alt="" style="width:50px;margin-right:5px;display:block"></div>
                <div class="weui-cell__bd">
                    <p style="color: #323233;font-size: 14px;line-height: 30px">${dl.pname}</p>
                </div>
            </div>
        </c:forEach>
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <textarea class="weui-textarea" maxlength="200" name="discuzz" id="discuzz" readonly="readonly" placeholder="请输入宝贝评价~ ~" rows="3"
                          onchange="this.value=this.value.substring(0, 200)"
                          οnkeydοwn="this.value=this.value.substring(0, 200)"
                          onkeyup="this.value=this.value.substring(0, 200)">${discuzz.discuzz}</textarea>
                <c:if test="${order.is_discuzz!='是'}">
                    <div class="weui-textarea-counter"><span>0</span>/200</div>
                </c:if>
            </div>
        </div>
        <div class="weui-cell">
            <div class="weui-cell__bd">
                <div class="weui_uploader">
                    <div class="weui-uploader__hd">
                        <c:if test="${order.is_discuzz!='是'}">
                            <p class="weui-uploader__title"></p>
                            <div class="weui-uploader__info">0/5</div>
                        </c:if>
                    </div>
                    <div class="weui-uploader__bd">
                        <ul class="weui-uploader__files"></ul>
                        <c:if test="${order.is_discuzz!='是'}">
                            <div class="weui_cell_bd weui_cell_primary img_picker" ></div>
                        </c:if>
                    <input name="file_ids" value="${discuzz.file_ids}" type="hidden">
                    </div>
                </div>
            </div>
        </div>
        <div class="weui-cell">
            <div class="weui-cell__hd"><label class="weui-label">星级评分</label></div>
            <div class="weui-cell__bd" id="star" style="text-align: right;line-height: 15px;"></div>
        </div>
    </div>
    <c:if test="${order.is_discuzz!='是'}">
        <div class="weui-btn-area" style="padding: 10px;line-height: 15px;">
            <a href="javascript:mySubmit();" class="weui-btn weui-btn_warn" style="border-radius: 26px">提交评价</a>
        </div>
    </c:if>
    <c:if test="${order.is_discuzz=='是'}" >
        <div class="weui-form-preview__ft" style="padding: 10px;line-height: 15px;margin-top: 20px">
            <a href="${path }/wx/order_list.htm#tab5" class="weui-form-preview__btn weui-form-preview__btn_default" style="margin-right: 15px">返回订单</a>
            <a href="${path }/wx/home.htm" class="weui-form-preview__btn weui-form-preview__btn_default" style="background: #ff2333;color: #ffffff;margin-left: 15px">再逛一会</a>
        </div>
    </c:if>
</form>
</body>
<c:if test="${order.is_discuzz!='是'}">
    <script type="text/javascript">
        $(function () {
            $('#star').raty({
                number: 5,
                starOff:'${path}/resource/style4wx_1/images/star-off-big.png',
                starOn:'${path}/resource/style4wx_1/images/star-on-big.png',
                click: function(score, evt) {
                    $('#star').raty('score', score);
                    $("input[name=score]").val(score);
                }
            });
            $("#discuzz").attr("readonly",false);
        });
    </script>
</c:if>
<c:if test="${order.is_discuzz=='是'}">
    <script type="text/javascript">
        $(function () {
            var arr = $("input[name=file_ids]").val().split(",");
            console.log(arr);
            $('#star').raty({
                number: '5',
                starOff:'${path}/resource/style4wx_1/images/star-off-big.png',
                starOn:'${path}/resource/style4wx_1/images/star-on-big.png',
                readOnly: true,
                score:'${discuzz.score}'
            });
            if(arr != ""){
                for(var i=0;i<arr.length;i++){
                    var $preview = $('<li class="weui-uploader__file" style="background-image:url(${path}/img/'+arr[i]+'.htm)"></li>');
                    $('.weui-uploader__files').append($preview);
                }
            }
            $("#discuzz").attr("readonly",true);
        });
    </script>
</c:if>
<script type="text/javascript">
    var files = new Array();
    var file_ids = "";
    var $list = $('.weui-uploader__files');
    var uploader;
    $(function() {
        uploader = WebUploader.create({
            // 选完文件后，是否自动上传。
            auto: true,
            // swf文件路径
            swf: 'http://cdn.staticfile.org/webuploader/0.1.5/Uploader.swf',
            duplicate : true,
            // 文件接收服务端。
            server: '${path}/upload.htm',
            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '.img_picker',
            // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
            // resize: false,
            // 只允许选择图片文件。
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });

        //当有文件被添加进队列的时候
        uploader.on('fileQueued', function(file) {
            uploader.makeThumb( file, function( error, src ) {
                if ($(".weui-uploader__files li").length > 5) {
                    // $img.hide();
                    $.toptip('只能上传五张照片', 'error');
                    return;
                }else{
                    if($(".weui-uploader__files li").length == 0){
                        $list.append('<li class="weui-uploader__file" ></li>');
                    }
                    $list.append('<li class="weui-uploader__file weui-uploader__file_status" id="'+ file.id +'" style="background-image:url('+src+')"><div class="closeLayer" name="'+file.name+'" onClick="cancelUpload(this)" ><img src="${path}/resource/img/close1.png"></div><div class="weui-uploader__file-content">0%</div></li>');
                    if ( error ) {
                        $('#' + file.id).find('.weui-uploader__file-content').html('<i class="weui-icon-warn"></i>');
                        return;
                    }
                }
            });
        });
        //文件上传过程中创建进度条实时显示。
        uploader.on('uploadProgress', function(file, percentage) {
            $('#' + file.id).find('.weui-uploader__file-content').html(percentage * 100 + '%');
        });
        uploader.on('uploadSuccess', function(file, response) {
            $('#' + file.id).removeClass('weui-uploader__file_status');
            if (files.length < 5) {
                var end = $.parseJSON(response._raw);
                var file_ = {
                    id: end.file_id,
                    name: end.file_name
                }
                files.push(file_);
                // console.log(files);
                $('#' + file.id).css("background-image","url(${path}/img/"+ end.file_id + ".htm)");
                $('.weui-uploader__info').html(files.length + '/' + 5);
                getFileids();
            }
        });
        uploader.on('uploadError', function(file) {
            $('#' + file.id).find('.weui-uploader__file-content').html('<i class="weui-icon-warn"></i>');
        });
        uploader.on('uploadComplete', function(file) {
            $('#' + file.id).find('.weui-uploader__file-content').fadeOut();
        });

        FastClick.attach(document.body);
    });


    function mySubmit(){
        // $.confirm("确认提交吗？",function(){
            if($("input[name=score]").val()===''){
                $.toast("请为商品星级评分", "text");
                return;
            }
            $("#discuzzForm").submit();
        // });
    }

    //删除上传图片
    function cancelUpload(obj){
        uploader.removeFile(uploader.getFile($(obj).parent().attr('id')),true);
        $(obj).parent().remove();
        let arr_index = files.findIndex(value => {
            return value.name === obj.getAttribute("name");
        })
        files.splice(arr_index,1);
        console.log(files);
        $('.weui-uploader__info').html(files.length + '/' + 5);
        getFileids();
    }

    function getFileids() {
        if(files.length === 0){
            file_ids = "";
        }
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if (i === 0) {
                file_ids = file.id;
            } else {
                file_ids += "," + file.id
            }
        }
        $("input[name='file_ids']").val(file_ids);
    }

    $("#discuzz").change(function(){
        $(".weui-textarea-counter span").html($("#discuzz").val().length)
    })
    $("#discuzz").keydown(function(){
        $(".weui-textarea-counter span").html($("#discuzz").val().length)
    })
    $("#discuzz").keyup(function(){
        $(".weui-textarea-counter span").html($("#discuzz").val().length)
    })
</script>
</html>
