<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<!-- head 中 -->
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<!-- body 最后 -->
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	
	<!-- 如果使用了某些拓展插件还需要额外的JS -->
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body,html{background-color: #EBEBEB;}
		.weui-cell__ft p{font-size: 15px;color: #FF6B2F;}
		.weui-cell_switch {padding-top: 10px;padding-bottom: 10px;}
		.weui-btn_primary{width: 90%;}
		.weui-btn {width: 90%;font-size: 13px;}
		div{font-size: 14px;}
		.swiper-slide img{width:100%;height:250px;}
		.fanhui_img{width:30px; height:30px; background:rgba(12,12,12,0.5); border-radius:50%;position:absolute;top:6px;bottom:0;left:6px;z-index:100;text-align:center;line-height:30px;}
		.fanhui_img img{width:12px;padding-top:4px;}
		.weui-cells{margin:0;}
		.count-box{ width:50%; background:#ffffff; position:absolute; top:1px; right:10px;}
		.count-box-top{width:100%; height:45px; padding-top:10px; text-align:center;}
		.count-box-top span{width: 30px; height: 30px; border:1px solid #ccc; display:inline-block; text-align: center; line-height: 30px; font-size: 18px; font-weight: bold; color: #3d4145;}
		.count-box-top .count-inp{border:1px solid #ccc; width:50px; font-weight:normal;line-height:30px;}
		.weui-cells:before{border-top: 0px solid #d9d9d9;}
		.weui-c{padding: 20px 15px;}
		.my_divv{width: 100%;position: fixed;bottom: 0;}
		.my_divv a{float:left;height: 50px;line-height: 50px;}
		.weui-btn {font-size: 14px; border-radius: 0px;}
		.weui-btn+.weui-btn {margin-top:0;}
		.weui-btn_warn{width: 25%;}
		.weui-btn_default{width: 25%}
		.weui-btn_primary{width: 50%}
		.weui-btn:after { border-radius: 0px;}
		.weui-photo-browser-modal.weui-photo-browser-modal-visible {z-index: 999;}
	</style>
  </head>
  <body ontouchstart>
	    <div class="fanhui_img" onclick="javascript:history.go(-1)"><img src="../../../neshop2/resource/img/fanhui.png"></div>
<div class="swiper-container">
  <div class="swiper-wrapper">
    <div class="swiper-slide"><img onclick="javascript:show_img('${path}/img/${product.main_file_id}.htm');" src="${path}/img/${product.main_file_id}.htm" alt=""></div>
  </div>
  <div class="swiper-pagination"></div>
  </div>
		<div class="weui-cells weui-cells_form">
		  <div class="weui-cell weui-cell_switch">
		    <div class="weui-cell__bd">${product.name }</div>
		    <div class="weui-cell__ft">
		      <p>¥ ${product.price }</p>
		    </div>
		  </div>
		</div>
		<div class="weui-cells weui-cells_form">
		  <div class="weui-cell">
		    <div class="weui-cell__hd"><label class="weui-label">快递费用(首件)</label></div>
		    <div class="weui-cell__bd" style="width: 50%;background: #ffffff;position: absolute;top: 15%;right:-35%;font-size: 15px;color: #FF6B2F;">
		      <input class="weui-input" readonly="readonly" value="¥ ${product.carriage }">
		    </div>
		   </div>
		  <div class="weui-cell">
		    <div class="weui-cell__hd"><label class="weui-label">快递费用(续件)</label></div>
		    <div class="weui-cell__bd" style="width: 50%;background: #ffffff;position: absolute;top: 15%;right:-35%;font-size: 15px;color: #FF6B2F;">
		      <input class="weui-input" readonly="readonly" value="¥ ${product.more_carriage }">
		    </div>
		   </div>
		   <div class="weui-cell weui-c">
		    <div class="weui-cell__hd"><label class="weui-label">购买数量(${product.unit })</label></div>
		    <div class="weui-cell__bd">
    			  <div class="count-box">
                      <div class="count-box-top" style="text-align: right;">
                          <span id="divice">-</span>
                          <span class="count-inp"><input  id="number" style="border:0;width:50px; text-align:center;" type="Number" value="1"></span>
                          <span id="add">+</span>
                      </div>
                  </div>
		    </div>
		  </div>
		  <div class="weui-panel__ft" id="to_pcontent">
		    <a href="javascript:show_pcontent();" class="weui-cell weui-cell_access weui-cell_link">
		      <div class="weui-cell__bd">查看商品详情</div>
		      <span class="weui-cell__ft"></span>
		    </a>    
		  </div>
		</div>
		<div style="height: 60px;"></div>
		
		<div id="my_pcontent" class='weui-popup__container'>
	      <div class="weui-popup__overlay"></div>
	      <div class="weui-popup__modal">
	      	<div class="weui-panel weui-panel_access" id="pcontent">
			  <div class="weui-panel__hd">商品详情</div>
			  ${product.content }
	        <a href="javascript:;" style="width: 100%;height:50px;font-size: 18px;line-height: 50px;" class="weui-btn weui-btn_primary close-popup">关闭</a>
			</div>
	      </div>
	    </div>
		
		<div class="my_divv">
			<a href="javascript:del_cart();" class="weui-btn weui-btn_warn">删除</a>
			<a href="${path }/view_cp/wx/kefu.html" style="background: #FF6600;color: #fff;" class="weui-btn weui-btn_default">客服</a>
			<a href="javascript:edit_cart();" class="weui-btn weui-btn_primary">确定修改</a>
		</div>
		
      <div class="weui-tabbar" style="display:none;">
        <a href="${path }/wx/index.htm#tab1" class="weui-tabbar__item weui-bar__item--on">
          <div class="weui-tabbar__icon">
            <img src="${path }/resource/img/icon_nav_button.png" alt="">
          </div>
          <p class="weui-tabbar__label">首页</p>
        </a>
        <a href="${path }/wx/index.htm#tab2" class="weui-tabbar__item">
          <div class="weui-tabbar__icon">
            <img src="${path }/resource/img/icon_nav_cell.png" alt="">
          </div>
          <p class="weui-tabbar__label">分类</p>
        </a>
        <a href="${path }/wx/index.htm#tab3" class="weui-tabbar__item">
          <div class="weui-tabbar__icon">
            <span class="weui-badge" style="position: absolute;top: -.4em;right: 1em;">1</span>
            <img src="${path }/resource/img/icon_nav_msg.png" alt="">
          </div>
          <p class="weui-tabbar__label">购物车</p>
        </a>
        <a href="${path }/wx/index.htm#tab4" class="weui-tabbar__item">
          <div class="weui-tabbar__icon">
            <img src="${path }/resource/img/icon_nav_article.png" alt="">
          </div>
          <p class="weui-tabbar__label">我的</p>
        </a>
      </div>
    <script>
		  $(function() {
		    FastClick.attach(document.body);
		  });
	</script>
	<script>
 	function show_pcontent(){
 		$("#my_pcontent").popup();
 	}
 	
	function show_img(img) {
		$.photoBrowser({
		      items: [
		         img
		      ],
	       	  onSlideChange: function(index) {
	            console.log(this, index);
	          },
	          onOpen: function() {
	            console.log("onOpen", this);
	          },
	          onClose: function() {
	            console.log("onClose", this);
	          }
	    }).open();
    };
    
	$(function(){
		//console.log('${product.file_ids}');
		file_ids = '${product.file_ids}';
		file_idss= file_ids.split(",");
		
		for (var i = 1; i < file_idss.length; i++) {
			//alert(file_idss[i]);
			$('.swiper-wrapper').append('<div class="swiper-slide"><img onclick="javascript:show_img(\'${path}/img/'+file_idss[i]+'.htm\');" src="${path}/img/'+file_idss[i]+'.htm" alt=""></div>');
		}
		
	})
	
	var mySwiper = new Swiper('.swiper-container', {
		autoplay:2000,//可选选项，自动滑动
		autoplayDisableOnInteraction : false,
		parallax : true,
		pagination : '.swiper-pagination',
		paginationClickable :true,
	})
	  	function edit_cart(){
	  		$.ajax({
	    		url:"${path}/wx/edit_order.htm",
	    		type:"get",
	    		data:{"kid":"${order_detail.kid}","cnt":$("#number").val()},
	    		success:function(response){
	    			window.location = "/neshop2/wx/cart.htm";
	    		},
	    		error:function(){
	    			$.toast("修改失败", "forbidden");
	    		}
	    	}) 
	  	}
	  	
	  	function del_cart(){
	  		$.ajax({
	    		url:"${path}/wx/del_order.htm",
	    		type:"get",
	    		data:{"kid":"${order_detail.kid}"},
	    		success:function(response){
	    			window.location = "/neshop2/wx/cart.htm";
	    		},
	    		error:function(){
	    			$.toast("修改失败", "forbidden");
	    		}
	    	}) 
	  	}
	  	$('#divice').click(function(){
		   var c=$('.count-inp input').val();
		   if(c>1){
		    c--;
		    $('.count-inp input').val(c);
		}
		});
		$('#add').click(function(){
		    var c=$('.count-inp input').val();
		    c++;
		    $('.count-inp input').val(c);
		});
	</script>

  </body>

</html>
