<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>购物车</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
  	<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>
  	<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/index.css"/>
  	<link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
  	<link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
  	<script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
	  <script src="${path }/resource/js/fastclick.js"></script>
  	<script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.demos-title {text-align: center;font-size: 14px;color: black;font-weight: 400;margin: 0 15%;}
		.demos-sub-title {text-align: center;color: #888;font-size: 14px;}
		.demos-header {padding: 10px 0;}
		.demos-content-padded {padding: 15px;}
		.demos-second-title {text-align: center;font-size: 24px;color: #3cc51f;font-weight: 400;margin: 0 15%;}
		footer {text-align: center;font-size: 14px;padding: 20px;}
		footer a {color: #999;text-decoration: none;}
		.weui-grid__icon {width: 100%;height: 150px;margin: 0 auto;}
		.weui-grid {position: relative;float: left;padding: 0px 0px;width: 50%;box-sizing: border-box;}
		.weui-footer, .weui-grid__label {text-align: center;margin-bottom:1px;font-size: 14px;background-color: #fff;}
		span{color: black;}
	 	.sspan{text-align: center;font-size: 14px;color:#F37DF;}
		.weui-grids{border-top: 1px solid #d9d9d9;}
		.weui-cells__title{color:black; height:35px; line-height:35px;}
		.weui-grid__icon+.weui-grid__label {margin-top: 0px;}
		.weui-panel:first-child{ margin-top: 10px;}
		.weui-media-box_appmsg .weui-media-box__hd {width: 50px;height: 50px;}
		.icon-box {margin-bottom: 25px;margin-top: 25px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
		.icon-box i {margin: auto;text-align: center;}
		.placeholder {margin: 5px;background-color: #fff;height: 100px;text-align: center;color: #cfcfcf;}
		.weui-s{width: 100%}
		.weui-flex__item{width: 25%;float:left;}
		.demos-title span {color: #DBDBDB}
		.weui-cells {font-size: 14px;}
		.weui-cell_link {font-size: 12px;}
		.fanhui_img{width:30px; height:30px; background:rgba(12,12,12,0.5); border-radius:50%;text-align:center;line-height:30px; float:left; margin-right:20px;}
		.fanhui_img img{width:12px;padding-top:4px;}
		.weui-popup__overlay, .weui-popup__container {z-index: 7;}
		.vf_4{background: url(${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/pub_main1.png) no-repeat 0 -100px;background-size: auto 400px;}
		.weui-swiped-btn {
			line-height: 6;
		}
		.weui-popup__modal{
			background: #f4f4f4;
		}
		.weui-count .weui-count__btn{
			border-radius: 0;
			border: none;
			width: 28px;
			height: 28px;
		}
		.weui-count__btn.weui-count__decrease{
			color: #323233;
			background-color: #f7f8fa;
		}
		.weui-count__btn.weui-count__increase{
			color: #323233;
			background-color: #f7f8fa;
		}
		.weui-count .weui-count__btn:after, .weui-count .weui-count__btn:before{
			background: #5f5f9e;
			vertical-align: middle;
		}
		.weui-cell__ft{
			display: block;
			float: right;
		}
		.weui-panel:after, .weui-panel:before{
			display: none
		}
		.addBorder:before{
			border-top: none;
		}
		.weui-btn:after{
			display: none;
		}
	</style>
  </head>
  <body>
		<div id="carts" class="weui-popup__container">
		  <div class="weui-popup__overlay"></div>
		  <div class="weui-popup__modal">
			  <c:if test="${cartList=='[]'}" >
				  <header class='demos-header'>
					  <div class="icon-box">
						  <i class="empty-list__icon" style="">
							  <img src="${path}/resource/img/cart.png" width="150" height="150" alt="">
						  </i>
					  </div>
					  <h1 class="demos-title" style="color:black;margin-bottom: 25px;">购物车是空的</h1>
					  <a href="${path }/wx/home.htm" style="width: 25%;font-size: 14px;border-radius: 26px;border:1px solid #ff2333;background: #f7f8fa;color: #ff2333" class="weui-btn weui-btn_primary">马上逛逛</a>
				  </header>
			  </c:if>
			  <c:if test="${cartList!='[]'}" >
				  <div class="weui-panel weui-panel_access" style="border-radius: 6px;width: 96%;margin: auto;margin-top: 10px">
					  <div class="weui-cells__title">购物车 <span style="float: right">合计<span style="float:right;color: #ff2333;margin-left: 10px"> ￥<b name="price_total">${price_total }</b></span></span></div>
					  <div class="weui-panel__bd" >
						  <div class="weui-cells">
							  <c:forEach items="${cartList }" var="c">
								  <div class="weui-cell weui-cell_swiped">
									  <div class="weui-cell__bd">
										  <div class="weui-cell">
											  <div class="weui-cell__hd"><a href="${path}/wx/orders.htm?kid=${c.p_kid }"><img src="${path}/img/${c.main_file_id}.htm" alt="" style="width:88px;height:88px;margin-right:10px;display:block"></a></div>
											  <div class="weui-cell__bd" style="width: 60%;">
												  <h4 class="weui-media-box__title" style="font-size: 14px;line-height: 40px;">${c.p_name }</h4>
												  <p style="width: 50%;display: inline-block;line-height: 30px;margin-top: 2px"><span class="price" style="color: #ff2333;font-size: 14px;">￥${c.price }</span>/${c.unit }</p>
												  <div class="weui-cell__ft" style="width: 50%;margin-top: 2px" >
													  <span class="price" ></span>
													  <div class="weui-count">
														  <a class="weui-count__btn weui-count__decrease"></a>
														  <input class="weui-count__number" type="number" id="${c.kid}" value="${c.cnt }" name="${c.price }" />
														  <a class="weui-count__btn weui-count__increase"></a>
													  </div>
												  </div>
											  </div>

										  </div>
									  </div>
									  <div class="weui-cell__ft">
										  <a class="weui-swiped-btn weui-swiped-btn_warn delete-swipeout" href="javascript:del_cart('${c.kid}');">删除</a>
									  </div>
								  </div>
							  </c:forEach>
						  </div>
					  </div>
					  <div class="weui-panel__ft">
						  <a href="${path }/wx/classify.htm" class="weui-cell weui-cell_access weui-cell_link addBorder">
							  <div class="weui-cell__bd" style="font-size: 14px">继续添加商品</div>
							  <span class="weui-cell__ft"></span>
						  </a>
					  </div>
				  </div>
				  <%--						      <div class="weui-panel weui-panel_access">--%>
				  <%--							        <div class="weui-cells__title">收货信息</div> --%>
				  <%--							        <div class="weui-panel__bd" >--%>
				  <%--							        	<c:if test="${address!=null}">--%>
				  <%--								          <a href="${path}/wx/address.htm" class="weui-media-box weui-media-box_appmsg">--%>
				  <%--								            <div class="weui-media-box__bd" >--%>
				  <%--								              <h4 class="weui-media-box__title" style="font-size: 12px;">${address.mobile1 } ${address.member_name } 收</h4>--%>
				  <%--								              <p style="font-size: 12px;"><span>${address.province } ${address.city } ${address.region } ${address.street }</span></p>--%>
				  <%--								            </div>--%>
				  <%--								          </a>--%>
				  <%--							        	</c:if>--%>
				  <%--							        	<c:if test="${address==null}">--%>
				  <%--								           <a href="${path }/wx/add_address.htm" class="weui-cell weui-cell_access weui-cell_link">--%>
				  <%--								            <div class="weui-cell__bd">新增收货地址</div>--%>
				  <%--								            <span class="weui-cell__ft"></span>--%>
				  <%--								          </a>--%>
				  <%--							        	</c:if>--%>
				  <%--							        </div>--%>
				  <%--						      </div>--%>
				  <div class="weui-panel weui-panel_access">
<%--					  <div class="weui-cells__title">合计 <span style="float:right;color: #FF6B2F;">￥<b name="price_total">${price_total }</b></span></div>--%>
					  <div class="weui-panel__bd">
						  <div class="weui-cells weui-cells_form">
<%--							  <div class="weui-cell weui-cell_switch">--%>
<%--								  <div class="weui-cell__bd">合计</div>--%>
<%--								  <div class="weui-cell__ft">--%>
<%--									  <p style="color: #FF6B2F;">￥<span name="price_total">${price_total }</span></p>--%>
<%--								  </div>--%>
<%--							  </div>--%>
<%--							  <div class="weui-cell weui-cell_switch">--%>
<%--								  <div class="weui-cell__bd">运费</div>--%>
<%--								  <div class="weui-cell__ft">--%>
<%--									  <p>￥${carriage }</p>--%>
<%--								  </div>--%>
<%--							  </div>--%>
<%--							  <div class="weui-cell weui-cell_switch">--%>
<%--								  <div class="weui-cell__bd">合计</div>--%>
<%--								  <div class="weui-cell__ft" >--%>
<%--									  <p>￥${totalsum }</p>--%>
<%--								  </div>--%>
<%--							  </div>--%>
						  </div>
					  </div>
				  </div>
				  <button onclick="to_cart();" class="weui-btn weui-btn_primary" style="margin-top: 15px;margin-bottom: 5px;width: 95%;border-radius: 26px">结算</button>
				  <p class="weui-msg__desc" style="color: #ff2333;text-align: center;font-size: 12.5px;padding-top: 10px;"><i class="weui-icon-warn" style="font-size: 10px;margin-bottom: .4em;"></i> 支付完成后务必点击“返回商家”或“完成”</p>
			  </c:if>
			  <footer>
<%--				  <div class="footer">--%>
<%--					  <p class="mf_o4"><a href="http://www.sxxlkj.com" style="color: #586c94;font-size: 10px;">中科讯龙</a> 提供技术支持</p>--%>
<%--				  </div>--%>
				  <div class="weui-loadmore weui-loadmore_line weui-loadmore_dot">
					  <span class="weui-loadmore__tips"></span>
				  </div>
				  <div style="height:50px; line-height:50px; clear:both;"></div>
			  </footer>
		  </div>
		</div>
		<div class="v_nav">
			<div class="vf_nav">
				<ul>
					<li><a href="${path }/wx/home.htm"><i class="vf_1"></i><span>首页</span></a></li>
					<li><a href="${path }/wx/classify.htm"><i class="vf_3"></i><span>全部商品</span></a></li>
					<li><a href="${path }/wx/cart.htm" style="color: #FF2233;"><i class="vf_4"></i><span>购物车</span></a></li>
					<li><a href="${path }/wx/user.htm"><i class="vf_5"></i><span>我的</span></a></li>
				</ul>
			</div>
		</div>
    	<script>
		  $(function() {
			  document.addEventListener('visibilitychange', function() {
				  console.log(document.visibilityState)
				  // alert(document.visibilityState);
				  if(document.visibilityState === 'visible'){
					  history.go(0);
				  }
			  });
		  	$('.weui-cell_swiped').swipeout();
		  	$("#carts").popup();
		    FastClick.attach(document.body);
		  });

		  function to_cart(){
				<%--addr = '${address}';--%>
				// if(addr == ''){
				// 	$.toptip('亲，请填写收货地址 ~ ~! ', 'warning');
				// }else{
					window.location.href="${path }/wx/to_cart.htm";
				// }
			}

		  function del_cart(kid){
			  $.confirm("", "确认删除?", function() {
				  $.showLoading();
				  var ajaxTimeOut = $.ajax({
					  type : "GET",
					  url : "${path}/wx/del_order.htm",
					  data:{kid:kid},
					  timeout : 60000, //超时时间设置，单位毫秒
					  dateType : 'json',
					  success : function(msg) {
						  $.hideLoading();
						  $.toast("删除成功");
						  setTimeout(function() {
							  history.go(0)
						  }, 500)
					  },
					  error : function(response) {
						  $.hideLoading();
						  $.toptip('😣 服务好像出问题了，请重试', 'warning');
					  },
					  complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
						  if(status=='timeout'){//超时,status还有success,error等值的情况
							  ajaxTimeOut.abort(); //取消请求
							  $.hideLoading();
							  $.toptip('😣 网络好像出问题了，请重试', 'warning');
						  }
					  },
					  failure : function(response) {
						  $.hideLoading();
						  $.toptip('😣 好像出问题了，请重试', 'warning');
					  }
				  });
			  }, function() {});
		  }

		  var MAX = 99, MIN = 1;
		  $('.weui-count__decrease').click(function (e) {
			  var $input = $(e.currentTarget).parent().find('.weui-count__number');
			  var number = parseInt($input.val() || "0") - 1;
			  if (number < MIN){
				  del_cart($input[0].id);
			  }else {
				  edit_cart($input[0].id,number);
				  $input.val(number)
				  var $price_total = $("b[name=price_total]");
				  var price_total = parseFloat($price_total.html())-parseFloat($input[0].name)
				  $price_total.html(price_total.toFixed(2));
			  }
		  })
		  $('.weui-count__increase').click(function (e) {
			  var $input = $(e.currentTarget).parent().find('.weui-count__number');
			  console.log;
			  var number = parseInt($input.val() || "0") + 1;
			  if (number > MAX){
				  // number = MAX;
				  $.toast("最多只能买99件哦！", "text");
			  }else {
				  edit_cart($input[0].id,number);
				  $input.val(number)
				  var $price_total = $("b[name=price_total]");
				  var price_total = parseFloat($price_total.html())+parseFloat($input[0].name)
				  $price_total.html(price_total.toFixed(2));
			  }
		  })

		  function edit_cart(kid,number){
			  $.showLoading();
			  var ajaxTimeOut = $.ajax({
				  type : "GET",
				  url:"${path}/wx/edit_order.htm",
				  data:{kid:kid,cnt:number},
				  timeout : 60000, //超时时间设置，单位毫秒
				  dateType : 'json',
				  success : function(msg) {
					  $.hideLoading();
					  // $.toast("修改成功");
					  // setTimeout(function() {
						  // history.go(0)
					  // }, 500)
				  },
				  error : function(response) {
					  $.hideLoading();
					  $.toptip('😣 服务好像出问题了，请重试', 'warning');
				  },
				  complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
					  if(status=='timeout'){//超时,status还有success,error等值的情况
						  ajaxTimeOut.abort(); //取消请求
						  $.hideLoading();
						  $.toptip('😣 网络好像出问题了，请重试', 'warning');
					  }
				  },
				  failure : function(response) {
					  $.hideLoading();
					  $.toptip('😣 好像出问题了，请重试', 'warning');
				  }
			  });
		  }
		</script>
  </body>
</html>
