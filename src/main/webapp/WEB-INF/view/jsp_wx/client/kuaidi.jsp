<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>物流信息</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #f4f4f4;}
		.weui-cells {font-size: 14px;}
		.weui-cells__title {font-size: 10px;}
		.weui-cell__ft {color: black}
		.weui-cell__bd {color: #999;}
		.weui-media-box__desc {line-height: 1.5;}
		.weui-c{padding: 10px 15px;
	    position: relative;
	    display: flex;
	    -webkit-box-align: center;
	    align-items: center;
	       float: right;
	    }
		.btnBorder{
			border-radius: 26px;
		}
		.btnBorder:after{
			display: none;
		}
		.weui-form-preview__btn{
			background: #ffffff;
			line-height: 40px;
			border-radius: 26px;
		}
		.weui-form-preview__btn:after{
			border-left: none;
		}
		.weui-form-preview__ft:before{
			border-top: none;
		}
		.weui-form-preview__ft:after{
			border-top: none;
		}
		.weui-panel:after, .weui-panel:before{
			display: none;
		}
		.weui-cells:after, .weui-cells:before{
			display: none;
		}
		.weui-btn:after{
			display: none;
		}
	</style>
  </head>
  <body ontouchstart>
		  <div class="weui-panel weui-panel_access" style="margin: 10px;border-radius: 8px;">
		  <div class="weui-panel__bd">
		    <a class="weui-media-box weui-media-box_appmsg">
		      <div class="weui-media-box__hd">
		        <img class="weui-media-box__thumb" src="${path }/img/${product.main_file_id}.htm">
		      </div>
		      <div class="weui-media-box__bd">
		        <h4 class="weui-media-box__title">物流状态 ${orderLogistics.state }</h4>
		        <p class="weui-media-box__desc">${logistics.name } ${orderLogistics.logistics_code }</p>
		      </div>
		    </a>
		  </div>
		</div>
		<div class="weui-cells weui-cells_form" style="margin: 10px;border-radius: 8px;">
		  <div class="weui-cell">
		    <div class="weui-cell__bd">
				<div class="weui-cells__title" style="margin: 0 10px 10px 10px;font-size: 15px;font-weight: bold;padding: 0;color: #000">物流信息</div>
		      <textarea class="weui-textarea" style="color:black;font-size: 13px;" readOnly="readonly" rows="18">${orderLogistics.explain_traces }</textarea>
		    </div>
		  </div>
		</div>
		<c:if test="${orderDetail.status_order!='已完成'}" >
	        <c:if test="${orderLogistics.state=='已签收'}" >
<%--	        	<p class="weui-msg__desc" style="color: red;text-align: center;font-size: 12.5px;padding-top: 10px;"><i class="weui-icon-warn" style="font-size: 10px;margin-bottom: .4em;"></i> 确认收货后,订单完成。</p>--%>

		  		<div class="weui-btn-area">
					<a href="${path }/wx/confirm_take.htm?logisticsId=${orderLogistics.kid }&kid=${orderDetail.kid}" class="weui-btn weui-btn_primary" style="border-radius: 26px;font-size: 16px">确认收货</a>
				</div>
	        </c:if>
        </c:if>
        <c:if test="${orderLogistics.state=='在途中'}" >
		  <div class="weui-btn-area">
			  <a href="javascript:;" class="weui-btn weui-btn_disabled weui-btn_primary" style="border-radius: 26px;font-size: 16px">确认收货</a>
			  <p class="weui-msg__desc" style="color: red;text-align: center;font-size: 12.5px;padding-top: 10px;"><i class="weui-icon-warn" style="font-size: 10px;margin-bottom: .4em;"></i> 快件在途中,请耐心等待。</p>
		  </div>
        </c:if>
	    <c:if test="${orderLogistics.state=='问题件'}" >
		  <div class="weui-btn-area">
			  <a href="http://www.sxxlkj.com/neshop2/view_cp/wx/kefu.html" class="weui-btn weui-btn_primary" style="border-radius: 26px;font-size: 16px">联系客服</a>
			  <p class="weui-msg__desc" style="color: red;text-align: center;font-size: 12.5px;padding-top: 10px;"><i class="weui-icon-warn" style="font-size: 10px;margin-bottom: .4em;"></i> 问题件,请联系客服处理。</p>
		  </div>
        </c:if>
		<c:if test="${orderDetail.status_order=='已完成'}" >
			<div class="weui-form-preview__ft" style="padding: 10px;line-height: 15px;margin-top: 20px">
				<a href="${path }/wx/order_list.htm#tab5" class="weui-form-preview__btn weui-form-preview__btn_default" style="margin-right: 15px">返回订单</a>
				<a href="${path }/wx/home.htm" class="weui-form-preview__btn weui-form-preview__btn_default" style="background: #ff2333;color: #ffffff;margin-left: 15px">再逛一会</a>
			</div>
        </c:if>
   	   <script>
		  $(function() {
		    FastClick.attach(document.body);
		  });
		  
 		function myClose(){
 			WeixinJSBridge.call('closeWindow');
 		}
		</script>
  </body>
</html>
