<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>订单详情</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	  <META HTTP-EQUIV="expires" CONTENT="Wed, 26 Feb 1997 08:21:57 GMT">
	<meta http-equiv="Expires" content="0" />
<%--	  <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>--%>
<%--	  <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/index.css"/>--%>
	  <link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
	  <link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
	  <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/vant.css"/>
	  <script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
	  <script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
	  <script src="${path }/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #f4f4f4;}
		.weui-media-box__desc {line-height: 1.5;}
		.weui-c{padding: 10px 15px;
	    position: relative;
	    display: flex;
	    -webkit-box-align: center;
	    align-items: center;
	       float: right;
	    
	    }
		    .weui-c:before {
		    content: " ";
		    position: absolute;
		    top: 0;
		    right: 0;
		    height: 1px;
		    color: #d9d9d9;
		    left: 15px;
		}
		.weui-cell{
			padding: 8px 12px;
		}
		.weui-cells{
			border-radius: 8px;
			width: 96%;
			margin: auto;
		}
		.weui-popup__modal{
			overflow-y: visible;
			border-radius: 20px 20px 0 0;
			background: #f4f4f4;
		}
		.weui-media-box{
			background: #ffffff;
			margin-bottom: 10px;
			border-radius: 8px;
		}
		.weui-cells:after, .weui-cells:before{
			display: none;
		}
		.weui-btn:after{
			display: none;
		}
	</style>
  </head>
  <body ontouchstart>
  	  <div class="weui-cells__title"></div>
	  <div class="weui-panel weui-panel_access" style="border-radius: 8px;width: 96%;margin: auto">
			<div class="weui-panel__bd" >
				<c:if test="${order.address!=null}">
					<a href="javascript:" class="weui-media-box weui-media-box_appmsg">
						<div class="weui-media-box__bd" style="font-size: 13px;">
							<p><span name="address_np">${order.name } ${order.phone }</span></p>
							<h4 class="weui-media-box__title" name="address_pcrs">${order.address }</h4>
							<span hidden name="address_id"></span>
						</div>
					</a>
				</c:if>
				<c:if test="${order.address==null && address!=null}">
				  <a href="javascript:" class="weui-media-box weui-media-box_appmsg">
					<div class="weui-media-box__bd" style="font-size: 13px;">
						<p><span name="address_np">${address.mobile1 } ${address.member_name }</span></p>
					  	<h4 class="weui-media-box__title" name="address_pcrs">${address.province }${address.city }${address.region }${address.street }</h4>
						<span hidden name="address_id">${address.kid}</span>
					</div>
				  </a>
				</c:if>
<%--				<a href="${path }/wx/address.htm" class="weui-cell weui-cell_access weui-cell_link">--%>
				<c:if test="${order.status_order=='下单中' || order.status_order=='已下单'}" >
					<a href="javascript:;" class="weui-cell weui-cell_access weui-cell_link open-popup" data-target="#half">
						<div class="weui-cell__bd">收货地址</div>
						<span class="weui-cell__ft"></span>
					</a>
				</c:if>
			</div>
	  </div>
		<div class="weui-cells__title"></div>
		<div class="weui-cells">
			<c:forEach items="${detailList }" var="dl">
				<div class="weui-cell">
		            <div class="weui-cell__hd"><img src="${path}/img/${dl.main_file_id}.htm" alt="" style="width:88px;margin-right:5px;display:block"></div>
		            <div class="weui-cell__bd">
		              <p style="color: #323233;font-size: 14px;line-height: 30px">${dl.pname }</p>
			          <div style="line-height: 30px"> <span style="color: #ff2333;font-size: 16px;">￥${dl.price }</span><span style="float: right;color: #969799;font-size: 13px"> x ${dl.cnt }</span></div>
		            </div>
		            <div class="weui-cell__ft" style="color: #999; ">
			            <c:if test="${dl.status_order=='已发货'}" >
			            	<div style="margin-top:10px;padding-left:14px;"><a href="${path }/wx/kuaidi.htm?logisticsId=${dl.logistics_id}&orderDetailId=${dl.kid}" class="weui-btn weui-btn_mini weui-btn_plain-primary" style="border-radius: 26px;">查看物流</a></div>
			            </c:if>
			            <c:if test="${dl.status_order=='已收货'}" >
			            	<div style="margin-top:10px;padding-left:14px;"><a href="${path }/wx/kuaidi.htm?logisticsId=${dl.logistics_id}&orderDetailId=${dl.kid}" class="weui-btn weui-btn_mini weui-btn_plain-primary" style="border-radius: 26px;">查看物流</a></div>
			            </c:if>
			            <c:if test="${dl.status_order=='已完成'}" >
			            	<div style="margin-top:10px;padding-left:14px;"><a href="${path }/wx/kuaidi.htm?logisticsId=${dl.logistics_id}&orderDetailId=${dl.kid}" class="weui-btn weui-btn_mini weui-btn_plain-primary" style="border-radius: 26px;">查看物流</a></div>
			            </c:if>
			            <c:if test="${dl.status_order=='已付款'}" >
			            	<div style="padding-left:30px;">
								<a style=" font-size: 12px;position: relative;top: -18px;right: 6px;color: #ff6a00;">等待商家发货</a>
			            		<a style="font-size: 13px;border-radius: 26px;position: relative; top: -6px;" href="${path }/wx/order_return.htm?orderDetailId=${dl.kid}" class="weui-btn weui-btn_plain-default">退款/退货</a>
			            	</div>
			            </c:if>
		            </div>
		          </div>	
			</c:forEach>
		</div>
        <div class="weui-cells__title"></div>
     	<div class="weui-cells">
		  <div class="weui-cell" style="line-height: 26px">
		    <div class="weui-cell__bd">
		      <p style="color: #323233;font-size: 14px">商品总价</p>
		    </div>
		    <div class="weui-cell__ft" style="color: #ff2333;font-size: 14px;">￥${order.product_price }</div>
		  </div>
		  <div class="weui-cell" style="line-height: 26px">
		    <div class="weui-cell__bd">
		      <p style="color:  #323233;font-size: 14px">运费</p>
		    </div>
		    <div class="weui-cell__ft" style="color: #ff2333;font-size: 14px;">￥${order.logistics_price }</div>
		  </div>
		  <div class="weui-cell" style="line-height: 26px">
		    <div class="weui-cell__bd">
		      <p style="color: #323233;font-size: 14px">合计</p>
		    </div>
		    <div class="weui-cell__ft" style="color: #ff2333;font-size: 14px;">￥${order.total_price }</div>
		  </div>
		</div>

		<div class="weui-cells__title"></div>
		<div class="weui-cells">
			<div class="weui-cell" style="line-height: 26px">
				<div class="weui-cell__bd">
					<p style="color: #323233;font-size: 14px">订单编号:</p>
				</div>
				<div class="weui-cell__ft" style="font-size: 14px">${order.order_no }</div>
			</div>
			<div class="weui-cell" style="line-height: 26px">
				<div class="weui-cell__bd">
					<p style="color: #323233;font-size: 14px">下单日期:</p>
				</div>
				<div class="weui-cell__ft" style="font-size: 14px">${time }</div>
			</div>
			<c:if test="${order.is_pay=='是'}" >
				<div class="weui-cell" >
					<div class="weui-cell__bd">
						<p style="color: #323233;font-size: 14px">支付时间:</p>
					</div>
					<div class="weui-cell__ft" style="font-size: 14px">${pay_time }</div>
				</div>
			</c:if>
<%--			<div class="weui-cell">--%>
<%--				<div class="weui-cell__bd">--%>
<%--					<h4 class="weui-media-box__title" style="color: #999;font-size: 14px;">收货信息:</h4>--%>
<%--					<p class="weui-media-box__desc" style="color: black;">${order.phone } ${order.name } 收</p>--%>
<%--					<p class="weui-media-box__desc" style="color: black;">${order.address }</p>--%>
<%--				</div>--%>
<%--			</div>--%>
		</div>
		
		<%-- <c:if test="${order.status_order=='已发货'}" >
			<div class="weui-cells__title">物流信息</div>
			<div class="weui-cells weui-cells_form">
			  <div class="weui-cell">
			    <div class="weui-cell__bd">
			      <textarea class="weui-textarea" readOnly="readonly" rows="3"></textarea>
			    </div>
			  </div>
			</div>
		</c:if> --%>


		<div class="weui-btn-area">
			<c:if test="${order.status_order=='下单中'}" >
			  <a href="javascript:to_pay();" class="weui-btn weui-btn_primary" style="border-radius: 26px">微信支付</a>
			  <a href="javascript:del_order();" class="weui-btn weui-btn_warn" style="border-radius: 26px;background-color: #ff2333">取消订单</a>
			</c:if>
		</div>

	  <div id="half" class='weui-popup__container popup-bottom'>
		  <div class="weui-popup__overlay"></div>
		  <div class="weui-popup__modal">
			  <div class="toolbar">
				  <div class="toolbar-inner">
					  <a href="javascript:;" class="picker-button close-popup"><i role="button" tabindex="0" class="van-icon van-icon-cross van-popup__close-icon van-popup__close-icon--top-right"><!----></i></a>
					  <h1 class="title">收货地址</h1>
				  </div>
			  </div>
			  <div class="modal-content">
				  <div class="weui-cells " style="border-radius: 6px;margin: 10px auto;background: #f4f4f4">
					  <c:forEach  items="${addressList}" var="a" >
						  <a href="javascript:edit_order_address('${a.mobile1 } ${a.member_name }','${a.province }${a.city }${a.region }${a.street }','${a.kid}');" class="weui-media-box weui-media-box_appmsg"><%--href="${path}/wx/edit_from_address.htm?kid=${a.kid}"--%>
							  <div class="weui-media-box__bd" style="margin-left: 10px">
								  <h4 class="weui-media-box__title" style="font-size: 14px;">${a.mobile1 } ${a.member_name }</h4>
								  <p style="font-size: 14px;line-height: 25px">  <c:if test="${a.is_default=='是' }"><span style="background-color:#fddadc;color: #ff2333;font-size: 12px;margin-right: 5px;line-height: 20px;padding: 2px">默认</span></c:if><span>${a.province }${a.city }${a.region }${a.street }</span></p>
							  </div>
							  <div class="weui-cell__ft">
<%--								  <c:if test="${a.is_default=='是' }">--%>
<%--									  <i class="weui-icon-success"></i>--%>
<%--								  </c:if>--%>
<%--								  <c:if test="${a.is_default=='否' }">--%>
<%--									  <i class="weui-icon-info-circle"></i>--%>
<%--								  </c:if>--%>
							  </div>
						  </a>
					  </c:forEach>
				  </div>
				  <div style="margin-bottom: 10px">
					  <c:if test="${addressList.size() == 0}" >
						  <div class="weui-loadmore weui-loadmore_line">
							  <span class="weui-loadmore__tips">您还没有收货地址哦！</span>
						  </div>
						  <a href="${path }/wx/add_address.htm" id="add_addresss" class="weui-btn weui-btn_primary" style="font-size: 14px;margin: 0 16px; border-radius: 26px;background: #ff2333;line-height: 40px" class="weui-btn weui-btn_primary">新建地址</a>
					  </c:if>
					  <c:if test="${addressList.size() > 0}" >
						  <a href="${path }/wx/add_address.htm" class="weui-btn weui-btn_primary" style="background: #ff2333;border-radius: 26px;margin: 0 16px;font-size: 16px;line-height: 40px">+ 新建收货地址</a>
					  </c:if>
				  </div>

			  </div>
		  </div>
	  </div>
  </body>
  <script type="text/javascript">

	  $(function() {
		  document.addEventListener('visibilitychange', function() {
			  console.log(document.visibilityState)
			  // alert(document.visibilityState);
			  if(document.visibilityState === 'visible'){
				  history.go(0);
			  }
		  });
		  FastClick.attach(document.body);
	  });

	 function edit_order_address(address_np,address_pcrs,address_id){
		 $("span[name=address_id]").html(address_id);
		 $("span[name=address_np]").html(address_np);
		 $("h4[name=address_pcrs]").html(address_pcrs);
		 $.closePopup();
	 }

	 function del_order(){
		  $.confirm("您确定要删除订单吗?", "确认删除?", function() {
			  $.ajax({
	      			type : "POST",
	      			url : "${path }/wx/cancel_order.htm?kid=${order.kid }",
	      			dateType : 'json',
	      			success : function(msg) {
	      					$.toast("已删除！");
	      					self.location=document.referrer;
	      			}
	      		}); 
	        }, function() {

	        });
     };

	 function to_pay(){
		 var addr = '${address}';
		 var address_id = $("span[name=address_id]").html();
		 if(addr == ''){
			 $.toptip('亲，请填写收货地址 ~ ~! ', 'warning');
		 }else{
			 window.location.href="${path }/wx/to_pay.htm?order_id=${order.kid}&address_id="+address_id;
		 }
	 }

  </script>
</html>
