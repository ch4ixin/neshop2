<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>
<% 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragrma","no-cache"); 
response.setDateHeader("Expires",0); 
%> 
<!DOCTYPE html >
<html>
<head>
<base href="${basePath}resource/mobile/" />
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<title>我的</title>
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<link rel="stylesheet" type="text/css" href="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/css/user.css"/>
<link rel="stylesheet" type="text/css" href="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>
<script src="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/js/modernizr.js"></script>
<script type="text/javascript" src="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/js/jquery.js"></script>
<style type="text/css">
	body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #F4F4F4;}
	.vf_5{
		background: url(${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/pub_main1.png) no-repeat 0 -133px;background-size: auto 400px;
	}
	.goods{
		width: 23% !important;
		margin: 0 !important;
		display: inline-block !important;
		text-align: center !important;
		height: 100% !important;
		/*position: relative;*/
	}
	.goods dt{
		width: 100%;
		height: 40px;
		line-height: 40px !important;
	}
	.goods dl{
		margin-left: 0 !important;
		text-align: center !important;
	}
	.picture{
		width: 25px;
		height: 25px;
		margin-top: 14px;
	}
	.badge{
		position: absolute;
		top: 18px;
		right: 8px;
		-webkit-transform: translate(50%, -50%);
		transform: translate(50%, -50%);
		-webkit-transform-origin: 100%;
		transform-origin: 100%;
		display: inline-block;
		box-sizing: border-box;
		min-width: 16px;
		padding: 0 3px;
		color: #fff;
		font-weight: 500;
		font-size: 12px;
		font-family: -apple-system-font, Helvetica Neue, Arial, sans-serif;
		line-height: 1.2;
		text-align: center;
		background-color: #ff2333;
		border: 2px solid #fff;
		border-radius: 50%;
	}
	.badgeBox{
		width: 40px;
		height: 44px;
		margin: auto;
		position: relative;
	}
	.com_top dl dt span{
		text-indent: 0;
	}
</style>
<body>
      <div id="tbh5v0">
		<div class="user_com">
			<div class="com_top">
				<%-- <h2><a href="${path }/wx/cp/user_info.htm">设置</a></h2> --%>
				<dl>
					<dt><img style="border:2px solid #fff;" src="${member.user_headimgurl }">
						<span style="line-height: 120%;margin-top: 2px">${member.user_nick }</span>
						<div class="member_leavl" style="line-height: 60%">
							<img style="width: 15px;height: 15px;display: inline-block;margin-left: 42%" src="${path}/resource/img/level1.png" alt="">
							<span style="display: inline-block;font-size: 12px;margin-left: -1%;line-height: 17px">${member_level}</span>
						</div>
					</dt>
				</dl>
					<div class="uer_topnav">
						<ul>
							<li class="bain"><a href="${path }/wx/order_list.htm"><span>${my_order }</span>我的订单</a></li>
							<li class="bain"><a ><span>0</span>优惠券</a></li>
							<li><a ><span>${member.integral}</span>我的积分</a></li>
						</ul>
					</div>
			</div>

		<%-- <div class="Wallet">
			<ul>
				<li class="bain1"><strong>￥${member.money }元</strong><span>余额</span></li>
				<li class="bain1"><strong>0</strong><span>优惠券</span></li>
				<li><strong>${member.integral }</strong><span>积分</span></li>
			</ul>
			<a href="${path }/wx/cp/account_detail.htm"><em class="Icon Icon1"></em><dl><dt>我的钱包</dt><dd style="color:#aaaaaa;">查看我的钱包</dd></dl></a>
		</div> --%>
		<div class="Wallet">
			<a href="${path }/wx/order_list.htm" style="border-bottom:1px solid #eeeeee;height: 40px"><dl style="margin-left: 6px"><dt style="font-weight: bold">我的订单</dt><dd style="margin-right: 38px">查看全部订单</dd></dl></a>
			<div style="height: 90px;padding-left: 10px">
				<a href="${path }/wx/order_list.htm#tab2" class="goods">
					<div class="badgeBox">
						<c:if test="${stayPayOrder.size() > 0 }">
							<span class="badge">${stayPayOrder.size()}</span>
						</c:if>
						<img src="${path}/resource/img/daifukuan.png" class="picture">
					</div>
					<dl><dt>待付款</dt></dl>
				</a>
				<a href="${path }/wx/order_list.htm#tab3" class="goods">
					<div class="badgeBox">
						<c:if test="${staySendOrder.size() > 0 }">
							<span class="badge">${staySendOrder.size()}</span>
						</c:if>
						<img src="${path}/resource/img/daifahuo.png" class="picture">
					</div>
					<dl><dt>待发货</dt></dl>
				</a>
				<a href="${path }/wx/order_list.htm#tab4" class="goods">
					<div class="badgeBox">
						<c:if test="${finishOrder.size() > 0 }">
							<span class="badge">${finishOrder.size()}</span>
						</c:if>
						<img src="${path}/resource/img/daishouhuo.png" class="picture">
					</div>
					<dl><dt>待收货</dt></dl>
				</a>
				<a href="${path }/wx/order_list.htm#tab5" class="goods">
					<div class="badgeBox">
						<c:if test="${zfinishOrder.size() > 0 }">
							<span class="badge">${zfinishOrder.size()}</span>
						</c:if>
						<img src="${path}/resource/img/wancheng1.png" class="picture">
					</div>
					<dl><dt>待评价</dt></dl>
				</a>
			</div>
<%--			<a ><em class="Icon Icon3"></em><dl class="b"><dt>我的优惠券</dt><dd>暂未开放</dd></dl></a>--%>
<%--			<a ><em class="Icon Icon4"></em><dl class="b"><dt>我的评价</dt><dd>暂未开放</dd></dl></a>--%>
			<%-- <a href="${path }/wx/cp/collection.htm"><em class="Icon Icon10"></em><dl><dt>我的收藏</dt><dd>&nbsp;</dd></dl></a> --%>
		</div>
		<div class="Wallet">
			<a href="${path }/wx/address.htm"><em class="Icon Icon5"></em><dl><dt>地址管理</dt><dd>&nbsp;</dd></dl></a>
			<!--<a href="user.php?act=affiliate"><em class="Icon Icon6"></em><dl class="b"><dt>我的推荐</dt><dd>&nbsp;</dd></dl></a>-->
<%--			<a ><em class="Icon Icon7"></em><dl class="b"><dt>我的留言</dt><dd>暂未开放</dd></dl></a>--%>
		</div>
<%--		<div class="Wallet">--%>
			<!-- <a href="javascript:void(0)" onClick="window.location.href='user.php?act=logout'" ><em class="Icon Icon8"></em><dl><dt>注销登录</dt></dl></a> -->
<%--		</div>--%>
		</div>
<%--		  <div class="footer" >--%>
<%--	      <div class="links"  id="ECS_MEMBERZONE">--%>
<%--				      <a ><span>便捷</span></a>--%>
<%--				      <a ><span>可靠</span></a>--%>
<%--				      <a ><span>精品</span></a>--%>
<%--				  </div>--%>
<%--			  	  <p class="mf_o4">&copy;2017 <a href="http://www.sxxlkj.com" style="color: #586c94;font-size: 12px;">山西悦龙斋科技有限公司</a> 版权所有</p>--%>
<%--			</div>--%>
 		</div>
	  <div class="v_nav">
		  <div class="vf_nav">
			  <ul>
				  <li><a href="${path }/wx/home.htm"><i class="vf_1"></i><span>首页</span></a></li>
				  <li><a href="${path }/wx/classify.htm"><i class="vf_3"></i><span>全部商品</span></a></li>
				  <li><a href="${path }/wx/cart.htm"><i class="vf_4"></i><span>购物车</span></a></li>
				  <li><a href="${path }/wx/user.htm" style="color: #FF2233;"><i class="vf_5"></i><span>我的</span></a></li>
			  </ul>
		  </div>
	  </div>
    </body>
</html>
