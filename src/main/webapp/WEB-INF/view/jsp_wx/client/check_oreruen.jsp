<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>

<!DOCTYPE html>
<html>
  <head>
    <title>退款/退货</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {
		  height: 100%;
		  -webkit-tap-highlight-color: transparent;
		}
		.demos-title {
		  text-align: center;
		  font-size: 34px;
		  color: #3cc51f;
		  font-weight: 400;
		  margin: 0 15%;
		}
		
		.demos-sub-title {
		  text-align: center;
		  color: #888;
		  font-size: 14px;
		}
		
		.demos-header {
		  padding: 35px 0;
		}
		
		.demos-content-padded {
		  padding: 15px;
		}
		
		.demos-second-title {
		  text-align: center;
		  font-size: 24px;
		  color: #3cc51f;
		  font-weight: 400;
		  margin: 0 15%;
		}
		
		footer {
		  text-align: center;
		  font-size: 14px;
		  padding: 20px;
		}
		
		footer a {
		  color: #999;
		  text-decoration: none;
		}
		.weui-msg__desc{
			text-align: left;
		}
		.weui-msg__title {
    		margin-bottom: 25px;
    	}
	</style>
	<script type="text/javascript">
		function myClose(){
			WeixinJSBridge.call('closeWindow');
		}
	</script>
  </head>
  <body>
		<div class="weui-msg">
		      <c:if test="${order_return.order_status == '申请审核中'}">
			      <div class="weui-msg__icon-area">
			      	<i class="weui-icon-info weui-icon_msg"></i>
			      </div>
			      <div class="weui-msg__text-area">
			        <h2 class="weui-msg__title">${order_return.order_status }</h2>	
			        <p class="weui-msg__desc" style="color: black;">·退款原因:${order_return.problem }</p>
			        <p class="weui-msg__desc">·退款会在1~2个工作日审核完成</p>
			        <p class="weui-msg__desc">·退货商家在收到快递之后1~2个工作日审核完成</p>
			        <p class="weui-msg__desc">·审核通过后退款会以原支付方式返回,请耐心等待</p>
			      </div>
		      </c:if>
		      <c:if test="${order_return.order_status == '审核完成'}">
			      <div class="weui-msg__icon-area">
			      	<i class="weui-icon-success weui-icon_msg"></i>
			      </div>
			      <div class="weui-msg__text-area">
			        <h2 class="weui-msg__title">${order_return.order_status }</h2>	
			        <p class="weui-msg__desc" style="color: black;">·退款原因:${order_return.problem }</p>
			        <p class="weui-msg__desc" style="color: black;">·退款状态:${order_return.money_status }</p>
			        <p class="weui-msg__desc">·退款会在1~2个工作日审核完成</p>
			        <p class="weui-msg__desc">·退货商家在收到快递之后1~2个工作日审核完成</p>
			        <p class="weui-msg__desc">·审核通过后退款会以原支付方式返回,请耐心等待</p>
			      </div>
		      </c:if>
		      <c:if test="${order_return.order_status == '拒绝退款'}">
			      <div class="weui-msg__icon-area">
			      	<i class="weui-icon-warn weui-icon_msg-primary"></i>
			      </div>
			      <div class="weui-msg__text-area">
			        <h2 class="weui-msg__title">${order_return.order_status }</h2>	
			        <p class="weui-msg__desc" style="color: black;">·退款原因:${order_return.problem }</p>
			        <p class="weui-msg__desc" style="color: red;">·拒绝原因:${order_return.message }</p>
			        <p class="weui-msg__desc">·退款会在1~2个工作日审核完成</p>
			        <p class="weui-msg__desc">·退货商家在收到快递之后1~2个工作日审核完成</p>
			        <p class="weui-msg__desc">·审核通过后退款会以原支付方式返回,请耐心等待</p>
			      </div>
		      </c:if>
	      <div class="weui-msg__opr-area">
	        <p class="weui-btn-area">
	          <a href="${path }/view_cp/wx/kefu.html" class="weui-btn weui-btn_primary">联系客服</a>
	          <a href="javascript:myClose();" class="weui-btn weui-btn_default">关闭</a>
	        </p>
	      </div>
<%--	      <div class="weui-msg__extra-area">--%>
<%--	        <div class="weui-footer">--%>
<%--	          <p class="weui-footer__links">--%>
<%--	            <a href="http://www.sxxlkj.com" class="weui-footer__link">山西悦龙斋科技有限公司</a>--%>
<%--	          </p>--%>
<%--	        </div>--%>
<%--	      </div>--%>
	    </div>
	<script>
	  $(function() {
	    FastClick.attach(document.body);
	  });
	</script>
  </body>
</html>
