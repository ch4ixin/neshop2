<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>
<script type="text/javascript">

function open_popup(){
	$("#protect").popup();
}

</script>
<style>
<!--
body, html {
  height: 100%;
  -webkit-tap-highlight-color: transparent;
}

footer {
  text-align: center;
  font-size: 14px;
  padding: 20px;
}

footer a {
  color: #999;
  text-decoration: none;
}
.weui_article{
  font-size: 14px;
  background: #fff;
  padding: 10px 15px;
  margin: 10px;
  border-radius: 6px;
}
.weui_article p{
  margin: 5px 0;
    color:#666;
}
-->
</style>
<div id="protect" class='weui-popup-container'>
      <div class="weui-popup-overlay"></div>
      <div class="weui-popup-modal">
        <article class="weui_article">
          <h2 style="font-weight:700;text-align: center">店铺入驻协议</h2>
          <section>
              <p>尊敬的商家： <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您好，如果您已经准备完成全部资料，并准备提交申请，在您提交申请前，请务必仔细阅读如下全部声明事项：</p>
              <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. 您确认并知悉，您的申请需依次经过线上与线下两次审核，请您在线上审核通过后，等待线下业务经理到门店现场审核，经线下审核通过后方可操作上线。如其中任一项审核未通过或您提交了虚假资质或信息，则悦龙斋平台有权对申请主体提供的资质和信息进行删除及操作下线等处理。</p>
              <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. 您承诺您申请主体为依据中国法律合法成立并有效存续的法人或其它商事主体，能够独立承担法律责任，并具备从事申请业务应具备的行政许可、第三方合法授权或同意，及具有履行合作所需的一切权力、权利及能力。</p>
              <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. 您承诺所提供证照等资质和信息真实、合法、有效且不侵犯任何第三方合法权益。</p>
              <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. 您承诺提交审核的证照等资质和信息应符合国家法律、法规、规章的规定，申请主体未伪造、变造、冒用或盗用他人证照等资质，未侵犯他人拥有合法权益的信息等；提供的商号、商标、图片等均具有发布和使用的充分权利或授权，如因申请主体提供的证照等资质或信息造成用户及相关第三方损失或承担责任的，申请主体存在质量和安全问题，由此造成的用户及相关第三方投诉、索赔、经济损失等相关后果均由申请主体予以承担。</p>
              <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;对于以上声明，您如果存在异议，请停止申请或与招商人员联系；如您继续填写并提交申请，则视为您理解上述声明，并确认遵守全部事项并在违反时承担责任。</p>
          	<a href="javascript:;" class="weui_btn weui_btn_primary close-popup" style="margin-top: 30px;border-radius: 26px">我知道了</a>
          </section>
        </article>
      </div>
    </div>