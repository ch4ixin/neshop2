<%@ page language="java" pageEncoding="UTF-8" %>
<%@ include file="../common/init.jsp" %>
<!DOCTYPE html >
<html>
<head>
    <meta name="Generator" content="ECSHOP v2.7.3"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
    <META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
    <META HTTP-EQUIV="Expires" CONTENT="0">
    <title>商品管理</title>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>
    <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/category_list.css"/>
    <link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
    <link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
    <link rel="stylesheet" type="text/css" href="${path}/resource/webuploader/webuploader2/webuploader.css">
    <script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
    <script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
    <script type="text/javascript" src="${path}/resource/webuploader/webuploader.min.js"></script>
    <script src="${path }/resource/js/fastclick.js"></script>
    <style type="text/css">
        body, html {height: 100%;-webkit-tap-highlight-color: transparent;}
        .filtrate_term li {width: 23.5%;}
        .touchweb-com_searchListBox.openList .item .price_box {height: 20px;}
        .filtrate_term li {height: 40px;}
        .vf_nav ul li {width: 33.33%;}
        .vf_nav ul li a {color: #858585;}
        .vf_6 {background: url(${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/shnav.png) no-repeat 0 2px;background-size: auto 86px;}
        .weui-label {font-size: 14px;}
        .weui-cells {margin-top: 0;}
        .weui-cell__bd {font-size: 14px;}
        .weui-cell__ft {color: #ff2333;}
        .weui-panel__bd .weui-media-box {background-color: #fff;margin-bottom: 8px;padding: 10px 8px 20px 10px;}
        .weui-media-box {padding: 5px;}
        .weui-media-box_appmsg .weui-media-box__hd {margin-right: .8em;width: 90px;height: 90px;line-height: 90px;text-align: center;border-radius: 10px;position: relative;}
        .weui-navbar__item.weui-bar__item--on {color: #ff2333;background-color: #F4F4F4;border-top: 2px solid #ff2333;font-weight: 600}
        .weui-navbar__item:after {border-right: none}
        .weui-navbar__item {color: #353535;font-size: 14px}
        .weui-navbar {background: #ffffff;}
        .weui-navbar:after {border-bottom: none;}
        .weui-search-bar:after {border-bottom: none;}
        .weui-switch-cp__input:checked~.weui-switch-cp__box, .weui-switch:checked{background: #ff3323;border-color:#ff3323;}
        .evaluate{border-radius: 4px;color: #393939;border: 1px solid #c1c1c1;display: inline-block;padding: 0 1.2em;line-height: 2.1em;font-size: 12px;}
        .evaluate a{color: #393939}
        .evaluate .weui-media-box_appmsg,.evaluate .weui-panel__bd .weui-media-box{display: none}
        .product_btn{position: absolute;bottom: 8px;right: 10px;}
        .weui-uploader__file {width: 100px;height: 100px;position: relative;margin-right: 20px;}
        .closeLayer {z-index: 99;float: right;width: 23px;height: 23px;position: absolute;top: -12px;right: -14px;}
        .closeLayer img{width: 20px;height: 20px;}
        .weui-uploader__files{margin-top: 10px;}
        .weui-toptips {z-index: 500;}
        .weui-media-box_appmsg .weui-media-box__thumb{left: 0;top: 0;right: 0;bottom: 0;position: absolute;margin: auto;}
    </style>
<body ontouchstart>
<%--<div class="weui-search-bar" id="searchBar">--%>
<%--    <form class="weui-search-bar__form">--%>
<%--        <div class="weui-search-bar__box">--%>
<%--            <i class="weui-icon-search"></i>--%>
<%--            <input type="search" class="weui-search-bar__input" id="searchInput" placeholder="搜索" required="">--%>
<%--            <a href="javascript:" class="weui-icon-clear" id="searchClear"></a>--%>
<%--        </div>--%>
<%--        <label class="weui-search-bar__label" id="searchText">--%>
<%--            <i class="weui-icon-search"></i>--%>
<%--            <span>搜索</span>--%>
<%--        </label>--%>
<%--    </form>--%>
<%--    <a href="javascript:" class="weui-search-bar__cancel-btn" id="searchCancel">取消</a>--%>
<%--</div>--%>
<div class="weui-tab">
    <div class="weui-navbar" style="height:44px">
        <a class="weui-navbar__item weui-bar__item--on" href="#tab1">在售</a>
        <a class="weui-navbar__item" href="#tab2">下架</a>
        <a class="weui-navbar__item " href="#tab3">发布</a>
    </div>
    <div class="weui-tab__bd">
        <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
            <div class="weui-panel_access">
                <c:forEach items="${onShelfList}" var="osl">
                    <div class="weui-panel__bd">
                        <a href="${path }/wx/orders.htm?kid=${osl.kid}" class="weui-media-box weui-media-box_appmsg">
                            <div class="weui-media-box__hd">
                                <img class="weui-media-box__thumb" src="${path}/img/${osl.main_file_id}.htm">
                            </div>
                            <div class="weui-media-box__bd">
                                <p class="weui-media-box__title" style="font-size: 14px;margin-top: -30px;white-space: normal;">${osl.name}</p>
                                <p class="weui-media-box__desc" style="font-size: 12px;margin-top: 10px"><s style="color:#999;font-size:10px;margin-left:2px;">￥${osl.market_price }</s></p>
                            </div>
                            <div class="weui-cell__ft">￥${osl.price }</div>
                            <div class="product_btn">
                                <object class="evaluate"><a href="${path }/wx/orders.htm?kid=${osl.kid}">查看</a></object>
                                <object class="evaluate"><a href="${path }/wx/shop/product_edit.htm?kid=${osl.kid}">编辑</a></object>
                                <object class="evaluate"><a href="javascript:under('${osl.kid}');">下架</a></object>
                            </div>
                        </a>
                    </div>
                </c:forEach>
                <div class="weui-loadmore weui-loadmore_line weui-loadmore_dot">
                    <span class="weui-loadmore__tips"></span>
                </div>
            </div>
        </div>
        <div id="tab2" class="weui-tab__bd-item">
            <div class="weui-panel_access">
                <c:forEach items="${offShelfList}" var="ofsl">
                    <div class="weui-panel__bd">
                        <a href="${path }/wx/orders.htm?kid=${ofsl.kid}" class="weui-media-box weui-media-box_appmsg">
                            <div class="weui-media-box__hd">
                                <img class="weui-media-box__thumb" src="${path}/img/${ofsl.main_file_id}.htm">
                            </div>
                            <div class="weui-media-box__bd">
                                <p class="weui-media-box__title" style="font-size: 14px;margin-top: -30px;white-space: normal;">${ofsl.name}</p>
                                <p class="weui-media-box__desc" style="font-size: 12px;margin-top: 10px"><s style="color:#999;font-size:10px;margin-left:2px;">￥${ofsl.market_price }</s></p>
                            </div>
                            <div class="weui-cell__ft">￥${ofsl.price }</div>
                            <div class="product_btn">
                                <object class="evaluate"><a href="${path }/wx/orders.htm?kid=${ofsl.kid}">查看</a></object>
                                <object class="evaluate"><a href="${path }/wx/shop/product_edit.htm?kid=${ofsl.kid}">编辑</a></object>
                                <object class="evaluate"><a href="javascript:upShelf('${ofsl.kid}');">上架</a></object>
                                <object class="evaluate"><a href="javascript:product_del('${ofsl.kid}');">删除</a></object>
                            </div>
                        </a>
                    </div>
                </c:forEach>
                <div class="weui-loadmore weui-loadmore_line weui-loadmore_dot">
                    <span class="weui-loadmore__tips"></span>
                </div>
            </div>
        </div>
        <div id="tab3" class="weui-tab__bd-item">
            <div class="weui-cells weui-cells_form">
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label class="weui-label">商品名称</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" name="name" type="text" pattern="" placeholder="请输入商品名称">
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__bd">
                        <div class="weui_uploader">
                            <div class="weui-uploader__hd">
                                <p class="weui-uploader__title">商品图片</p>
                                <div class="weui-uploader__info">0/5</div>
                            </div>
                            <div class="weui-uploader__bd">
                                <ul class="weui-uploader__files"></ul>
                                <div class="weui_cell_bd weui_cell_primary img_picker" ></div>
                                <input name="file_ids" value="" type="hidden">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="weui-cell weui-cell_select" style="padding-left: 15px">
                    <div class="weui-cell__hd"><label class="weui-label">商品分类</label></div>
                    <div class="weui-cell__bd">
                        <select id="shop_type" class="weui-select" name="select1">
                            <c:forEach items="${cateList}" var="cl">
                                <option class="weui-input" type="text" value="${cl.kid}">${cl.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label class="weui-label">首件运费</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" type="number" name="carriage" pattern="[0-9]*" placeholder="请输入首件运费">
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label class="weui-label">续件运费</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" type="number" name="more_carriage" pattern="[0-9]*"
                               placeholder="请输入续件运费">
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label class="weui-label">商品价格</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" type="number" name="price" pattern="[0-9]*" placeholder="请输入商品价格">
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label class="weui-label">市场价格</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" type="number" name="market_price" pattern="[0-9]*"
                               placeholder="请输入市场价格">
                    </div>
                </div>
                <div class="weui-cell">
                    <div class="weui-cell__hd"><label class="weui-label">规格单位</label></div>
                    <div class="weui-cell__bd">
                        <input class="weui-input" type="text" name="unit" pattern="" placeholder="请输入规格单位">
                    </div>
                </div>
                <div class="weui-cell weui-cell_switch">
                    <div class="weui-cell__bd">是否上架</div>
                    <div class="weui-cell__ft">
                        <input class="weui-switch"  type="checkbox"  checked="checked" onclick="onOff()">
                        <input class="weui_input" type="hidden" name="status_sys" value="上架中"/>
                    </div>
                </div>
            </div>
            <div class="weui-btn-area">
                <a class="weui-btn weui-btn_primary" style="background: #FF2233;border-radius: 26px;font-size: 14px;line-height: 40px"
                   href="javascript:submit()" id="showTooltips">发布</a>
            </div>
        </div>
    </div>
</div>
<div style="height:50px; line-height:50px; clear:both;"></div>

<div class="v_nav">
    <div class="vf_nav">
        <ul>
            <li><a href="${path }/wx/shop/product.htm" style="color: #FF2233;"><i class="vf_6"></i><span>商品管理</span></a></li>
            <li><a href="${path }/wx/shop/order.htm"><i class="vf_7"></i><span>订单管理</span></a></li>
            <li><a href="${path }/wx/shop/store.htm"><i class="vf_8"></i><span>店铺管理</span></a></li>
        </ul>
    </div>
</div>
<script>
    var files = new Array();
    var file_ids = "";
    var $list = $('.weui-uploader__files');
    var uploader;
    $(function() {
        uploader = WebUploader.create({
            // 选完文件后，是否自动上传。
            auto: true,
            // swf文件路径
            swf: 'http://cdn.staticfile.org/webuploader/0.1.5/Uploader.swf',
            duplicate : true,
            // 文件接收服务端。
            server: '${path}/upload.htm',
            // 选择文件的按钮。可选。
            // 内部根据当前运行是创建，可能是input元素，也可能是flash.
            pick: '.img_picker',
            // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
            // resize: false,
            // 只允许选择图片文件。
            accept: {
                title: 'Images',
                extensions: 'gif,jpg,jpeg,bmp,png',
                mimeTypes: 'image/*'
            }
        });

        //当有文件被添加进队列的时候
        uploader.on('fileQueued', function(file) {
            uploader.makeThumb( file, function( error, src ) {
                console.log($(".weui-uploader__files li").length);
                if ($(".weui-uploader__files li").length > 5) {
                    // $img.hide();
                    $.toptip('只能上传五张照片', 'error');
                    return;
                }else{
                    if($(".weui-uploader__files li").length == 0){
                        $list.append('<li class="weui-uploader__file" ></li>');
                    }
                    $list.append('<li class="weui-uploader__file weui-uploader__file_status" id="'+ file.id +'" style="background-image:url('+src+')"><div class="closeLayer" name="'+file.name+'" onClick="cancelUpload(this)" ><img src="${path}/resource/img/close1.png"></div><div class="weui-uploader__file-content">0%</div></li>');
                    if ( error ) {
                        $('#' + file.id).find('.weui-uploader__file-content').html('<i class="weui-icon-warn"></i>');
                        return;
                    }
                }
            });
        });
        //文件上传过程中创建进度条实时显示。
        uploader.on('uploadProgress', function(file, percentage) {
            $('#' + file.id).find('.weui-uploader__file-content').html(percentage * 100 + '%');
        });
        uploader.on('uploadSuccess', function(file, response) {
            $('#' + file.id).removeClass('weui-uploader__file_status');
            if (files.length < 5) {
                var end = $.parseJSON(response._raw);
                var file_ = {
                    id: end.file_id,
                    name: end.file_name
                }
                files.push(file_);
                // console.log(files);
                $('#' + file.id).css("background-image","url(${path}/img/"+ end.file_id + ".htm)");
                $('.weui-uploader__info').html(files.length + '/' + 5);
                getFileids();
            }
        });
        uploader.on('uploadError', function(file) {
            $('#' + file.id).find('.weui-uploader__file-content').html('<i class="weui-icon-warn"></i>');
        });
        uploader.on('uploadComplete', function(file) {
            $('#' + file.id).find('.weui-uploader__file-content').fadeOut();
        });

        FastClick.attach(document.body);
    });

    //删除上传图片
    function cancelUpload(obj){
        uploader.removeFile(uploader.getFile($(obj).parent().attr('id')),true);
        $(obj).parent().remove();
        let arr_index = files.findIndex(value => {
            return value.name === obj.getAttribute("name");
        })
        files.splice(arr_index,1);
        console.log(files);
        $('.weui-uploader__info').html(files.length + '/' + 5);
        getFileids();
    }

    function getFileids() {
        if(files.length === 0){
            file_ids = "";
        }
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if (i === 0) {
                file_ids = file.id;
            } else {
                file_ids += "," + file.id
            }
        }
        $("input[name='file_ids']").val(file_ids);
    }

    // 是否上架
    function onOff(){
        console.log($('input[type=checkbox]').is(':checked'));
        if($('.weui-switch').is(':checked') == true){
            $("input[name=status_sys]").val("上架中");
        }else{
            $("input[name=status_sys]").val("已下架");
        }
    }

    // 商品发布
    function submit() {
        var name = $("input[name='name']").val(); // 商品名称
        var carriage = $("input[name='carriage']").val(); // 首件运费
        var more_carriage = $("input[name='more_carriage']").val(); // 续件运费
        var price = $("input[name='price']").val(); // 商品价格
        var market_price = $("input[name='market_price']").val();//市场价格
        var unit = $("input[name='unit']").val();//规格单位
        var cate_code = $("#shop_type").val();
        var status_sys = $("input[name=status_sys]").val();
        var file_ids = $("input[name='file_ids']").val();

        if (name === '') {
            $.toptip('请输入商品名称', 'warning');
            return false;
        }
        if (file_ids === '') {
            $.toptip('请上传商品图片', 'warning');
            return false;
        }
        if (cate_code === '') {
            $.toptip('请输入商品分类', 'warning');
            return false;
        }
        if (carriage === '') {
            $.toptip('请输入首件运费', 'warning');
            return false;
        }
        if (more_carriage === '') {
            $.toptip('请输入续件运费', 'warning');
            return false;
        }
        if (price === '') {
            $.toptip('请输入商品价格', 'warning');
            return false;
        }
        if (unit === '') {
            $.toptip('请输入商品价格', 'warning');
            return false;
        }
        $.showLoading();
        var ajaxTimeOut = $.ajax({
            type : "POST",
            url: "${path}/wx/shop/product_add.htm",
            data: {name:name,cate_code:cate_code,carriage:carriage,more_carriage:more_carriage,price:price,market_price:market_price,unit:unit,status_sys:status_sys,file_ids:file_ids,main_file_id:files[0].id},
            timeout : 60000, //超时时间设置，单位毫秒
            dataType: "json",
            success: function(data){
                $.hideLoading();
                $.toast("已发布");
                history.go(0);
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                $.hideLoading();
                $.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
            },
            complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                if(status=='timeout'){//超时,status还有success,error等值的情况
                    ajaxTimeOut.abort(); //取消请求
                    $.hideLoading();
                    $.toptip('😣 网络好像出问题了，请重试', 'warning');
                }
            },
            failure : function(response) {
                $.hideLoading();
                $.toptip('😣 好像出问题了，请重试', 'warning');
            }
        });
    }

    //商品下架
    function under(kid) {
        $.confirm("您确定要下架该商品吗?", "确认下架?", function() {
            $.showLoading();
            var ajaxTimeOut = $.ajax({
                type : "POST",
                url: "${path}/wx/shop/product_upper_or_lower.htm",
                data: {kid:kid,status_sys:'已下架'},
                timeout : 60000, //超时时间设置，单位毫秒
                dataType: "json",
                success: function(data){
                    $.hideLoading();
                    $.toast("已下架");
                    sessionStorage.setItem('refresh', 'true');
                    history.go(0);
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    $.hideLoading();
                    $.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
                },
                complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                    if(status=='timeout'){//超时,status还有success,error等值的情况
                        ajaxTimeOut.abort(); //取消请求
                        $.hideLoading();
                        $.toptip('😣 网络好像出问题了，请重试', 'warning');
                    }
                },
                failure : function(response) {
                    $.hideLoading();
                    $.toptip('😣 好像出问题了，请重试', 'warning');
                }
            });
        });
    }
    //商品上架
    function upShelf(kid) {
        $.confirm("您确定要上架该商品吗?", "确认上架?", function() {
            $.showLoading();
            var ajaxTimeOut = $.ajax({
                type : "POST",
                url: "${path}/wx/shop/product_upper_or_lower.htm",
                data: {kid:kid,status_sys:'上架中'},
                timeout : 60000, //超时时间设置，单位毫秒
                dataType: "json",
                success: function(data){
                    $.hideLoading();
                    $.toast("已上架");
                    sessionStorage.setItem('refresh', 'true');
                    history.go(0);
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    $.hideLoading();
                    $.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
                },
                complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                    if(status=='timeout'){//超时,status还有success,error等值的情况
                        ajaxTimeOut.abort(); //取消请求
                        $.hideLoading();
                        $.toptip('😣 网络好像出问题了，请重试', 'warning');
                    }
                },
                failure : function(response) {
                    $.hideLoading();
                    $.toptip('😣 好像出问题了，请重试', 'warning');
                }
            });
        });
    }
    //商品删除
    function product_del(kid) {
        $.confirm("您确定要删除该商品吗?", "确认删除?", function() {
            $.showLoading();
            var ajaxTimeOut = $.ajax({
                type : "POST",
                url: "${path}/wx/shop/product_del.htm",
                data: {kid:kid},
                timeout : 60000, //超时时间设置，单位毫秒
                dataType: "json",
                success: function(data){
                    $.hideLoading();
                    $.toast("已删除");
                    sessionStorage.setItem('refresh', 'true');
                    history.go(0);
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    $.hideLoading();
                    $.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
                },
                complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
                    if(status=='timeout'){//超时,status还有success,error等值的情况
                        ajaxTimeOut.abort(); //取消请求
                        $.hideLoading();
                        $.toptip('😣 网络好像出问题了，请重试', 'warning');
                    }
                },
                failure : function(response) {
                    $.hideLoading();
                    $.toptip('😣 好像出问题了，请重试', 'warning');
                }
            });
        });
    }
</script>
</body>
</html>