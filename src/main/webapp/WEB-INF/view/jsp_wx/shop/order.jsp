<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>
<% 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragrma","no-cache"); 
response.setDateHeader("Expires",0); 
%> 
<!DOCTYPE html >
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">
<title>订单管理</title>
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/category_list.css"/>
<link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
<link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/vant.css"/>
<script src="http://res.wx.qq.com/open/js/jweixin-1.6.0.js"></script>
<script src="${path }/resource/js/clipboard.min.js"></script>
<style type="text/css">
	body, html {
		height: 100%;
		-webkit-tap-highlight-color: transparent;
	}
	.vf_nav ul li {
		width: 33.33%;
	}
	.vf_7{background: url(${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/shnav.png) no-repeat 0 -28px;background-size: auto 86px;}
	.weui-tab__bd .weui-tab__bd-item{background: #F4F4F4}
	.weui-navbar+.weui-tab__bd{padding-top: 40px}
	.weui-navbar__item{font-size: 14px;padding: 10px 0}
	.weui-navbar__item{color: #353535}
	.weui-navbar__item.weui-bar__item--on{color: #ff2333;background-color: #F4F4F4;border-top: 2px solid #ff2333;font-weight: 600;}
	.weui-navbar__item:after{display: none}
	.weui-navbar:after{border-bottom: none;}
	.weui-navbar {z-index: 9;}
	.weui-dialog{border-radius: 6px}
	.weui-popup__modal{overflow-y: visible;border-radius: 20px 20px 0 0;background: #FFFFFF;}
	.toolbar{background: #FFFFFF;}
	.weui-label{font-size: 16px;}
	.weui-btn_primary{background: #ff2333;border-radius: 26px;width: 98%;font-size: 16px}
	.weui-dialog__title{font-size: 16px;}
	.weui-prompt-text{font-size: 13px;}
	.weui-dialog__ft{font-size: 16px;}
	.weui-prompt-input{width: 90%;}
	.weui-btn_mini{
		padding: 0 1em;
	}
	.icon-box {margin-bottom: 25px;margin-top: 25px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
	.icon-box i {margin: auto;text-align: center;}
	.icon{margin: 0 auto;width: 150px}
	.demos-header {padding: 50px 0;background-color:#F4F4F4;}
	.demos-title span {color: #DBDBDB}
	.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;margin: 0 15%;}
	.back{width: 25%;font-size: 14px;border-radius: 26px;border: 1px solid #ff2333;background: #f7f8fa;color: #ff2333;}
	.share-pop-header{padding: 20px 0 4px;font-size: 14px;line-height: 20px;text-align: center;}
	.share-pop-content{position: relative;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: start;-webkit-align-items: flex-start;align-items: flex-start;padding: 16px 8px;overflow-x: scroll;}
	.share-pop__item{border: 0;padding: 0 0px;height: auto;line-height: auto;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-orient: vertical;-webkit-box-direction: normal;-webkit-flex-direction: column;flex-direction: column;-webkit-box-align: center;-webkit-align-items: center;align-items: center;-webkit-flex-shrink: 0;flex-shrink: 0;width: 80px;background-color: #fff;}
	.share-pop__item-icon{width: 48px;height: 48px;}
	.share-pop__item-text{color: #646566;font-size: 12px;margin-top: 8px;height: 16px;line-height: 16px;white-space: nowrap;}
	#share-guide{position: fixed;top: 0;left: 0;width: 100%;height: 100%;background-color: rgba(0,0,0,.8);z-index: 2000;padding-top: 140px;box-sizing: border-box;color: #fff;text-align: center;line-height: 30px}
	.share-guide__arrow{position: absolute;top: 12px;right: 33px;background: url(${path}/resource/img/share-arrow.png) no-repeat;width: 66px;height: 117px;background-size: contain;}
	.share-guide__title{font-size: 20px;font-weight: 500;}
</style>
<body ontouchstart="">
	<div class="weui-tab">
		<div class="weui-navbar" style="background-color: #fff">
			<a class="weui-navbar__item weui-bar__item--on" href="#tab1" id="tabb1">待付款</a>
			<a class="weui-navbar__item" href="#tab2" id="tabb2">待发货</a>
			<a class="weui-navbar__item" href="#tab3" id="tabb3">已发货</a>
			<a class="weui-navbar__item" href="#tab4" id="tabb4">全部</a>
		</div>
		<div class="weui-tab__bd">
			<div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
				<c:if test="${stayPayOrder=='[]'}" >
					<header class='demos-header'>
						<div class="icon-box">
							<img src="${path}/resource/img/zanwudingdan.png" class="icon">
						</div>
						<h1 class="demos-title" style="color:black;margin-bottom: 25px;">还没有人购买过您的商品</h1>
						<a href="javascript:" class="weui-btn back open-popup" data-target="#popup_share">分享店铺</a>
					</header>
				</c:if>
				<c:if test="${stayPayOrder!='[]'}" >
					<div class="weui-cells__title">共${stayPayOrder.size()}条</div>
					<div class="weui-cells">
						<c:forEach items="${stayPayOrder }" var="ol">
							<div class="weui-cell">
								<div class="weui-cell__hd"><img src="${path}/img/${ol.main_file_id}.htm" alt="" style="width:88px;height:88px;margin-right:5px;display:block"></div>
								<div class="weui-cell__bd">
									<p style="color: #323233;font-size: 14px;line-height: 20px">${ol.product_name }</p>
									<div style="line-height: 30px">
										<span style="color: #ff2333;font-size: 14px;">￥${ol.price}</span>
										<span style="color: #969799;font-size: 12px"> x ${ol.cnt }</span>
									</div>
								</div>
								<div style="text-align: center">
									<span style="color: #ff2333;font-size: 16px">￥${ol.price_total }</span>
									<div class="weui-cell__ft" style="color: #999;margin-top: 2px">
										<div>
											<a href="javascript:reminder('提醒用户付款','您好！您的订单长时间未支付，提醒您避免过期，请尽快下单哦<a href=\'${path}/wx/order_detail.htm?kid=${ol.kid}\'>立即下单</a>','${ol.member_id}')" class="weui-btn weui-btn_mini weui-btn_plain-primary">提醒付款</a>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="weui-loadmore weui-loadmore_line weui-loadmore_dot">
						<span class="weui-loadmore__tips"></span>
					</div>
				</c:if>
			</div>
			<div id="tab2" class="weui-tab__bd-item">
				<c:if test="${staySendOrder=='[]'}" >
					<header class='demos-header'>
						<div class="icon-box">
							<img src="${path}/resource/img/zanwudingdan.png" class="icon">
						</div>
						<h1 class="demos-title" style="color:black;margin-bottom: 25px;">还没有人购买过您的商品</h1>
						<a href="javascript:" class="weui-btn back open-popup" data-target="#popup_share">分享店铺</a>
					</header>
				</c:if>
				<c:if test="${staySendOrder!='[]'}" >
					<div class="weui-cells__title">共${staySendOrder.size()}条</div>
					<div class="weui-cells">
						<c:forEach items="${staySendOrder }" var="ol">
							<div class="weui-cell">
								<div class="weui-cell__hd"><img src="${path}/img/${ol.main_file_id}.htm" alt="" style="width:88px;height:88px;margin-right:5px;display:block"></div>
								<div class="weui-cell__bd">
									<p style="color: #323233;font-size: 14px;line-height: 20px">${ol.product_name }</p>
									<div style="line-height: 30px">
										<span style="color: #ff2333;font-size: 14px;">￥${ol.price}</span>
										<span style="color: #969799;font-size: 12px"> x ${ol.cnt }</span>
									</div>
								</div>
								<div style="text-align: center">
									<span style="color: #ff2333;font-size: 16px">￥${ol.price_total }</span>
									<div class="weui-cell__ft" style="color: #999;margin-top: 2px">
										<div style="padding-left:5px;">
											<a href="javascript:deliver('${ol.order_no}','${ol.kid}');" class="weui-btn weui-btn_mini weui-btn_plain-primary">物流发货</a>
										</div>
									</div>
								</div>

							</div>
						</c:forEach>
					</div>
					<div class="weui-loadmore weui-loadmore_line weui-loadmore_dot">
						<span class="weui-loadmore__tips"></span>
					</div>
				</c:if>
			</div>
			<div id="tab3" class="weui-tab__bd-item">
				<c:if test="${finishOrder=='[]'}" >
					<header class='demos-header'>
						<div class="icon-box">
							<img src="${path}/resource/img/zanwudingdan.png" class="icon">
						</div>
						<h1 class="demos-title" style="color:black;margin-bottom: 25px;">还没有人购买过您的商品</h1>
						<a href="javascript:" class="weui-btn back open-popup" data-target="#popup_share">分享店铺</a>
					</header>
				</c:if>
				<c:if test="${finishOrder!='[]'}" >
					<div class="weui-cells__title">共${finishOrder.size()}条</div>
					<div class="weui-cells">
						<c:forEach items="${finishOrder }" var="ol">
							<div class="weui-cell">
								<div class="weui-cell__hd"><img src="${path}/img/${ol.main_file_id}.htm" alt="" style="width:88px;height:88px;margin-right:5px;display:block"></div>
								<div class="weui-cell__bd">
									<p style="color: #323233;font-size: 14px;line-height: 20px">${ol.product_name }</p>
									<div style="line-height:30px">
										<span style="color: #ff2333;font-size: 14px;">￥${ol.price}</span>
										<span style="color: #969799;font-size: 12px"> x ${ol.cnt }</span>
									</div>
								</div>
								<div style="text-align: center">
									<span style="color: #ff2333;font-size: 16px">￥${ol.price_total }</span>
									<div class="weui-cell__ft" style="color: #999;margin-top: 2px ">
										<div>
											<a href="${path }/wx/shop/kuaidi.htm?orderDetailId=${ol.kid}" class="weui-btn weui-btn_mini weui-btn_plain-primary">查看物流</a>
										</div>
									</div>
								</div>

							</div>
						</c:forEach>
					</div>
					<div class="weui-loadmore weui-loadmore_line weui-loadmore_dot">
						<span class="weui-loadmore__tips"></span>
					</div>
				</c:if>
			</div>
			<div id="tab4" class="weui-tab__bd-item">
				<c:if test="${orderList=='[]'}" >
					<header class='demos-header'>
						<div class="icon-box">
							<img src="${path}/resource/img/zanwudingdan.png" class="icon">
						</div>
						<h1 class="demos-title" style="color:black;margin-bottom: 25px;">还没有人购买过您的商品</h1>
						<a href="javascript:" class="weui-btn back open-popup" data-target="#popup_share">分享店铺</a>
					</header>
				</c:if>
				<c:if test="${orderList!='[]'}" >
					<div class="weui-cells__title">共${orderList.size()}条</div>
					<div class="weui-cells">
						<c:forEach items="${orderList }" var="ol">
							<div class="weui-cell">
								<div class="weui-cell__hd"><img src="${path}/img/${ol.main_file_id}.htm" alt="" style="width:88px;height:88px;margin-right:5px;display:block"></div>
								<div class="weui-cell__bd">
									<p style="color: #323233;font-size: 14px;line-height: 20px">${ol.product_name }</p>
									<div style="line-height: 30px">
										<span style="color: #ff2333;font-size: 14px;">￥${ol.price}</span>
										<span style="color: #969799;font-size: 12px"> x ${ol.cnt }</span>
									</div>
								</div>
								<div style="text-align: center">
									<h4 class="weui-media-box__title" style="font-size: 14px;color:#ff6a00;text-align: center">${ol.status_order}</h4>
									<span style="color: #ff2333;font-size: 16px;">￥${ol.price_total }</span>
									<div class="weui-cell__ft" style="color: #999;text-align: left ">
										<div style="padding-left:5px;">
											<c:if test="${ol.status_order=='购物车'}" >
												<a href="javascript:reminder('提醒用户下单','您好！感谢您添加至购物车，提醒您库存不多，请尽快下单哦<a href=\'${path}/wx/cart.htm\'>购物车</a>','${ol.member_id}');" class="weui-btn weui-btn_mini weui-btn_plain-primary">提醒下单</a>
											</c:if>
											<c:if test="${ol.status_order=='下单中'}" >
												<a href="javascript:reminder('提醒用户付款','您好！您的订单长时间未支付，提醒您避免过期，请尽快下单哦<a href=\'${path}/wx/order_detail.htm?kid=${ol.kid}\'>立即下单</a>','${ol.member_id}')" class="weui-btn weui-btn_mini weui-btn_plain-primary">提醒付款</a>
											</c:if>
											<c:if test="${ol.status_order=='已付款'}" >
												<a href="javascript:deliver('${ol.order_no}','${ol.kid}');" class="weui-btn weui-btn_mini weui-btn_plain-primary">物流发货</a>
											</c:if>
											<c:if test="${ol.status_order=='已发货' }" >
												<a href="${path }/wx/shop/kuaidi.htm?orderDetailId=${ol.kid}" class="weui-btn weui-btn_mini weui-btn_plain-primary">查看物流</a>
											</c:if>
											<c:if test="${ol.status_order=='已收货'}" >
											</c:if>
											<c:if test="${ol.status_order=='已完成'}" >
											</c:if>
											<c:if test="${ol.status_order=='已退单'}" >
											</c:if>
											<c:if test="${ol.status_order=='已取消'}" >
											</c:if>
										</div>
									</div>
								</div>
							</div>
						</c:forEach>
					</div>
					<div class="weui-loadmore weui-loadmore_line weui-loadmore_dot">
						<span class="weui-loadmore__tips"></span>
					</div>
				</c:if>
			</div>
		</div>
	</div>
	<div class="v_nav">
		<div class="vf_nav">
			<ul>
				<li><a href="${path }/wx/shop/product.htm"><i class="vf_6"></i><span>商品管理</span></a></li>
				<li><a href="${path }/wx/shop/order.htm" style="color: #FF2233;"><i class="vf_7"></i><span>订单管理</span></a></li>
				<li><a href="${path }/wx/shop/store.htm"><i class="vf_8"></i><span>店铺管理</span></a></li>
			</ul>
		</div>
	</div>

	<div id="deliver" class='weui-popup__container popup-bottom'>
		<div class="weui-popup__overlay"></div>
		<div class="weui-popup__modal">
			<div class="toolbar">
				<div class="toolbar-inner">
					<a href="javascript:;" class="picker-button close-popup"><i role="button" tabindex="0" class="van-icon van-icon-cross van-popup__close-icon van-popup__close-icon--top-right"></i></a>
					<h1 class="title" style="font-size: 17px">物流发货</h1>
				</div>
			</div>
			<div class="modal-content">
				<div class="weui-cells__title"></div>
				<div class="weui-cells weui-cells_form">
					<div class="weui-cell">
						<div class="weui-cell__hd"><label class="weui-label">订单号</label></div>
						<div class="weui-cell__bd">
							<input class="weui-input" type="number" placeholder="" name="order_no" readonly>
							<input class="weui-input" type="text" placeholder="" name="order_detail_id" hidden>
						</div>
					</div>
					<div class="weui-cell weui-cell_select weui-cell_select-after">
						<div class="weui-cell__hd"><label class="weui-label">物流公司</label></div>
						<div class="weui-cell__bd">
							<select class="weui-select" name="logistics_com">
								<c:forEach items="${logisticsList }" var="ll">
									<option value="${ll.code}">${ll.name}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="weui-cell">
						<div class="weui-cell__hd"><label class="weui-label">运单号</label></div>
						<div class="weui-cell__bd">
							<input class="weui-input" type="text" placeholder="" name="logistics_code">
						</div>
					</div>
				</div>
				<div class="weui-btn-area">
					<a class="weui-btn weui-btn_primary" href="javascript:deliver_sub();" id="showTooltips">发货</a>
				</div>
			</div>
		</div>
	</div>
	<div id="popup_share" class='weui-popup__container popup-bottom'>
		<div class="weui-popup__overlay"></div>
		<div class="weui-popup__modal">
			<div class="tee-view share-pop-header">
				<span class="tee-text">立即分享给好友</span>
			</div>
			<div class="tee-view share-pop-content">
				<div class="tee-view share-item" >
					<a href="javascript:" class="share-pop__item open-popup" data-target="#share-guide">
						<div class="tee-view share-pop__item-icon">
							<img src="${path}/resource/img/share-icon-wechat.png" alt="" style="width: 100%;height: 100%">
						</div>
						<span class="tee-text share-pop__item-text">分享给好友</span>
						<span class="tee-text share-pop__item-desc"></span>
					</a>
				</div>
				<div class="tee-view share-item" >
					<a href="javascript:" class="share-pop__item open-popup" data-target="#share-guide">
						<div class="tee-view share-pop__item-icon">
							<img src="${path}/resource/img/share-sheet-wechat-moments.png" alt="" style="width: 100%;height: 100%">
						</div>
						<span class="tee-text share-pop__item-text">分享到朋友圈</span>
						<span class="tee-text share-pop__item-desc"></span>
					</a>
				</div>
				<div class="tee-view share-item" >
					<div class="share-pop__item close-popup" id="copyBtn">
						<div class="tee-view share-pop__item-icon">
							<img src="${path}/resource/img/share-copy.png" alt="" style="width: 100%;height: 100%">
						</div>
						<span class="tee-text share-pop__item-text">复制链接</span>
						<span class="tee-text share-pop__item-desc"></span>
					</div>
				</div>
			</div>
			<div class="service_content" style="text-align: center;margin-top: 20px">
				<a href="javascript:;" class="weui_btn close-popup" style="width: 90%;height: 40px;background: #f44;border: none;border-radius: 26px;color: #ffffff;text-align: center;line-height: 40px;display: inline-block;margin-bottom: 4px;font-size: 14px;">取消</a>
			</div>
		</div>
	</div>

	<div id="share-guide" class="tee-view weui-popup__container close-popup">
		<div class="tee-view share-guide__arrow"></div>
		<div class="tee-view share-guide__title">立即分享给好友吧</div>
		<div class="tee-view share-guide__sub" style="font-size: 14px">点击屏幕右上角将本页面分享给好友</div>
	</div>
	<script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
	<script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
	<script src="${path }/resource/js/fastclick.js"></script>
	<script>
		$("#copyBtn").click(function(){
            let url1= "http://zs.sxxlkj.com/neshop2/wx/shop_product.htm?rank_code=DESC&shop_id=${shop.kid}"
			let encodeURL = encodeURI(encodeURI(url1))
			console.log(encodeURL)
			var url = "【" +"${shop.name}" + "】" + "的精选好货，这个店我很喜欢，推荐给你，点击查看" +'👉'+ encodeURL;
			console.log(url)
			var successful;
			if (navigator.userAgent.match(/(iPhone|iPod|iPad|iOS)/i)) { //ios
				var copyDOM = document.createElement('div');  //要复制文字的节点
				copyDOM.innerHTML = url;
				document.body.appendChild(copyDOM);
				var range = document.createRange();
				// 选中需要复制的节点
				range.selectNode(copyDOM);
				// 执行选中元素
				window.getSelection().addRange(range);
				// 执行 copy 操作
				successful = document.execCommand('copy');
				// 移除选中的元素
				window.getSelection().removeAllRanges();
				$(copyDOM).hide()
			}else{
				var oInput = document.createElement('input')
				oInput.value = url;
				document.body.appendChild(oInput)
				oInput.select() // 选择对象
				successful = document.execCommand('Copy') // 执行浏览器复制命令
				oInput.className = 'oInput'
				oInput.style.display = 'none'
				oInput.remove()
			}
			if(successful){
				$.toast("复制成功",{time:2000},function(){
					$("#popup_share").hide();
				});
			}else{
				$.toast("复制失败",{time:2000},function(){
					$("#popup_share").hide();
				});
			}
		});

		$(function() {
			wx.config({
				debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				appId: '${jsConfig.appId}', // 必填，公众号的唯一标识
				timestamp: ${jsConfig.timestamp}, // 必填，生成签名的时间戳
				nonceStr: '${jsConfig.nonceStr}', // 必填，生成签名的随机串
				signature: '${jsConfig.signature}',// 必填，签名，见附录1
				jsApiList: ['checkJsApi','updateAppMessageShareData','updateTimelineShareData'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
			});
			// $.showLoading();
			document.onreadystatechange = function(){
				if(document.readyState=="complete"){
					// $.hideLoading();
				}
			}
			FastClick.attach(document.body);

			wx.ready(function () {   //需在用户可能点击分享按钮前就先调用
				wx.checkJsApi({
					jsApiList: [
						'updateAppMessageShareData',
						'updateTimelineShareData',
					],
					success: function (res) {
						//alert(JSON.stringify(res));
					}
				});

				wx.updateAppMessageShareData({
					title: '【${shop.name}】的精选好货', // 分享标题
					desc: '为您严格把关挑选，厂家发货配送服务，正品保障。', // 分享描述
					link: 'http://zs.sxxlkj.com/neshop2/wx/shop_product.htm?rank_code=DESC&shop_id=${shop.kid}', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					imgUrl: 'http://zs.sxxlkj.com/neshop2/img/${shop.logo_file_id}.htm', // 分享图标
					success: function () {
						// 设置成功
					}
				})
				wx.updateTimelineShareData({
					title: '【${shop.name}】的精选好货', // 分享标题
					desc: '为您严格把关挑选，厂家发货配送服务，正品保障。', // 分享描述
					link: 'http://zs.sxxlkj.com/neshop2/wx/shop_product.htm?rank_code=DESC&shop_id=${shop.kid}', // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					imgUrl: 'http://zs.sxxlkj.com/neshop2/img/${shop.logo_file_id}.htm', // 分享图标
					success: function () {
						// 设置成功
					}
				})
			});
		});

		function reminder(title,input,member_id){
			$.prompt({
				text: "输入提醒内容，不可出现不和谐文字",
				title: title,
				input: input,
				empty: false,
				onOK: function(text) {
					$.showLoading();
					var ajaxTimeOut = $.ajax({
						type : "GET",
						url: "${path}/wx/shop/sendTextMsg.htm",
						timeout : 60000, //超时时间设置，单位毫秒
						data: {member_id:member_id,content:text},
						dataType: "json",
						success: function(data){
							$.hideLoading();
							console.log(data);
							$.toast("已提醒");
						},
						error : function(XMLHttpRequest, textStatus, errorThrown) {
							$.hideLoading();
							$.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
						},
						complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
							if(status=='timeout'){//超时,status还有success,error等值的情况
								ajaxTimeOut.abort(); //取消请求
								$.hideLoading();
								$.toptip('😣 网络好像出问题了，请重试', 'warning');
							}
						},
						failure : function(response) {
							$.hideLoading();
							$.toptip('😣 好像出问题了，请重试', 'warning');
						}
					});
				}
			});
		}

		function deliver(order_no,order_detail_id){
			$("#deliver").popup();
			$("input[name=order_no]").val(order_no);
			$("input[name=order_detail_id]").val(order_detail_id);
		}

		function deliver_sub(){
			var order_no = $("input[name=order_no]").val();
			var logistics_com_code = $("select[name=logistics_com] option:selected").val();
			var logistics_code = $("input[name=logistics_code]").val();
			var order_detail_id = $("input[name=order_detail_id]").val();
			if(order_no === ''){$.toptip('订单异常，未获取订单号', 'warning');return false;}
			if(logistics_com_code === ''){$.toptip('请选择物流公司', 'warning');return false;}
			if(logistics_code === ''){$.toptip('请输入运单号', 'warning');return false;}

			$.showLoading();
			var ajaxTimeOut = $.ajax({
				type : "POST",
				url: "${path}/wx/shop/deliver.htm",
				timeout : 60000, //超时时间设置，单位毫秒
				data: {order_id:order_no,logistics_com_code:logistics_com_code,logistics_code:logistics_code,order_detail_id:order_detail_id},
				dataType: "json",
				success: function(data){
					$.hideLoading();
					console.log(data);
					$.toast("已发货");
					window.location.href="${path }/wx/shop/kuaidi.htm?orderDetailId="+order_detail_id;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					$.hideLoading();
					$.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
				},
				complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
					if(status=='timeout'){//超时,status还有success,error等值的情况
						ajaxTimeOut.abort(); //取消请求
						$.hideLoading();
						$.toptip('😣 网络好像出问题了，请重试', 'warning');
					}
				},
				failure : function(response) {
					$.hideLoading();
					$.toptip('😣 好像出问题了，请重试', 'warning');
				}
			});
		}
		// 关闭分享遮罩
		$("#share-guide").click(function(){
			$("#share-guide").hide();
		});
	</script>
</body>
</html>