<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>
<html>
<head>
<title>店铺认证</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<link rel="stylesheet" href="${path}/resource/style4wx1/extend/jqweui8.2/lib/weui.min.css" />
<link rel="stylesheet" href="${path}/resource/style4wx1/extend/jqweui8.2/css/jquery-weui.min.css" />
<link rel="stylesheet" href="${path}/resource/style4wx1/css/base-wx.css?v=1.2" />
<script type="text/javascript" src="${path}/resource/style4wx1/extend/jquery-2.1.4.js" ></script>
<script type="text/javascript" src="${path}/resource/style4wx1/extend/jqweui8.2/js/jquery-weui.min.js" ></script>
<script type="text/javascript" src="${path}/resource/style4wx1/extend/jqweui8.2/lib/fastclick.js" ></script>
<script type="text/javascript" src="${path}/resource/style4wx_1/extend/jqweui8.2/js/city-picker.js" charset="utf-8"></script>
<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.6.0.js"></script>
</head>
<style type="text/css">
      html,
      body {
          height: 100%;
          width: 100%;
          margin: 0px;
          padding: 0px;
      }
      #container {
          width: 100%;
          height: 100%
      }
      #container {
   min-width:100%;
   min-height:100%;
}
/* 上传图片开始 */
	.up_left,.up_right{
		width:45%;
		float:left;
		overflow:hidden;
		border-radius: 10px;
	}
	.up_right{
		margin-left:5%;
	}
	.weui_cells .weui_uploader_bd img{
		display:inline-block;
		margin:0 auto;
	}
/* 上传图片结束 */
		#affirm_btn{margin-right: 25px;}
		#centerDiv{
			width: 100%;
		    height: 5%;
		    background: #fff;
		}
      .fr{float: right;}
/*表单样式*/
	  .weui_cells {
		  margin: 10px 10px 20px 10px;
		  border-radius: 6px;
	  }
	  .weui_label{
		  font-weight: 500;
	  }
	  input::-webkit-input-placeholder{
		  color:#d5d5d6;
	  }
	  .weui_cell_bd ::-webkit-input-placeholder{
		  color:#d5d5d6;
	  }
	  .weui_btn:after{
		  border: none;
	  }
	  .weui_cells:before{
		  border: none;
	  }
	  .weui_cell:before{
		width: 90%;
	  }
	  .line:before{
		  border: none;
	  }
  </style>
<body>
	<input name="user_id_p" type="hidden" />
	<input name="user_id_n" type="hidden" />
	<input name="shop_permit" type="hidden" />
	<input name="shop_license" type="hidden" />
<%--	<div class="weui_cells_title">商户认证入驻</div>--%>
	<div class="weui_cells weui_cells_form">
		<div class="weui_cell">
			<div class="weui_cell_hd">
				<label class="weui_label">店铺名称</label>
			</div>
			<div class="weui_cell_bd weui_cell_primary">
				<input id="shop_name" class="weui_input" type="text" name="shop_name"  placeholder="请输入商户名称" />
			</div>
		</div>
		<div class="weui_cell">
			<div class="weui_cell_hd">
				<label class="weui_label">负责人姓名</label>
			</div>
			<div class="weui_cell_bd weui_cell_primary">
				<input id="name" class="weui_input" type="text" name="name"  placeholder="请输入负责人姓名" />
			</div>
		</div>
		<div class="weui_cell">
			<div class="weui_cell_hd">
				<label class="weui_label">联系电话</label>
			</div>
			<div class="weui_cell_bd weui_cell_primary">
				<input id="mobile" class="weui_input" type="number" name="mobile" maxlength="11" placeholder="便于后续与您联系" />
			</div>
		</div>
		<div class="weui_cell">
			<div class="weui_cell_hd">
				<label for="" class="weui_label">所在地</label>
			</div>
			<div class="weui_cell_bd weui_cell_primary">
				<input class="weui_input" id='city-picker' placeholder="请选择所在地" value="" />
				<input class="weui_input" type="hidden" name="province" />
				<input class="weui_input" type="hidden" name="city" />
				<script>

				wx.config({
					debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
					appId: '${jsConfig.appId}', // 必填，公众号的唯一标识
					timestamp: ${jsConfig.timestamp}, // 必填，生成签名的时间戳
					nonceStr: '${jsConfig.nonceStr}', // 必填，生成签名的随机串
					signature: '${jsConfig.signature}',// 必填，签名，见附录1
					jsApiList: ['chooseImage','uploadImage'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
				});

				function register() {
					var shop_name = $("input[name='shop_name']").val();		//店铺名
					var name = $("input[name='name']").val();		//姓名
					var mobile = $("#mobile").val();				//手机
					var province = $("input[name='province']").val();//省份
					var city = $("input[name='city']").val();		//城市
					var user_id_p = $("input[name='user_id_p']").val();//身份证正面
					var user_id_n = $("input[name='user_id_n']").val();//身份证反面
					var shop_license = $("input[name='shop_license']").val();//营业执照
					var shop_permit = $("input[name='shop_permit']").val();//许可证

				    if (shop_name === '') {$.toptip('请输入店铺名称', 'error');return false;}
				    if (name === '') {$.toptip('请输入真实姓名', 'error');return false;}
					if (mobile === '') {$.toptip('请输入手机号', 'error');return false;}
					if (user_id_p === '') {$.toptip("请上传身份证正面", "error");return false;}
					if (user_id_n === '') {$.toptip("请上传身份证反面", "error");return false;}
					if (shop_license === '') {$.toptip("请上传营业执照", "error");return false;}
					$.ajax({
							type : "POST",
							url : "${path}/wx/shop/register.htm",
							data : "shop_type=企业店铺&shop_name=" + shop_name + "&name=" + name + "&mobile=" + mobile + "&province=" + province + "&city=" + city
							+ "&user_id_p=" + user_id_p + "&user_id_n=" + user_id_n + "&shop_permit=" + shop_permit + user_id_n + "&shop_license=" + shop_license,
							dateType : 'json',
							success : function(msg) {
								msg = $.parseJSON(msg);
								if (msg.end == 'false') {
									$.toptip(msg.msg, 'error');
								} else if((msg.end == 'true')){
									// $.toptip(msg.msg, 'success');
									$.toast(msg.msg, "success", function() {
										history.go(0);
									});
								}
							}
						});
				}
				//禁用地区选择
				$("#city-picker").cityPicker({
					showDistrict : false,
					onChange : function(picker, values, displayValues) {
						console.log(displayValues);
						$("input[name=province]").val(displayValues[0]);
						$("input[name=city]").val(displayValues[1]);
					}
				});

				function upImage(){//身份证正面
					wx.chooseImage({
						count: 1, // 默认9
						sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
						sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
						success: function (res) {
							$.showLoading("上传中...");
							//wx.previewImage();
							wx.uploadImage({
								localId: res.localIds[0], // 需要上传的图片的本地ID，由chooseImage接口获得
								isShowProgressTips: 1, // 默认为1，显示进度提示
								success: function (res) {
									var serverId = res.serverId; // 返回图片的服务器端ID
									console.log(serverId);
									$.ajax({
										type : "GET",
										url : "${path}/wx_upload.htm",
										data : "media_id=" + serverId,
										dateType : 'json',
										success : function(msg) {
											msg = $.parseJSON(msg);
											$("input[name=user_id_p]").val(msg.file_id);
											$("#aaa").attr("src","${path}/img/"+msg.file_id+".htm");
											$.hideLoading();
										}
									});
								}
							});
						}
					});//#chooseImage
				}
				function upImage1(){//身份证反面
					wx.chooseImage({
						count: 1, // 默认9
						sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
						sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
						success: function (res) {
							$.showLoading("上传中...");
							//wx.previewImage();
							wx.uploadImage({
								localId: res.localIds[0], // 需要上传的图片的本地ID，由chooseImage接口获得
								isShowProgressTips: 1, // 默认为1，显示进度提示
								success: function (res) {
									var serverId = res.serverId; // 返回图片的服务器端ID
									console.log(serverId);
									$.ajax({
										type : "GET",
										url : "${path}/wx_upload.htm",
										data : "media_id=" + serverId,
										dateType : 'json',
										success : function(msg) {
											msg = $.parseJSON(msg);
											$("input[name=user_id_n]").val(msg.file_id);
											$("#bbb").attr("src","${path}/img/"+msg.file_id+".htm");
											$.hideLoading();
										}
									});
								}
							});
						}
					});//#chooseImage
				}

				function upImage2(){//营业执照
					wx.chooseImage({
						count: 1, // 默认9
						sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
						sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
						success: function (res) {
							$.showLoading("上传中...");
							//wx.previewImage();
							wx.uploadImage({
								localId: res.localIds[0], // 需要上传的图片的本地ID，由chooseImage接口获得
								isShowProgressTips: 1, // 默认为1，显示进度提示
								success: function (res) {
									var serverId = res.serverId; // 返回图片的服务器端ID
									console.log(serverId);
									$.ajax({
										type : "GET",
										url : "${path}/wx_upload.htm",
										data : "media_id=" + serverId,
										dateType : 'json',
										success : function(msg) {
											msg = $.parseJSON(msg);
											$("input[name=shop_license]").val(msg.file_id);
											$("#ccc").attr("src","${path}/img/"+msg.file_id+".htm");
											$.hideLoading();
										}
									});
								}
							});
						}
					});//#chooseImage
				}

				function upImage3(){//许可证
					wx.chooseImage({
						count: 1, // 默认9
						sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
						sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
						success: function (res) {
							$.showLoading("上传中...");
							//wx.previewImage();
							wx.uploadImage({
								localId: res.localIds[0], // 需要上传的图片的本地ID，由chooseImage接口获得
								isShowProgressTips: 1, // 默认为1，显示进度提示
								success: function (res) {
									var serverId = res.serverId; // 返回图片的服务器端ID
									console.log(serverId);
									$.ajax({
										type : "GET",
										url : "${path}/wx_upload.htm",
										data : "media_id=" + serverId,
										dateType : 'json',
										success : function(msg) {
											msg = $.parseJSON(msg);
											$("input[name=shop_permit]").val(msg.file_id);
											$("#ddd").attr("src","${path}/img/"+msg.file_id+".htm");
											$.hideLoading();
										}
									});
								}
							});
						}
					});//#chooseImage
				}
				</script>
			</div>
		</div>
		<div class="weui_cell">
			<div class="weui_cell_bd weui_cell_primary">
				<div class="weui_uploader">
					<div class="weui_uploader_bd">
						<div class="up_left">
							<img id="aaa" onclick="upImage()" src="${path}/resource/img/shop1.jpg" width="100%" height="110"/>
						</div>
						<div class="up_right">
							<img id="bbb" onclick="upImage1()" src="${path}/resource/img/shop2.jpg" width="100%" height="110"/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="weui_cell line">
			<div class="weui_cell_bd weui_cell_primary">
				<div class="weui_uploader">
					<div class="weui_uploader_bd">
						<div class="up_left">
							<img id="ccc" onclick="upImage2()" src="${path}/resource/img/shop3.png" width="100%" height="110"/>
						</div>
						<div class="up_right">
							<img id="ddd" onclick="upImage3()" src="${path}/resource/img/shop4.png" width="100%" height="110"/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="weui_cells_title" style="margin-top: 20px">
			<p class="weui_media_desc" style="font-size:12px;color:#999">点击认证表示同意
				<a style="font-size:13px;color: #0675ff " href="javascript:open_popup()">《店铺入驻协议》</a>
			</p>
		</div>
		<div class="weui_cells_title" style="margin-bottom: 30px">
			<button id="register" type="button" onclick="register()" class="weui_btn weui_btn_primary" style="border-radius: 26px">认证</button><!--  disabled="disabled" style="background-color: #EF4F4F;" -->
		</div>
	</div>
	<jsp:include page="to_cox_protect.jsp" />
</body>
</html>