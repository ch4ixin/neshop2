<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp" %>
<% 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragrma","no-cache"); 
response.setDateHeader("Expires",0); 
%> 
<!DOCTYPE html >
<html>
<head>
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">
<title>店铺管理</title>
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
	<link rel="stylesheet" type="text/css" href="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/css/user.css"/>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>
<link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/category_list.css"/>
<link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
<link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
<script src="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/js/modernizr.js"></script>
<script type="text/javascript" src="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/js/jquery.js"></script>
<script src="http://res.wx.qq.com/open/js/jweixin-1.6.0.js"></script>
<style type="text/css">
	body, html {
		height: 100%;
		-webkit-tap-highlight-color: transparent;background-color: #F4F4F4;
	}
	.vf_nav ul li {
		width: 33.33%;
	}
	.vf_8{background: url(${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/shnav.png) no-repeat 0 -56px;background-size: auto 86px;}
	.com_top dl dt span{
		text-indent: 0;
	}
</style>
<body ontouchstart>
	<div id="tbh5v0">
		<div class="user_com">
			<div class="com_top" style="height: 130px">
				<dl>
					<dt>
						<c:if test="${shop.logo_file_id =='' }">
							<img src="${path}/resource/img/dianpu.png" onclick="upLogoFile()" id="logo_file" style="background: #fff">
						</c:if>
						<c:if test="${shop.logo_file_id !='' }">
							<img src="${path}/img/${shop.logo_file_id}.htm" onclick="upLogoFile()" id="logo_file" style="background: #fff">
						</c:if>
						<span>${shop.name}</span>
					<dd></dd>
					</dt>
				</dl>
	<%--			<div class="uer_topnav">--%>
	<%--				<ul>--%>
	<%--					<li class="bain"><a href="${path }/wx/order_list.htm"><span>0</span>店铺订单</a></li>--%>
	<%--					<li class="bain"><a ><span>0</span>优惠券</a></li>--%>
	<%--					<li><a ><span>0</span>店铺积分</a></li>--%>
	<%--				</ul>--%>
	<%--			</div>--%>
			</div>
	<%--       待完善--%>
			<div class="Wallet" style="min-height: 198px">
				<div style="border-bottom: 1px solid #eaeaea">
					<em class="Icon Icon11"></em>
					<p style="padding: 12px 0 6px 46px;font-weight: bold;font-size: 14px">基本信息</p>
				</div>
				<div style="font-size: 14px;margin: 6px 0 10px 18px;line-height: 30px;">
					<p>店铺名称：${shop.name }</p>
					<p>店铺类型：${shop.type }</p>
					<p>店铺等级：${shop.level }</p>
					<p>店铺地址：${shop.province }${shop.city }${shop.address }</p>
					<p>店铺负责人：${shop.link_man }</p>
					<p>负责人联系方式：${shop.link_mobile }</p>
				</div>
			</div>
			<div class="Wallet" style="min-height: 190px;">
				<div style="border-bottom: 1px solid #eaeaea">
					<em class="Icon Icon12"></em>
					<p style="padding: 12px 0 6px 46px;font-weight: bold;font-size: 14px">主体信息</p>
				</div>
				<div style="font-size: 14px;margin: 6px 20px;line-height: 24px;height: 128px">
					<div style="display: inline-block;width: 48%;text-align: center;margin-right: 2%">
						<p style="margin-bottom: 4px">店铺许可证：</p>
						<img src="${path}/img/${shop.shop_permit}.htm" style="width: 150px;height: 100px">
					</div>
					<div style="display: inline-block;width: 48%;text-align: center">
						<p style="margin-bottom: 4px">店铺营业执照：</p>
						<img src="${path}/img/${shop.shop_license}.htm" style="width: 150px;height: 100px">
					</div>
				</div>
			</div>
			<div class="weui-loadmore weui-loadmore_line weui-loadmore_dot">
				<span class="weui-loadmore__tips"></span>
			</div>
		</div>
	</div>
	<div class="v_nav">
		<div class="vf_nav">
			<ul>
				<li><a href="${path }/wx/shop/product.htm"><i class="vf_6"></i><span>商品管理</span></a></li>
				<li><a href="${path }/wx/shop/order.htm"><i class="vf_7"></i><span>订单管理</span></a></li>
				<li><a href="${path }/wx/shop/store.htm" style="color: #FF2233;"><i class="vf_8"></i><span>店铺管理</span></a></li>
			</ul>
		</div>
	</div>
	<script>
		$(function() {
			wx.config({
				debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				appId: '${jsConfig.appId}', // 必填，公众号的唯一标识
				timestamp: ${jsConfig.timestamp}, // 必填，生成签名的时间戳
				nonceStr: '${jsConfig.nonceStr}', // 必填，生成签名的随机串
				signature: '${jsConfig.signature}',// 必填，签名，见附录1
				jsApiList: ['chooseImage','uploadImage'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
			});
		})

		function upLogoFile(){//上传logo
			wx.chooseImage({
				count: 1, // 默认9
				sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
				sourceType: ['album'], // 可以指定来源是相册还是相机，默认二者都有 , 'camera'
				success: function (res) {
					// $.showLoading("上传中...");
					//wx.previewImage();
					wx.uploadImage({
						localId: res.localIds[0], // 需要上传的图片的本地ID，由chooseImage接口获得
						isShowProgressTips: 1, // 默认为1，显示进度提示
						success: function (res) {
							var serverId = res.serverId; // 返回图片的服务器端ID
							console.log(serverId);
							$.ajax({
								type : "GET",
								url : "${path}/wx_upload.htm",
								data : "media_id=" + serverId,
								dateType : 'json',
								success : function(msg) {
									msg = $.parseJSON(msg);
									// $("input[name=user_id_p]").val(msg.file_id);
									$("#logo_file").attr("src","${path}/img/"+msg.file_id+".htm");
									$.ajax({
										type : "POST",
										url : "${path}/wx/shop/edit.htm",
										data : "kid=${shop.kid}&logo_file_id=" + msg.file_id,
										dateType : 'json',
										success : function(msg) {
											// $.hideLoading();
											msg = $.parseJSON(msg);
											if (msg.end == 'false') {
												$.toptip(msg.msg, 'error');
											} else if((msg.end == 'true')){
												// $.toptip(msg.msg, 'success');
												$.toast(msg.msg, "success", function() {
													// history.go(0);
												});
											}
										}
									});
								}
							});
						}
					});
				}
			});//#chooseImage
		}
	</script>
	<script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
	<script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
	<script src="${path }/resource/js/fastclick.js"></script>
	<script src="${path }/resource/jqweui/js/swiper.min.js"></script>
</body>
</html>