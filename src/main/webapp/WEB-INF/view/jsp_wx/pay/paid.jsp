<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>
<html>
<head>
<base href="${basePath}resource/mobile/" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>结算成功</title>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<meta name="Keywords" content="" />
<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
<script src="../../../../neshop2/resource/js/fastclick.js"></script>
<style type="text/css">
body, html {
  height: 100%;
  -webkit-tap-highlight-color: transparent;
  background: #f4f4f4;
}
.demos-title {
  text-align: center;
  font-size: 20px;
  color: #3cc51f;
  font-weight: 400;
  margin: 0 10%;
}


.demos-header {
  padding: 25px 0;
  background: #ffffff;
}
footer {
  text-align: center;
  font-size: 14px;
  padding: 20px;
}

footer a {
  color: #999;
  text-decoration: none;
}
.weui-form-preview{
  margin-top: 10px;
}
.weui-form-preview__bd{
  padding: 6px 15px;
}
.weui-icon_msg{
  font-size: 30px;
  margin-right: 5px;
  margin-top: -2px;
}

</style>
<script type="text/javascript">
	function myClose(){
		WeixinJSBridge.call('closeWindow');
	}
</script>
</head>
 <body>
  <jsp:include page="../common/css-js.jsp" flush="true" />
  <header class='demos-header'>
    <h1 class="demos-title"> <i class="weui-icon-success weui-icon_msg"></i>支付成功!</h1>
  </header>
  <div class="weui-form-preview">
    <div class="weui-form-preview__hd">
      <label class="weui-form-preview__label">微信支付</label>
      <em class="weui-form-preview__value">¥ ${order.end_price}</em>
    </div>
    <div class="weui-form-preview__bd">
      <div class="weui-form-preview__item">
        <label class="weui-form-preview__label">订单编号</label>
        <span class="weui-form-preview__value">${order.order_no }</span>
      </div>
      <div class="weui-form-preview__item">
        <label class="weui-form-preview__label">创建时间</label>
        <span class="weui-form-preview__value">${order.created }</span>
      </div>
      <div class="weui-form-preview__item">
        <label class="weui-form-preview__label">付款时间</label>
        <span class="weui-form-preview__value">${order.pay_time }</span>
      </div>
      <div class="weui-form-preview__item">
        <label class="weui-form-preview__label">收货人</label>
        <span class="weui-form-preview__value">${order.name } ${order.phone }</span>
      </div>
      <div class="weui-form-preview__item">
        <label class="weui-form-preview__label">收货地址</label>
        <span class="weui-form-preview__value">${order.address}</span>
      </div>
      <div class="weui-form-preview__item">
        <label class="weui-form-preview__label">积分+</label>
        <span class="weui-form-preview__value">${order.total_integral}</span>
      </div>
    </div>
    <div class="weui-form-preview__ft">
      <a class="weui-form-preview__btn weui-form-preview__btn_default" href="${path }/wx/home.htm" target="_top">首页</a>
      <a class="weui-form-preview__btn weui-form-preview__btn_primary" href="${path }/wx/order_detail.htm?kid=${order.kid}" target="_top">订单详情</a>
    </div>
  </div>
  <br>
  <div class="weui-btn-area">
    <a href="javascript:myClose();" class="weui-btn weui-btn_default btnBorder">关闭</a>
  </div>
 </body>
</html>