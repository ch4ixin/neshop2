<%@ page language="java" pageEncoding="UTF-8"%>
<script type="text/javascript">
	function onBridgeReady(){
		WeixinJSBridge.invoke(
			'getBrandWCPayRequest', 
			${json},
			function(res){
				//alert(res);
				//alert(JSON.stringify(res));
				// 使用以上方式判断前端返回,微信团队郑重提示：res.err_msg将在用户支付成功后返回
				// ok，但并不保证它绝对可靠。
				if(res.err_msg == "get_brand_wcpay_request:ok" ) {
					window.location.href = "${path}/neshop2/wx/pay/paid.htm?oid=${order.kid}";
				}else if(res.err_msg == "get_brand_wcpay_request:cancle" ) {
					window.location.href = "${path}/neshop2/wx/order_list.htm";
				}else{
					window.location.href = "${path}/neshop2/wx/order_list.htm";
				}
			}
		); 
	}
	if (typeof WeixinJSBridge == "undefined"){
		if( document.addEventListener ){
			document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
		}else if (document.attachEvent){
			document.attachEvent('WeixinJSBridgeReady', onBridgeReady); 
			document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
		}
	}else{
		onBridgeReady();
	}
</script>
