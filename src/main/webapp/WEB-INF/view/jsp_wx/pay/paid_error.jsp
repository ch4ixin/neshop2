<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>结账支付</title>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<jsp:include page="../common/css-js.jsp" flush="true"/>
</head>
<body>

<section class="ui-container">
<br>
<div style="text-align:center;">
<br><br><br>
 <i class="ui-icon-warn" style="font-size:120px; color:#fc7100;"></i>
<br><br><h1 style="font-size:24px;">${error_info} </h1>
</div>
</section>
</body>
</html>