<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>

<!DOCTYPE html>
<html>
  <head>
    <title>我的业绩</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
	.weui-grid__icon{
	  font-size:24px;
	    width: 100%;
	    text-align: center;
	}
	.money{
	  padding:20px;
	}
	.weui-media-box__title {
	    font-size: 14px;
	}
	</style>
  </head>
  <body >
	<c:if test="${myList=='[]' }">
   		<div class="weui-msg">
		  <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
		  <div class="weui-msg__text-area">
		    <h2 class="weui-msg__title">还没有业绩记录</h2>
		    <p class="weui-msg__desc">用户扫描您的分销二维码成为您的客户,购物成功之后会有相应的业绩记录。</p>
		  </div>
		  <div class="weui-msg__opr-area">
		    <p class="weui-btn-area">
<%-- 				      <a href="${path }/wx/my_cousumer.htm" class="weui-btn weui-btn_primary">挣业绩去</a> --%>
		      <a href="javascript:myClose();" class="weui-btn weui-btn_default">关闭页面</a>
		    </p>
		  </div>
		  <div class="weui-msg__extra-area">
		    <div class="weui-footer">
		      <p class="weui-footer__links">
		        <a href="http://www.sxxlkj.com" class="weui-footer__link">山西悦龙斋科技有限公司</a>
		      </p>
		    </div>
		  </div>
		</div>
   	</c:if>
   	<c:if test="${myList!='[]' }">
	<div class="weui-panel weui-panel_access">
	  <div class="weui-grids">
		  <div class="weui-grid js_grid">
		 	 <p class="weui-grid__label">
		      	佣金总金额
		    </p>
		    <div class="weui-grid__icon">
		      ${retail.total_money }
		    </div>
	 	 </div>
		  <div class="weui-grid js_grid">
		 	 <p class="weui-grid__label">
		      	未提现佣金
		    </p>
		    <div class="weui-grid__icon">
		      ${retail.deposit_money }
		    </div>
	 	 </div>
	 	 <div class="weui-grid js_grid">
		 	 <p class="weui-grid__label">
		      	已提现佣金
		    </p>
		    <div class="weui-grid__icon">
		      ${retail.then_money }
		    </div>
	 	 </div>
		</div>
	  <div class="money">
	  	<a href="javascript:tixian();" class="weui-btn weui-btn_primary">提现</a>
 		<p class="weui-msg__desc" style="color: red;text-align: center;font-size: 12.5px;padding-top: 10px;">* 佣金提现必须是1元起</p>
	  </div>
	  <div class="weui-panel__hd">最新业绩记录<span style="float:right;">共${myList_size }条</span></div>
		  <div class="weui-panel__bd">
		   	<div class="myList">
			   		<c:forEach items="${myList }" var="ml">
				   		<a href="javascript:void(0);" class="weui-media-box weui-media-box_appmsg">
					      <div class="weui-media-box__hd">
					        <img class="weui-media-box__thumb" src="${ml.user_headimgurl }">
					      </div>
					      <div class="weui-media-box__bd">
					        <h4 class="weui-media-box__title">客户:${ml.user_nick }</h4>
					        <p class="weui-media-box__desc">${ml.time }成交一单金额为 ${ml.end_price }元,可以提成 
					       	 <c:if test="${retail_lv=='6' }">${ml.one_deduct_money }</c:if> 
					       	 <c:if test="${retail_lv=='9' }">${ml.two_deduct_money }</c:if> 
					  		 元</p>
					      </div>
					    </a>
			   		</c:forEach>
			   	<div class="weui-panel__ft">
				    <a href="javascript:view_more();" class="weui-cell weui-cell_access weui-cell_link">
				      <div class="weui-cell__bd">查看更多</div>
				      <span class="weui-cell__ft"></span>
				    </a>    
				 </div>
		   	</div>
		   	<div class="myListt">
			   		<c:forEach items="${myListt }" var="ml">
				   		<a href="javascript:void(0);" class="weui-media-box weui-media-box_appmsg">
					      <div class="weui-media-box__hd">
					        <img class="weui-media-box__thumb" src="${ml.user_headimgurl }">
					      </div>
					      <div class="weui-media-box__bd">
					        <h4 class="weui-media-box__title">客户:${ml.user_nick }</h4>
					        <p class="weui-media-box__desc">${ml.time }成交一单金额为 ${ml.end_price }元,可以提成 
					         <c:if test="${retail_lv=='6' }">${ml.one_deduct_money }</c:if> 
					       	 <c:if test="${retail_lv=='9' }">${ml.two_deduct_money }</c:if>  
			        		元</p>
					      </div>
					    </a>
			   		</c:forEach>
			   		<div class="weui-loadmore weui-loadmore_line">
						<span class="weui-loadmore__tips" style="font-size: 10px;">已经到底了...</span>
				  </div>
		   	</div>
		  </div>
	</div>
	</c:if>
 	   <script>
	 	  function myClose(){
				WeixinJSBridge.call('closeWindow');
		  }
	 	  
	 	  function tixian(){
	 		  //console.log(${retail.deposit_money }<=0);
	 		  if('${retail.deposit_money }'<1){
		 		 $.alert("提现佣金必须是一元起.");
	 		  }else{
	 			$.confirm({
	 				  title: '佣金${retail.deposit_money }元',
	 				  text: '提现到零钱',
	 				  onOK: function () {
	 					 $.showLoading("正在提现...");
	 		 			 $.ajax({
				      			type : "POST",
				      			url : "${path }/fxwx/deposit.htm?get_money=${retail.deposit_money }",
				      			dateType : 'json',
				      			success : function(msg) {
				      				$.hideLoading();
				      				msg = $.parseJSON(msg);
				      				console.log(msg);
				      				if(msg.end == "true"){
				    	            	  $.alert({
					    	            	  title: msg.msg,
					    	            	  text: "将到账微信钱包-零钱,请注意查收",
					    	            	  onOK: function () {
						      					self.location=document.referrer;
					    	            	  }
				    	            		});
				      				}else{
					    	            $.alert("系统繁忙,请稍后重试...", "提现失败");
				      					
				      				}
				      			}
				      		});
	 				  },
	 				  onCancel: function () {
	 				  }
	 			});
	 		  }
		  }
	 	  
		  $(function() {
		    FastClick.attach(document.body);
		    $(".myList").show();
		    $(".myListt").hide();
		  });
		  
		  function view_more(){
			  $(".myList").hide();
			  $(".myListt").show();
		  }
		</script>
  </body>
</html>
