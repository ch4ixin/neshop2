<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>

<!DOCTYPE html>
<html>
  <head>
    <title>客户订单</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
	.weui-grid__icon{
	  font-size:24px;
	}
	.money{
	padding:20px;
	}
	.weui-grid {
	    width: 50%;
	}
	</style>
  </head>
  <body >
  	<c:if test="${myList=='[]' }">
   		<div class="weui-msg">
		  <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
		  <div class="weui-msg__text-area">
		    <h2 class="weui-msg__title">客户没有订单记录</h2>
		    <p class="weui-msg__desc">用户购物成功之后会有相应的业绩记录。</p>
		  </div>
		  <div class="weui-msg__opr-area">
		    <p class="weui-btn-area">
<%-- 				      <a href="${path }/wx/my_cousumer.htm" class="weui-btn weui-btn_primary">挣业绩去</a> --%>
		      <a href="javascript:myClose();" class="weui-btn weui-btn_default">关闭页面</a>
		    </p>
		  </div>
		  <div class="weui-msg__extra-area">
		    <div class="weui-footer">
		      <p class="weui-footer__links">
		        <a href="http://www.sxxlkj.com" class="weui-footer__link">山西悦龙斋科技有限公司</a>
		      </p>
		    </div>
		  </div>
		</div>
   	</c:if>
   	<c:if test="${myList!='[]' }">
		<div class="weui-panel weui-panel_access">
				<div class="weui-grids">
				  <div class="weui-grid js_grid">
				      <p class="weui-grid__label">
				                       客户订单总数
				      </p>
				    <div class="weui-grid__icon">
				      ${myList_size }
				    </div>
				  </div>
				  <div class="weui-grid js_grid">
				 	 <p class="weui-grid__label">
				      	业绩总佣金
				    </p>
				    <div class="weui-grid__icon">
				      ${retailStore.total_money }
				    </div>
			 	 </div>
				</div>
			  <div class="weui-panel__hd">业绩记录</div>
			  <div class="weui-panel__bd">
				   		<c:forEach items="${myList }" var="ml">
					   		<a href="javascript:void(0);" class="weui-media-box weui-media-box_appmsg">
						      <div class="weui-media-box__hd">
						        <img class="weui-media-box__thumb" src="${ml.user_headimgurl }">
						      </div>
						      <div class="weui-media-box__bd">
						        <h4 class="weui-media-box__title">${ml.user_nick }</h4>
						        <p class="weui-media-box__desc">${ml.time }成交一单金额为 ${ml.end_price }元,可以提额 ${ml.one_deduct_money } 元</p>
						      </div>
						    </a>
				   		</c:forEach>
			  </div>
		</div>
  	</c:if>
 	   <script>
 		function myClose(){
 			WeixinJSBridge.call('closeWindow');
 		}
 	   
		  $(function() {
		    FastClick.attach(document.body);
		  });
		</script>
  </body>
</html>
