var levelStore = Ext.create('Ext.data.Store', {
	remoteSort : true,
	autoLoad : true,
	pageSize : 50,
	sorters : {
		property : 'level',
		direction : 'asc'
	},
	proxy : {
		type : "ajax",
		url : getServerHttp() + "/cp/memberLevel/list_data.htm",
		reader : {
			type : 'json',
			root : 'list',
			totalProperty : 'page.totalRow'
		}
	}
});

var edit_form_panel = Ext.create("Ext.form.Panel", {
	url : getServerHttp() + "/cp/member/edit.htm",
	buttonAlign : "center",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	items : [/*{
		fieldLabel : "分销代码",
		name : "code",
		allowBlank : false
	},*/{
		fieldLabel : "等级",
		name : "level_id",
		allowBlank : false,
		xtype : "combobox",
		emptyText : '请选择等级',
		store : levelStore,
		displayField : 'name',
		valueField : 'kid',
		editable : false
	},{
		fieldLabel : "备注",
		name : "note"
	},{
		fieldLabel : "用户id",
		name : "kid",
		//allowBlank : false,
		readOnly : true
		//hidden : true
	}],
	buttons : [ {
		text : "保存",
		formBind : true, // only enabled once the form is valid
		disabled : true,
		handler : function() {
			var form = this.up("form").getForm();
			if (form.isValid()) {
				form.submit({
					waitMsg : "保存中...",
					success : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
						edit_form_panel_win.close();
						dataStore.load();
					},
					failure : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
					}
				});
			}
		}
	} ]
});

var edit_form_panel_win = Ext.create("Ext.Window", {
	title : "用户编辑",
	closeAction : "hide",
	items : edit_form_panel
});

function myEdit(kid) {

	Ext.Ajax.request({
		url : getServerHttp() + "/cp/member/edit_form.htm?kid=" + kid,
		success : function(response) {
			var json = Ext.JSON.decode(response.responseText);
			edit_form_panel.getForm().reset();
			edit_form_panel.getForm().setValues(json);
			edit_form_panel_win.show();
		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});
}// #myEdit
