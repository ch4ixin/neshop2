var edit_form_panel = Ext.create("Ext.form.Panel", {
	url : "/cp/redPacketModel/edit.htm",
	buttonAlign : "center",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	items : [
			{
				fieldLabel : "活动名称",
				name : "act_name",
				allowBlank : false,
			}, {
				fieldLabel : "商户名称",
				name : "send_name",
				allowBlank : false,
			}, {
				fieldLabel : "祝福语",
				name : "wishing",
				allowBlank : false,
			},  {
				xtype : 'numberfield',
				value : '0',
				minValue : 0,
				allowDecimals : true, // 允许小数点
				allowNegative : false,
				fieldLabel : "付款金额",
				name : "total_amount",
				allowBlank : false,
			}, {
				xtype : "radiogroup",
				fieldLabel : "红包类型",
				value : 1,
				width : 300,
				style : {
					display : 'table',
					marginTop : '10px'
				},
				items : [ {
					boxLabel : "裂变红包",
					name : "amt_type",
					checked : true,
					width : 100,
					inputValue : "裂变红包"
				}, {
					boxLabel : "普通红包",
					name : "amt_type",
					width : 100,
					inputValue : "普通红包"
				} ]

			},  {
				fieldLabel : "备注",
				name : "remark",
			}, {
				fieldLabel : "kid",
				name : "kid",
				hidden : true
			}
			 ],
	buttons : [ {
		text : "保存",
		formBind : true, // only enabled once the form is valid
		disabled : true,
		handler : function() {
			var form = this.up("form").getForm();
			if (form.isValid()) {
				form.submit({
					waitMsg : "保存中...",
					success : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
						edit_form_panel_win.close();
						dataStore.load();
					},
					failure : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
					}
				});
			}
		}
	} ]
});

var edit_form_panel_win = Ext.create("Ext.Window", {
	title : "等级编辑",
	closeAction : "hide",
	items : edit_form_panel
});

function myEdit(kid) {
	Ext.Ajax.request({
		url : "/cp/redPacketModel/edit_form.htm?kid=" + kid,
		success : function(response) {
			var json = Ext.JSON.decode(response.responseText);
			edit_form_panel.getForm().reset();
			console.log(json);
			edit_form_panel.getForm().setValues(json);
			edit_form_panel_win.show();
		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});
}// #myEdit
