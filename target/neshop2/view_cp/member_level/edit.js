var edit_form_panel = Ext.create("Ext.form.Panel", {
	url : getServerHttp() + "/cp/memberLevel/edit.htm",
	buttonAlign : "center",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	items : [
			{
				fieldLabel : "姓名",
				name : "name",
				allowBlank : false
			}, {
				fieldLabel : "等级",
				name : "level",
				allowBlank : false,
			}, {
				fieldLabel : "积分",
				name : "integral",
				allowBlank : false,
			}, {
				fieldLabel : "描述",
				name : "note",
			}, {
				fieldLabel : "kid",
				name : "kid",
			}
			 ],
	buttons : [ {
		text : "保存",
		formBind : true, // only enabled once the form is valid
		disabled : true,
		handler : function() {
			var form = this.up("form").getForm();
			if (form.isValid()) {
				form.submit({
					waitMsg : "保存中...",
					success : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
						edit_form_panel_win.close();
						dataStore.load();
					},
					failure : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
					}
				});
			}
		}
	} ]
});

var edit_form_panel_win = Ext.create("Ext.Window", {
	title : "等级编辑",
	closeAction : "hide",
	items : edit_form_panel
});

function myEdit(kid) {
	Ext.Ajax.request({
		url : getServerHttp() + "/cp/memberLevel/edit_form.htm?kid=" + kid,
		success : function(response) {
			var json = Ext.JSON.decode(response.responseText);
			edit_form_panel.getForm().reset();
			console.log(json);
			edit_form_panel.getForm().setValues(json);
			edit_form_panel_win.show();
		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});
}// #myEdit
