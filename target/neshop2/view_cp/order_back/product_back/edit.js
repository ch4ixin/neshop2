var detailStore1;

detailStore1 = Ext.create('Ext.data.Store', {
	remoteSort : true,
	autoLoad : true,
	sorters : {
		property : 'created',
		direction : 'DESC'
	},
	proxy : {
		type : "ajax",
		url : getServerHttp() + "/cp/order/return_product.htm",
		reader : {
			type : 'json',
			root : 'list'
		}
	},
	listeners : {
		'beforeload' : function(store, op, options) {
			var params = {
				order_id : rec.get('kid')
			};
			Ext.apply(detailStore1.proxy.extraParams, params);
		}
	}
}); // #detailStore1



function mylimit(kid) {
	Ext.Msg.confirm("提示:", "确定修改选定的记录?", function(e) {
		if (e == "yes") {
			Ext.Ajax.request({
				url : getServerHttp() + "/cp/order/return_product_money.htm?kid=" + kid,
				success : function(response) {
					var json = Ext.decode(response.responseText);
					Ext.Msg.alert("提示", json.tip.msg);
					dataStore1.load();
				},
				failure : function(response) {
					Ext.Msg.alert("提示", "操作失败!");
				}
			});
		}//#if
	});
}// #mylimit

