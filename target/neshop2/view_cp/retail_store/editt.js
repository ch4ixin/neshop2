	var memberDataStore = Ext.create('Ext.data.Store', {
		remoteSort : true,
		autoLoad : true,
		sorters : {
			property : 'created',
			direction : 'DESC'
		},
		proxy : {
			type : "ajax",
			url : getServerHttp()+"/cp/retail_store/member_order_list.htm",
			reader : {
				type : 'json',
				root : 'list'
			}
		},
		listeners : {
			'beforeload' : function(store, op, options) {
				var params = {
					retail_id : retail_id
				};
				Ext.apply(memberDataStore.proxy.extraParams, params);
			}
		}
	}); //#memberDataStore

	var find_member_panel11 = Ext.create('Ext.grid.Panel', {
		store : memberDataStore,
		buttonAlign : "center",
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			store : memberDataStore, // same store GridPanel is using
			dock : 'bottom',
			displayInfo : true
		} ],
		columns : [{
			text : '客户昵称',
			dataIndex : 'user_nick',
			flex : 1
		}, {
			text : '订单编号',
			dataIndex : 'order_no',
			flex : 1
		}, {
			text : '订单金额',
			dataIndex : 'end_price',
			flex : 1
		}, {
			text : '订单时间',
			dataIndex : 'created',
			flex : 1,
			renderer : function(val) {
				if (val != '') {
					return Ext.Date.format(new Date(val), "Y-m-d H:i:s");
				}
			}
		}]
	});


	var editt_form_panel_win = Ext.create("Ext.Window", {
		title : "分销商业绩",
		closeAction : "hide",
		buttonAlign : "center",
		closeAction : "hide",
		width : 700, // 宽度
		height : 400, // 长度
		layout : "fit", // 窗口布局类型
		maximizable : true, // 设置是否可以最大化
		items : [find_member_panel11]
	});


	var retail_id = '';
	function myEditt() {
		retail_id = kid;
		editt_form_panel_win.show();
	}//#myEdit
