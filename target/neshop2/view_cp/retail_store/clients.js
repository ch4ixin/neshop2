	var memberStore = Ext.create('Ext.data.Store', {
		remoteSort : true,
		autoLoad : true,
		sorters : {
			property : 'created',
			direction : 'DESC'
		},
		proxy : {
			type : "ajax",
			url : getServerHttp()+"/cp/retail_store/member_list.htm",
			reader : {
				type : 'json',
				root : 'list'
			}
		},
		listeners : {
			'beforeload' : function(store, op, options) {
				var params = {
					retail_id : retail_id
				};
				Ext.apply(memberStore.proxy.extraParams, params);
			}
		}
	}); //#memberDataStore

	var clients_member_panel = Ext.create('Ext.grid.Panel', {
		store : memberStore,
		buttonAlign : "center",
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			store : memberStore, // same store GridPanel is using
			dock : 'bottom',
			displayInfo : true
		} ],
		columns : [{
			text : '图片',
			dataIndex : 'user_headimgurl',
			flex : 1,
			renderer : function(value, cellmeta, record, rowIndex, columnIndex, store) {
				var img_url = record.data["user_headimgurl"];
				return '<img width=50 height=50 src='+img_url+'>';
			}
		}, {
			text : '客户昵称',
			dataIndex : 'user_nick',
			flex : 1
		}]
	});


	var clients_form_panel_win = Ext.create("Ext.Window", {
		title : "分销商用户列表",
		closeAction : "hide",
		buttonAlign : "center",
		closeAction : "hide",
		width : 700, // 宽度
		height : 400, // 长度
		layout : "fit", // 窗口布局类型
		maximizable : true, // 设置是否可以最大化
		items : [clients_member_panel]
	});

	var retail_id = '';
	function myClients(kid) {
		retail_id = kid;
		clients_form_panel_win.show();
	}//#myEdit
