var edit_form_panel = Ext.create("Ext.form.Panel", {
	url : "/cp/img/edit.htm",
	buttonAlign : "center",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	items : [{
		fieldLabel : "楼层",
		name : 'floor_id',
		flex : 1
	},{
		fieldLabel : "名称",
		name : 'name',
		flex : 1
	}, {
		fieldLabel : "编号",
		name : 'code',
		flex : 1
	},{
		fieldLabel : "链接地址",
		name : 'url',
		flex : 1
	},{
		fieldLabel : "id",
		name : "kid",
		hidden : true
	} ],
	buttons : [ {
		text : "保存",
		formBind : true, // only enabled once the form is valid
		disabled : true,
		handler : function() {
			var form = this.up("form").getForm();
			if (form.isValid()) {
				form.submit({
					waitMsg : "保存中...",
					success : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
						edit_form_panel_win.close();
						dataStore.load();
					},
					failure : function(form, action) {
						Ext.Msg.alert("提示", "失败");
					}
				});
			}
		}
	} ]
});

var edit_form_panel_win = Ext.create("Ext.Window", {
	title : "商品编辑",
	closeAction : "hide",
	items : edit_form_panel
});

function myEdit(kid) {
	Ext.Ajax.request({
		url : "/cp/img/edit_form.htm?kid="+kid ,
		success : function(response) {	
			var json = Ext.decode(response.responseText);
			edit_form_panel.getForm().reset();
			edit_form_panel.getForm().setValues(json.img);
			edit_form_panel_win.show();	

		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});
}// #myEdit
