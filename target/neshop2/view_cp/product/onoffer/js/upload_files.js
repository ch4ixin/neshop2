var files = new Array();
var $img = $('#logo_img');
var $msg = $('#msg');
function initUploader() {

	var file = {
		id : '',
		name : ''
	}

	$list1 = $('#thelist1');
	$list2 = $('#thelist2');
	$btn1 = $('#ctlBtn');

	var state = 'pending';

	var uploader1 = WebUploader.create({
		// 选完文件后，是否自动上传。
		auto : true,
		// swf文件路径
		swf : 'http://cdn.staticfile.org/webuploader/0.1.5/Uploader.swf',

		duplicate : true,
		// 文件接收服务端。
		server : '/neshop2/upload.htm',
		// 选择文件的按钮。可选。
		// 内部根据当前运行是创建，可能是input元素，也可能是flash.
		pick : '#picker1',

		// 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
		resize : false
	});

	// 当有文件被添加进队列的时候
	uploader1.on('fileQueued', function(file) {
		if (files.length > 5) {
			$img.hide();
			return;
		}
	});
	// 文件上传过程中创建进度条实时显示。
	uploader1
			.on(
					'uploadProgress',
					function(file, percentage) {

						var $li = $('#' + file.id), $percent = $li
								.find('.progress .progress-bar');

						// 避免重复创建
						if (!$percent.length) {
							$percent = $(
									'<div class="progress progress-striped active">'
											+ '<div class="progress-bar" role="progressbar" style="width: 0%">'
											+ '</div>' + '</div>')
									.appendTo($li).find('.progress-bar');
						}

						$li.find('p.state').text('上传中');

						$percent.css('width', percentage * 100 + '%');
					});

	uploader1
			.on(
					'uploadSuccess',
					function(file, response) {
						$('#' + file.id).find('p.state').text('已上传');

						var end = $.parseJSON(response._raw);
						var popo = Ext.getCmp('file_ids').getValue();
						var main_img_id = Ext.getCmp('img').getValue();
						var oioi = popo.split(",");
						if (main_img_id != "") {
							if (oioi.length < 5) {
								var file = {
									id : end.file_id,
									name : end.file_name
								}
								files.push(file);
								var ids = Ext.getCmp('img').getValue();
								if (oioi[0] == "" && ids == "") {
									$list1
											.append('<div style="display:inline-block;">'
													+ '<div style="margin-left:10px" class="'
													+ file.id
													+ ' item">'
													+ '<span style="margin-left:10px">'
													+ "<img src='/neshop2/img/"
													+ file.id
													+ ".htm?mw=120&mh=120'/>"
													+ '</span>'
													+ '<p style="margin-left:10px" class="state"><a style="color:red;text-decoration:none;" href="javascript:del_file_ids(\''
													+ file.id
													+ '\')">取消上传</a></p>'
													+ '<h4 style="margin-left:10px" class="info">'
													+ '主图片'
													+ '</h4>'
													+ '</div>' + '</div>');
									getFileids();
								} else {
									$list1
											.append('<div style="display:inline-block;">'
													+ '<div style="margin-left:10px" class="'
													+ file.id
													+ ' item">'
													+ '<span style="margin-left:10px">'
													+ "<img src='/neshop2/img/"
													+ file.id
													+ ".htm?mw=120&mh=120'/>"
													+ '</span>'
													+ '<p style="margin-left:10px" class="state"><a style="color:red;text-decoration:none;" href="javascript:del_file_ids(\''
													+ file.id
													+ '\')">取消上传</a></p>'
													+ '</div>' + '</div>');

									getFileids();

								}

							} else {
								alert("只能添加五张照片");
							}
						} else {
							if (oioi.length < 5) {
								var file = {
									id : end.file_id,
									name : end.file_name
								}
								files.push(file);
								var ids = Ext.getCmp('img').getValue();
								if (oioi[0] == "" && ids == "") {
									$list1
											.append('<div style="display:inline-block;">'
													+ '<div style="margin-left:10px" class="'
													+ file.id
													+ ' item">'
													+ '<span style="margin-left:10px">'
													+ "<img src='/neshop2/img/"
													+ file.id
													+ ".htm?mw=120&mh=120'/>"
													+ '</span>'
													+ '<p style="margin-left:10px" class="state"><a style="color:red;text-decoration:none;" href="javascript:del_file_ids(\''
													+ file.id
													+ '\')">取消上传</a></p>'
													+ '<h4 style="margin-left:10px" class="info">'
													+ '主图片'
													+ '</h4>'
													+ '</div>' + '</div>');
									getFileids();
								} else {
									$list1
											.append('<div style="display:inline-block;">'
													+ '<div style="margin-left:10px" class="'
													+ file.id
													+ ' item">'
													+ '<span style="margin-left:10px">'
													+ "<img src='/neshop2/img/"
													+ file.id
													+ ".htm?mw=120&mh=120'/>"
													+ '</span>'
													+ '<p style="margin-left:10px" class="state"><a style="color:red;text-decoration:none;" href="javascript:del_file_ids(\''
													+ file.id
													+ '\')">取消上传</a></p>'
													+ '</div>' + '</div>');

									getFileids();

								}

							} else {
								alert("只能添加五张照片");
							}
						}

					});
	$list1.append('')

	uploader1.on('uploadError', function(file) {
		$('.' + file.id).find('p.state').text('上传出错');
	});

	uploader1.on('uploadComplete', function(file) {
		$('.' + file.id).find('.progress').fadeOut();
	});

	$btn1.on('click', function() {
		if (state === 'uploading') {
			uploader1.stop();
		} else {
			uploader1.upload();
		}
	});
}

var file_ids = "";
function getFileids() {
	var succ = Ext.getCmp('file_ids').getValue();
	var img;
	for (var i = 0; i < files.length; i++) {
		img = files[0].id;
		var file = files[i];
		if (i == 0) {
			file_ids = file.id;
		} else {
			file_ids += "," + file.id
		}

	}
	if (succ != "") {
		$("input[name='file_ids']").val(succ + "," + file.id);
	} else {
		$("input[name='file_ids']").val(file.id);
	}
	$("input[name='main_file_id']").val(img);
}

function del_file_ids(id) {
	var count = Ext.getCmp('file_ids').getValue();
	var abc = count.split(",");
	for (var i = 0; i < abc.length; i++) {
		var file = abc[i];
		if (file == id) {
			if (abc[0] == id) {
				if (count.substr(22, 1) == "," || count.substr(21, 1) == ",") {
					var cn = count.replace(id + ",", "");
					$("." + id).hide();
				} else {
					var cn = count.replace(id, "");
					$("." + id).hide();
				}
			} else {
				var cn = count.replace("," + id, "");
				$("." + id).hide();
			}
			Ext.getCmp('file_ids').setValue(cn);
			abc.splice(i, 1);
			var main_idsss = Ext.getCmp('img').getValue();
			if (abc[0] != id && main_idsss == "") {
				if ($("." + abc[0]).children("h4").text().substr(-3, 3) == '主图片') {
					$("." + abc[0]).append('');
				} else {
					$("." + abc[0]).append(
							'<h4 style="margin-left:10px" class="info">'
									+ '主图片' + '</h4>');
				}
			}
		}
	}
}