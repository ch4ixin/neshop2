onemenuStore = getStore('t_product_cate'); // 一级菜单
twomenuStore = getStore('t_product_cate'); // 二级菜单
threemenuStore = getStore('t_product_cate');// 三级菜单

var productStore = Ext.create('Ext.data.Store', {
	fields : [ 'name', 'kid' ],
	autoLoad : true,
	remoteSort : true,
	sorters : {
		property : 'name',
		direction : 'ASC'
	},
	proxy : {
		url : getServerHttp() + "/cp/product/info/brand.htm",
		type : "ajax",
		reader : {
			type : 'json',
			root : 'list'
		}
	}
});

var cateStore = Ext.create('Ext.data.Store', {
	remoteSort : true,
	autoLoad : true,
	pageSize : 50,
	sorters : {
		property : 'code',
		direction : 'asc'
	},
	proxy : {
		type : "ajax",
		url : getServerHttp() + "/cp/product/cate/list_data.htm",
		reader : {
			type : 'json',
			root : 'list',
			totalProperty : 'page.totalRow'
		}
	}
});

var edit_form_panel = Ext.create("Ext.form.Panel",{
					url : getServerHttp() + "/cp/onoffer/edit.htm",
					buttonAlign : "center",
					bodyStyle : "padding: 10px;",
					defaultType : "textfield",
					frame : true,
					layout : 'form',
					//height : 730,
					//width : 1000,
					defaults : {
						labelWidth : 200
					},
					items : [{
								xtype : "panel",
								html : '<div id="logo_img1"></div>'
										+ '<div id="uploader1" class="wu-example">'
										+ '<div id="thelist1" class="uploader-list" style="width:800px;autoScroll:true">'
										+ '</div>' + '<div class="btns">'
										+ '<span id="picker1">' + '上传图片'
										+ '</span>' + '</div>' + '</div>',
								width : 800,
								height : 260,
								border : false,
								style : {
									marginLeft : '100px'
								}
							},{
								fieldLabel : "商品分类",
								width : 300,
								name : "cate_code",
								allowBlank : false,
								xtype : "combobox",
								emptyText : '请选择分类',
								store : cateStore,
								displayField : 'name',
								valueField : 'kid',
								editable : false,
								style : {
									display : 'table',
									marginTop : '5px'
								}
							},
							{
								fieldLabel : "商品名称",
								name : "name",
								allowBlank : false,
								width : 300,
								style : {
									display : 'table',
									marginTop : '5px'
								}
							},
							{
								name : "kid",
								allowBlank : false,
								hidden : true,
								width : 300,
								style : {
									display : 'table',
									marginTop : '5px'
								}
							},
							{
								fieldLabel : "运费",
								xtype : 'numberfield',
								width : 300,
//								value : '0',
								minValue : 0,
								allowDecimals : true, // 允许小数点
								allowNegative : false,
								name : 'carriage',
								style : {
									display : 'table',
									marginTop : '5px'
								}
							},
							{
								fieldLabel : "续件运费",
								xtype : 'numberfield',
								width : 300,
//								value : '0',
								minValue : 0,
								allowDecimals : true, // 允许小数点
								allowNegative : false,
								name : 'more_carriage',
								style : {
									display : 'table',
									marginTop : '5px'
								}
							},
							{
								fieldLabel : "主图片",
								xtype : 'textfield',
								id : 'img',
								name : 'main_file_id',
								width : 300,
								style : {
									display : 'table'
								},
								hidden : true
							},
							{
								fieldLabel : "附图",
								xtype : 'textfield',
								id : 'file_ids',
								name : 'file_ids',
								width : 300,
								style : {
									display : 'table'
								},
								hidden : true
							}, {
								fieldLabel : "商品价格",
								xtype : 'numberfield',
								width : 300,
								name : 'price',
//								value : '0',
								minValue : 0,
								allowDecimals : true, // 允许小数点
								allowNegative : false,
								style : {
									display : 'table',
									marginTop : '5px'
								}
							}, {
								fieldLabel : "市场价格",
								xtype : 'numberfield',
								width : 300,
								name : 'market_price',
//								value : '0',
								minValue : 0,
								allowDecimals : true, // 允许小数点
								allowNegative : false,
								style : {
									display : 'table',
									marginTop : '5px'
								}
							},
							{
								fieldLabel : "规格(单位)",
								name : "unit",
								allowBlank : false,
								width : 300,
								style : {
									display : 'table',
									marginTop : '5px'
								}
							},
							{
								fieldLabel : "排列位置",
								name : "rank_code",
								allowBlank : false,
								width : 300,
								style : {
									display : 'table',
									marginTop : '5px'
								}
							}, {
								xtype : "radiogroup",
								fieldLabel : "是否下架",
								value : 1,
								width : 300,
								style : {
									display : 'table',
									marginTop : '10px'
								},
								items : [ {
									boxLabel : "上架",
									name : "status_sys",
									checked : true,
									width : 100,
									inputValue : "上架中"
								}, {
									boxLabel : "下架",
									name : "status_sys",
									width : 100,
									inputValue : "已下架"
								} ]

							}],
					buttons : [ {
						text : "保存",
						formBind : true, // only enabled once the form is
						disabled : true,
						handler : function() {
							var my_file_id = Ext.getCmp('img').getValue();
							if (my_file_id == "") {
								alert("主图片不能为空,请上传图片!");
								return;
							}
							var form = this.up("form").getForm();
							if (form.isValid()) {
								form.submit({
									waitMsg : "保存中...",
									success : function(form, action) {
										Ext.Msg.alert("提示",
												action.result.tip.msg);
										edit_form_panel_win.close();
										dataStore.load();
									},
									failure : function(form, action) {
										Ext.Msg.alert("提示", "操作失败");
									}
								});
							}
						}
					} ]
				});

var edit_form_panel_win = Ext.create("Ext.Window", {
	title : "商品信息",
	closeAction : "hide",
	items : edit_form_panel,
	maximizable : true, // 设置是否可以最大化
	draggable : false,
	//height : 800, // 高度
	//width : 1000, // 宽度
	layout : "fit", // 窗口布局类型
	modal : true, // 是否模态窗口，默认为false
	resizable : false
});

function mySoldOut(kid) {

	Ext.Ajax
			.request({
				url : getServerHttp() + "/cp/onoffer/edit_form.htm?kid=" + kid,
				success : function(response) {
					var json = Ext.decode(response.responseText);
					edit_form_panel.getForm().reset();
					edit_form_panel.getForm().setValues(json.product);
					$("#thelist1").html(' ');
					$("#picker1").html('上传图片');
					edit_form_panel_win.show();
					initUploader();

					Ext.getCmp('img').setValue(json.product.main_file_id);
					if (json.product.main_file_id == "") {
						$list1.append('');
					} else {
						$list1.append('<div style="display:inline-block;">'
										+ '<div class="'
										+ json.product.main_file_id
										+ ' item">'
										+ '<span>'
										+ "<img src='/neshop2/img/"
										+ json.product.main_file_id
										+ ".htm?mw=120&mh=120'/>"
										+ '</span>'
										+ '<p style="margin-left:10px" class="state"><a style="color:red;text-decoration:none;" href="javascript:del_file_id(\''
										+ json.product.main_file_id
										+ '\')">删除</a></p>'
										+ '<h4 style="margin-left:10px" class="info">'
										+ '主图片' + '</h4>' + '</div>' + '</div>');
					}
					if (json.product.file_ids == "") {
						$list1.append();
					} else {
						var files = json.product.file_ids.split(",");
						for (var i = 0; i < files.length; i++) {
							var file = files[i];
							$list1.append('<div style="display:inline-block;">'
											+ '<div style="margin-left:10px" class="'
											+ file
											+ ' item">'
											+ '<span style="margin-left:10px">'
											+ "<img src='/neshop2/img/"
											+ file
											+ ".htm?mw=120&mh=120'/>"
											+ '</span>'
											+ '<p style="margin-left:10px" class="state"><a style="color:red;text-decoration:none;" href="javascript:del_file_idss(\''
											+ file
											+ '\')">删除</a></p>'
											+ '</div>' + '</div>');
						}
					}
				},
				failure : function(response) {
					Ext.Msg.alert("提示", "操作失败!");
				}
			});

} // #myDel

function del_file_id(id) {
	var main_ids = Ext.getCmp('img').getValue();
	var ids = main_ids.replace(id, "");
	$("." + id).hide();
	Ext.getCmp('img').setValue(ids);
	if (ids == "") {
		var count_id = Ext.getCmp('file_ids').getValue();
		var abcd = count_id.split(",");
		$("." + abcd[0]).append(
				'<h4 style="margin-left:10px" class="info">' + '主图片' + '</h4>');
	}
}

function del_file_idss(id) {
	var count = Ext.getCmp('file_ids').getValue();
	var file_id = count.split(",");

	for (var i = 0; i < file_id.length; i++) {
		var one_file_id = file_id[i]
		if (one_file_id == id) {
			if (file_id[0] == id) {
				if (count.substr(22, 1) == "," || count.substr(21, 1) == ",") {
					var cn = count.replace(id + ",", "");
					$("." + id).hide();
				} else {
					var cn = count.replace(id, "");
					$("." + id).hide();
				}
			} else {
				var cn = count.replace("," + id, "");
				$("." + id).hide();
			}

		}
	}

	Ext.getCmp('file_ids').setValue(cn);
	file_id.splice(i, 1);
	var main_idss = Ext.getCmp('img').getValue();
	var count_one_id = Ext.getCmp('file_ids').getValue();
	var abcde = count_one_id.split(",");
	if (abcde[0] != id && main_idss == "") {
		if ($("." + abcde[0]).children("h4").text().substr(-3, 3) == '主图片') {
			$("." + abcde[0]).append('');
		} else {
			$("." + abcde[0]).append(
					'<h4 style="margin-left:10px" class="info">' + '主图片'
							+ '</h4>');
		}
	}
}