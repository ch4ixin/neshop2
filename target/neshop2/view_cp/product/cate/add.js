var add_form_panel = Ext.create("Ext.form.Panel", {
	url : getServerHttp()+"/cp/product/cate/add.htm",
	buttonAlign : "center",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	items : [ {
		fieldLabel : "编号",
		name : "code",
		allowBlank : false
	}, {
		xtype : 'textfield',
		fieldLabel : "分类名称",
		name : "name"
	}/*,{
		xtype : "panel",
		html : '<a href="">生成二维码</a>',
		width : '100%',
		border : false,
		style : {
			marginBottom : '10px',
			paddingLeft : '130px',
		}
	},{
		xtype : "panel",
		html : '<div><img id="logo_img" src="../../../resource/img/product.png" width="120" height="120"></div>'
				+ '<div id="uploader" class="wu-example">'
				+ '<div id="thelist" class="uploader-list">'
				+ '</div>'
				+ '<div class="btns">'
				+ '<div id="picker">'
				+ '上传二维码'
				+ '</div>'
				+ '</div>' + '</div>',
		width : '100%',
		height : 170,
		border : false,
		style : {
			marginBottom : '10px',
			paddingLeft : '130px',
		}
	},{
		name :"main_file_id",
		hidden :true
	}*/],
	buttons : [ {
		text : "保存",
		formBind : true, // only enabled once the form is valid
		disabled : true,
		handler : function() {
			var form = this.up("form").getForm();
			if (form.isValid()) {
				form.submit({
					waitMsg : "保存中...",
					success : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
						add_form_panel_win.close();
						dataStore.load();
					},
					failure : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
					}
				});
			}
		}
	} ]
});

var add_form_panel_win = Ext.create("Ext.Window", {
	title : "商品分类添加",
	closeAction : "hide",
	items : add_form_panel
});

var isInit = false;
function myAdd() {
	add_form_panel.getForm().reset();
	add_form_panel_win.show();
	if(isInit==false){
		initUploader();
		isInit = true;
	}
}