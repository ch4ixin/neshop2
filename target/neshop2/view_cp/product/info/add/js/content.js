var ue_context;
add_content_panel = Ext
		.create(
				"Ext.form.Panel",
				{
					url : getServerHttp() + '/cp/product/info/add.htm',
					title : '商品描述',
					frame : true,
					collapsible : true,
					buttonAlign : "center",
					style : {
						marginBottom : '5px'
					},
					bodyStyle : "padding : 10px;",
					items : [{
								fieldLabel : "商品名称",
								labelWidth : 130,
								xtype : "textfield",
								name : 'name',
								hidden : true
							},{
								xtype : 'textarea',
								id : 'memo',
								width : 500,
								height : 300,
								name : 'content',
								fieldLabel : '商品描述',
								hidden : true
							},{
								fieldLabel : "商品价格",
								xtype : 'numberfield',
								labelWidth : 130,
								name : 'price',
								value : '',
								minValue : 0,
								hidden : true
							},{
								fieldLabel : "商品分类",
								xtype : 'textfield',
								labelWidth : 130,
								name : 'cate_code',
								hidden : true
							}, {
								fieldLabel : "运费",
								labelWidth : 130,
								xtype : 'numberfield',
								name : 'carriage',
								minValue : 0,
								value : '',
								allowNegative : false,
								hidden : true
							}, 
							{
								fieldLabel : "续件运费",
								labelWidth : 130,
								xtype : 'numberfield',
								style : 'margin-left:5px',
								name : 'more_carriage',
								allowDecimals : true, // 允许小数点
								minValue : 0,
								value : '',
								hidden : true
							}, {
								fieldLabel : "市场价格",
								xtype : 'numberfield',
								labelWidth : 130,
								name : 'market_price',
								value : '',
								minValue : 0,
								hidden : true
							}, {
								fieldLabel : "规格(单位)",
								labelWidth : 130,
								xtype : 'textfield',
								name : 'unit',
								hidden : true
							}, {
								fieldLabel : "排位",
								labelWidth : 130,
								xtype : 'textfield',
								name : 'rank_code',
								hidden : true
							}, {
								fieldLabel : "图片信息",
								labelWidth : 130,
								xtype : "textfield",
								name : 'files_ids',
								value : '',
								hidden : true
							},, {
								xtype : "radiogroup",
								fieldLabel : "是否上架",
								width : 300,
								hidden : true,
								items : [ {
									name : "status_sys",
									boxLabel : "上架中",
									inputValue : "上架中",
									checked : true
								}, {
									name : "status_sys",
									boxLabel : "已下架",
									inputValue : "已下架"
								} ]
							},{
								xtype : "panel",
								html : '<iframe src= "edit.html?V=1" width="100%" height="100%" marginwidth="0" framespacing="0" marginheight="0" frameborder="0" ></iframe>',
								width : '100%',
								height : 800,
								border : false,
								style : {
									marginBottom : '10px',
									marginLeft : '10px',
								}
							}],
					buttons : [
							{
								text : "上一步",
								handler : function() {
									tabs.setActiveTab(add_form_panel);
								}
							},
							{
								text : "保存",
								formBind : true, // only enabled once the
								// form is valid
								disabled : true,
								handler : function() {
									// console.log(UE.getEditor("editor"))

									var form = this.up("form").getForm();
									var list = add_form_panel.getForm();
									var content = add_content_panel.getForm()
											.getValues();
									var list1 = add_form_panel.getForm()
											.getValues();
									
									form.findField("price").setValue(
											list1["price"]);
									form.findField("name").setValue(
											list1["name"]);
									form.findField("unit").setValue(
											list1["unit"]);
									form.findField("status_sys").setValue(
											list1["status_sys"]);
									form.findField("market_price").setValue(
											list1["market_price"]);
									form.findField("carriage").setValue(
											list1["carriage"]);
									form.findField("more_carriage").setValue(
											list1["more_carriage"]);
									form.findField("rank_code").setValue(
											list1["rank_code"]);
									form.findField("cate_code").setValue(
											list1["cate_code"]);
									
									var file_arr = [];
									for(var key in file_ids){
										file_arr.push(file_ids[key]);
									}
									
									//验证上传图片是否为空
									if(file_arr.length==0){
										alert("商品图片不能为空,请上传图片!");
										return;
									}
									
									//将上传的图片返回的kid转换成字符串(eg:91Fg6b8m1BJJzeCne6pp3W,KwtKrFMr2Fz4P1XDmjC1s9,GZEsqGQMgVCkw2bYAfJw4C)
									form.findField("files_ids").setValue(file_arr.join(","));

									is_submiting = true;
									var content = ue_context;
									form.findField("content").setValue(content);
									if (form.isValid() && list.isValid()) {
										form.submit({
											waitMsg : "保存中...",
											success : function(form, action) {
												Ext.Msg.alert("提示",action.result.tip.msg);
												uploader1.reset();
												window.location.reload();
											},
											failure : function(form, action) {
												Ext.Msg.alert("提示","保存失败！");
											}
										});
									}

								}
							} ]
				});
