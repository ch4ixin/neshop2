var edit_form_panel;
var edit_form_panel_win;
var listGridPanel;
var detailStore;

detailStore = Ext.create('Ext.data.Store', {
	remoteSort : true,
	autoLoad : true,
	sorters : {
		property : 'created',
		direction : 'DESC'
	},
	proxy : {
		type : "ajax",
		url : getServerHttp() + "/cp/order/list_detail.htm",
		reader : {
			type : 'json',
			root : 'list'
		}
	},
	listeners : {
		'beforeload' : function(store, op, options) {
			var params = {
				order_id : rec.get('kid')
			};
			Ext.apply(detailStore.proxy.extraParams, params);
		}
	}
}); // #detailStore

Ext.onReady(function() {
	edit_form_panel = Ext.create("Ext.form.Panel", {
		id : "formPanel",
		buttonAlign : "center",
		width : 800,
		frame : true,
		fieldDefaults : {
			labelAlign : 'reght',
			labelWidth : 70
		},
		items : [
				{
					xtype : 'container',
					layout : 'column',
					items : [
							{
								height : 245,
								columnWidth : .5,
								xtype : 'fieldset',
								checkbosToggle : true,
								title : '订单信息',
								autoHeight : true,
								defaults : {
									width : 360
								},
								defaultsType : 'textfield',
								items : [
										{
											xtype : 'displayfield',
											fieldLabel : "用户名",
											name : "uname"
										},
										{
											xtype : 'displayfield',
											fieldLabel : "订单编号",
											name : "order_no"
										},
										{
											xtype : 'displayfield',
											fieldLabel : "创建时间",
											name : "created",
											renderer : function(val) {
												if (val != '') {
													return Ext.Date.format(
															new Date(val),
															"Y-m-d H:i:s");
												}
											}
										},{
											xtype : 'displayfield',
											fieldLabel : "付款时间",
											name : "pay_time",
											renderer : function(val) {
												if (val != '') {
													return Ext.Date.format(
															new Date(val),
															"Y-m-d H:i:s");
												}
											}
										}, {
											xtype : 'displayfield',
											fieldLabel : "订单总额",
											name : "total_price"
										}, {
											xtype : 'displayfield',
											fieldLabel : "付款总额",
											name : "end_price"
										}, {
											xtype : 'displayfield',
											fieldLabel : "支付方式",
											name : "pay_way"
										}]
							}, {
								xtype : 'container',
								columnWidth : .5,
								layout : 'absolute',
								items : [ {
									height : 245,
									xtype : 'fieldset',
									checkbosToggle : true,
									title : '收货和物流信息',
									autoHeight : true,
									defaults : {
										width : 360
									},
									defaultsType : 'textfield',
									items : [{
										xtype : 'displayfield',
										fieldLabel : "姓名",
										name : "name",
										value : "",
									}, {
										xtype : 'displayfield',
										fieldLabel : "电话",
										name : "phone",
										value : "",
									}, {
										xtype : 'displayfield',
										fieldLabel : "收货地址",
										name : "address",
										value : "",
									}, {
										xtype : "displayfield",
										fieldLabel : "物流费用",
										name : "money_pay"
									}, {
										xtype : 'displayfield',
										fieldLabel : "配送时间",
										value : "不限送货时间"
									}, {
										xtype : 'displayfield',
										fieldLabel : "运送方式",
										name : "send_way"
									} ]
								} ]
							} ]
				}, {
					title : '商品信息'
				} ]
	});// #edit_form_panel

	listGridPanel = Ext.create('Ext.grid.Panel', {
		title : '订单详情',
		tbar : edit_form_panel,
		store : detailStore,
		dockedItems : [ {
			xtype : 'pagingtoolbar',
			store : dataStore, // same store GridPanel is using
			dock : 'bottom',
			displayInfo : true,
			hidden : true
		} ],
		columns : [{
			text : '商品名称',
			dataIndex : 'pname',
			flex : 1
		}, {
			text : '状态',
			dataIndex : 'status_order',
			flex : 1
		}, {
			text : '单价',
			dataIndex : 'price',
			flex : 1
		}, {
			text : '数量',
			dataIndex : 'cnt',
			flex : 1
		}, {
			text : '总价',
			dataIndex : 'price_total',
			flex : 1
		} ]
	}); // #listGridPanel

	edit_form_panel_win = Ext.create("Ext.Window", {
		buttonAlign : "center",
		closeAction : "hide",
		width : 800, // 宽度
		layout : "fit", // 窗口布局类型
		maximizable : true, // 设置是否可以最大化
		items : [ listGridPanel ],
		buttons : [ {
			text : "确定",
			formBind : false, // only enabled once the form is valid
			handler : function() {
				edit_form_panel_win.close();
			}
		} ]
	});// #edit_form_panel_win

});

function mylimit(kid) {
	Ext.Ajax.request({
		url : getServerHttp() + "/cp/order/edit_form.htm?kid=" + kid,
		success : function(response) {
			var json = Ext.JSON.decode(response.responseText);
			console.log(json);
			Ext.getCmp('formPanel').getForm().setValues(json.order);
			Ext.getCmp('formPanel').getForm().findField('uname').setValue(json.member.user_nick);
			// Ext.getCmp('formPanel').getForm().findField('logistics_name').setValue(json.logistics.name);
			// Ext.getCmp('formPanel').getForm().findField('logistics_code').setValue(json.orderLogistics.logistics_code);
			// Ext.getCmp('formPanel').getForm().findField('deliver_time').setValue(json.orderLogistics.created);
			edit_form_panel_win.show();
		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});
}// #mylimit

