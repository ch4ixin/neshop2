var dataStore;
var edit_order_panel;
var edit_order_panel_win;
var find_form_panel;
var find_form_panel_win;
var fa_searchFormPanel_win;


// 设置dataStore列表数据源参数
fa_dataStore = Ext.create('Ext.data.Store', {
	remoteSort : true,
	autoLoad : true,
	pageSize : 20,
	sorters : {
		property : 'created',
		direction : 'DESC'
	},
	proxy : {
		type : "ajax",
		url : getServerHttp() + "/cp/order/deliver/list_order.htm",
		reader : {
			type : 'json',
			root : 'list',
			totalProperty : 'page.totalRow'
		}
	},
	listeners : {
		'beforeload' : function(store, op, options) {
			var params = fa_searchFormPanel.getForm().getValues();
			Ext.apply(fa_dataStore.proxy.extraParams, params);
		}
	}
}); // #dataStore

var fa_searchFormPanel = Ext.create('Ext.form.Panel', {
	style : {
		marginBottom : '5px'
	},
	bodyStyle : {
		padding : '10px'
	},
	buttonAlign : "center",
	defaultType : "textfield",
	items : [ {
		xtype : "container",
		layout : "hbox",
		items : [ {
			xtype : "textfield",
			fieldLabel : "订单号",
			name : "srh.order_no"
		}, ]
	} ],
	buttons : [ {
		text : "搜索",
		icon : jcapp.getIcon("magnifier.png"),
		handler : function() {
			fa_dataStore.load({
			//params:params()
			});
			fa_dataStore.sync();
		}
	}, {
		text : "重置",
		icon : jcapp.getIcon("arrow_refresh.png"),
		handler : function() {
			this.up('form').getForm().reset();
			fa_dataStore.load({});
			fa_dataStore.sync();
		}
	} ]
}); //#fa_searchFormPanel

fa_searchFormPanel_win = Ext.create("Ext.Window", {
	title : "搜索条件",
	closeAction : "hide",
	items : fa_searchFormPanel
});

midwayListPanel = Ext.create('Ext.grid.Panel', {
	title : '发货中',
	tbar : [{
		text : '更新',
		xtype : 'button',
		icon : jcapp.getIcon("add.png"),
		store : fa_dataStore,
		handler : function() {
			refresh();
		}
	},{
		text : '搜索',
		xtype : 'button',
		icon : jcapp.getIcon("magnifier.png"),
		store : fa_dataStore,
		handler : function() {
			fa_searchFormPanel_win.show();
		}
	}],
	dockedItems : [ {
		xtype : 'pagingtoolbar',
		store : fa_dataStore, // same store GridPanel is using
		dock : 'bottom',
		displayInfo : true
	} ],
	store : fa_dataStore,
    viewConfig:{  
        enableTextSelection:true  
    },
	columns : [ {
		text : '订单号',
		dataIndex : 'order_id',
		flex : 1
	}, {
		text : '商品名称',
		dataIndex : 'pname',
		flex : 1
	}, {
		text : '数量',
		dataIndex : 'cnt',
		flex : 0.5
	}, {
		text : '规格(单位)',
		dataIndex : 'unit',
		flex : 0.5
	}, {
		text : '下单用户昵称',
		dataIndex : 'mname',
		flex : 1
	}, {
		text : '收件人',
		dataIndex : 'rname',
		flex : 1
	}, {
		text : '电话',
		dataIndex : 'rphone',
		flex : 1
	}, {
		text : '地址',
		dataIndex : 'address',
		flex : 1
	}, {
		text : '物流公司',
		dataIndex : 'name',
		flex : 1
	}, {
		text : '运单编号',
		dataIndex : 'logistics_code',
		flex : 1
	}, {
		text : '发货时间',
		dataIndex : 'created',
		flex : 1,
		renderer : function(val) {
			if (val != '') {
				return Ext.Date.format(new Date(val), "Y-m-d H:i:s");
			}
		}
	}, {
		text : '订单状态',
		dataIndex : 'status_logistics',
		flex : 1
	}, {
		text : '操作者',
		dataIndex : 'shop_admin_id',
		flex : 1,
		hidden : true
	}, {
		xtype : "actioncolumn",
		align : "center",
		text : '操作',
		items : [ {
			xtype : 'button',
			tooltip : '查看',
			icon : jcapp.getIcon("image_magnify.png"), // application_form_edit
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				myFind(rec.get('kid'));
			}
		}, {
			xtype : "container"
		}, {
			xtype : 'button',
			tooltip : '修改',
			icon : jcapp.getIcon("edit.png"), // application_form_edit
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				myEdit_order(rec.get('kid'),rec.get('kidd'));
			}
		} ]

	} ]
});

find_form_panel = Ext.create("Ext.form.Panel", {
	buttonAlign : "cnter",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	items : [ {
		xtype : 'displayfield',
		fieldLabel : "发货时间",
		name : "created",
		readOnly : true,
		renderer : function(val) {
			if (val != '') {
				return Ext.Date.format(new Date(val), "Y-m-d H:i:s");
			}
		},
	}, {
		xtype : 'textarea',
		fieldLabel : "物流跟踪",
		width : 750,
		height : 350,
		name : "explain_traces",
		readOnly : true
	}, {
		xtype : 'displayfield',
		fieldLabel : "快递状态",
		name : "state",
		readOnly : true,
		hidden : false
	} ]
});

find_form_panel_win = Ext.create("Ext.Window", {
	title : "物流信息",
	closeAction : "hide",
	items : find_form_panel
});

function myFind(kid) {
	Ext.Ajax.request({
		url :  getServerHttp() +"/cp/order/deliver/add_form.htm?kid="
				+ kid,
		success : function(response) {
			var json = Ext.JSON.decode(response.responseText);
			find_form_panel.getForm().reset();
			find_form_panel.getForm().setValues(json);
			find_form_panel_win.show();
		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});
}

function refresh() {
	var store = midwayListPanel.getStore();
	var count = store.getCount();
	for (var i = 0; i < count; i++) {
		var record = store.getAt(i); // 取record里面的字段
		var kid = record.data.kid;
		var kidd = record.data.kidd;
		console.log("kidd:"+kidd);
		
		Ext.Ajax.request({
			url :  getServerHttp() + "/cp/order/deliver/edit.htm?kid=" + kid +"&kidd="+kidd,
			success : function(response) {
				var json = Ext.JSON.decode(response.responseText);
				//location = "javascript:location.reload()";
			},
			failure : function(response) {
				Ext.Msg.alert("提示", "更新数据失败!");
			}
		});
		//alert(i);
		if(i == count-1){
			Ext.Msg.alert("提示", "更新数据完成！");
			fa_dataStore.load();
		}
	}

}

edit_order_panel = Ext.create("Ext.form.Panel", {
	url :  getServerHttp() + "/cp/order/deliver/edit_order.htm",
	buttonAlign : "center",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	width : 400,
	items : [ {
		fieldLabel : "订单号",
		name : "order_id",
		readOnly : true
	}, {
		xtype : 'combo',
		editable : false,
		minChars : 1,
		fieldLabel : "物流公司",
		name : "logistics_com_code",
		emptyText : '请选择',
		store : orderStore,
		displayField : 'name',
		valueField : 'code',
		hiddenName : 'logistics_com_code',
		multiSelect : false,
		mode : 'remote',
		queryParam : 'logistics_codes',
		triggerAction : 'all',
		selectOnFocus : true,
		forceSelection : true,
		allowBlank : false
	}, {
		fieldLabel : "运单编号",
		name : "logistics_code",
		allowBlank : false
	}, {
		fieldLabel : "kid",
		name : "kid",
		allowBlank : false,
		hidden : true

	}, {
        fieldLabel : "kidd",
        name : "kidd",
        hidden : true
    } ],
	buttons : [ {
		text : "修改",
		formBind : true, // only enabled once the form is valid
		disabled : true,
		handler : function() {
			var form = this.up("form").getForm();
			if (form.isValid()) {
				form.submit({
					waitMsg : "操作中...",
					success : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
						edit_order_panel_win.close();
						fa_dataStore.load();
						//location = "javascript:location.reload()"; // 窗口关闭后刷新当前页面
					},
					failure : function() {
						Ext.Msg.alert("提示", "修改物流信息失败!");
					}
				});
			}

		}
	} ]
});

edit_order_panel_win = Ext.create("Ext.Window", {
	title : "修改发货信息",
	closeAction : "hide",
	items : edit_order_panel
});

function myEdit_order(kid,kidd) {
	Ext.Ajax.request({
		url :  getServerHttp() + "/cp/order/deliver/edit_form.htm?kid="
				+ kid,
		success : function(response) {
			var json = Ext.JSON.decode(response.responseText);
			edit_order_panel.getForm().reset();
			edit_order_panel.getForm().setValues(json);
			edit_order_panel.getForm().findField('kidd').setValue(kidd);
			edit_order_panel_win.show();
		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});

}
