var edit_form_panel1 = Ext.create("Ext.form.Panel", {
	url : getServerHttp()+"/cp/redpacket/sendgroupredpack.htm",
	buttonAlign : "center",
	bodyStyle : "padding: 10px;",
	defaultType : "textfield",
	items : [{
		fieldLabel : "商户名称",
		name : "send_name",
		allowBlank : false,
		value : '悦龙斋',
	},{
		fieldLabel : "证书路径",
		name : "path",
		value : "C:/apiclient_cert.p12",
		allowBlank : false,
		hidden : true
	},{
		xtype : 'numberfield',
		fieldLabel : "红包金额",
		name : "total_amount",
		value : '0',
		minValue : 0,
		allowDecimals : true, // 允许小数点
		allowNegative : false,
		allowBlank : false
	},{
		xtype : 'numberfield',
		fieldLabel : "发放人数",
		name : "total_num",
		value : '0',
		minValue : 0,
		allowBlank : false
	},{
		fieldLabel : "活动名称",
		name : "act_name",
		allowBlank : false
	},{
		fieldLabel : "备注",
		name : "remark",
		value : "备注随便",
		hidden : true
	},{
		fieldLabel : "红包祝福语",
		name : "wishing",
		allowBlank : false
	},{
		fieldLabel : "openid",
		name : "openid",
		hidden : true
	},{
		fieldLabel : "kid",
		name : "kid",
		hidden : true
	}],
	buttons : [ {
		text : "发送",
		formBind : true, // only enabled once the form is valid
		disabled : true,
		handler : function() {
			var form = this.up("form").getForm();
			if (form.isValid()) {
				form.submit({
					waitMsg : "发送中...",
					success : function(form, action) {
						Ext.Msg.alert("提示", action.result.tip.msg);
						edit_form_panel_win1.close();
					},
					failure : function(form, action) {
						Ext.Msg.alert("提示", "发送失败");
					}
				});
			}
		}
	} ]
});

var edit_form_panel_win1 = Ext.create("Ext.Window", {
	title : "发送裂变红包",
	closeAction : "hide",
	items : edit_form_panel1
});

function myEditt(kid) {
	Ext.Ajax.request({
		url : getServerHttp()+"/cp/member/edit_form.htm?kid=" + kid,
		success : function(response) {
			var json = Ext.JSON.decode(response.responseText);
			edit_form_panel1.getForm().reset();
			edit_form_panel1.getForm().setValues(json);
			edit_form_panel_win1.show();
		},
		failure : function(response) {
			Ext.Msg.alert("提示", "操作失败!");
		}
	});
}// #myEdit
