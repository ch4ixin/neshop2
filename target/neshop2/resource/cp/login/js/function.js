//2016-08-08
//3.获取元素的兼容函数
//"#"id "."类名 "a"   
 //selecter表示为选择器,当它是字符串时,获取元素；当它为函数时，实现页面加载完成
function $(selecter,father){
   if(typeof selecter=="string"){
        father=father||document;
        selecter=selecter.replace(/^\s*|\s$/g,"");
        if(selecter.charAt(0)=="."){   //类名
        //slice(1),把.截取
        return  getClass(selecter.slice(1),father); 
        }else if(selecter.charAt(0)=="#"){  //id名
        return document.getElementById(selecter.slice(1));
        }else if(/^[a-z]+\d*$/g.test(selecter)){  //标签名
        return father.getElementsByTagName(selecter);
        }
     }
     else if(typeof selecter=="function"){
      window.onload=function(){
        selecter();
      }
     }

}
