{* TitlePicWidth: 2 *}
{* TitlePicHeight: 38 *}

{* 说明：$title_pic，分类标题图片地址； *}
{* 说明：$base_style，基本风格样式颜色； *}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="{$keywords}" />
<meta name="Description" content="{$description}" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<!-- TemplateBeginEditable name="doctitle" -->
<title>{$topic.title}_{$page_title}</title>
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
<link rel="shortcut icon" href="favicon.ico" />
<link rel="icon" href="animated_favicon.gif" type="image/gif" />
{if $topic.css neq ''}
<style type="text/css">
{
$topic.css
}
</style>
{/if}
{* 包含脚本文件 *}
{insert_scripts files='common.js'}

<!--
<link href="images/dingzhi/base.css" rel="stylesheet" type="text/css">
<link href="images/dingzhi/common.css" rel="stylesheet" type="text/css">
<script type="text/javascript" async="" src="images/dingzhi/visittrack.min.js"></script>
<script id="scarab-js-api" src="images/dingzhi/scarab-v2.js"></script>
<script src="images/dingzhi/jquery.min.js" type="text/javascript"></script>
<script src="images/dingzhi/base.js" type="text/javascript"></script>
<script src="images/dingzhi/public.js" type="text/javascript"></script> 
<script src="images/dingzhi/base(1).js" type="text/javascript"></script>
-->

<style>
HTML{FONT-FAMILY:Arial,Helvetica,sans-serif,"宋体"; text-align:left;word-break:break-all;text-overflow:ellipsis; }
body{FONT-SIZE:12px; COLOR:#666;}
body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,button,textarea,p,blockquote,th,td{padding:0; margin:0;}
ul,li{list-style:none;margin:0px;padding:0px}
form{ padding:0px; margin:0px}
td{FONT-SIZE:12px}
img{border:0px}
a{ COLOR:#666;TEXT-DECORATION:none;blr:expression(this.onFocus=this.blur());}
a:hover{color:#cc0001; text-decoration:underline;}
input, select { vertical-align: middle;}
.clear{clear:both; font-size:0; line-height:0; height:0;}
.clearfix:after{clear:both;display:block;content:"...";visibility:hidden;height:0;font-size:0;}
.clearfix{*zoom:1;}


.indexIcon{background:url(images/dingzhi/index_png24.png) no-repeat 0 0;}
.turnWrap{ width:100%;position:relative;height:300px; overflow:hidden;z-index:11}
.turnList{margin:0 auto;position:relative;z-index:0;overflow:hidden;}
.turnList ul{width:100% !important;}
.turnList li{ width:100% !important;height:300px;overflow:hidden;text-align:center;}
.turnList li a{ width:1000px; height:300px; margin:0 auto; display:block;}
.turnBtn{ position:absolute; width:100%; height:6px; text-align:center; left:0; bottom:20px; font-size:0; z-index:102;}
.smallUl{height:6px; margin: 0 auto; position: relative; text-align: center; z-index: 21;}
.turnBtn li{ width:36px; height:6px; display:inline-block; background:#fff; cursor:pointer; border-radius:0; margin:0 6px; text-indent:-999; line-height:0; overflow:hidden;*display:inline; zoom:1;}
.turnBtn .on{ background:#cc3333;}


.mainBody{ width:100%;}
.minAd{ width:100%; padding:35px 0 22px 0; background:#fff;}
.minAdList{ width:1000px; margin:0 auto; overflow:hidden;}
.minAdList ul{ width:1050px;}
.minAdList li{ float:left; display:inline; margin-right:17px; width:322px; height:200px;}
.minAdList li img{ width:322px; height:200px;}
.mainNavWrap{ width:100%; padding:30px 0 85px 0; background:#f7f7f7;}
.mainNavBox{ width:1000px; margin:0 auto;}
.mainNavTitle{ height:30px;}
.mainNavTitle h3{ height:30px; float:left;}
.mainNavTitle .tit1{ background-position:0 0; width:408px;}
.mainNavList{ width:1000px; overflow:hidden; position:relative; margin-top:85px;}
.mainNavList ul{ width:1100px; position:relative; left:-30px;}
.mainNavList li{ float:left; width:320px; height:115px; border-right:1px dotted #cccccc; border-left:1px dotted #fff; padding:0 0 0 30px; font-family:"微软雅黑";}
.mainNavList li i{ display:inline-block; float:left; vertical-align:middle; width:90px; height:90px; margin-top:15px; background:#fff; border-radius:50%; text-align:center;}
.mainNavList li .icon1{ background-position:0 -253px;}
.mainNavList li .icon2{ background-position:-103px -253px;}
.mainNavList li .icon3{ background-position:-208px -253px;}
.mainNavList li .mainNavInfo{ padding-left:34px; float:left; width:196px;}
.mainNavList li .mainNavInfo h3{ color:#666666; font-weight:normal; font-size:20px; height:20px; width:120px; overflow: hidden; line-height:20px; margin-bottom:10px;}
.mainNavList li .mainNavInfo .mainNavSub p{ float:left; width:48px; overflow:hidden; height:28px; line-height:28px; display:inline; margin-right:12px;}
.mainNavList li .mainNavInfo .mainNavSub p a{ color:#999;}
.mainNavList li .mainNavInfo .mainNavSub p a:hover{ color:#cc0001;}
.mainNavList li i img{ width:55px; height:55px; display:inline-block; margin-top:17px;}

.proWrap{ width:100%; background:#fff; padding:0 0 25px 0; font-family:"微软雅黑";}
.proBox{ width:1000px; margin:0 auto;}
.mainNavTitle .tit2{ background-position:0 -47px; width:551px;}
.mainNavTitle .tit3{ background-position:0 -99px; width:551px;}
.mainNavTitle .tit4{ background-position:0 -150px; width:387px;}
.mainNavTitle .tit5{ background-position:0 -201px; width:433px;}
.mainNavTitle a{ float:right; line-height:30px; color:#999999;}
.mainNavTitle a:hover{ color:#cc0001; text-decoration:none;}
.mainNavTitle a span{ font-family:"宋体"; padding-left:5px;}
.p10{ padding:30px 0 15px;}
.proListBox{ width:999px; height:598px; border-top:1px solid #ccc; border-left:1px solid #ccc;}
.proListTurn{ width:398px; height:597px; overflow:hidden; float:left; position:relative; border-bottom:1px solid #ccc; border-right:1px solid #ccc;}
.ListTurnPic{ width:398px; height:597px;}
.ListTurnPic ul{ width:5000px;}
.ListTurnPic li{ float:left; width:398px; height:597px;}
.ListTurnPic li a{ width:398px; height:597px; display:block;}
.ListTurnBtn{ width:100%; position:absolute;  font-size:0; text-align: center; height:12px; left:0; bottom:20px; overflow:hidden; z-index:2;}
.ListTurnBtn span{ display:inline-block; width:12px; height:12px; background:#fff; overflow:hidden; cursor:pointer; border-radius:0; margin:0 6px;}
.ListTurnBtn span b{ display:block; height:12px;}
.ListTurnBtn .on{ background:#cc3333;}
.cur .on{ background:#3a4a8d;}
.proList{ float:right; width:600px; height:596px;}
.proList li{ width:199px; border-bottom:1px solid #ccc; border-right:1px solid #ccc; height:278px; font-family:"微软雅黑"; padding-top:20px; float:left;}
.proList li .proPic{ width:100%; text-align:center; margin-bottom:15px;}
.proList li .proName{ height:20px; line-height:20px; padding:0 15px; font-size:14px; margin-bottom:5px;}
.proList li .proName a{ display:block; height:20px; width:169px; height:20px; color:#333; overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
.proList li .proName a:hover{ color:#cc0001;}
.proList li .proPrice{ padding:0 15px; height:20px; line-height:20px; color:#333;}
.proList li .proPrice strong{ font-weight:normal; font-size:14px; color:#cc0000;}
.proList li .proPrice span{ font-size:18px; color:#cc0000;}
.bannerAd{ width:1000px; height:100px; margin-top:18px;}
.logoList{ width:998px; border-top:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc; overflow:hidden; position:relative;}
.logoList ul{ position:relative; left:-2px; width:1000px; border-left:1px solid #ccc;}
.logoList li{ float:left; width:199px; height:128px; border-right:1px solid #ccc; border-bottom:1px solid #ccc;}
.logoList li a{ width:199px; height:128px; display:block; overflow:hidden;}
.customProcess{ width:100%; background:#f7f7f7; padding-bottom:65px;}
.custom{ width:1000px; margin:0 auto;}
.process{ background:url(images/dingzhi/custom.jpg) no-repeat top center; width:100%; height:195px; margin-top:30px;}

</style>
</head>
<body>
<script type="text/javascript" src="themes/68ecshopcom_360buy/js/base-2011.js"></script>
<div id="site-nav"> <!-- #BeginLibraryItem "/library/page_header.lbi" --><!-- #EndLibraryItem -->

<div class="midHeader" style="display:none">
    <div class="midHead">
        <div class="srdzLogo">
            <a href="http://www.cctea888.com/" target="_blank"></a>
        </div>
        <div class="sLogan">
            <span class="s-n1"></span>
            <span class="s-n2"></span>
            <span class="s-n3"></span>
        </div>
    </div>
</div>
		
<!--
<script type="text/javascript">
$(function(){

 jQuery.ajax({
	   	 type:'get',
	   	 url:'/navJSON.htm',
	   	 dataType:'json',
	   	 cache:false,
	   	 async:false,
	   	 success:function(data){
	   	 	if(data != null && data.list != null && data.list.length > 0){
	   	 		$(".menuList").empty();
	   				for(var i = 0;i<data.list.length;i++){
	   					if(data.list[i].parentTagId == 0){
	   						var ID = data.list[i].ID;
	   						var tag_name = data.list[i].tag_name;
	   						var tag_url = data.list[i].tag_url;
	   						var tag_icon = data.list[i].tag_icon;
	   						var parentTagId = data.list[i].parentTagId;
	   					
	   						var str1 = "<ul> <li class='clearfix'> <h3>";
	   						if(tag_icon){
	   							var str2 = "<i><img src='bill"+tag_icon+"'  width='15px' height='15px'/></i>";
	   						}else{
	   							var str2 = "<i><img src='dingzhi/img/m1.png'  width='15px' height='15px'/></i>";	
	   						}
	   						var str3 ="<span><a href='"+tag_url+"'><b>"+tag_name+"</b></a></span></h3>  <div class='item'> ";
	   						
	   						var str4 = "";
	   						var count= 0 ; //计数
	   						
	   						for(var j = 0;j<data.list.length;j++){
	   							var parentTagIdChild = data.list[j].parentTagId;
										
	   							if(parentTagIdChild == ID){
	   								if(count  != 0 && count % 3 == 0){
										str4=str4+"<p>";
									}
									if(count  == 0 ){
										str4=str4+"<p>";
									}
	   								var IDChild = data.list[j].ID;
	   								var tag_nameChild = data.list[j].tag_name;
	   								count++;
	   								var str4_="<a href='/list/"+IDChild+"-0-0-0-0-0-0.htm'  ><span>"+tag_nameChild+"</span></a> ";
	   								str4=str4+str4_;
	   								if(count  != 0 && count % 3 == 0){
	   									str4=str4+" </p>";
	   								}
	   							}
	   						}
	   						if(count % 3 != 0){
								str4=str4+" </p>";
							}
	   					   var str5 =" 	</div> </li></ul>";
	   						
	   					   var str =str1+str2+str3+str4+str5;
	   					
	   					   $(".menuList").append(str);
	   					}
	   						
	   					}
	   				}
	   				
	   			}
	        	});
		
});		
</script>

<script type="text/javascript" src="images/dingzhi/public_2014.js"></script>
<script type="text/javascript" src="images/dingzhi/superslide.2.1.js"></script>
<script type="text/javascript" src="images/dingzhi/Slider.js"></script>
<script type="text/javascript" src="images/dingzhi/index.js"></script>

<link href="images/dingzhi/index.css" rel="stylesheet" type="text/css">
-->
<!-- 导航栏 -->

<!-- 轮播广告  -->
<div class="turnWrap">
    <div class="turnList" style="position: relative; width: 1423px; height:300px;">
        <ul>
			<li>
                <a href="" target="_blank"><img src="images/dingzhi/banner.jpg" style="position:absolute; left: 0px; top: 0px;"></a>
            </li>
			<!--
            <li style="background-image: url(dingzhi/image/turn01.jpg); background-attachment: scroll; position: absolute; width: 1423px; left: 0px; top: 0px; display: list-item; opacity: 1; background-position: 50% 0%; background-repeat: no-repeat no-repeat;">
				<a href="" target="_blank"></a>
            </li>
            <li style="background-image: url(dingzhi/image/turn02.jpg); background-attachment: scroll; position: absolute; width: 1423px; left: 0px; top: 0px; display: list-item; opacity: 0; background-position: 50% 0%; background-repeat: no-repeat no-repeat;">
                <a href="" target="_blank"></a>
            </li>-->
            
        </ul>
    </div>
    <div class="turnBtn" style="display:none">
        <ul class="smallUl"><li class="">1</li><li class="">2</li><li class="on">3</li></ul>
    </div>
</div>
<div class="mainBody">
    <div class="minAd">
        <div class="minAdList">
            <ul class="clearfix">
                <li>
					<a href="" target="_blank">
                        <img src="images/dingzhi/2-1.jpg" width="322" height="200">
                    </a>
                </li>
                <li>
                    <a href="" target="_blank">
                        <img src="images/dingzhi/2-2.jpg" width="322" height="200">
                    </a>
                </li>
                <li>
                    <a href="" target="_blank">
                        <img src="images/dingzhi/2-3.jpg" width="322" height="200">
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- 订制主题 -->
    <div class="mainNavWrap">
        <div class="mainNavBox">
            <div class="mainNavTitle">
                <h3 class="indexIcon tit1" style="background:url(images/dingzhi/index_png24.png) no-repeat 0 0;"></h3>
            </div>
            <div class="mainNavList">
                <ul>
				<li class="clearfix">
					<i>
        				<img src="images/dingzhi/d36e110dae1f4f04a1842667dd136b76.png">
        			</i>
    				<div class="mainNavInfo"> 
                        <h3><a href="" target="_blank">个人定制</a></h3>
                        <div class="mainNavSub clearfix">
										           <p>
                                                    <a target="_blank" href="">婚宴</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">满月</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">纪念日</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="m">生日</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">感恩</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">节日</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">寿宴</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">收藏</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">聚会</a>
                                                   </p>
																																		                                    </div>
                    </div>
                </li>
                <li class="clearfix">
                    <i>
        				<img src="images/dingzhi/a29a45d4dd604316bb88adea72b9cfad.png">
        			</i>
    				<div class="mainNavInfo"> 
                        <h3><a href="" target="_blank">节日定制</a></h3>
                        <div class="mainNavSub clearfix">
										            <p>
                                                    <a target="_blank" href="">春节</a>
                                                   </p>
													 <p>
                                                    <a target="_blank" href="">七夕节</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">情人节</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">母亲节</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">中秋节</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">教师节</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">父亲节</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">感恩节</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">圣诞节</a>
                                                   </p>
																																		                                    </div>
                    </div>
                </li>
                <li class="clearfix">
                    <i>
        				<img src="images/dingzhi/f4b18ab307364919b5d1b41843bded6f.png">
        			</i>
    				<div class="mainNavInfo"> 
                                    <h3><a href="" target="_blank">企业定制</a></h3>
                                    <div class="mainNavSub clearfix">
										           <p>
                                                    <a target="_blank" href="">庆典</a>
                                                   </p>
												   <p>
                                                    <a target="_blank" href="">宴会</a>
                                                   </p>
													<p>
                                                    <a target="_blank" href="">会议</a>
                                                   </p>
												    <p>
                                                    <a target="_blank" href="">年会</a>
                                                   </p>	
												   <p>
                                                    <a target="_blank" href="">送礼</a>
                                                   </p>
									</div>
                    </div>
                </li>
                </ul>
            </div>
        </div>
    </div>

	
    <!-- 订制版块1 -->
    <div class="proWrap">
        <div class="proBox">
            <div class="container">
                <div class="mainNavTitle p10">
					<!--<h3 class="indexIcon tit3"></h3>-->
                    <h3 class="indexIcon tit3"></h3>
                    <a href="" target="_blank">更多<span>&gt;</span>
                    </a>
                </div>
                <div class="proListBox">
                    <div class="proListTurn">
                        <div class="ListTurnPic">
                            <ul>
                                <li style="background:url(images/dingzhi/3-1.jpg) no-repeat top center scroll;">
                                    <a href="" target="_blank"></a>
                                </li>
                                <!--<li style="background:url(dingzhi/image/3-2.jpg) no-repeat top center scroll;;">
                                    <a href="" target="_blank"></a>
                                </li>-->
                                
                            </ul>
                        </div>
                        <div class="ListTurnBtn">
                            <span class="on">
                                <b></b>
                            </span>
                            <!--<span>
                                <b></b>
                            </span>-->
                            
                        </div>
                    </div>
	<div class="proList">
                        <ul>	
<!--需判断分类产品，限制显示数量-->						
<!-- {foreach from=$sort_goods_arr item=sort key=sort_name name=sort_name} -->

						<!-- {foreach from=$sort item=goods  name=name} -->
                            <li>
                                <div class="proPic">
                                    <a href="{$goods.url}" title="{$goods.goods_style_name|escape:html}" target="_blank">
                                        <img src="{$goods.original_img}" alt="{$goods.goods_style_name|escape:html}" width="190" height="190">
                                    </a>
                                </div>
                                <div class="proName">
                                    <a href="{$goods.url}" target="_blank" title="{$goods.goods_style_name|escape:html}">{$goods.goods_style_name|truncate:16}</a>
                                </div>
                                <div class="proPrice">定制价：<strong>￥</strong>
									<!-- {if $goods.promote_price neq ""} --> 
									<span>{$goods.promote_price}</span>
									<!-- {else}--> 
									<span>{$goods.shop_price}</span>
									<!--{/if}--> 
                                </div>
                            </li>
						<!--{/foreach}-->
							<li>
                                <img src="images/dingzhi/4-11.jpg" alt="敬请期待" width="190" height="190">
										
                                <!-- <div class="proPic">
                                    <a href="" title="荣锦堂普洱茶紫茶熟砖250g" target="_blank">
                                        <img src="dingzhi/image/4-6.jpg" alt="荣锦堂普洱茶紫茶熟砖250g" width="190" height="190" />
                                    </a>
                                </div>
                                <div class="proName">
                                    <a href="" target="_blank" title="荣锦堂普洱茶紫茶熟砖250g">荣锦堂普洱茶紫茶熟砖250g</a>
                                </div>
                                <div class="proPrice">定制价：<strong>￥</strong>
                                    <span>199</span>
                                </div>-->
                            </li>
							<li>
                                <img src="images/dingzhi/4-11.jpg" alt="敬请期待" width="190" height="190">
                            </li>
							<li>
                                <img src="images/dingzhi/4-11.jpg" alt="敬请期待" width="190" height="190">
                            </li>
 <!--{/foreach}--> 					
						</ul>
     </div>					
                 
                </div>
            </div>
            <!-- 订制版块1 与 订制版块2 之间的广告-->
            <div class="bannerAd">
                <!--<a href="" target="_blank">-->
				<a href="" target="_blank">
                    <img src="images/dingzhi/ad-02.jpg" width="1000" height="100">
                </a>
            </div>
            <!-- 订制版块2 -->
            <div class="container" style="display:none">
                <div class="mainNavTitle p10">
                    <h3 class="indexIcon tit3"></h3>
                    <a href="" target="_blank">更多<span>&gt;</span>
                    </a>
                </div>
                <div class="proListBox">
                    <div class="proListTurn">
                        <div class="ListTurnPic">
                            <ul>
								<li style="background:url(dingzhi/image/4-9.jpg) no-repeat top center scroll;;">
                                    <a href="" target="_blank"></a>
                                </li>
                                <li style="background:url(dingzhi/image/4-1.jpg) no-repeat top center scroll;;">
                                    <a href="" target="_blank"></a>
                                </li>
                                <li style="background:url(dingzhi/image/4-2.jpg) no-repeat top center scroll;;">
                                    <a href="goods-15696.html" target="_blank"></a>
                                </li>
                                
                            </ul>
                        </div>
                        <div class="ListTurnBtn cur">
                            <span class="on">
                                <b></b>
                            </span>
                            <span>
                                <b></b>
                            </span>
                            
                        </div>
                    </div>

							
                    <div class="proList">
                        <ul>
                            <li>
                                <div class="proPic">
                                    <a href="" title="荣锦堂普洱茶紫茶熟砖250g" target="_blank">
                                        <img src="images/dingzhi/4-3.jpg" alt="荣锦堂普洱茶紫茶熟砖250g" width="190" height="190">
                                    </a>
                                </div>
                                <div class="proName">
                                    <a href="" target="_blank" title="荣锦堂普洱茶紫茶熟砖250g">荣锦堂普洱茶紫茶熟砖250g</a>
                                </div>
                                <div class="proPrice">定制价：<strong>￥</strong>
                                    <span>69</span>
                                </div>
                            </li>
                            <li>
                                <div class="proPic">
                                    <a href="" title="荣锦堂普洱茶紫茶熟砖250g" target="_blank">
                                        <img src="images/dingzhi/4-3.jpg" alt="荣锦堂普洱茶紫茶熟砖250g" width="190" height="190">
                                    </a>
                                </div>
                                <div class="proName">
                                    <a href="" target="_blank" title="荣锦堂普洱茶紫茶熟砖250g">荣锦堂普洱茶紫茶熟砖250g</a>
                                </div>
                                <div class="proPrice">定制价：<strong>￥</strong>
                                    <span>69</span>
                                </div>
                            </li>
                            <li>
                                <div class="proPic">
                                    <a href="" title="荣锦堂普洱茶紫茶熟砖250g" target="_blank">
                                        <img src="images/dingzhi/4-3.jpg" alt="荣锦堂普洱茶紫茶熟砖250g" width="190" height="190">
                                    </a>
                                </div>
                                <div class="proName">
                                    <a href="" target="_blank" title="荣锦堂普洱茶紫茶熟砖250g">荣锦堂普洱茶紫茶熟砖250g</a>
                                </div>
                                <div class="proPrice">定制价：<strong>￥</strong>
                                    <span>69</span>
                                </div>
                            </li>
                            <li>
                                <img src="images/dingzhi/4-11.jpg" alt="敬请期待" width="190" height="190">
										
                                <!-- <div class="proPic">
                                    <a href="" title="荣锦堂普洱茶紫茶熟砖250g" target="_blank">
                                        <img src="dingzhi/image/4-6.jpg" alt="荣锦堂普洱茶紫茶熟砖250g" width="190" height="190" />
                                    </a>
                                </div>
                                <div class="proName">
                                    <a href="" target="_blank" title="荣锦堂普洱茶紫茶熟砖250g">荣锦堂普洱茶紫茶熟砖250g</a>
                                </div>
                                <div class="proPrice">定制价：<strong>￥</strong>
                                    <span>199</span>
                                </div>-->
                            </li>
                            <li>
                                <img src="images/dingzhi/4-11.jpg" alt="敬请期待" width="190" height="190">
										
                                <!-- <div class="proPic">
                                    <a href="" title="荣锦堂普洱茶紫茶熟砖250g" target="_blank">
                                        <img src="dingzhi/image/4-6.jpg" alt="荣锦堂普洱茶紫茶熟砖250g" width="190" height="190" />
                                    </a>
                                </div>
                                <div class="proName">
                                    <a href="" target="_blank" title="荣锦堂普洱茶紫茶熟砖250g">荣锦堂普洱茶紫茶熟砖250g</a>
                                </div>
                                <div class="proPrice">定制价：<strong>￥</strong>
                                    <span>199</span>
                                </div>-->
                            </li>
                            <li>
                                <img src="images/dingzhi/4-11.jpg" alt="敬请期待" width="190" height="190">
										
                                <!-- <div class="proPic">
                                    <a href="" title="荣锦堂普洱茶紫茶熟砖250g" target="_blank">
                                        <img src="dingzhi/image/4-6.jpg" alt="荣锦堂普洱茶紫茶熟砖250g" width="190" height="190" />
                                    </a>
                                </div>
                                <div class="proName">
                                    <a href="" target="_blank" title="荣锦堂普洱茶紫茶熟砖250g">荣锦堂普洱茶紫茶熟砖250g</a>
                                </div>
                                <div class="proPrice">定制价：<strong>￥</strong>
                                    <span>199</span>
                                </div>-->
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- 品牌云集 版块 -->
            <div class="container">
                <div class="mainNavTitle p10">
                    <h3 class="indexIcon tit4"></h3>
                    <a href="" target="_blank">更多<span>&gt;</span>
                    </a>
                </div>
                <div class="logoList">
                    <ul class="clearfix">
                        <li>
                            <a href="" title="" target="_blank"><img src="images/dingzhi/logo01.jpg" alt="" width="199" height="128"></a>
                        </li>
						<li>
                            <a href="" title="" target="_blank"><img src="images/dingzhi/logo02.jpg" alt="" width="199" height="128"></a>
                        </li>
						<li>
                            <a href="" title="" target="_blank"><img src="images/dingzhi/logo01.jpg" alt="" width="199" height="128"></a>
                        </li>
						<li>
                            <a href="" title="" target="_blank"><img src="images/dingzhi/logo04.jpg" alt="" width="199" height="128"></a>
                        </li>
						<li>
                            <a href="" title="" target="_blank"><img src="images/dingzhi/logo05.jpg" alt="" width="199" height="128"></a>
                        </li>
                    </ul>
                </div>
            </div>
			
		
        </div>
    </div>
    <!-- 订制流程讲解 图片  -->
    <div class="customProcess">
        <div class="custom">
            <div class="mainNavTitle p10">
                <h3 class="indexIcon tit5"></h3>
            </div>
            <div class="process"></div>
        </div>
    </div>
</div>
  <!-- #BeginLibraryItem "/library/page_footer.lbi" --><!-- #EndLibraryItem --> 
</body></html>