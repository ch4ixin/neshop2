<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>订单详情</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.weui-cell__ft {color: #FF6B2F;}
		.weui-media-box {padding: 5px;}
		.demos-header {padding: 50px 0;background-color: #EBEBEB;}
		.icon-box {margin-bottom: 25px;margin-top: 25px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
		.icon-box i {margin: auto;text-align: center;}
		.demos-title span {color: #DBDBDB}
		.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;margin: 0 15%;}
		.weui-navbar__item{font-size: 14px;padding: 10px 0}
		.weui-navbar__item{color: #353535}
		.weui-navbar__item.weui-bar__item--on{color: #ff2333;background-color: #f1f1f1;border-bottom: 2px solid #ff2333;font-weight: 600;}
		.weui-tab__bd .weui-tab__bd-item{background: #F4F4F4}
		.weui-panel__bd .weui-media-box{height:83px;margin: 8px 10px 0px 10px;background: #fff;border-radius: 8px;padding:0 15px 0 0;}
		.weui-navbar+.weui-tab__bd{padding-top: 40px}
		.demos-header{background: #F4F4F4}
		.weui-panel:first-child{padding-top: 8px;margin: 0}
		.weui-navbar:after{display: none}
		.weui-navbar__item:after{display: none}
		.weui-panel{background: none}
		.weui-panel:after, .weui-panel:before{display: none}
		.weui-media-box:before{display: none}
	</style>
  </head>
  <body ontouchstart>
  <div id="order_lists" class="weui-popup__container">
	  <div class="weui-popup__overlay"></div>
	  <div class="weui-popup__modal">
			   <div class="weui-tab">
				  <div class="weui-navbar">
				    <a class="weui-navbar__item weui-bar__item--on cur" href="#tab1">全部 </a>
				    <a class="weui-navbar__item" href="#tab2">待付款</a>
				    <a class="weui-navbar__item" href="#tab3">待发货</a>
				    <a class="weui-navbar__item" href="#tab4">待收货</a>
				    <a class="weui-navbar__item" href="#tab5">已完成</a>
				  </div>
				  <div class="weui-tab__bd">
				      <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
				       <div class="weui-panel weui-panel_access">
					  <div class="weui-panel__bd">
					  <c:if test="${orderList=='[]'}" >
				           <header class='demos-header'>
				              <div class="icon-box">
						   		<i class="weui-icon-info weui-icon_msg" style=""></i>
						      </div>
						       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
							   <a href="${path }/wx/home.htm" style="width: 90%;font-size: 14px;" class="weui-btn weui-btn_primary">返回首页</a>
					    </header>
					  </c:if>
					  <c:if test="${orderList!='[]'}" >
				          <c:forEach items="${orderList }" var="ol">
						    <a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
						      <div class="weui-media-box__hd">
						        <img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
						      </div>
						      <div class="weui-media-box__bd">
							      <c:if test="${ol.status_order=='下单中'}" >
							        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#e64340">等待付款</span></h4>
							      </c:if>
							      <c:if test="${ol.status_order=='已下单'}" >
							        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品<span style="color:#e64340"> 等待发货</span></h4>
							      </c:if>
							      <c:if test="${ol.status_order=='已发货' }" >
							        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#e64340">等待收货</span></h4>
							      </c:if> 
							      <c:if test="${ol.status_order=='已收货'}" >
							        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#e64340">等待收货</span></h4>
							      </c:if> 
							       <c:if test="${ol.status_order=='已完成'}" >
							        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#e64340">已完成</span></h4>
							      </c:if>
							       <c:if test="${ol.status_order=='已退单'}" >
							        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#e64340">已退款</span></h4>
							      </c:if>
							       <c:if test="${ol.status_order=='已取消'}" >
							        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#e64340">已取消</span></h4>
							      </c:if>
						        <p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>	
						      </div>
						      <div class="weui-cell__ft">${ol.total_price }元</div>
						    </a>
					       </c:forEach>
					  </c:if>
					  </div>
					</div>
				    </div>
				    <div id="tab2" class="weui-tab__bd-item">
				      <div class="weui-panel weui-panel_access">
					  <div class="weui-panel__bd">
					  <c:if test="${stayPayOrder=='[]'}" >
				           <header class='demos-header'>
				              <div class="icon-box">
						   		<i class="weui-icon-info weui-icon_msg" style=""></i>
						      </div>
						       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
							   <a href="${path }/wx/home.htm" style="width: 90%;font-size: 14px;" class="weui-btn weui-btn_primary">返回首页</a>
					    </header>
					  </c:if>
					  <c:if test="${stayPayOrder!='[]'}" >
				          <c:forEach items="${stayPayOrder }" var="ol">
						    <a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
						      <div class="weui-media-box__hd">
						        <img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
						      </div>
						      <div class="weui-media-box__bd">
						        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#e64340">等待付款</span></h4>
						        <p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>	
						      </div>
						      <div class="weui-cell__ft">${ol.end_price }元</div>
						    </a>
					       </c:forEach>
					  </c:if>
					  </div>
					</div>
				    </div>
				    <div id="tab3" class="weui-tab__bd-item">
				            <div class="weui-panel weui-panel_access">
					  <div class="weui-panel__bd">
					  <c:if test="${staySendOrder=='[]'}">
				           <header class='demos-header'>
				              <div class="icon-box">
						   		<i class="weui-icon-info weui-icon_msg" style=""></i>
						      </div>
						       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
							   <a href="${path }/wx/home.htm" style="width: 90%;font-size: 14px;" class="weui-btn weui-btn_primary">返回首页</a>
					    </header>
					  </c:if>
					  <c:if test="${staySendOrder!='[]'}" >
				          <c:forEach items="${staySendOrder }" var="ol">
						    <a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
						      <div class="weui-media-box__hd">
						        <img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
						      </div>
						      <div class="weui-media-box__bd">
							        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品 <span style="color:#e64340"> 等待发货</span></h4>
						        	<p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>	
						      </div>
						      <div class="weui-cell__ft">${ol.total_price }元</div>
						    </a>
					       </c:forEach>
					  </c:if>
					  </div>
					</div>
				    </div>
				   <div id="tab4" class="weui-tab__bd-item">
				            <div class="weui-panel weui-panel_access">
					  <div class="weui-panel__bd">
					  <c:if test="${finishOrder=='[]'}" >
				           <header class='demos-header'>
				              <div class="icon-box">
						   		<i class="weui-icon-info weui-icon_msg" style=""></i>
						      </div>
						       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
							   <a href="${path }/wx/home.htm" style="width: 90%;font-size: 14px;" class="weui-btn weui-btn_primary">返回首页</a>
					    </header>
					  </c:if>
					  <c:if test="${finishOrder!='[]'}" >
				          <c:forEach items="${finishOrder }" var="ol">
						    <a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
						      <div class="weui-media-box__hd">
						        <img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
						      </div>
						      <div class="weui-media-box__bd">
						        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品<span style="color:#e64340"> 等待收货</span></h4>
						        <p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>	
						      </div>
						      <div class="weui-cell__ft">${ol.total_price }元</div>
						    </a>
					       </c:forEach>
					  </c:if>
					  </div>
					</div>
				    </div>
				     <div id="tab5" class="weui-tab__bd-item">
				            <div class="weui-panel weui-panel_access">
							  <div class="weui-panel__bd">
							  <c:if test="${zfinishOrder=='[]'}" >
						           <header class='demos-header'>
						              <div class="icon-box">
								   		<i class="weui-icon-info weui-icon_msg" style=""></i>
								      </div>
								       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关订单</h1>
									   <a href="${path }/wx/home.htm" style="width: 90%;font-size: 14px;" class="weui-btn weui-btn_primary">返回首页</a>
							      </header>
							  </c:if>
							  <c:if test="${zfinishOrder!='[]'}" >
						          <c:forEach items="${zfinishOrder }" var="ol">
								    <a href="${path }/wx/order_detail.htm?kid=${ol.kid}" class="weui-media-box weui-media-box_appmsg">
								      <div class="weui-media-box__hd">
								        <img class="weui-media-box__thumb" src="${path}/img/${ol.main_file_id}.htm">
								      </div>
								      <div class="weui-media-box__bd">
								        <h4 class="weui-media-box__title" style="font-size: 14px">共${ol.product_sum }件商品<span style="color:#e64340"> 已完成</span></h4>
								        <p class="weui-media-box__desc" style="font-size: 12px">${ol.time}</p>	
								      </div>
								      <div class="weui-cell__ft">${ol.total_price }元</div>
								    </a>
							       </c:forEach>
							  </c:if>
							  </div>
							</div>
				    </div>
				  </div>
				</div>
	  </div>
	</div>
 	   <script>
		  $(function() {
		 	$("#order_lists").popup();
		    FastClick.attach(document.body);
		  });
		</script>
  </body>

</html>
