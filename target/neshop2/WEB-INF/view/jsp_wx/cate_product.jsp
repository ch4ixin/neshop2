<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>
<% 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragrma","no-cache"); 
response.setDateHeader("Expires",0); 
String name = request.getParameter("name");
String rank_code = request.getParameter("rank_code");
%>
<!DOCTYPE html >
<html>
<head>
<base href="${basePath}resource/mobile/" />
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="minimal-ui=yes,width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title>悦龙斋厂家速购  ${param.cname}</title>
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<link rel="stylesheet" type="text/css" href="themesmobile/68ecshopcom_mobile/css/public.css"/>
<link rel="stylesheet" type="text/css" href="themesmobile/68ecshopcom_mobile/css/category_list.css"/>
<script type="text/javascript" src="themesmobile/68ecshopcom_mobile/js/jquery.js"></script>
<style type="text/css">
	.filtrate_term li {width: 23.5%;}
	.touchweb-com_searchListBox.openList .item .price_box {height: 20px;}
	.filtrate_term li {height: 40px;}
</style>
</head>
<body>
<section class="_pre" >
<!-- <header id="head_search_box" style="position: fixed; top: 0px; width: 100%;">
<div class="search_header">
    
</div>
</header> -->
<!-- <div style="height:50px;" class="empty_div">返回首页</div> -->
<section class="filtrate_term" id="product_sort" style="width: 100%;">
<ul>
	<li class="" ><a href="${path}/wx/home.htm" >首页</a></li>
	<li class="" id="desc"><a href="${path}/wx/cate_product.htm?cname=${param.cname}&rank_code=DESC">最新</a></li>
	<li class="" id="asc"><a href="${path}/wx/cate_product.htm?&cname=${param.cname}&rank_code=ASC" >热销</a></li>
	<li class=""><a href="javascript:;" class="show_type  show_list">&nbsp;</a></li>
</ul>
</section>
</section>
<section style="width:100%;height:100%; overflow:hidden;">
<div style="height:5px;"></div>
<script type="text/javascript">
	$(function(){
		var rank_code= '${param.rank_code}';
		//console.log($('#desc'));
		if(rank_code == 'DESC'){
			$('#desc').attr('class','on');
			$('#asc').attr('class','');
		}else{
			$('#desc').attr('class','');
			$('#asc').attr('class','on');
		}
	});
</script>
<div class="touchweb-com_searchListBox openList" id="goods_list">
   <form action="javascript:void(0)" method="post" name="ECS_FORMBUY" id="ECS_FORMBUY" >
	<div id="J_ItemList" style="opacity: 1;" >
		<c:forEach items="${product_list }" var="pl">
		  <div class="product single_item info" id="more_element_1"> 
		   <li> <a href="${path}/wx/orders.htm?kid=${pl.kid}" class="item"> 
		     <div class="pic_box"> 
		      <div class="active_box"> 
		       <span style=" background-position:0px -71px">精品</span>
		      </div> 
		      <img src="${path}/img/${pl.main_file_id}.htm" /> 
		     </div> 
		     <div class="title_box">
		       ${pl.name}
		     </div> 
		     <div class="price_box">
		      <span class="new_price"> <i>￥${pl.price}元</i> </span> 
			</div>
		     </a> 
		    <span class="bug_car" onclick="to_orders()"><i class="icon-shop_cart"></i></span> 
		   </li> 
		  </div>
	  </c:forEach>
	</div>
</form>
</div>
</section> 
<script>
   $('.show_type').bind("click", function() {
    if ($('#goods_list').hasClass('openList')){
	$('#goods_list').removeClass('openList');
	$(this).removeClass('show_list');
	}
	else
	{
	$('#goods_list').addClass('openList');	
	$(this).addClass('show_list');
	}
	});
   
   function to_orders(){
	   window.location.href="${path}/wx/orders.htm?kid=${pl.kid}";
   }
</script>
<script type="Text/Javascript" language="JavaScript">
<!--
function selectPage(sel)
{
  sel.form.submit();
}
//-->
</script>
<script>
function goTop(){
	$('html,body').animate({'scrollTop':0},600);
}
</script>
<a href="javascript:goTop();" class="gotop" style=" z-index:9999"><img src="themesmobile/68ecshopcom_mobile/images/topup.png"></a> 
<div class="footer" style="background-color: #FFF;">
      <div class="links"  id="ECS_MEMBERZONE">
	      <a ><span>便捷</span></a>
	      <a ><span>可靠</span></a>
	      <a ><span>精品</span></a>
	  </div>
  	  <p class="mf_o4">&copy;2017 <a href="http://www.sxxlkj.com" style="color: #586c94;font-size: 12px;">山西悦龙斋科技有限公司</a> 版权所有</p>
</div>
</body>
</html>