<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>${product.name }</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	  <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/public.css"/>
	  <link rel="stylesheet" type="text/css" href="${path }/resource/mobile/themesmobile/68ecshopcom_mobile/css/index.css"/>
	  <link rel="stylesheet" href="${path }/resource/jqweui/css/weui.min.css">
	  <link rel="stylesheet" href="${path }/resource/jqweui/css/jquery-weui.css">
	  <script src="${path }/resource/jqweui/js/jquery-2.1.4.js"></script>
	  <script src="${path }/resource/jqweui/js/jquery-weui.js"></script>
	  <script src="${path }/resource/js/fastclick.js"></script>
	  <script src="${path }/resource/jqweui/js/swiper.min.js"></script>
	<style type="text/css">
		body,html{background-color: #EFEFF4;}
		.swiper-container{background:#ffffff;}
		.weui-cell__ft p{font-size: 15px;color: #FF6B2F;}
		.weui-cell_switch {padding-top: 10px;padding-bottom: 10px;}
		.swiper-container-horizontal>.swiper-pagination-bullets{bottom:-4px;}
		.weui-cells {font-size: 14px;}
		.swiper-slide img{width:100%;height:250px;}
		.fanhui_img{width:30px; height:30px; background:rgba(12,12,12,0.5); border-radius:50%;position:absolute;top:6px;bottom:0;left:6px;z-index:100;text-align:center;line-height:30px;}
		.fanhui_img img{width:12px;padding-top:4px;}
		.weui-cells{margin:0;}
		.count-box{ width:50%; background:#ffffff; position:absolute; top:1px; right:10px;}
		.count-box-top{width:100%; height:45px; padding-top:10px; text-align:center;}
		.count-box-top span{width: 30px; height: 30px; border:1px solid #ccc; display:inline-block; text-align: center; line-height: 30px; font-size: 18px; font-weight: bold; color: #3d4145;}
		.count-box-top .count-inp{border:1px solid #ccc; width:50px; font-weight:normal;line-height:30px;}
		.weui-cells:before{border-top: 0px solid #d9d9d9;}
		.weui-c{padding: 20px 15px;}
		.my_divv{width: 100%;position: fixed;bottom: 0;}
		.my_divv a{float:left;height: 50px;line-height: 50px;}
		.weui-btn+.weui-btn {margin-top:0;}
		.weui-btn {font-size: 14px; border-radius: 0px;}
		.weui-btn:after { border-radius: 0px;}
		.weui-photo-browser-modal.weui-photo-browser-modal-visible {z-index: 999;}
		.weui-panel__bd img{width:100%;}
	</style>
  </head>
  <body ontouchstart>
   <%-- <div class="fanhui_img" onclick="${path }/wx/home.htm"><img src="../../../neshop2/resource/img/fanhui.png"></div> --%>
		<div class="swiper-container" id="pb1">
		  <div class="swiper-wrapper">
		    <div class="swiper-slide"><img onclick="javascript:show_img('${path}/img/${product.main_file_id}.htm');" src="${path}/img/${product.main_file_id}.htm" alt=""></div>
		  </div>
		  <div class="swiper-pagination"></div>
		</div>
		<div class="weui-cells weui-cells_form">
		  <div class="weui-cell weui-cell_switch">
		    <div class="weui-cell__bd">${product.name }</div>
		    <div class="weui-cell__ft">
		      <p>￥${product.price }</p>
		    </div>
		  </div>
		</div>
		<div class="weui-cells weui-cells_form">
		  <div class="weui-cell">
		    <div class="weui-cell__hd"><label class="weui-label">快递费用(首件)</label></div>
		    <div class="weui-cell__bd" style="width: 50%;background: #ffffff;position: absolute;top: 15%;right:-35%;font-size: 15px;color: #FF6B2F;">
		      <input class="weui-input" readonly="readonly" value="¥ ${product.carriage }">
		    </div>
		   </div>
		  <div class="weui-cell">
		    <div class="weui-cell__hd"><label class="weui-label">快递费用(续件)</label></div>
		    <div class="weui-cell__bd" style="width: 50%;background: #ffffff;position: absolute;top: 15%;right:-35%;font-size: 15px;color: #FF6B2F;">
		      <input class="weui-input" readonly="readonly" value="¥ ${product.more_carriage }">
		    </div>
		   </div>
		   <div class="weui-cell weui-c">
		    <div class="weui-cell__hd"><label class="weui-label">购买数量(${product.unit })</label></div>
		    <div class="weui-cell__bd">
    			  <div class="count-box">
                      <div class="count-box-top" style="text-align: right;">
                          <span id="divice">-</span>
                          <span class="count-inp"><input  id="number" style="border:0;width:50px; text-align:center;" type="Number" value="1"></span>
                          <span id="add">+</span>
                      </div>
                  </div>
		    </div>
		  </div>
			<div class="weui-panel__ft">
				<a href="javascript:" class="weui-cell weui-cell_access weui-cell_link open-popup" data-target="#popup_service">
					<div class="weui-cell__bd">服务 收货后结算 · 快递发货</div>
					<span class="weui-cell__ft"></span>
				</a>
			</div>
		</div>
	   <div class="weui-panel weui-panel_access" id="pcontent">
		   <div class="weui-panel__hd">商品详情</div>
		   <div class="weui-panel__bd">
			   ${product.content }
		   </div>
		   <div class="weui-loadmore">
			   <i class="weui-loading"></i>
			   <span class="weui-loadmore__tips">正在加载</span>
		   </div>
	   </div>
       <a href="javascript:goTop();" class="gotop"><img src="${path}/resource/mobile/themesmobile/68ecshopcom_mobile/images/topup.png"></a>
	   <footer>
		   <div class="footer">
			   <p class="mf_o4"><a href="http://www.sxxlkj.com" style="color: #586c94;font-size: 10px;">中科讯龙</a> 提供技术支持</p>
		   </div>
		   <div style="height:50px; line-height:50px; clear:both;"></div>
	   </footer>
		<div class="my_divv">
			<a href="${path }/view_cp/wx/kefu.html" style="width: 20%" class="weui-btn weui-btn_default">客服</a>
			<a href="${path }/wx/cart.htm" style="width: 20%" class="weui-btn weui-btn_default">购物车</a>
			<a href="javascript:add_cart();"  style="background: #FF6600;color: #fff;width: 30%" class="weui-btn weui-btn_default">加入购物车</a>
			<a href="javascript:buy_now()" style="width: 30%" class="weui-btn weui-btn_primary">立即购买</a>
		</div>
	   <div id="popup_service" class='weui-popup__container popup-bottom'>
		   <div class="weui-popup__overlay"></div>
		   <div class="weui-popup__modal">
			   <article class="weui_article">
				   <section>
					   <p>收货后结算</p>
					   <p>该店铺交易由有赞提供资金存管服务，当符合以下条件时，资金自动结算给商家：买家确认收货或到达约定的自动确认收货日期。交易资金未经有赞存管的情形（储值型、电子卡券等）不在本服务范围内。</p>
					   <p>快递发货</p>
					   <p>可快递发货配送上门</p>
					   <a href="javascript:;" class="weui_btn weui_btn_primary close-popup" style="margin-top: 30px">我知道了</a>
				   </section>
			   </article>
		   </div>
	   </div>
	<script>
	$(function(){
		FastClick.attach(document.body);
		// $.showLoading();
		document.onreadystatechange = function(){
			if(document.readyState=="complete"){
				$('.weui-loadmore').hide();
				// $.hideLoading();
			}
		}

		//console.log('${product.file_ids}');
		file_ids = '${product.file_ids}';
		file_idss= file_ids.split(",");
		
		for (var i = 1; i < file_idss.length; i++) {
			//alert(file_idss[i]);
			$('.swiper-wrapper').append('<div class="swiper-slide"><img onclick="javascript:show_img(\'${path}/img/'+file_idss[i]+'.htm\');" src="${path}/img/'+file_idss[i]+'.htm" alt=""></div>');
		}
	})

	function goTop(){
		$('html,body').animate({'scrollTop':0},600);
	}

	function show_img(img) {
		$.photoBrowser({
			items: [
				img
			],
			onSlideChange: function(index) {
				console.log(this, index);
			},
			onOpen: function() {
				console.log("onOpen", this);
			},
			onClose: function() {
				console.log("onClose", this);
			}
		}).open();
	};
	
	var mySwiper = new Swiper('.swiper-container', {
		autoplay:2000,//可选选项，自动滑动
		autoplayDisableOnInteraction : false,
		parallax : true,
		pagination : '.swiper-pagination',
		paginationClickable :true,
	});

	$('#divice').click(function(){
	   var c=$('.count-inp input').val();
	   if(c>1){
		c--;
		$('.count-inp input').val(c);
	}
	});

	$('#add').click(function(){
		var c=$('.count-inp input').val();
		c++;
		$('.count-inp input').val(c);
	});

	function add_cart(){
		$.showLoading();
		var ajaxTimeOut = $.ajax({
			type : "POST",
			url:"${path}/wx/add_cart.htm",
			data:{product_id:'${product.kid}',number:$("#number").val()},
			timeout : 60000, //超时时间设置，单位毫秒
			dataType: "json",
			success: function(data){
				$.closePopup()
				$.hideLoading();
				$.toast("已添加购物车");
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				$.hideLoading();
				$.toptip('错误 '+XMLHttpRequest.status +" - "+XMLHttpRequest.readyState+" - "+textStatus, 'warning');
			},
			complete : function(XMLHttpRequest,status){ //请求完成后最终执行参数
				if(status=='timeout'){//超时,status还有success,error等值的情况
					ajaxTimeOut.abort(); //取消请求
					$.hideLoading();
					$.toptip('😣 网络好像出问题了，请重试', 'warning');
				}
			},
			failure : function(response) {
				$.hideLoading();
				$.toptip('😣 好像出问题了，请重试', 'warning');
			}
		});
	}

	function buy_now(){
		window.location.href="${path }/wx/buy_now.htm?product_id=${product.kid}&number="+$("#number").val();
	}
	</script>
  </body>

</html>
