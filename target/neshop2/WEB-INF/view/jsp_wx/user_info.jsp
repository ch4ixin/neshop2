<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>
<% 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragrma","no-cache"); 
response.setDateHeader("Expires",0); 
%> 
<!DOCTYPE html >
<html>
<head>
<base href="${basePath}resource/mobile/" />
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<title>最便捷可靠的移动购物平台</title>
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<link rel="stylesheet" type="text/css" href="themesmobile/68ecshopcom_mobile/css/user.css"/>
<link rel="stylesheet" type="text/css" href="themesmobile/68ecshopcom_mobile/css/public.css"/>
<script src="themesmobile/68ecshopcom_mobile/js/modernizr.js"></script>
<script type="text/javascript" src="themesmobile/68ecshopcom_mobile/js/jquery.js"></script>
<style type="text/css">
	body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
</style>
<body>
      <div id="tbh5v0">
		<div class="user_com">
			<div class="com_top">
				<%-- <h2><a href="${path }/wx/cp/user_info.htm">设置</a></h2> --%>
				<dl>
					<dt><img src="${member.user_headimgurl }"><span>${member.user_nick }</span><dd></dd></dt>
				</dl>
			</div>
		<div class="uer_topnav">
			<ul>
				<li class="bain"><a href="${path }/wx/order_list.htm"><span>${my_order }</span>我的订单</a></li>
				<li class="bain"><a ><span>0</span>我的收藏</a></li>
				<li><a ><span>0</span>我的评价</a></li>
			</ul>
		</div>
		<%-- <div class="Wallet">
			<ul>
				<li class="bain1"><strong>￥${member.money }元</strong><span>余额</span></li>
				<li class="bain1"><strong>0</strong><span>优惠券</span></li>
				<li><strong>${member.integral }</strong><span>积分</span></li>
			</ul>
			<a href="${path }/wx/cp/account_detail.htm"><em class="Icon Icon1"></em><dl><dt>我的钱包</dt><dd style="color:#aaaaaa;">查看我的钱包</dd></dl></a>
		</div> --%>
		<div class="Wallet">
			<a href="${path }/wx/order_list.htm" ><em class="Icon Icon2"></em><dl class="b"><dt>全部订单</dt><dd>查看订单</dd></dl></a>
			<a ><em class="Icon Icon3"></em><dl class="b"><dt>我的优惠券</dt><dd>暂未开放</dd></dl></a>
			<a ><em class="Icon Icon4"></em><dl class="b"><dt>我的评价</dt><dd>暂未开放</dd></dl></a>
			<%-- <a href="${path }/wx/cp/collection.htm"><em class="Icon Icon10"></em><dl><dt>我的收藏</dt><dd>&nbsp;</dd></dl></a> --%>
		</div>
		<div class="Wallet">
			<a href="${path }/wx/address.htm"><em class="Icon Icon5"></em><dl class="b"><dt>地址管理</dt><dd>&nbsp;</dd></dl></a>
			<!--<a href="user.php?act=affiliate"><em class="Icon Icon6"></em><dl class="b"><dt>我的推荐</dt><dd>&nbsp;</dd></dl></a>-->
			<a ><em class="Icon Icon7"></em><dl class="b"><dt>我的留言</dt><dd>暂未开放</dd></dl></a>
		</div>
		<div class="Wallet">
			<!-- <a href="javascript:void(0)" onClick="window.location.href='user.php?act=logout'" ><em class="Icon Icon8"></em><dl><dt>注销登录</dt></dl></a> -->
		</div>
		</div>
		  <div class="footer" >
	      <div class="links"  id="ECS_MEMBERZONE">
				      <a ><span>便捷</span></a>
				      <a ><span>可靠</span></a>
				      <a ><span>精品</span></a>
				  </div>
			  	  <p class="mf_o4">&copy;2017 <a href="http://www.sxxlkj.com" style="color: #586c94;font-size: 12px;">山西悦龙斋科技有限公司</a> 版权所有</p>
			</div>
 		</div>
    </body>
</html>
