<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
 	<base href="${basePath}resource/mobile/" />
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;margin: 0 15%;}
		.demos-sub-title {text-align: center;color: #888;font-size: 14px;}
		.demos-header {padding: 10px 0;}
		.demos-content-padded {padding: 15px;}
		.demos-second-title {text-align: center;font-size: 24px;color: #3cc51f;font-weight: 400;margin: 0 15%;}
		footer {text-align: center;font-size: 14px;padding: 20px;}
		footer a {color: #999;text-decoration: none;}
		.weui-grid__icon {width: 100%;height: 150px;margin: 0 auto;}
		.weui-grid { float: left;padding: 0px 0px;width:49.5%;box-sizing: border-box;}
		.weui-grid:nth-child(odd) { margin-right:1%;margin-bottom:1%;}
		.weui-grid:nth-child(even) {margin:0;margin-bottom:1%;}
		.weui-footer, .weui-grid__label {text-align: center;margin-bottom:1px;font-size: 14px;background-color: #fff;}
		.p_span{background:#ffffff;}
		span{color: black;}
	 	.sspan{font-size: 16px;color:FF0000;} 
		.weui-grids{border-top: 1px solid #d9d9d9;}
		.weui-cells__title{color:black;}
		.weui-grid__icon+.weui-grid__label {margin-top: 0px;}
		.weui-panel:first-child{ margin-top: 10px;}
		.weui-media-box_appmsg .weui-media-box__hd {width: 50px;height: 50px;}
		.weui-grid__icon{height:200px;}
		.icon-box {margin-bottom: 35px;margin-top: 35px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
		.icon-box i {margin: auto;text-align: center;}
		.weui-cell__ft p{font-size: 15px;color: #FF6B2F;}
		.placeholder {margin: 5px;background-color: #fff;height: 100px;text-align: center;color: #cfcfcf;}
		.weui-s{width: 100%}
		.weui-flex__item{width: 25%;float:left;}
		.demos-title span {color: #DBDBDB}
		.weui-tab__bd .weui-tab__bd-item {display: none;height: 100%;overflow: auto;}
		.weui-cells {font-size: 14px;}
		.weui-title{font-size: 14px;background: #ffffff;color: #3d4145;padding: 5px;text-align: justify;overflow: hidden;text-overflow: ellipsis;overflow: hidden; white-space: nowrap;}
		.weui-loadmore_line .weui-loadmore__tips {background-color: #EFEFF4;}
	</style>
  </head>
  <body ontouchstart>
		 <div id="products" class="weui-popup__container">
		  <div class="weui-popup__overlay"></div>
		  <div class="weui-popup__modal">
		    <div class="weui-tab">
			      <div class="weui-tab__bd">
			      	<c:if test="${product_list=='[]'}" >
				           <header class='demos-header'>
				              <div class="icon-box">
						   		<i class="weui-icon-info weui-icon_msg" style=""></i>
						      </div>
						       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">没有相关商品</h1>
							   <a href="${path }/wx/home.htm" style="width: 90%;font-size: 14px;" class="weui-btn weui-btn_primary">返回首页</a>
					    </header>
					</c:if>
					<c:if test="${product_list!='[]'}" >
						<%-- <header>
					      <div class="weui-search-bar" id="searchBar" >
							  <form class="weui-search-bar__form" action="${path }/wx/search.htm">
							    <div class="weui-search-bar__box">
							      <i class="weui-icon-search"></i>
							      <input type="search" name="pname" id="pname" class="weui-search-bar__input" id="searchInput" placeholder="搜索" required="">
							      <a href="javascript:" class="weui-icon-clear" id="searchClear"></a>
							    </div>
							    <label class="weui-search-bar__label" id="searchText">
							      <i class="weui-icon-search"></i>
							      <span>搜索</span>
							    </label>
							  </form>
							  <a href="javascript:" class="weui-search-bar__cancel-btn" id="searchCancel">取消</a>
							</div>
					      </header> --%>
				        <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
				          <div class="weui-grids">	
				          		<c:forEach items="${product_list}" var="list">
							     <a href="${path}/wx/orders.htm?kid=${list.kid}" class="weui-grid">
							    	 <div class="weui-grid__icon">
							         <img src="${path}/img/${list.main_file_id}.htm" alt="">
							         </div>
							       <p class="weui-title">${list.name}</p>
							      <p class="p_span"><%-- ${list.name}  --%><span style="color:#FF0000;" class="sspan">￥${list.price}</span><s style="color:#999;font-size:12px;margin-left:10px;">￥${list.market_price}</s></p>
							     </a>	
								</c:forEach>
						    </div>
								<div class="weui-loadmore weui-loadmore_line">
								  <span class="weui-loadmore__tips">已经到底了</span>
								</div>
				        </div>
					</c:if>
			      </div>
			  </div>
		  </div>
		</div>
		<div style="height:50px; line-height:50px; clear:both;"></div>
		<script>
		  $(function() {
			$("#products").popup();
		    FastClick.attach(document.body);
		  });
		</script>
  </body>

</html>
