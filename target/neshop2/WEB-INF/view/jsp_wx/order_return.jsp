<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>申请退款/退款</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.weui-cells {font-size: 14px;}
		.weui-cells__title {font-size: 10px;}
		.weui-cell__ft {color: black}
		.weui-cell__bd {color: #999;}
		.weui-media-box__desc {line-height: 1.5;}
		.weui-c{padding: 10px 15px;position: relative;display: flex;-webkit-box-align: center;align-items: center;float: right;}
		.oreturn{    
		-webkit-appearance: none;
	    border: 0;
	    outline: 0;
	    background-color: transparent;
	    width: 100%;
	    font-size: inherit;
	    height: 44px;
	    line-height: 44px;
	    position: relative;
	    z-index: 1;
	    padding-right: 30px;
	    padding-left: 0;
	    }
	</style>
  </head>
  <body ontouchstart>
		<div class="weui-panel weui-panel_access">
		  <div class="weui-panel__bd">
		    <a class="weui-media-box weui-media-box_appmsg">
		      <div class="weui-media-box__hd">
		        <img class="weui-media-box__thumb" src="${path }/img/${product.main_file_id}.htm">
		      </div>
		      <div class="weui-media-box__bd">
		        <h4 class="weui-media-box__title">${product.name}</h4>
		        <p class="weui-media-box__desc" style="color: #FF6B2F;font-size: 13px;">${orderDetail.price}元/${product.unit} x ${orderDetail.cnt }</p>
		      </div>
		    </a>
		  </div>
		</div>	
		
		<div class="weui-cells weui-cells_form">
	      <div class="weui-cell weui-cell_select weui-cell_select-after">
	        <div class="weui-cell__hd">
	          <label for="" class="weui-label">退款原因</label>
	        </div>
	        <div class="weui-cell__bd">
	        	<input style="color: black" class="weui-input oreturn" id="oreturn" type="text" readOnly="readonly"  name="problem" placeholder="请选择" value=""> 
	        </div>
	      </div>
		</div>

		<div class="weui-cells weui-cells_form">
		   <div class="weui-cell">
		    <div class="weui-cell__hd"><label class="weui-label">退款金额:</label></div>
		    <div class="weui-cell__bd">
		      <input class="weui-input" type="number" style="color: #FF6B2F;" readOnly="readonly" name="return_money" value="${orderDetail.price_total }">
		    </div>
		  </div>
		</div>
		<div class="weui-cells__tips" style="font-size: 12px;">最多 ￥${orderDetail.price_total } ,含发货运费 ￥ 0.00</div>
		
		<div class="weui-cells weui-cells_form">
		   <div class="weui-cell">
		    <div class="weui-cell__hd"><label class="weui-label">退款说明:</label></div>
		    <div class="weui-cell__bd">
		      <input class="weui-input" type="text" name="note" style="color: black" placeholder="选填">
		    </div>
		  </div>
		</div>
		<br>
		<a href="javascript:submit_oreturn();" class="weui-btn weui-btn_primary" style="width: 95%;margin:0 auto;">提交</a>
   	   <script>
		  $(function() {
		    FastClick.attach(document.body);
		  });
		  
 		function myClose(){
 			WeixinJSBridge.call('closeWindow');
 		}
 		
 		function submit_oreturn(){
 			var product_id = '${product.kid}';
 			var order_detail_id = '${orderDetail.kid}';
 			var note = $("input[name='note']").val();
 			var problem = $("input[name='problem']").val();
 			var return_money = $("input[name='return_money']").val();
 			var order_no = '${order_no}';
 			var return_type = '${return_type}';
 			
	 		$.ajax({
				type : "POST",
				url : "${path}/wx/submit_oreturn.htm",
				data : {
					product_id:product_id,
					order_detail_id:order_detail_id,
					note:note,
					problem:problem,
					return_money:return_money,
					order_no:order_no,
					return_type:return_type
				},
				dateType : 'json',
				success : function(msg) {
					msg = $.parseJSON(msg);
					//console.log(msg);
					$.toast("提交成功");
					window.location.href="${path}/wx/check_oreturn.htm?kid="+msg.msg; 
						
				}
			}); 
 		}
 		
 	      $(document).on("click", "#oreturn", function() {
 	         $.actions({
 	           title: "退款原因",
 	           onClose: function() {
 	             console.log("close");
 	           },
 	           actions: [
 	             {
	 	           text: "商品瑕疵",
	 	           onClick: function() {
	 	        	  $("#oreturn").val("商品瑕疵"); 
	 	            }
	 	         },{
 	               text: "退运费",
 	               onClick: function() {
 	                $("#oreturn").val("退运费"); 
 	               }
 	             },{
	 	           text: "质量问题",
	 	           onClick: function() {
	 	        	  $("#oreturn").val("质量问题"); 
	 	            }
	 	         },{
	 	           text: "少件/漏发",
	 	           onClick: function() {
	 	        	  $("#oreturn").val("少件/漏发"); 
	 	            }
	 	         },{
	 	           text: "收到商品时有划痕或破损",
	 	           onClick: function() {
	 	        	  $("#oreturn").val("收到的商品有划痕或破损"); 
	 	            }
	 	         }
 	           ]
 	         });
 	       });
		</script>
  </body>
</html>
