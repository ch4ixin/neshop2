<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.weui-cells {font-size: 14px;}
		.weui-cells__title {font-size: 10px;}
		.weui-cell__ft {color: black}
		.weui-cell__bd {color: #999;}
		.weui-media-box__desc {line-height: 1.5;}
		body, html {
		  height: 100%;
		  -webkit-tap-highlight-color: transparent;
		}
		.demos-title {
		  text-align: center;
		  font-size: 30px;
		  color: #3cc51f;
		  font-weight: 400;
		  margin: 0 15%;
		}
		
		.demos-sub-title {
		  text-align: center;
		  color: #888;
		  font-size: 14px;
		}
		
		.demos-header {
		  padding: 35px 0;
		}
		
		.demos-content-padded {
		  padding: 15px;
		}
		
		.demos-second-title {
		  text-align: center;
		  font-size: 24px;
		  color: #3cc51f;
		  font-weight: 400;
		  margin: 0 15%;
		}
		
		footer {
		  text-align: center;
		  font-size: 14px;
		  padding: 20px;
		}
		
		footer a {
		  color: #999;
		  text-decoration: none;
		}
	</style>
	<script type="text/javascript">
	function myClose(){
		WeixinJSBridge.call('closeWindow');
	}
	</script>
  </head>
  <body ontouchstart>
	 
    <header class='demos-header'>
      <h1 class="demos-title">交易完成</h1>
    </header>

    <div class="weui-form-preview">
      <div class="weui-form-preview__hd">
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">订单项金额</label>
          <em class="weui-form-preview__value">¥${orderDetail.price_total }</em>
        </div>
      </div>
      <div class="weui-form-preview__bd">
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">商品</label>
          <span class="weui-form-preview__value">${product.name }</span>
        </div>
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">收件人</label>
          <span class="weui-form-preview__value">${order.name } ${order.phone }</span>
        </div>
         <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">收货地址</label>
          <span class="weui-form-preview__value">${order.address }</span>
        </div>
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">快递信息</label>
          <span class="weui-form-preview__value">${logistics.name } ${orderLogistics.logistics_code }</span>
        </div>
      </div>
      <div class="weui-form-preview__ft">
        <a class="weui-form-preview__btn weui-form-preview__btn_default" href="${path }/wx/product.htm">返回商城首页</a>
        <a href="javascript:myClose();" class="weui-form-preview__btn weui-form-preview__btn_primary">关闭</a>
      </div>
    </div>
    <br>
     <script>
		  $(function() {
		    FastClick.attach(document.body);
		  });
	</script>
  </body>
</html>
