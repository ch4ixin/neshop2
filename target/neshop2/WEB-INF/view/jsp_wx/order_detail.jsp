<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.weui-cells {font-size: 14px;}
		.weui-cells__title {font-size: 10px;}
		.weui-cell__ft {color: black}
		.weui-cell__bd {color: #999;}
		.weui-media-box__desc {line-height: 1.5;}
		.weui-c{padding: 10px 15px;
	    position: relative;
	    display: flex;
	    -webkit-box-align: center;
	    align-items: center;
	       float: right;
	    
	    }
		    .weui-c:before {
		    content: " ";
		    position: absolute;
		    top: 0;
		    right: 0;
		    height: 1px;
		    color: #d9d9d9;
		    left: 15px;
		}
		.weui-btn+.weui-btn {margin-top: 3px;}
	</style>
  </head>
  <body ontouchstart>
  
   	    <div class="weui-cells__title">订单信息</div>
		<div class="weui-cells">
		  <div class="weui-cell">
		    <div class="weui-cell__bd">
		      <p>订单编号:</p>
		    </div>
		    <div class="weui-cell__ft">${order.order_no }</div>
		  </div>
		  <div class="weui-cell">
		    <div class="weui-cell__bd">
		      <p>下单日期:</p>
		    </div>
		    <div class="weui-cell__ft">${time }</div>
		  </div>
		  <div class="weui-cell">
		    <div class="weui-cell__bd">
		      <p>支付时间:</p>
		    </div>
		    <div class="weui-cell__ft">${pay_time }</div>
		  </div>
		  <div class="weui-cell">
		    <div class="weui-cell__bd">
		      <h4 class="weui-media-box__title" style="color: #999;font-size: 14px;">收货信息:</h4>
		      <p class="weui-media-box__desc" style="color: black;">${order.phone } ${order.name } 收</p>
              <p class="weui-media-box__desc" style="color: black;">${order.address }</p>
		    </div>
		  </div>
		</div>
		<div class="weui-cells__title">购物清单</div>
		<div class="weui-cells">
			<c:forEach items="${detailList }" var="dl">
				<div class="weui-cell">
		            <div class="weui-cell__hd"><img src="${path}/img/${dl.main_file_id}.htm" alt="" style="width:50px;margin-right:5px;display:block"></div>
		            <div class="weui-cell__bd">
		              <p style="color: black;">${dl.pname }</p>
			          <div style="/* text-align:center; */">${dl.cnt } x <span style="color: #FF6B2F;font-size: 14px;">${dl.price }元</span></div>
		            </div>
		            <div class="weui-cell__ft" style="color: #999; ">
			            <c:if test="${order.status_order=='已发货'}" >
			            	<div style="margin-top:10px;padding-left:5px;"><a href="${path }/wx/kuaidi.htm?logisticsId=${dl.logistics_id}&orderDetailId=${dl.kid}" class="weui-btn weui-btn_mini weui-btn_plain-primary">查看物流</a></div>
			            </c:if>
			            <c:if test="${order.status_order=='已收货'}" >
			            	<div style="margin-top:10px;padding-left:5px;"><a href="${path }/wx/kuaidi.htm?logisticsId=${dl.logistics_id}&orderDetailId=${dl.kid}" class="weui-btn weui-btn_mini weui-btn_plain-primary">查看物流</a></div>
			            </c:if>
			            <c:if test="${order.status_order=='已完成'}" >
			            	<div style="margin-top:10px;padding-left:5px;"><a href="${path }/wx/kuaidi.htm?logisticsId=${dl.logistics_id}&orderDetailId=${dl.kid}" class="weui-btn weui-btn_mini weui-btn_plain-primary">查看物流</a></div>
			            </c:if>
			            <c:if test="${order.status_order=='已下单'}" >
			            	<div style="padding-left:5px;">
			            		<a style="font-size: 13px;" href="${path }/wx/order_return.htm?orderDetailId=${dl.kid}" class="weui-btn weui-btn_plain-default">退款/退货</a>
			            		<a style="font-size: 13px;" class="weui-btn weui-btn_disabled weui-btn_default">等待商家发货</a>
			            	</div>
			            </c:if>
		            </div>
		          </div>	
			</c:forEach>
		</div>
        <div class="weui-cells__title">订单金额</div>
     	<div class="weui-cells">
		  <div class="weui-cell">
		    <div class="weui-cell__bd">
		      <p style="color: black">商品总价</p>
		    </div>
		    <div class="weui-cell__ft" style="color: #FF6B2F;font-size: 14px;">${order.total_price }元</div>
		  </div>
		  <div class="weui-cell">
		    <div class="weui-cell__bd">
		      <p style="color: black">运费</p>
		    </div>
		    <div class="weui-cell__ft" style="color: #FF6B2F;font-size: 14px;">${order.money_pay }元</div>
		  </div>
		  <div class="weui-cell">
		    <div class="weui-cell__bd">
		      <p style="color: black">应付总额</p>
		    </div>
		    <div class="weui-cell__ft" style="color: #FF6B2F;font-size: 14px;">${order.end_price }元</div>
		  </div>
		</div>
		
		<%-- <c:if test="${order.status_order=='已发货'}" >
			<div class="weui-cells__title">物流信息</div>
			<div class="weui-cells weui-cells_form">
			  <div class="weui-cell">
			    <div class="weui-cell__bd">
			      <textarea class="weui-textarea" readOnly="readonly" rows="3"></textarea>
			    </div>
			  </div>
			</div>
		</c:if> --%>
		<br>
		<c:if test="${order.status_order=='下单中'}" >
	      <a href="${path }/wx/to_cart.htm?order_id=${order.kid }" style="margin-top: 15px;margin-bottom: 5px;font-size: 15px;" class="weui-btn weui-btn_primary">使用微信支付</a>
	      <a href="javascript:del_order();" style="margin-top: 15px;margin-bottom: 5px;font-size: 15px;" class="weui-btn weui-btn_warn">删除订单</a>
		</c:if>
 	   <script>
		  $(function() {
		    FastClick.attach(document.body);
		  });
		</script>
  </body>
  <script type="text/javascript">
	 function del_order(){
		  $.confirm("您确定要删除订单吗?", "确认删除?", function() {
			  $.ajax({
	      			type : "POST",
	      			url : "${path }/wx/cancel_order.htm?kid=${order.kid }",
	      			dateType : 'json',
	      			success : function(msg) {
	      					$.toast("已删除！");
	      					self.location=document.referrer;
	      					
	      			}
	      		}); 
	        }, function() {

	        });
     };

  </script>
</html>
