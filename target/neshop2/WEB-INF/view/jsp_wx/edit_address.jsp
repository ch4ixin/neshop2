<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body,html{
			background-color: #EBEBEB;
		}
		div{font-size: 14px;}
		.weui-btn {width: 90%;font-size: 13px;}
	</style>
  </head>
  <body ontouchstart>
    <div class="weui-cells__title" style="font-size: 10px;">收货人信息</div>
    <div class="weui-cells weui-cells_form">
      <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">姓名</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" name="consignee" placeholder="真实姓名" value="${member_address.member_name }">
        </div>
      </div>
      <div class="weui-cell">
        <div class="weui-cell__hd"><label class="weui-label">电话</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" type="tel" name="mobile1" placeholder="手机号码" value="${member_address.mobile1 }">
        </div>
      </div>
      <div class="weui-cell">
        <div class="weui-cell__hd"><label for="name" class="weui-label">城市</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" id="start" type="text" value="${member_address.province} ${member_address.city} ${member_address.region}">
          <input class="weui_input" type="hidden" name="province" value="${member_address.province}"/>
		  <input class="weui_input" type="hidden" name="city" value="${member_address.city}"/>
		  <input class="weui_input" type="hidden" name="region" value="${member_address.region}"/>
        </div>
      </div>
	  <div class="weui-cell">
	    <div class="weui-cell__bd">
	      <input class="weui-input" type="text" name="address" placeholder="详细地址" value="${member_address.street}">
	    </div>
	  </div>
    </div>
    <div class="weui-cells weui-cells_form">
      <div class="weui-cell weui-cell_switch">
        <div class="weui-cell__bd">设置为默认地址</div>
        <div class="weui-cell__ft">
          <input class="weui-switch" type="checkbox" onclick="onOff()">
          <input class="weui_input" type="hidden" name="is_default" value="${member_address.is_default}"/>
        </div>
      </div>
    </div>
    <br>
		<a href="javascript:submit();" class="weui-btn weui-btn_primary">确定</a>
		<a href="javascript:myDel();" class="weui-btn weui-btn_warn">删除</a>
	<script>
		  $(function() {
		    FastClick.attach(document.body);
		  });
	</script>
	<script>
      $("#start").cityPicker({
        title: "选择收货城市",
        onChange: function (picker, values, displayValues) {
          console.log(values, displayValues);
          console.log(displayValues);
	      $("input[name=province]").val(displayValues[0]);
	      $("input[name=city]").val(displayValues[1]);
	      $("input[name=region]").val(displayValues[2]);
        }
      });
      
      function onOff(){
	      console.log($('input[type=checkbox]').is(':checked'));
	      if($('.weui-switch').is(':checked') == true){
	    	  $("input[name=is_default]").val("是");
	      }else{
	    	  $("input[name=is_default]").val("否");
	      }
      }
      
      function submit() {
    		var member_name = $("input[name='consignee']").val(); // 收货人姓名
    		var province = $("input[name='province']").val(); // 省份
    		var city = $("input[name='city']").val(); // 城市
    		var region = $("input[name='region']").val(); // 区域
    		var street = $("input[name='address']").val();//街道
    		var mobile1 = $("input[name='mobile1']").val();//联系方式1
    		var is_default = $("input[name='is_default']").val();//联系方式1
    		
    		var phoneNum = /^1[345789]\d{9}$/;
    	    if (phoneNum.test(mobile1)) {
    	    	if(member_name == ''){
    	    		$.toptip('请输入真实姓名', 'warning');
    	    		return false;
    	    	}
    	    	if(city == ''){
    	    		$.toptip('请选择城市', 'warning');
    	    		return false;
    	    	}
    	        $.ajax({
        			type : "POST",
        			url : "${path}/wx/edit_address.htm",
        			data : "member_name=" + member_name + "&province=" + province + "&city=" + city + "&region=" + region + "&street=" + street+ "&mobile1=" + mobile1+ "&is_default=" + is_default+ "&kid=" + '${member_address.kid}',
        			dateType : 'json',
        			success : function(msg) {
        					$.toast("更新成功");
        					self.location=document.referrer;
        					
        			}
        		}); 
       		}else{
       			$.toptip('请输入正确的手机号码！', 'warning');
       			return false;
       		}
    	}
      
      function myDel() {
    		var kid ='${member_address.kid}';//kid
    	    $.confirm("您确定要删除该收货地址吗?", "确认删除?", function() {
    	        $.ajax({
    	    		type : "POST",
    	    		url : "${path}/wx/del_address.htm",
    	    		data : "kid=" + kid,
    	    		dateType : 'json',
    	    		success : function(msg) {
    	    				$.toast("已删除！");
    	    				self.location=document.referrer;
    	    		}
    	    	}); 
    	      }, function() {

    	      });
    	}
	</script>

  </body>

</html>
