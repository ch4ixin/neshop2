<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="init.jsp"%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<link rel="stylesheet" href="${path}/resource/style4wx1/extend/jqweui8.2/lib/weui.min.css" />
<link rel="stylesheet" href="${path}/resource/style4wx1/extend/jqweui8.2/css/jquery-weui.min.css" />
<link rel="stylesheet" type="text/css" href="${path}/resource/style4wx1/font/iconfont.css"/>
<link rel="stylesheet" href="${path}/resource/style4wx1/css/base-wx.css?v=1.2" />
<script type="text/javascript" src="${path}/resource/style4wx1/extend/jquery-2.1.4.js" ></script>
<script type="text/javascript" src="${path}/resource/style4wx1/extend/jqweui8.2/js/jquery-weui.min.js" ></script>
<script type="text/javascript" src="${path}/resource/style4wx1/extend/jqweui8.2/lib/fastclick.js" ></script>
<script type="text/javascript" src="${path}/resource/style4wx/js/lib/layer/layer.js"></script>