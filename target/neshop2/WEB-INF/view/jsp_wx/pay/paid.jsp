<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>
<html>
<head> 
<base href="${basePath}resource/mobile/" /> 
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
<title>结账成功</title> 
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<meta name="Keywords" content="" />
<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
<script src="../../../../neshop2/resource/js/fastclick.js"></script>
<style type="text/css">
body, html {
  height: 100%;
  -webkit-tap-highlight-color: transparent;
}
.demos-title {
  text-align: center;
  font-size: 30px;
  color: #3cc51f;
  font-weight: 400;
  margin: 0 15%;
}

.demos-sub-title {
  text-align: center;
  color: #888;
  font-size: 14px;
}

.demos-header {
  padding: 35px 0;
}

.demos-content-padded {
  padding: 15px;
}

.demos-second-title {
  text-align: center;
  font-size: 24px;
  color: #3cc51f;
  font-weight: 400;
  margin: 0 15%;
}

footer {
  text-align: center;
  font-size: 14px;
  padding: 20px;
}

footer a {
  color: #999;
  text-decoration: none;
}
</style>
<script type="text/javascript">
	function myClose(){
		WeixinJSBridge.call('closeWindow');
	}
</script>
</head> 
 <body>
  <jsp:include page="../common/css-js.jsp" flush="true" />   
  
    <header class='demos-header'>
      <h1 class="demos-title">支付成功</h1>
    </header>

    <div class="weui-form-preview">
      <div class="weui-form-preview__hd">
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">付款金额</label>
          <em class="weui-form-preview__value">¥ ${order.end_price}</em>
        </div>
      </div>
      <div class="weui-form-preview__bd">
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">订单号</label>
          <span class="weui-form-preview__value">${order.order_no }</span>
        </div>
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">收件人</label>
          <span class="weui-form-preview__value">${order.name } ${order.phone }</span>
        </div>
         <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">收货地址</label>
          <span class="weui-form-preview__value">${order.address }</span>
        </div>
      </div>
      <div class="weui-form-preview__ft">
        <a class="weui-form-preview__btn weui-form-preview__btn_default" href="${path }/wx/home.htm">返回商城首页</a>
        <a href="${path }/wx/order_list.htm" class="weui-form-preview__btn weui-form-preview__btn_primary">我的订单</a>
      </div>
    </div>
 </body>
</html>