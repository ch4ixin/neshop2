<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
	body, html {height: 100%;width:100%;}
	.imgclass {height:100%;}
	.imgclass img{width:100%;height:99%;}
	</style>
  </head>
  <body>
	<div class="imgclass" ><img src="../../neshop2/resource/img/youhui.jpg"></div>
  </body>

</html>
