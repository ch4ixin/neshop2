<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body,html{background-color: #EBEBEB;}
		#add_address{position:absolute;bottom:60px;width: 100%;font-size: 17px;}
		#add_addresss{position:absolute;bottom:10px;width: 100%;font-size: 17px;}
		
	</style>
  </head>
  <body ontouchstart>
    <div class="weui-cells__title"></div>
    <div class="weui-cells weui-cells_radio">
    	<c:forEach  items="${addressList}" var="a">
		      <a href="${path}/wx/edit_from_address.htm?kid=${a.kid}" class="weui-media-box weui-media-box_appmsg">
	            <div class="weui-media-box__bd" >
	              <h4 class="weui-media-box__title" style="font-size: 12px;">${a.mobile1 } ${a.member_name } 收</h4>
		           <p style="font-size: 12px;"><span>${a.province } ${a.city } ${a.region } ${a.street }</span></p>
	            </div>
	            <div class="weui-cell__ft">
		          <c:if test="${a.is_default=='是' }">
			          <i class="weui-icon-success"></i>
		          </c:if>
		          <c:if test="${a.is_default=='否' }">
			          <i class="weui-icon-info-circle"></i>
		          </c:if>
		        </div>
	          </a>
   		</c:forEach>
    </div>
     <a href="${path }/wx/cart.htm" id="add_address" class="weui-btn weui-btn_default" >返回购物车</a>
     <a href="${path }/wx/add_address.htm" id="add_addresss" class="weui-btn weui-btn_primary" >新增收货地址</a>
 	<script>
	  $(function() {
	    FastClick.attach(document.body);
	  });
	</script>

  </body>

</html>
