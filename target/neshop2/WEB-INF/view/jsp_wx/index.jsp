<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>嗨购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;margin: 0 15%;}
		.demos-sub-title {text-align: center;color: #888;font-size: 14px;}
		.demos-header {padding: 10px 0;}
		.demos-content-padded {padding: 15px;}
		.demos-second-title {text-align: center;font-size: 24px;color: #3cc51f;font-weight: 400;margin: 0 15%;}
		footer {text-align: center;font-size: 14px;padding: 20px;}
		footer a {color: #999;text-decoration: none;}
		.weui-grid__icon {width: 100%;height: 150px;margin: 0 auto;}
		.weui-grid {position: relative;float: left;padding: 0px 0px;width: 50%;box-sizing: border-box;}
		.weui-footer, .weui-grid__label {text-align: center;margin-bottom:1px;font-size: 14px;background-color: #fff;}
		span{color: black;}
	 	.sspan{text-align: center;font-size: 14px;color:#F37DF;} 
		.weui-grids{border-top: 1px solid #d9d9d9;}
		.weui-cells__title{color:black;}
		.weui-grid__icon+.weui-grid__label {margin-top: 0px;}
		.weui-panel:first-child{ margin-top: 10px;}
		.weui-media-box_appmsg .weui-media-box__hd {width: 50px;height: 50px;}
		.icon-box {margin-bottom: 25px;margin-top: 25px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
		.icon-box i {margin: auto;text-align: center;}
		.weui-cell__ft p{font-size: 15px;color: #FF6B2F;}
		.placeholder {margin: 5px;background-color: #fff;height: 100px;text-align: center;color: #cfcfcf;}
		.weui-s{width: 100%}
		.weui-flex__item{width: 25%;float:left;}
		.demos-title span {color: #DBDBDB}
		.weui-tab__bd .weui-tab__bd-item {display: none;height: 100%;overflow: auto;}
		.weui-cells {font-size: 14px;}
	</style>
  </head>
  <body ontouchstart>
		<div class="weui-tab">
		      <div class="weui-tab__bd">
		        <div id="tab1" class="weui-tab__bd-item">
		          <div class="weui-grids">	
		          		<c:forEach items="${product_list}" var="list">
						  <a href="${path}/wx/orders.htm?kid=${list.kid}" class="weui-grid js_grid">
					        <div class="weui-grid__icon">
					          <img src="${path}/img/${list.main_file_id}.htm" alt="">
					        </div>
					        <p class="weui-grid__label">${list.name} <span style="color:#F37DF;" class="sspan">￥${list.price}</span></p>
					      </a>	
						</c:forEach>
				    </div>
		        </div>
		        <div id="tab2" class="weui-tab__bd-item" style="background-color: #fff;">
					    	<header class='demos-header'>
						      <h1 class="demos-title"><span>___&nbsp;&nbsp;&nbsp;&nbsp;</span> 汾酒 <span>&nbsp;&nbsp;&nbsp;&nbsp;___</span></h1>
						    </header>
					    
						    <div class="weui-s" >
							    <c:forEach items="${product_list}" var="list">
							     <a href="${path}/wx/orders.htm?kid=${list.kid}">
								      <div class="weui-flex__item" >
									      <div class="placeholder">
									      	<img width="100%" height="100%" src="${path}/img/${list.main_file_id}.htm" alt="">
									      	 </div>
									      	 <h4 class="weui-media-box__title" style="font-size: 14px;color: black; text-align:center;">${list.name }</h4>
									     
								      </div>
							      </a>
							    </c:forEach>
						    </div>
		        </div>
		        <div id="tab3" class="weui-tab__bd-item">
				        <c:if test="${cartList=='[]'}" >
				            <header class='demos-header'>
				               <div class="icon-box">
							   		<i class="weui-icon-info weui-icon_msg" style=""></i>
							   </div>
		  				       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">购物车中无商品</h1>
		 					   <a href="javascript:history.go(0)" style="width: 90%;font-size: 14px;" class="weui-btn weui-btn_primary">去逛逛</a>
						    </header>
						</c:if>
						<c:if test="${cartList!='[]'}" >
		          	      <div class="weui-panel weui-panel_access">
						        <div class="weui-cells__title">购物清单</div>
						        <div class="weui-panel__bd">
								        <c:forEach items="${cartList}" var="c">
								          <a href="${path}/wx/edit_orders.htm?kid=${c.p_kid }&oid=${c.kid}" class="weui-media-box weui-media-box_appmsg">
								            <div class="weui-media-box__hd">
								              <img class="weui-media-box__thumb" src="${path}/img/${c.main_file_id}.htm" alt="">
								            </div>
								            <div class="weui-media-box__bd" >
								              <h4 class="weui-media-box__title" style="font-size: 14px;">${c.p_name }</h4>
								              <p>${c.cnt } x <span style="color: #FF6B2F;" style="font-size: 14px;">${c.price }元</span></p>
								            </div>
								          </a>
								        </c:forEach>
						        </div>
						        <div class="weui-panel__ft">
						          <a href="javascript:history.go(0)" class="weui-cell weui-cell_access weui-cell_link">
						            <div class="weui-cell__bd">继续添加商品</div>
						            <span class="weui-cell__ft"></span>
						          </a>    
						        </div>
					      </div>
					      <div class="weui-panel weui-panel_access">
						        <div class="weui-cells__title">收货信息</div>
						        <div class="weui-panel__bd" style="border-top: 1px solid #d9d9d9;">
							          <a href="${path}/wx/address.htm" class="weui-media-box weui-media-box_appmsg">
							            <div class="weui-media-box__bd" >
							              <h4 class="weui-media-box__title" style="font-size: 12px;">186****0890 柴鑫 收</h4>
							              <p style="font-size: 13px;">太原市长治路学府街高新国际1101</span></p>
							            </div>
							          </a>
						        </div>
					      </div>
					      <div class="weui-panel weui-panel_access">
						        <div class="weui-cells__title">订单金额 <span style="float:right;color: #FF6B2F;">应付 <b>19.00元</b></span></div>
						        <div class="weui-panel__bd">
							            <div class="weui-cells weui-cells_form" style="font-size: 12px;">
										  <div class="weui-cell weui-cell_switch">
										    <div class="weui-cell__bd">商品总价</div>
										    <div class="weui-cell__ft">
										      <p>19.00元</p>
										    </div>
										  </div>
										  <div class="weui-cell weui-cell_switch">
										    <div class="weui-cell__bd">运费</div>
										    <div class="weui-cell__ft">
										      <p>0.00元</p>
										    </div>
										  </div>
										</div>
						        </div>
					      </div>
					      
					      <a href="javascript:;" class="weui-btn weui-btn_primary" style="margin-top: 30px;font-size: 18px;">使用微信支付</a>
						</c:if>
		        </div>
		        <div id="tab4" class="weui-tab__bd-item">
		          		<div class="weui-panel weui-panel_access">
					      <div class="weui-panel__bd">
							          <a class="weui-media-box weui-media-box_appmsg">
							            <div class="weui-media-box__hd">
							              <img class="weui-media-box__thumb" src="${member.user_headimgurl}" alt="">
							            </div>
							            <div class="weui-media-box__bd" >
							              <h4 class="weui-media-box__title" style="font-size: 14px;">${member.user_nick}</h4>
							            </div>
							          </a>
					        </div>
					      </div>
					      
					      <div class="weui-cells user-info">
							  <div class="weui-cell">
							    <div class="weui-cell__bd">
							      <p><i class="weui-icon-circle"></i>我的订单</p>
							    </div>
							    <div class="weui-cell__ft"></div>
							  </div>
							   <div class="weui-cell">
							    <div class="weui-cell__bd">
							      <p><i class="weui-icon-circle"></i>购物车</p>
							    </div>
							    <div class="weui-cell__ft"></div>
							  </div>
							   
							</div>
							
							<div class="weui-cells user-info" style="margin-top: 0.5em;">
								<div class="weui-cell">
								    <div class="weui-cell__bd">
								      <p><i class="weui-icon-circle"></i>我的优惠券</p>
								    </div>
								    <div class="weui-cell__ft"></div>
								  </div>
							</div>
							
							<div class="weui-cells user-info" style="margin-top: 0.5em;">
							  <div class="weui-cell">
							    <div class="weui-cell__bd">
							      <p><i class="weui-icon-circle"></i>设置</p>
							    </div>
							    <div class="weui-cell__ft"></div>
							  </div>
							</div>
		        </div>
		      </div>
		
		      <div class="weui-tabbar">
		        <a href="#tab1" class="weui-tabbar__item">
		          <div class="weui-tabbar__icon">
		            <img src="${path }/resource/img/icon_nav_button.png" alt="">
		          </div>
		          <p class="weui-tabbar__label">首页</p>
		        </a>
		        <a href="#tab2" class="weui-tabbar__item">
		          <div class="weui-tabbar__icon">
		            <img src="${path }/resource/img/icon_nav_cell.png" alt="">
		          </div>
		          <p class="weui-tabbar__label">分类</p>
		        </a>
		        <a href="#tab3" class="weui-tabbar__item">
		          <div class="weui-tabbar__icon">
		            <span class="weui-badge" style="position: absolute;top: -.4em;right: 1em;">1</span>
		            <img src="${path }/resource/img/icon_nav_msg.png" alt="">
		          </div>
		          <p class="weui-tabbar__label">购物车</p>
		        </a>
		        <a href="#tab4" class="weui-tabbar__item">
		          <div class="weui-tabbar__icon">
		            <img src="${path }/resource/img/icon_nav_article.png" alt="">
		          </div>
		          <p class="weui-tabbar__label">我的</p>
		        </a>
		      </div>
		    </div>
<script>
  $(function() {
	  	console.log(window.location.hash);
	  	 var id = window.location.hash;
	  	 $(id).attr('class', 'weui-tab__bd-item weui-tab__bd-item--active'); 
	  	 $("a[href="+id+"]").attr('class', 'weui-tabbar__item weui-bar__item--on');
  });
</script>

  </body>

</html>
