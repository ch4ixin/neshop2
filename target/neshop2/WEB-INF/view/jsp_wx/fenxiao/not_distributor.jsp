<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>

<!DOCTYPE html>
<html>
  <head>
    <title>您还不是分销商</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
	     body{min-height:100%;}
		.demos-header {padding: 0px 0}
		.icon-box {margin-bottom: 25px;margin-top: 25px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
		.icon-box i {margin: auto;text-align: center;}
		.demos-title span {color: #DBDBDB}
		.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;}
		.weui-grid {padding: 5px 5px;}
		.weui-grid__icon {width: 100%;height: 100%;}
		.weui-footer__text{padding:10px 0;}
		.demos-header{display:none;}
	</style>
  </head>
  <body>
		<div class="weui-msg">
		  <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
		  <div class="weui-msg__text-area">
		    <h2 class="weui-msg__title">您还不是分销商</h2>
		    <p class="weui-msg__desc">如有需要点击下方"联系客服"或者拨打热线:<a href="tel:0351-3853826">0351-3853826</a></p>
		  </div>
		  <div class="weui-msg__opr-area">
		    <p class="weui-btn-area">
		      <a href="http://www.sxxlkj.com/neshop2/view_cp/wx/kefu.html" class="weui-btn weui-btn_plain-primary">联系客服</a>
		      <a href="javascript:myClose();" class="weui-btn weui-btn_default">关闭页面</a>
		    </p>
		  </div>
		  <div class="weui-msg__extra-area">
		    <div class="weui-footer">
		      <p class="weui-footer__links">
		        <a href="http://www.sxxlkj.com" class="weui-footer__link">山西悦龙斋科技有限公司</a>
		      </p>
		    </div>
		  </div>
		</div>
	<script>
	  $(function() {
	    FastClick.attach(document.body);
	  });
	  
 	  function myClose(){
			WeixinJSBridge.call('closeWindow');
	  }
	</script>
  </body>
</html>
