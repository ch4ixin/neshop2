<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>

<!DOCTYPE html>
<html>
  <head>
    <title>我的客户</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
	     body{min-height:100%;}
		.demos-header {padding: 0px 0}
		.demos-title span {color: #DBDBDB}
		.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;}
		.weui-grid {padding: 5px 5px;}
		.weui-grid__icon {width: 100%;height: 100%;}
		.weui-footer__text{padding:10px 0;}
	</style>
  </head>
  <body>
       <header class='demos-header'>
		        <div class="weui-grid__icon">
		          <img src="https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=${Qr_code }" alt="" style="width: 50%;height: 50%;margin: 0 auto;">
		        </div>
      			<h1 class="demos-title" style="color:black;margin-bottom: 10px;">还没有客户. 加油(ง •̀_•́)ง</h1>
		        <h1 class="demos-title" style="color:#999;padding: 5px 0">用户扫描之后并关注就会成为您的客户！</h1>
	   </header>
	<script>
	  $(function() {
	    FastClick.attach(document.body);
	  });
	</script>
  </body>
</html>
