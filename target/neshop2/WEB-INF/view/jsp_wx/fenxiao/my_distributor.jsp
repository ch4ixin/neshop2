<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<title>我的分销商</title> 
	<style class="csscreations">
	* {margin: 0; padding: 0;}
	.tree ul {
	padding-left: 33px; position: relative;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
	}
	.tree li {
	text-align: center;
	list-style-type: none;
	position: relative;
	padding: 2px 0px 30px 20px;
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
	}
	.tree li::after{
	    content: '';
	    position: absolute;
	    top: 34px;
	    right: 76%;
	    border-left: 1px solid #666;
	    width: 31px;
	    height: 100%;
	}
	.tree li::before, .tree li::after{
	    right: 80%;
	    left: auto;
	    border-top: 1px solid #666;
	}
	.tree li:only-child::after, .tree li:only-child::before{
	display: none;
	}
	.tree li:only-child{ padding-top: 0;}
	.tree li:first-child::before, .tree li:last-child::after{
	border: 0 none;
	}
	.tree li:last-child::after{
		right: 80%;
	    left: auto;
	    border-top: 1px solid #666;
	}
	.tree li:last-child::before{
	border-right: 1px solid #ccc;
	border-radius: 0 5px 0 0;
	-webkit-border-radius: 0 5px 0 0;
	-moz-border-radius: 0 5px 0 0;
	}
	.tree li:first-child::after{
	border-radius: 5px 0 0 0;
	-webkit-border-radius: 5px 0 0 0;
	-moz-border-radius: 5px 0 0 0;
	}
	/* .tree ul ul::before{
	    content: '';
	    position: absolute;
	    top: 20%;
	    left: 0;
	    border-top: 1px solid #666;
	    width: 20px;
	    height: 63px;
	} */
	.tree li a{
	border: 1px solid #ccc;
	padding: 5px 10px;
	text-decoration: none;
	color: #666;
	font-family: arial, verdana, tahoma;
	font-size: 11px;
	display: inline-block;
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
	}
	.tree li a:hover, .tree li a:hover+ul li a {
	background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
	}
	.tree li a:hover+ul li::after, 
	.tree li a:hover+ul li::before, 
	.tree li a:hover+ul::before, 
	.tree li a:hover+ul ul::before{
	border-color:  #94a0b4;
	}
	.tree li a {
	
	}
	.all>li>a{
		float:left;
		height:100%;
		position:relative;
		
	}
	.all .all_a>a>i{
	display:block;
	    position: absolute;
	    top: 50%;
	    left:108px;
	    border-top: 1px solid #666;
	    width: 21px;
	    height: 63px;
	}
	.all>li>ul{
		float:left;
	}
	img{
	width:25px;
	height:25px;
	}
	.tree .all_a>a{
	padding: 5px 22px;
	}
	.all{
	width:100%;
	margin-top:50px;
	}
	.tree{
	width:100%;
	height:100%;
	overflow:hidden;
	}
	.weui-panel__hdd {
	    padding: 14px 15px 10px;
	    color: #999;
	    font-size: 13px;
	    position: relative;
	    border-bottom: 1px solid #e5e5e5;
	}
	.weui-panel__hd:after {
	    border-bottom: 0px solid #e5e5e5;
	}
	.weui-grid__label{
	width:63px;
	}
	</style>
</head>
<body>
		   	   <c:if test="${code_length=='3' }">
		   	    <c:if test="${distributor_list=='[]' }">
		   	    	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
					<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
					<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
					<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
					<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
					<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
			   		<div class="weui-msg">
					  <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
					  <div class="weui-msg__text-area">
					    <h2 class="weui-msg__title">您还没有下级分销商！</h2>
					    <p class="weui-msg__desc">如有需要拓展下级点击下方"联系客服"或者拨打热线:<a href="tel:0351-3853826">0351-3853826</a></p>
					  </div>
					  <div class="weui-msg__opr-area">
					    <p class="weui-btn-area">
						  <a href="http://www.sxxlkj.com/neshop2/view_cp/wx/kefu.html" class="weui-btn weui-btn_plain-primary">联系客服</a>
					      <a href="javascript:myClose();" class="weui-btn weui-btn_default">关闭页面</a>
					    </p>
					  </div>
					  <div class="weui-msg__extra-area">
					    <div class="weui-footer">
					      <p class="weui-footer__links">
					        <a href="http://www.sxxlkj.com" class="weui-footer__link">山西悦龙斋科技有限公司</a>
					      </p>
					    </div>
					  </div>
					</div>
			   	</c:if>
			   	<c:if test="${distributor_list!='[]' }">
		 	   <%-- <div class="weui-panel weui-panel_access">
			   		<div class="weui-panel__hdd" style="text-align: center;color:black;">一级分销:${retailStore.name }(${retailStore.code })</div>
			   </div> --%>
			   <p class="weui-msg__desc" style="color: red;text-align: center;font-size: 12.5px;padding-top: 10px;">* 点击下方模块可查看我的分销商业绩详情</p>
   	   		  <c:forEach items="${distributor_list }" var="dl">
				<div class="tree">
					<ul class="all">
						<li class="all_a">
							<a href="${path }/fxwx/retail_order.htm?retail_id=${dl.kid}" class="weui-grid js_grid">
						        <div class="weui-grid__icon">
						          <img src="../../../../neshop2/resource/img/timg.jpg" alt="" style="width:50px;height:50px;">
						        </div>
						        <p class="weui-grid__label">
						          ${dl.name }
						        </p>
						        <p class="weui-grid__label">
						          ${dl.code }
						        </p>
						         <i class="all_b"></i>
						      </a>
								<ul class="all_top">
							      <c:forEach items="${dl.children }" var="cd">
										<li>
											<a href="${path }/fxwx/retail_order.htm?retail_id=${cd.kid}" class="weui-grid js_grid">
										        <div class="weui-grid__icon">
										          <img src="../../../../neshop2/resource/img/timg.jpg" alt="">
										        </div>
										        <p class="weui-grid__label">
										          ${cd.name }
										        </p>
										        <p class="weui-grid__label">
										          ${cd.code }
										        </p>
										      </a>
										</li>
									</c:forEach>
								</ul>
						</li>
					</ul>
				</div>
			</c:forEach>
			</c:if>
   	   </c:if>
   	   <c:if test="${code_length=='6' }">
	   	   	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
			<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
			<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
			<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
			<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
			<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
				<c:if test="${distributor_list=='[]' }">
			   		<div class="weui-msg">
					  <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
					  <div class="weui-msg__text-area">
					    <h2 class="weui-msg__title">您还没有下级分销商！</h2>
					    <p class="weui-msg__desc">如有需要拓展下级点击下方"联系客服"或者拨打热线:<a href="tel:0351-3853826">0351-3853826</a></p>
					  </div>
					  <div class="weui-msg__opr-area">
					    <p class="weui-btn-area">
						  <a href="http://www.sxxlkj.com/neshop2/view_cp/wx/kefu.html" class="weui-btn weui-btn_plain-primary">联系客服</a>
					      <a href="javascript:myClose();" class="weui-btn weui-btn_default">关闭页面</a>
					    </p>
					  </div>
					  <div class="weui-msg__extra-area">
					    <div class="weui-footer">
					      <p class="weui-footer__links">
					        <a href="http://www.sxxlkj.com" class="weui-footer__link">山西悦龙斋科技有限公司</a>
					      </p>
					    </div>
					  </div>
					</div>
			   	</c:if>
				<c:if test="${distributor_list!='[]' }">
					<div class="weui-panel__hd" style="text-align: center;color:black;">
		   	  		   	<%-- ${retailStore.name }(${retailStore.code })  二级分销 --%>
		   	  		   <p class="weui-msg__desc" style="color: red;text-align: center;font-size: 12.5px;padding-top: 10px;">* 点击下方模块可查看我的分销商业绩详情</p>
		   	  		   </div>
		   		 		<div class="weui-grids">
						  <c:forEach items="${distributor_list }" var="dl">
						      <a href="${path }/fxwx/retail_order.htm?retail_id=${dl.kid}" class="weui-grid js_grid">
						        <div class="weui-grid__icon">
						          <img src="../../../../neshop2/resource/img/timg.jpg" alt="" >
						        </div>
						        <p  class="weui-grid__label" style="width:100%;text-align:center;">
						          ${dl.name }
						        </p>
						        <p class="weui-grid__label" style="width:100%;text-align:center;">
						          ${dl.code }
						        </p>
						      </a>
					      </c:forEach>
				   	  </div>
				</c:if>
   	   </c:if>
   	   <c:if test="${code_length=='9' }">
   	    	<%-- <div class="weui-panel weui-panel_access">
			 		<div class="weui-panel__hdd" style="text-align: center;color:black;">三级分销:${retailStore.name }(${retailStore.code })</div>
			</div> --%>
   	   		<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
				<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
				<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
				<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
				<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
				<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
		   		<div class="weui-msg">
				  <div class="weui-msg__icon-area"><i class="weui-icon-info weui-icon_msg"></i></div>
				  <div class="weui-msg__text-area">
				    <h2 class="weui-msg__title">您还没有下级分销商！</h2>
				    <p class="weui-msg__desc">如有需要拓展下级点击下方"联系客服"或者拨打热线:<a href="tel:0351-3853826">0351-3853826</a></p>
				  </div>
				  <div class="weui-msg__opr-area">
				    <p class="weui-btn-area">
					  <a href="http://www.sxxlkj.com/neshop2/view_cp/wx/kefu.html" class="weui-btn weui-btn_plain-primary">联系客服</a>
				      <a href="javascript:myClose();" class="weui-btn weui-btn_default">关闭页面</a>
				    </p>
				  </div>
				  <div class="weui-msg__extra-area">
				    <div class="weui-footer">
				      <p class="weui-footer__links">
				        <a href="http://www.sxxlkj.com" class="weui-footer__link">山西悦龙斋科技有限公司</a>
				      </p>
				    </div>
				  </div>
				</div>
   	   </c:if>
<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script>

function myClose(){
	WeixinJSBridge.call('closeWindow');
}

function viticala(){
	var x=[];
	for(var i=0; i<$('.all_top').length;i++){
    var height=$('.all_top').eq(i).height();
    x[i]=height;
    if(x[i]>0){
	var b=x[i]/2-64;
	$('.all>li>a').eq(i).css({"margin-top":b});
    }
	}		
}
viticala();
function viif(){
var a=[];
for(var r=0;r<$('.all_top').length;r++){
	var d=$('.all_top').eq(r).children('li').length;
	a.push(d);
		
}
console.log(a);
for(var c=0;c<a.length;c++){
	if(a[c]==0)
	$('.all a i').eq(c).css({width:'0px'});
	if(a[c]==1){
		$('.all a i').eq(c).css({width:'54px'});
	}
  
 }
}
viif();
</script>
</body>
</html>
