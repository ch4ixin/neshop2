<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../common/init.jsp"%>

<!DOCTYPE html>
<html>
  <head>
    <title>我的客户</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="desciption" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="pragma" content="no-cache">  
	<meta http-equiv="cache-control" content="no-cache">  
	<meta http-equiv="expires" content="0">     
	
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
	     body{min-height:100%;}
		.demos-header {padding: 0px 0}
		.icon-box {margin-bottom: 25px;margin-top: 25px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
		.icon-box i {margin: auto;text-align: center;}
		.demos-title span {color: #DBDBDB}
		.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;}
		.weui-grid {padding: 5px 5px;}
		.weui-grid__icon {width: 100%;height: 110px;}
		.weui-footer__text{padding:10px 0;}
		.demos-header{display:none;}
	</style>
  </head>
  <body>
 	   <c:if test="${Qr_code!=''}" >
			<div class="weui-pull-to-refresh__layer">
			    <div class='weui-pull-to-refresh__arrow'></div>
			    <div class='weui-pull-to-refresh__preloader'></div>
			  </div>
		  	  <p class='weui-pull-to-refresh__arrow look_a' style="text-align:center;font-size:12px;margin:10px 0; color:#999999 ">下拉查看我的分销二维码</p>
		       <header class='demos-header'>
				        <div class="weui-grid__icon" style="height: 100%;">
				          <img src="https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=${Qr_code }" alt="" style="width: 50%;height: 50%;margin: 0 auto;">
				        </div>
			 	 		<c:if test="${cousumer_list=='[]'}" >
				      		 <h1 class="demos-title" style="color:black;margin-bottom: 10px;">还没有客户. 加油(ง •̀_•́)ง</h1>
				 		</c:if>
				       <h1 class="demos-title" style="color:#999;padding: 5px 0">用户扫描之后并关注就会成为您的客户！</h1>
			   </header>
			  
			  <div class="weui-grids">
				  <c:forEach items="${cousumer_list }" var="cl">
				      <a href="${path }/fxwx/cousumer_order.htm?member_id=${cl.kid}" class="weui-grid js_grid">
				        <div class="weui-grid__icon">
				          <img src="${cl.user_headimgurl }" alt="">
				        </div>
				        <p class="weui-grid__label">
				          ${cl.user_nick }
				        </p>
				      </a>
			      </c:forEach>
			      
		   	 </div>
			  <div class="weui-footer">
			   <p class="weui-footer__text">· · ·</p>
			  </div>      		 
 	   </c:if>
	<script>
	$(document.body).pullToRefresh(0);
	$(document.body).on("pull-to-refresh", function() {
			 $(".demos-header").css({"display":"block"});
			 $(".look_a").css({"display":"none"});
			  $(document.body).pullToRefreshDone();
		/* setTimeout(function(){
		},1000); */
	});
  $(function() {
    FastClick.attach(document.body);
  });
	</script>
  </body>
</html>