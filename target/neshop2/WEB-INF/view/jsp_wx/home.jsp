<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>
<% 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragrma","no-cache"); 
response.setDateHeader("Expires",0); 
%> 
<!DOCTYPE html >
<html>
<head>
<base href="${basePath}resource/mobile/" />
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="0">
<title>悦龙斋速购</title>
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<link rel="icon" href="../../../resource/img/favicon.ico" type="image/x-ico" />
<link rel="stylesheet" type="text/css" href="themesmobile/68ecshopcom_mobile/css/public.css"/>
<link rel="stylesheet" type="text/css" href="themesmobile/68ecshopcom_mobile/css/index.css"/>
<script type="text/javascript" src="themesmobile/68ecshopcom_mobile/js/jquery.js"></script>
<script type="text/javascript" src="themesmobile/68ecshopcom_mobile/js/TouchSlide.1.1.js"></script>
<script type="text/javascript" src="themesmobile/68ecshopcom_mobile/js/jquery.more.js"></script>
<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
<script src="../../../neshop2/resource/js/fastclick.js"></script>
<body>
<div id="page" class="showpage">
<div>
 
<style type="text/css">
	.scrollimg{position:relative; overflow:hidden; margin:0px auto; /* 设置焦点图最大宽度 */}
	.scrollimg .hd{ position: absolute;bottom:0px;text-align: center;width: 100%;}
	.scrollimg .hd li{display: inline-block;width: .4em;height: .4em;margin: 0 .4em;-webkit-border-radius: .8em;-moz-border-radius: .8em;-ms-border-radius: .8em;-o-border-radius: .8em; border-radius: .8em;background: #FFF;filter: alpha(Opacity=60);opacity: .6;box-shadow: 0 0 1px #ccc; text-indent:-100px; overflow:hidden; }
	.scrollimg .hd li.on{ filter: alpha(Opacity=90);opacity: .9;background: #f8f8f8;box-shadow: 0 0 2px #ccc; }
	.scrollimg .bd{position:relative; z-index:0;}
	.scrollimg .bd li{position:relative; text-align:center;}
	.scrollimg .bd li img{background:url(themesmobile/68ecshopcom_mobile/images/loading.gif) center center no-repeat;  vertical-align:top; width:100%;/* 图片宽度100%，达到自适应效果 */}
	.scrollimg .bd li a{-webkit-tap-highlight-color:rgba(0,0,0,0);}  /* 去掉链接触摸高亮 */
	.scrollimg .bd li .tit{display:block; width:100%;  position:absolute; bottom:0; text-indent:10px; height:28px; line-height:28px; background:url(themesmobile/68ecshopcom_mobile/images/focusBg.png) repeat-x; color:#fff;  text-align:left;}
	.vf_nav ul li {width: 50%;}
	.scroll_hot .bd ul li .product_kuang {width: 100%;height: 120px; margin: auto;overflow: hidden;position: relative;}
	.scroll_hot .bd ul li .product_kuang img {width: 100% !important;height:100%;margin: auto;}
	.scroll_hot .hd .on {color: #D6505B;}
	.v_nav {background-color: #fff;box-shadow:0.5px 0.5px 10px #220;}
	.vf_5 {
	    background: url(../images/pub_goods.png) no-repeat;
	    background-size: auto 400px;
	}
	.vf_1{
	background: url(../../../neshop2/resource/mobile/themesmobile/68ecshopcom_mobile/images/pub_main1.png) no-repeat 0 -1px;background-size: auto 400px;
}
</style>
<%-- <div id="scrollimg" class="scrollimg">
				<div class="bd">
					<ul>
			          <li><a href="affiche.php?ad_id=3&uri="><img src="${path }/resource/img/fenjiu.jpg" width="100%" /></a></li>
          			</ul>
				</div>
				<div class="hd">
					<ul></ul>
				</div>
			</div> --%>
			<script type="text/javascript">
				TouchSlide({ 
					slideCell:"#scrollimg",
					titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
					mainCell:".bd ul", 
					effect:"leftLoop", 
					autoPage:true,//自动分页
					autoPlay:true //自动播放
				});
			</script>
 
<div id="fake-search" class="index_search">
  <div class="index_search_mid">
  <span><img src="themesmobile/68ecshopcom_mobile/images/xin/icosousuo.png"></span>
    <input  type="text" id="search_text" class="search_text" value="请输入您所搜索的商品"/>
  </div>
</div>
<div class="entry-list clearfix">
	<nav>
		<ul>
			<li>
				<a href="${path }/wx/classify.htm">
					<img alt="全部商品" src="images/201508/1440437165699930301.png" />
					<span>全部商品</span>
				</a>
			</li>
			<!-- 			<li>
				<a href="topic.php?topic_id=6">
					<img alt="智慧茶香" src="images/201508/1440439281976779943.png" />
					<span>智慧茶香</span>
				</a>
			</li>
						<li>
				<a href="stores.php">
					<img alt="店铺街" src="images/201508/1440439257667525408.png" />
					<span>店铺街</span>
				</a>
			</li> -->
						<li>
				<a href="${path }/wx/favourable.htm">
					<img alt="优惠活动" src="images/201508/1440439952487090605.png" />
					<span>优惠活动</span>
				</a>
			</li>
			<!-- 			<li>
				<a href="pro_search.php">
					<img alt="团购" src="images/201508/1440439318451279676.png" />
					<span>团购</span>
				</a>
			</li>
						<li>
				<a href="exchange.php">
					<img alt="积分商城" src="images/201508/1440439335793850371.png" />
					<span>积分商城</span>
				</a>
			</li> -->
						<li>
				<a href="${path }/wx/cart.htm">
					<img alt="购物车" src="images/201508/1440439353048484531.png" />
					<span>购物车</span>
				</a>
			</li>
						<li>
				<a href="${path }/wx/user.htm">
					<img alt="个人中心" src="images/201508/1440439367001464442.png" />
					<span>个人中心</span>
				</a>
			</li>
					</ul>
	</nav>
</div> 
<div class="floor_images">
  <dl>
  	<c:forEach items="${home_m.billboards }" var="bb">
	    <dt><a href='${path}/wx/orders.htm?kid=${bb.product_id}' ><img src='${path }/img/${bb.main_file_id }.htm' width='1' height='1' border='0' /></a></dt>
  	</c:forEach>
  </dl>
  <ul>
  <li class="brom">
   
  </li>
    <li>
  
  </li>
  </ul>
</div>
 
<div class="floor_images">
  <dl>
    <dt> 
 </dt>
    <dd> 
    <span class="Edge"> 
 </span> 
<span> 
 </span> </dd>
  </dl>
<strong>
 </strong>
</div>
<section class="index_floor">
  <div class="floor_body1" >
    <h2><em></em>热销商品<div class="geng"><a href="${path}/wx/products.htm" >更多</a> <span></span></div></h2>
    <div id="scroll_hot" class="scroll_hot">
      <div class="bd">
        <ul>
        	<c:forEach items="${home_m.asc_list }" var="al">
	           <li>
	            <a href="${path}/wx/orders.htm?kid=${al.kid}" title="${al.name }">
	             <div class="index_pro"> 
	              <div class="product_kuang">
	                <img src="${path}/img/${al.main_file_id}.htm"></div>
	              <div class="goods_name">${al.name }</div>
	              <div class="price">
	                   <a href="${path}/wx/orders.htm?kid=${al.kid}" class="btns"><img src="themesmobile/68ecshopcom_mobile/images/index_flow.png"></a>
	             	   <span href="goods.php?id=552" class="price_pro">￥${al.price }元</span>
	              </div>
	              </div>
	            </a>
	          </li>
        	</c:forEach>
        </ul>
          </div>
        <div class="hd">
          <ul></ul>
        </div>
      </div>
    </div>
  </section>
  <script type="text/javascript">
    TouchSlide({ 
      slideCell:"#scroll_hot",
      titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
      effect:"leftLoop", 
      autoPage:true, //自动分页
      //switchLoad:"_src" //切换加载，真实图片路径为"_src" 
    });
  </script>
<section class="index_floor">
  <div class="floor_body1" >
    <h2>
      <em></em>
      新品上市      <div class="geng"><a href="${path}/wx/products.htm" >更多</a> <span></span></div>
    </h2>
    <div id="scroll_new" class="scroll_hot">
      <div class="bd">
        	<ul>
        		<c:forEach items="${home_m.desc_list }" var="dl">
	                <li>
			            <a href="${path}/wx/orders.htm?kid=${dl.kid}" title="${dl.price }">
			             <div class="index_pro"> 
			              <div class="product_kuang">
			                <img src="${path}/img/${dl.main_file_id}.htm"></div>
			              <div class="goods_name">${dl.name }</div>
			              <div class="price">
			                 <a href="${path}/wx/orders.htm?kid=${dl.kid}" class="btns"><img src="themesmobile/68ecshopcom_mobile/images/index_flow.png"></a>
			              <span href="goods.php?id=552" class="price_pro">￥${dl.price }元</span>
			              </div>
			              </div>
			            </a>
			          </li>
		          </c:forEach>
	         </ul>
          </div>
        <div class="hd">
          <ul></ul>
        </div>
      </div>
    </div>
  </section>
  <script type="text/javascript">
    TouchSlide({ 
      slideCell:"#scroll_new",
      titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
      effect:"leftLoop", 
      autoPage:true, //自动分页
      //switchLoad:"_src" //切换加载，真实图片路径为"_src" 
    });
  </script>
 
 
 
 
<div id="index_banner" class="index_banner">
				<div class="bd">
					<ul>
								</ul>
				</div>
				<div class="hd">
					<ul></ul>
				</div>
			</div>
			<script type="text/javascript">
				TouchSlide({ 
					slideCell:"#index_banner",
					titCell:".hd ul", //开启自动分页 autoPage:true ，此时设置 titCell 为导航元素包裹层
					mainCell:".bd ul", 
					effect:"leftLoop", 
					autoPage:true,//自动分页
					autoPlay:true //自动播放
				});
			</script>
<footer> 
<div class="footer" >
      <div class="links"  id="ECS_MEMBERZONE">
	      <a ><span>便捷</span></a>
	      <a ><span>可靠</span></a>
	      <a ><span>精品</span></a>
	  </div>
  	  <p class="mf_o4"><a href="http://www.sxxlkj.com" style="color: #586c94;font-size: 10px;">中科讯龙</a> 提供云服务</p>
</div>
<div style="height:50px; line-height:50px; clear:both;"></div>
<div class="v_nav">
	<div class="vf_nav">
		<ul>
			<li><a href="${path }/wx/home.htm" style="color: #FF2233;"><i class="vf_1"></i><span>首页</span></a></li>
			<li><a href="${path }/wx/classify.htm"><i class="vf_3"></i><span>分类</span></a></li>
		</ul>
	</div>
</div>
</footer>
<script>
function goTop(){
	$('html,body').animate({'scrollTop':0},600);
}
</script>
	<a href="javascript:goTop();" class="gotop"><img src="themesmobile/68ecshopcom_mobile/images/topup.png"></a> 
</div>
 <div id="search_hide" class="search_hide">
 		<h2> <span class="close"><img src="themesmobile/68ecshopcom_mobile/images/close.png"></span>关键搜索</h2>
	 	<div id="mallSearch" class="search_mid">
	        <div id="search_tips" style="display:none;"></div>
	          <ul class="search-type">
	          	<li  class="cur"  num="0">商品</li>
	          </ul>	
	          <div class="searchdotm"> 
		          <form class="mallSearch-form" method="get" name="searchForm" id="searchForm" action="${path }/wx/search.htm" onSubmit="">
		              <div class="mallSearch-input">
		                <div id="s-combobox-135">
		                    <input aria-haspopup="true" role="combobox" class="s-combobox-input" name="pname" id="pname" tabindex="9" accesskey="s" onkeyup="" autocomplete="off"  value="请输入关键词" onFocus="if(this.value=='请输入关键词'){this.value='';}else{this.value=this.value;}" onBlur="if(this.value=='')this.value='请输入关键词'"  type="text">
		                </div>
		                <input type="submit" value="" class="button"  >
		              </div>
		          </form>
	         </div> 
        </div>
 				<!--		<div class="search_body">
                                <div class="search_box">
                                    <form action="search.php" method="post" id="searchForm" name="searchForm">
                                        <div>
											<select id='search_type' name="search_type" style="width:15%;">
												<option value='search'>宝贝</option>
												<option value='stores'>店铺</option>
											</select>
                                            <input class="text" type="search" name="keywords" id="keywordBox" autofocus>
                                            <button type="submit" value="搜 索" ></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            -->
        <section class="mix_recently_search"><h3>热门搜索</h3>
	     <ul>
	       <li>
		    <a href="${path }/wx/search.htm?pname=商务汾阳王">商务汾阳王</a>
		   </li>
		      <li>
		    <a href="${path }/wx/search.htm?pname=福益德亚麻籽油">福益德亚麻籽油</a>
		   </li>
		      <li>
		    <a href="${path }/wx/search.htm?pname=汾酒股份20年">汾酒股份20年</a>
		   </li>
		      <li>
		    <a href="${path }/wx/search.htm?pname=老闫家芝麻饼">老闫家芝麻饼</a>
		   </li>
		      <li>
		    <a href="${path }/wx/search.htm?pname=老闫家芝麻饼">东杏老酒</a>
		   </li>
	     </ul>
	   </section>
   </div>  
</div>
<script type="Text/Javascript" language="JavaScript">
function selectPage(sel)
{
   sel.form.submit();
}
</script>
<script type="text/javascript">
$(function() {
	$.showLoading();
 	document.onreadystatechange = function(){   
        if(document.readyState=="complete"){   
	        $.hideLoading();
        } 
	}
	
    $('#search_text').click(function(){
        $(".showpage").children('div').hide();
        $("#search_hide").css('position','fixed').css('top','0px').css('width','100%').css('z-index','999').show();
    })
    $('#get_search_box').click(function(){
        $(".showpage").children('div').hide();
        $("#search_hide").css('position','fixed').css('top','0px').css('width','100%').css('z-index','999').show();
    })
    $("#search_hide .close").click(function(){
        $(".showpage").children('div').show();
        $("#search_hide").hide();
    })
});
</script>
<script>
$('.search-type li').click(function() {
    $(this).addClass('cur').siblings().removeClass('cur');
    $('#searchtype').val($(this).attr('num'));
});
$('#searchtype').val($(this).attr('0'));
</script>
</body>
</html>