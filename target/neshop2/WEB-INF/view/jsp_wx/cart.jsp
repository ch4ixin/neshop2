<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>

<!DOCTYPE html>
<html>
  <head>
    <title>悦龙斋厂家速购</title>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
	<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
	<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
	<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
	<script src="../../../neshop2/resource/js/fastclick.js"></script>
	<style type="text/css">
		body, html {height: 100%;-webkit-tap-highlight-color: transparent;background-color: #EBEBEB;}
		.demos-title {text-align: center;font-size: 15px;color: black;font-weight: 400;margin: 0 15%;}
		.demos-sub-title {text-align: center;color: #888;font-size: 14px;}
		.demos-header {padding: 10px 0;}
		.demos-content-padded {padding: 15px;}
		.demos-second-title {text-align: center;font-size: 24px;color: #3cc51f;font-weight: 400;margin: 0 15%;}
		footer {text-align: center;font-size: 14px;padding: 20px;}
		footer a {color: #999;text-decoration: none;}
		.weui-grid__icon {width: 100%;height: 150px;margin: 0 auto;}
		.weui-grid {position: relative;float: left;padding: 0px 0px;width: 50%;box-sizing: border-box;}
		.weui-footer, .weui-grid__label {text-align: center;margin-bottom:1px;font-size: 14px;background-color: #fff;}
		span{color: black;}
	 	.sspan{text-align: center;font-size: 14px;color:#F37DF;} 
		.weui-grids{border-top: 1px solid #d9d9d9;}
		.weui-cells__title{color:black; height:35px; line-height:35px;}
		.weui-grid__icon+.weui-grid__label {margin-top: 0px;}
		.weui-panel:first-child{ margin-top: 10px;}
		.weui-media-box_appmsg .weui-media-box__hd {width: 50px;height: 50px;}
		.icon-box {margin-bottom: 25px;margin-top: 25px;display: -webkit-box;display: -webkit-flex;display: flex;-webkit-box-align: center;-webkit-align-items: center;align-items: center}
		.icon-box i {margin: auto;text-align: center;}
		.weui-cell__ft p{font-size: 15px;color: #FF6B2F;}
		.placeholder {margin: 5px;background-color: #fff;height: 100px;text-align: center;color: #cfcfcf;}
		.weui-s{width: 100%}
		.weui-flex__item{width: 25%;float:left;}
		.demos-title span {color: #DBDBDB}
		.weui-tab__bd .weui-tab__bd-item {display: none;height: 100%;overflow: auto;}
		.weui-cells {font-size: 14px;}
		.weui-cell_link {font-size: 12px;}
		.fanhui_img{width:30px; height:30px; background:rgba(12,12,12,0.5); border-radius:50%;text-align:center;line-height:30px; float:left; margin-right:20px;}
		.fanhui_img img{width:12px;padding-top:4px;}
	</style>
  </head>
  <body ontouchstart>
		<div id="carts" class="weui-popup__container">
		  <div class="weui-popup__overlay"></div>
		  <div class="weui-popup__modal">
		    	<div class="weui-tab">
			      <div class="weui-tab__bd">
			        <div id="tab3" class="weui-tab__bd-item weui-tab__bd-item--active">
					        <c:if test="${cartList=='[]'}" >
					            <header class='demos-header'>
					               <div class="icon-box">
								   		<i class="weui-icon-info weui-icon_msg" style=""></i>
								   </div>
			  				       <h1 class="demos-title" style="color:black;margin-bottom: 25px;">购物车是空的</h1>
			 					   <a href="${path }/wx/home.htm" style="width: 90%;font-size: 14px;" class="weui-btn weui-btn_primary">马上逛逛</a>
							    </header>
							</c:if>
							<c:if test="${cartList!='[]'}" >
			          	      <div class="weui-panel weui-panel_access">
			          	     
							        <div class="weui-cells__title">购物清单</div>
							        <div class="weui-panel__bd">
									        <c:forEach items="${cartList}" var="c">
									          <a href="${path}/wx/edit_orders.htm?kid=${c.p_kid }&oid=${c.kid}" class="weui-media-box weui-media-box_appmsg">
									            <div class="weui-media-box__hd">
									              <img class="weui-media-box__thumb" src="${path}/img/${c.main_file_id}.htm" alt="">
									            </div>
									            <div class="weui-media-box__bd" >
									              <h4 class="weui-media-box__title" style="font-size: 14px;">${c.p_name }</h4>
									              <p>${c.cnt } x <span style="color: #FF6B2F;" style="font-size: 14px;">${c.price }元</span></p>
									            </div>
									          </a>
									        </c:forEach>
							        </div>
							        <div class="weui-panel__ft">
							          <a href="${path }/wx/classify.htm" class="weui-cell weui-cell_access weui-cell_link">
							            <div class="weui-cell__bd">继续添加商品</div>
							            <span class="weui-cell__ft"></span>
							          </a>    
							        </div>
						      </div>
						      <div class="weui-panel weui-panel_access">
							        <div class="weui-cells__title">收货信息</div> 
							        <div class="weui-panel__bd" >
							        	<c:if test="${address!=null}">
								          <a href="${path}/wx/address.htm" class="weui-media-box weui-media-box_appmsg">
								            <div class="weui-media-box__bd" >
								              <h4 class="weui-media-box__title" style="font-size: 12px;">${address.mobile1 } ${address.member_name } 收</h4>
								              <p style="font-size: 12px;"><span>${address.province } ${address.city } ${address.region } ${address.street }</span></p>
								            </div>
								          </a>
							        	</c:if>
							        	<c:if test="${address==null}">
								           <a href="${path }/wx/add_address.htm" class="weui-cell weui-cell_access weui-cell_link">
								            <div class="weui-cell__bd">新增收货地址</div>
								            <span class="weui-cell__ft"></span>
								          </a>
							        	</c:if>
							        </div>
						      </div>
						      <div class="weui-panel weui-panel_access">
							        <div class="weui-cells__title">订单金额 <span style="float:right;color: #FF6B2F;">应付 <b>${totalsum }元</b></span></div>
							        <div class="weui-panel__bd">
								            <div class="weui-cells weui-cells_form" style="font-size: 12px;">
											  <div class="weui-cell weui-cell_switch">
											    <div class="weui-cell__bd">商品总价</div>
											    <div class="weui-cell__ft">
											      <p>${price_total }元</p>
											    </div>
											  </div>
											  <div class="weui-cell weui-cell_switch">
											    <div class="weui-cell__bd">运费</div>
											    <div class="weui-cell__ft">
											      <p>${carriage }元</p>
											    </div>
											  </div>
											</div>
							        </div>
						      </div>
						      <a href="javascript:to_cart();" class="weui-btn weui-btn_primary" style="margin-top: 15px;margin-bottom: 5px;width: 95%;">使用微信支付</a>
						      <p class="weui-msg__desc" style="color: red;text-align: center;font-size: 12.5px;padding-top: 10px;"><i class="weui-icon-warn" style="font-size: 10px;margin-bottom: .4em;"></i> 支付完成后务必点击“返回商家”或“完成”</p>
							</c:if>
			        </div>
			      </div>
			    </div>
		  </div>
		</div>
    	<script>
		  $(function() {
		  	$("#carts").popup();
		    FastClick.attach(document.body);
		  });
		</script>
		<script>
			$(function(){
			})
			function to_cart(){
				addr = '${address}';
				if(addr == ''){
					$.toptip('亲，请填写收货地址 ~ ~! ', 'warning');
				}else{
					window.location.href="${path }/wx/to_cart.htm"; 
				}
			}
		</script>
  </body>

</html>
