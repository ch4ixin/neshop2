<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="./common/init.jsp" %>
<% 
response.setHeader("Cache-Control","no-store"); 
response.setHeader("Pragrma","no-cache"); 
response.setDateHeader("Expires",0); 
%>
<!DOCTYPE html >
<html>
<head>
<base href="${basePath}resource/mobile/" />
<meta name="Generator" content="ECSHOP v2.7.3" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache"> 
<META HTTP-EQUIV="Expires" CONTENT="0">
<title>悦龙斋厂家速购</title>
<meta name="Keywords" content="" />
<meta name="Description" content="" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<link rel="icon" href="../../../resource/img/favicon.ico" type="image/x-ico" />
<link rel="stylesheet" type="text/css" href="themesmobile/68ecshopcom_mobile/css/public.css"/>
<link rel="stylesheet" type="text/css" href="themesmobile/68ecshopcom_mobile/css/catalog.css"/>
<script type="text/javascript" src="themesmobile/68ecshopcom_mobile/js/jquery.js"></script>
<link rel="stylesheet" href="//cdn.bootcss.com/weui/1.1.1/style/weui.min.css">
<link rel="stylesheet" href="//cdn.bootcss.com/jquery-weui/1.0.1/css/jquery-weui.min.css">
<script src="//cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/jquery-weui.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/swiper.min.js"></script>
<script src="//cdn.bootcss.com/jquery-weui/1.0.1/js/city-picker.min.js"></script>
<script src="../../../neshop2/resource/js/fastclick.js"></script>

<!-- <script type="text/javascript" src="js/common.js"></script> --><style>
.goods_nav{width:30%; float:right; right:5px; overflow:hidden; position:fixed;margin-top:25px; z-index:9999999}
.category2 dl{height: 10%;}
.category2 dt{margin-left:1%;}
.category2 dd{margin-left:2%;width: 98%;}
.vf_nav ul li {width: 50%;}
.category2 dt a { width: 42%;float: left;padding: 3%;border: 1px solid #f4f4f4;margin-bottom: 1%;}
.pic_i{width:100%;height:120px;}
.category2 dt a {display: block;font-size: 16px;line-height: 20px;color: #999;background-color: #fff;margin-left: 1%}
.category2 dt {width: 98%;}
.title_box {font-size: 14px;background: #ffffff;color: #3d4145;padding: 5px;text-align: justify;overflow: hidden;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;}
.price_box{background: #ffffff;}
.price_box span i{color: #FF0000}
.v_nav {background-color: #fff;box-shadow:0.5px 0.5px 10px #220;}
.vf_3{
	background: url(../../../neshop2/resource/mobile/themesmobile/68ecshopcom_mobile/images/pub_main1.png) no-repeat 0 -72px;background-size: auto 400px;
}
</style>
</head>
<body style="width: 100%;height: 100%;overflow: hidden;">
  <div class="container">
    <div class="category-box">
      <div class="category1" style="outline: none;" tabindex="5000">
        <ul class="clearfix" style="padding-bottom:50px;">
           	<!-- <li class="cur" style="margin-top:46px"></li> -->
          <c:forEach items="${cateList }" var="one">
            <li>${one.name }</li>
          </c:forEach>
        </ul>
      </div>
      <div class="category2" style=" outline: none;" tabindex="5001">
        <dl style="display: none; margin-top:46px; padding-bottom:50px;display: block;">
	          <span>
	            <a href="${path }/wx/products.htm">
	              <em>全部 >></em>
	              <img src="../../../neshop2/resource/img/ylzgg.jpg">
	            </a>
	          </span>
	          
        </dl>
      </div>
    </div>
  </div>
  <div class="v_nav">
    	<div class="vf_nav">
			<ul>
				<li><a href="${path }/wx/home.htm"><i class="vf_1"></i><span>首页</span></a></li>
				<li><a href="${path }/wx/classify.htm" style="color: #FF2233;"><i class="vf_3"></i><span>分类</span></a></li>
			</ul>
		</div>
  </div>
  <script type="text/javascript">
   	$(function(){
  		/*给第一个li添加样式和模拟点击事件  */
  		$(".clearfix li").eq(0).addClass("cur").css("margin-top","46px");
  		$(".clearfix li").eq(0).trigger('click');
  	})
  	//点击li以后，发送ajax,获得下级分类
  	$(".clearfix li").on("click",function(){
  		$.showLoading();
  		var oneName = $(this).html().trim();
  		$.ajax({
  			url:"${path}/wx/cy_product.htm?name="+oneName,
  			dataType:"json",
  			success:function(result){
  				/*清空div，再添加dl  */
  				$(".category2").empty();
  				var $dl = $('<dl style="display: none; margin-top:46px; padding-bottom:50px;display: block;"><span>'+
  	            '<a href="${path}/wx/cate_product.htm?cname='+oneName+'&rank_code=DESC">'+
  	          	'<em>全部>></em><img src="../../../neshop2/resource/img/ylzgg.jpg"></a></span></dl>');
  				$(".category2").append($dl); 
				
  				var $dt = '<dt>@products</dt><dd><div class="fenimg"></div></dd>';
 				var products = '';
  				
 				/*添加dt,二级分类  */
  				var plist = result.list;
  				for(var i=0;i<plist.length;i++){
  					//products += '<a href=${path}/wx/orders.htm?kid='+pkid+'> '+pName+'</a>';
  					products += '<a href=${path}/wx/orders.htm?kid='+plist[i].kid+' class="item">'
		  						+'<div class="pic_box">'
		  						+'<div class="active_box">'
		  						+'</div>'
		  						+'<img class="pic_i" src=${path}/img/'+plist[i].main_file_id+'.htm>'
		  						+'</div>'
		  						+'<div class="title_box">'
		  						+''+plist[i].name+'</div>'
		  						+'<div class="price_box">'
		  						+'<span class="new_price">'
		  						+'<i>￥'+plist[i].price+'</i>'
		  						+'<s style="color:#999;font-size:13px;margin-left:15px;">￥'+plist[i].market_price+'</s>'
		  						+'</span>'
		  						+'</div>';
  					//console.log(products);
  					//在dl之后，添加兄弟dt
  				} 
  				$dt = $dt.replace('@products',products);
  				//console.log($dt);
				$(".category2 dl").after($dt);
				$.hideLoading();
  			},
  			error:function(){
  				alert("加载失败");
  			}
  		})
  	}) 
  </script>
  <script src="themesmobile/68ecshopcom_mobile/js/category.js"></script>
  <script src="themesmobile/68ecshopcom_mobile/js/jquery.nicescroll.min.js"></script>
</body>
</html>